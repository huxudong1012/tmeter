/****************************************Copyright (c)**************************************************
**
**                                 许继电能仪表有限公司
**                                     
**                                       IE    科                                       
**
**                                 
**--------------文件信息--------------------------------------------------------------------------------
**文   件   名: main
**创   建   人: 
**最后修改日期: 
**描        述: 基于lpc17xx  操纵系统选用keil_RTX
**              						
**--------------历史版本信息----------------------------------------------------------------------------
** 创建人: 
** 版  本: 
** 日　期: 
** 描　述: 
**
**--------------当前版本修订------------------------------------------------------------------------------
** 修改人: 
** 日　期:
** 描　述:
**
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
#include <RTL.h>                      /* RTX kernel functions & defines      */
#include <LPC17xx.H>                  /* LPC17xx definitions                 */




/*********************************************************************************************************
** 函数名称: NVIC_PriorityGroupSet
** 功能描述: NVIC优先级组设置   * 皮函数    svc 1
** 输　入: tick
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void _NVIC_SetPriorityGrouping            (U32 NVIC_PriorityGroup) {
  NVIC_SetPriorityGrouping(NVIC_PriorityGroup);
}
//__asm void NVIC_PriorityGroupSet (U32 NVIC_PriorityGroup) {
//   // Function wrapper for Unpriviliged/Priviliged mode. 
//        LDR     R12,=__cpp(_NVIC_SetPriorityGrouping)
//        MRS     R3,IPSR
//        LSLS    R3,#24
//        BXNE    R12
//        MRS     R3,CONTROL
//        LSLS    R3,#31
//        BXEQ    R12
//        SVC     1
//        BX      LR
//
//        ALIGN
//}
void __svc(1) NVIC_PriorityGroupSet(U32 NVIC_PriorityGroup);

/*********************************************************************************************************
** 函数名称: NVIC_EnableSet
** 功能描述: 使能中断   * 皮函数    svc 2
** 输　入: tick
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void _NVIC_EnableIRQ            (IRQn_Type IRQn) {
  NVIC_EnableIRQ(IRQn);
}
//__asm void NVIC_EnableSet (U32 NVIC_PriorityGroup) {
//   // Function wrapper for Unpriviliged/Priviliged mode. 
//        LDR     R12,=__cpp(_NVIC_EnableIRQ)
//        MRS     R3,IPSR
//        LSLS    R3,#24
//        BXNE    R12
//        MRS     R3,CONTROL
//        LSLS    R3,#31
//        BXEQ    R12
//        SVC     1
//        BX      LR
//
//        ALIGN
//}
void __svc(2) NVIC_EnableSet(IRQn_Type IRQn); //只能在用户模式使用

/*********************************************************************************************************
** 函数名称: NVIC_DisableSet
** 功能描述: 使能中断   * 皮函数    svc 3
** 输　入: tick
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void _NVIC_DisableIRQ(IRQn_Type IRQn) {
	NVIC_DisableIRQ(IRQn);
}
//__asm void NVIC_DisableSet (U32 NVIC_PriorityGroup) {
//   // Function wrapper for Unpriviliged/Priviliged mode. 
//        LDR     R12,=__cpp(_NVIC_DisableIRQ)
//        MRS     R3,IPSR
//        LSLS    R3,#24
//        BXNE    R12
//        MRS     R3,CONTROL
//        LSLS    R3,#31
//        BXEQ    R12
//        SVC     1
//        BX      LR
//
//        ALIGN
//}
void __svc(3) NVIC_DisableSet (IRQn_Type IRQn);

/*********************************************************************************************************
** 函数名称: NVIC_PrioritySet
** 功能描述: 优先级设置   * 皮函数    svc 4
** 输　入: tick
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/ 
void _NVIC_SetPriority            (IRQn_Type IRQn, U32 priority) {
  NVIC_SetPriority(IRQn,priority);
}

//__asm void NVIC_PrioritySet (IRQn_Type IRQn, U32 priority) {
//   // Function wrapper for Unpriviliged/Priviliged mode. 
//        LDR     R12,=__cpp(_NVIC_SetPriority)
//        MRS     R3,IPSR
//        LSLS    R3,#24
//        BXNE    R12
//        MRS     R3,CONTROL
//        LSLS    R3,#31
//        BXEQ    R12
//        SVC     1
//        BX      LR
//
//        ALIGN
//}
void __svc(4) NVIC_PrioritySet(IRQn_Type IRQn, U32 priority);


