/****************************************Copyright (c)**************************************************
**
**                                 许继电能仪表有限公司
**                                     
**                                       IE    科                                       
**
**                                 
**--------------文件信息--------------------------------------------------------------------------------
**文   件   名: main
**创   建   人: 
**最后修改日期: 
**描        述: 基于lpc17xx  操纵系统选用keil_RTX
**              						
**--------------历史版本信息----------------------------------------------------------------------------
** 创建人: 
** 版  本: 
** 日　期: 
** 描　述: 
**
**--------------当前版本修订------------------------------------------------------------------------------
** 修改人: 
** 日　期:
** 描　述:
**
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
#ifndef __TARGET_NVIC_H
#define __TARGET_NVIC_H

extern void __svc(4) NVIC_PrioritySet(IRQn_Type IRQn, U32 priority);
//__asm void NVIC_PrioritySet (IRQn_Type IRQn, U32 priority);
extern void __svc(1) NVIC_PriorityGroupSet(U32 NVIC_PriorityGroup);
extern void __svc(2) NVIC_EnableSet(IRQn_Type IRQn);
//__asm void NVIC_EnableSet (U32 NVIC_PriorityGroup);
extern void __svc(3) NVIC_DisableSet (IRQn_Type IRQn);


#endif /*__TARGET_NVIC_H */
