;/*----------------------------------------------------------------------------
; *      RL-ARM - RTX
; *----------------------------------------------------------------------------
; *      Name:    SVC_TABLE.S
; *      Purpose: Pre-defined SVC Table for CORTEX M3
; *      Rev.:    V4.05
; *----------------------------------------------------------------------------
; *      This code is part of the RealView Run-Time Library.
; *      Copyright (c) 2004-2009 KEIL - An ARM Company. All rights reserved.
; *---------------------------------------------------------------------------*/


                AREA    SVC_TABLE, CODE, READONLY

                EXPORT  SVC_Count

SVC_Cnt         EQU    (SVC_End-SVC_Table)/4
SVC_Count       DCD     SVC_Cnt

; Import user SVC functions here.
               IMPORT  _NVIC_SetPriorityGrouping	    ;NVIC优先级组设置
			   IMPORT  _NVIC_EnableIRQ					;使能中断
			   IMPORT  _NVIC_DisableIRQ                 ;关闭中断
			   IMPORT  _NVIC_SetPriority                ;设置中断优先级

                EXPORT  SVC_Table
SVC_Table
; Insert user SVC functions here. SVC 0 used by RTL Kernel.
               DCD     _NVIC_SetPriorityGrouping                 ; user SVC function
			   DCD     _NVIC_EnableIRQ
			   DCD     _NVIC_DisableIRQ
			   DCD	   _NVIC_SetPriority

SVC_End

                END

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
