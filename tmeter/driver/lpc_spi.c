/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#include <LPC17xx.H>                 /* LPC17xx Definitions                  */
#include <RTL.h> 
/* SSPxSR - bit definitions. */
#define TFE     0x01
#define TNF     0x02
#define RNE     0x04
#define RFF     0x08
#define BSY     0x10

#define PAGE_SZ                 256     /* Page Size */


/* SPI Flash Commands */
#define SPI_WRITE_SR            0x01
#define SPI_PAGE_PROGRAM        0x02
#define SPI_READ_DATA           0x03
#define SPI_WRITE_DISABLE       0x04
#define SPI_READ_SR             0x05
#define SPI_WRITE_ENABLE        0x06
//#define SPI_CLEAR_SR_FLAGS      0x30
//#define SPI_BLOCK_ERASE        0xD8
#define SPI_SECTOR_ERASE        0x20

/* Status Register Bits */
#define SR_WIP                  0x01
//#define SR_E_FAIL               0x20
//#define SR_P_FAIL               0x40

/*********************************************************************************************************
** 函数名称: spi_init
** 功能描述: spi硬件初始化
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void spi_init (void) {
  	/* Initialize and enable the SSP Interface module. */
  	LPC_SC->PCONP       |= (1 << 10);           /* Enable power to SSPI1 block */

  	/* SSEL is GPIO, output set to high. */
  	LPC_GPIO0->FIODIR   |=  (1<<6);            /* P0.6 is output             */
  	LPC_GPIO0->FIOSET   |=  (1<<6);            /* set P0.6 high (SSEL inact.)*/
  	LPC_PINCON->PINSEL0 &= ~(3<<12);             /* P0.6 SSEL (used as GPIO)   */

  	LPC_GPIO1->FIODIR   |=  (1<<17);            /* P1.15 is output             */
  	LPC_GPIO1->FIOSET   |=  (1<<17);            /* set P1.15 high (SSEL inact.)*/
  	LPC_PINCON->PINSEL3 &= ~(3<<2);             /* P1.15 SSEL (used as GPIO)   */

  	/* SCK, MISO, MOSI are SSP pins. */
  	LPC_PINCON->PINSEL0 &= ~(3UL<<14);          /* P0.7 cleared               */
  	LPC_PINCON->PINSEL0 |=  (2UL<<14);          /* P0.7 SCK1                  */
  	LPC_PINCON->PINSEL0 &= ~((3<<16) | (3<<18));  /* P0.8, P0.9 cleared        */
  	LPC_PINCON->PINSEL0 |=  ((2<<16) | (2<<18));  /* P0.8 MISO1, P0.9 MOSI1    */

  	LPC_SC->PCLKSEL0    &= ~(3<<20);            /* PCLKSP1 = CCLK/4 ( 25MHz)   */
  	LPC_SC->PCLKSEL0    |=  (1<<20);            /* PCLKSP1 = CCLK   (100MHz)   */

  	LPC_SSP1->CPSR       = 250;                 /* 100MHz / 250 = 400kBit      */
                                              /* maximum of 18MHz is possible*/    
  	LPC_SSP1->CR0        = 0x0007;              /* 8Bit, CPOL=0, CPHA=0        */
  	LPC_SSP1->CR1        = 0x0002;              /* SSP1 enable, master         */

  	/* WP pins. */
  	LPC_GPIO0->FIODIR   |=  (1<<5);            /* P0.5 is output             */
  	LPC_GPIO0->FIOSET   |=  (1<<5);            /* set P0.5 high (SSEL inact.)*/
  	LPC_PINCON->PINSEL0 &= ~(3<<10);             /* P0.5 SSEL (used as GPIO)   */
  	LPC_GPIO0->FIOSET = 1<<5;
}


/*********************************************************************************************************
** 函数名称: spi_hi_speed
** 功能描述: spi高速模式
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/

void spi_hi_speed (BOOL on) {
  	/* Set a SPI clock to low/high speed for SD/MMC. */
  	if (on == __TRUE) {
    	/* Max. 12 MBit used for Data Transfer. */
    	LPC_SSP1->CPSR = 10;                      /* 100MHz / 10 = 10MBit        */
  	}else {
    	/* Max. 400 kBit used in Card Initialization. */
    	LPC_SSP1->CPSR = 250;                     /* 100MHz / 250 = 400kBit      */
  	}
}
/*********************************************************************************************************
** 函数名称: spi_ss
** 功能描述: spi片选控制
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 flashSelect = 0;	 	//片选标志，为0使用U8，非零使用U21，硬件对应2011年7月版本
void spi_ss (U32 ss) {
  	/* Enable/Disable SPI Chip Select (drive it high or low). */
  	if (ss) {
    	/* SSEL is GPIO, output set to high. */
		if(!flashSelect){    
			LPC_GPIO0->FIOSET = 1<<6;
		}else{
		  	LPC_GPIO1->FIOSET = 1<<17;
		}
  	} else {
    	/* SSEL is GPIO, output set to low. */
    	if(!flashSelect){
			LPC_GPIO0->FIOCLR = 1<<6;
		}else{
			LPC_GPIO1->FIOCLR = 1<<17;
		}  
  	}
}

/*********************************************************************************************************
** 函数名称: spi_send
** 功能描述: spi一字节数据发送接收
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 spi_send (U8 outb) {
  /* Write and Read a byte on SPI interface. */

  	LPC_SSP1->DR = outb;
  	while (LPC_SSP1->SR & BSY);                 /* Wait for transfer to finish */
  	return (LPC_SSP1->DR);                      /* Return received value       */
}
/*********************************************************************************************************
** 函数名称:Init_spi
** 功能描述: 初始化
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
int Init_spi(void)  {

  	/* Initialize SPI host interface. */
  	spi_init ();

  	flashSelect = 1;
  	spi_ss (0);
  	spi_send (SPI_WRITE_ENABLE);
  	spi_ss (1);

  	spi_ss (0);
  	spi_send (SPI_WRITE_SR);
  	spi_send (0x00);
  	spi_ss (1);
//  spi_ss (0);
//  spi_send (SPI_CLEAR_SR_FLAGS);
//  spi_ss (1);

  	flashSelect = 0;
  	spi_ss (0);
  	spi_send (SPI_WRITE_ENABLE);
  	spi_ss (1);

  	spi_ss (0);
  	spi_send (SPI_WRITE_SR);
  	spi_send (0x00);
  	spi_ss (1);


  	return (0);
}

/*********************************************************************************************************
** 函数名称:spi_EraseSector
** 功能描述: flash扇区擦除
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
int spi_EraseSector(U32 adr)  {
  	U8  sr;

  	if(adr >= 0x400000) {
  		flashSelect = 1; 	//使用U21
		adr -= 0x400000;
	}else flashSelect = 0;				//使用U8

  	/* Write Enable */
  	spi_ss (0);
  	spi_send (SPI_WRITE_ENABLE);
  	spi_ss (1);

  	/* Erase Sector */
  	spi_ss (0);
  	spi_send (SPI_SECTOR_ERASE);
  	spi_send ((U8)(adr >> 16));
  	spi_send ((U8)(adr >>  8));
  	spi_send ((U8)(adr >>  0));
  	spi_ss (1);

  	/* Wait until done */
  	spi_ss (0);
  	spi_send (SPI_READ_SR);
  	do {
    	os_dly_wait(2);
		sr = spi_send (0xFF);
  	} while (sr & SR_WIP);
  	spi_ss (1);

  /* Check for Error */
//  if (sr & SR_E_FAIL) {
//    spi_ss (0);
//    spi_send (SPI_CLEAR_SR_FLAGS);
//    spi_ss (1);
//    return (1);
//  }

  	return (0);
}


/*********************************************************************************************************
** 函数名称:spi_ProgramPage
** 功能描述: flash页数据编程
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
int spi_ProgramPage (U32 adr, U32 sz, U8 *buf)  {
  	U32 cnt;
  	U8  sr;

  	if(adr >= 0x400000) {
  		flashSelect = 1; 	//使用U21
		adr -= 0x400000;
	}else flashSelect = 0;				//使用U8

  	while (sz) {
    	cnt = PAGE_SZ - (adr & (PAGE_SZ - 1));
    	if (cnt > sz) cnt = sz;

    	/* Write Enable */
    	spi_ss (0);
    	spi_send (SPI_WRITE_ENABLE);
    	spi_ss (1);

    	/* Program Page */
    	spi_ss (0);
    	spi_send (SPI_PAGE_PROGRAM);
    	spi_send ((U8)(adr >> 16));
    	spi_send ((U8)(adr >>  8));
    	spi_send ((U8)(adr >>  0));
    	adr += cnt;
    	sz  -= cnt;
    	while (cnt--) {
      		spi_send (*buf++);
    	}
    	spi_ss (1);

    	/* Wait until done */
    	spi_ss (0);
    	spi_send (SPI_READ_SR);
    	do {
//			os_dly_wait(1);
      		sr = spi_send (0xFF);
    	} while (sr & SR_WIP);
    	spi_ss (1);

    /* Check for Error */
//    if (sr & SR_P_FAIL) {
//      spi_ss (0);
//      spi_send (SPI_CLEAR_SR_FLAGS);
//      spi_ss (1);
//      return (1);
//    }
  	}

  	return (0);
}


/*********************************************************************************************************
** 函数名称:spi_ReadData
** 功能描述: flash数据读取
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/

int spi_ReadData (U32 adr, U32 sz, U8 *buf)  {
  	if(adr >= 0x400000) {
  		flashSelect = 1; 	//使用U21
		adr -= 0x400000;
	}else flashSelect = 0;				//使用U8

  	/* Read Data */
  	spi_ss (0);
  	spi_send (SPI_READ_DATA);
  	spi_send ((U8)(adr >> 16));
  	spi_send ((U8)(adr >>  8));
  	spi_send ((U8)(adr >>  0));
  	while (sz--) {
    	*buf++ = spi_send (0xFF);
  	}
  	spi_ss (1);

  	return (0);
}
/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/

