/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __LPC_I2C_H
#define __LPC_I2C_H

#define VIC_I2C0    	9
#define PriorityI2C0	12
#define VIC_I2C1    	19
#define PriorityI2C1	13


#define I2C_MAXSPEED        400000		// I2C max speed: 400k/s

/* Status Errors */
#define I2C_OK                      0 // transfer ended No Errors
#define I2C_IDLE                    1 // bus idle
#define I2C_BUSY                    2 // transfer busy
#define I2C_ERROR                   3 // err: general error
#define I2C_NO_DATA                 4 // err: No data in block
#define I2C_NACK_ON_DATA            5 // err: No ack on data
#define I2C_NACK_ON_ADDRESS         6 // err: No ack on address
#define I2C_DEVICE_NOT_PRESENT      7 // err: Device not present
#define I2C_ARBITRATION_LOST        8 // err: Arbitration lost
#define I2C_TIME_OUT                9 // err: Time out occurred
#define I2C_SLAVE_ERROR             10 // err: Slave mode error
#define I2C_INIT_ERROR              11 // err: Initialization (not done)
#define I2C_RETRIES                 12 // err: Initialization (not done)

/* I2C Message Flag Type*/
#define I2C_MSG_AA		0x4
#define I2C_MSG_SI		0x8
#define I2C_MSG_STOP	0x10
#define I2C_MSG_START	0x20

#define I2C_EnableI2C(x)  (x?(LPC_I2C1->I2CONSET |= (1<<6)):(LPC_I2C0->I2CONSET |= (1<<6)))
#define I2C_DisableI2C(x)  (x?(LPC_I2C1->I2CONCLR |= (1<<6)):(LPC_I2C0->I2CONCLR |= (1<<6)))
#define __I2C_ReceiveData(x)  (x?(LPC_I2C1->I2DAT):(LPC_I2C0->I2DAT))
#define __I2C_SendData(x,data) (x?(LPC_I2C1->I2DAT = data):(LPC_I2C0->I2DAT = data))
#define __I2C_SetFlag(x,data) (x?(LPC_I2C1->I2CONSET = data):(LPC_I2C0->I2CONSET = data))
#define __I2C_ClearFlag(x,data) (x?(LPC_I2C1->I2CONCLR = data):(LPC_I2C0->I2CONCLR = data))

typedef enum {
	I2C_MASTER = 1,
	I2C_SLAVE = 2
} LPC_I2C_Mode_t;
/* Device number */
typedef enum {
  I2C0 = 0,
  I2C1,
  I2C2
} LPC_I2C_Channel_t;

typedef enum {
	WRITE=0,	//transmit
	READ=1, 	//receive
	WRITETHENREAD = 2
} LPC_I2C_TransMode_t;
#define I2C_ReceiveOneFrame       0x1
typedef struct {
	U8 address;	// slave address to sent/receive message
	U8 nrBytes;	// number of bytes in message buffer
	U8 *buf;		// pointer to application message buffer	
	LPC_I2C_TransMode_t transMode;	// write or read flag
	U8 nrWriteBytes ;	// write byte number, only used in "WriteThenRead" mode

	int dataCount ;	// count the Tx/Rx number

	LPC_I2C_Mode_t	Mode;
	int	State;
	unsigned  char I2C_Flag;
	LPC_I2C_Channel_t DevNum;
	U32 TimeTransferFirstData;//I2C传输的首地址时间
} LPC_I2C_Msg_t;


extern int I2C_Init (LPC_I2C_Channel_t DevNum,LPC_I2C_Mode_t Mode,U32 BusSpeed,U32 SlaveAddr);


extern int I2C_Transfer (LPC_I2C_Channel_t DevNum,U8 addr, U8 *pMsg , U32 numMsg, 
	LPC_I2C_TransMode_t transMode, U32 numWrite);
extern int I2C_MasterWrite (LPC_I2C_Channel_t DevNum,U8 addr, U8 *pMsg , U32 numMsg);

extern U32 TimeTick;

extern void I2C0_VectISR (void);

extern LPC_I2C_Msg_t	I2C0Msg,I2C1Msg;

//extern void init_i2c_device(void);


#endif //__LPC_I2C_H
