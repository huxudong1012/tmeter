#ifndef __LPC_ADC_H
#define __LPC_ADC_H

 /***********************************************************************
 *         BU MMS China, Philips Semiconductor Software Support
 *         Embest info&Tech Co. Software Support
 *---------------------------------------------------------------------------
 * The software is delivered "AS IS" without warranty or condition of any
 * kind, either express, implied or statutory.  Everybody can use it as 
 * it is opened and without copyright. We will not take any law responsibility
 * for any problem produced by using this software.
 *---------------------------------------------------------------------------
 *    File name: 	LPC_Adc.h 
 *    Description: 	Define API for A/D Converter
 *
 *    History:
 *    1. Date: 		Nov 20, 2004
 *       Author: 	Shawn Zhang
 *       Description: Create
 *
 *	$Revision: 1.0 $
 **********************************************************************/

#include "LPC17xx.h"
#include "rtl.h"


#define ADC_MAXCLK	4500000L	// 4.5MHz

/* A/D Port number */
#define ADCPort0  0x1
#define ADCPort1  0x2
#define ADCPort2  0x4
#define ADCPort3  0x8
#define ADCPort4  0x10
#define ADCPort5  0x20
#define ADCPort6  0x40
#define ADCPort7  0x80

/* A/D Control Register bit descriptions */
#define ADCR_CLKDIV_BIT		8
#define ADCR_BURST_BIT		16
#define ADCR_CLKS_BIT		17
#define ADCR_PDN_BIT		21
#define ADCR_TEST_BIT		22
#define ADCR_START_BIT		24
#define ADCR_EDGE_BIT		27

/* A/D Data Register descriptions */
#define ADDR_DATA_BIT		4
#define ADDR_CHN_BIT		24

#define ADDR_OVERRUN	0x40000000
#define ADDR_DONE		0x80000000

/* A/D START Type */
#define START_No	0
#define START_Now	1
#define START_When_P0_16	2
#define START_When_P0_22	3
#define START_When_MAT0_1	4
#define START_When_MAT0_3	5
#define START_When_MAT1_0	6
#define START_When_MAT1_1	7

/* A/D START Conversion Edge Type */
#define START_Edge_Falling	0
#define START_Edge_Rising	1

typedef struct {
	U8 ADCPort;
	
} LPC_ADC_Config_t;

/* Declare functions */
int adcPortInit(U8 PortNum);
unsigned long adcGetREGValue_ADGDR(void);
void adcPowerOff(void);
extern int start_adc_conver(void);
extern int choose_channel(U8 PortNum);
extern void adcPowerOn(void);

#endif //__LPC_ADC_H
