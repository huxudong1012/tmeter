/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __RX8025T_H__
#define __RX8025T_H__

	#define RX8025T 0x64				//器件地址

/*******************************************************************************************/	
	//RX8025T操作命令定义



	//寄存器名称定义
	typedef enum {
	  SEC = 0,
	  MIN,
	  HOUR,
	  WEEK,
	  DAY,
	  MONTH,
	  YEAR,
	  RAM,
	  MinAlarm,
	  HourAlarm,
	  WeekAlarm,
//	  DayAlarm,
	  TimerCounter0,
	  TimerCounter1,
	  ExtensionRegister,
	  FlagRegister,
	  ControlRegister

	}RegisterName;

	typedef enum {
		Second = 0,		//当前秒
		Minute,		//当前分
		Hour,		//当前时
		Date,	    //当前日期
		Month,		//当前月份
		Year,		//当前年份
		Week			//当前星期
	}current;

	extern U8 currentClock[7];


	extern U8  BCDToDec(const U8 bcd);
	extern U8 DecToBCD(U8 Dec);
	extern U8  HexToDec(U32 bcd,U8 *dec);
	extern U8 getCurrentTime(U8* currentTime);
	extern U8 getCurrentDate(U8* currentDate);
	extern U8 getCurrentWeek(U8* currentWeek);
	
	extern U8 initRx8025t(void);
	extern U8 adjustTime(U8* currentTime);
	extern U8 getCurrentTDW(void);
	extern U8 adjustDateTime(U8* currentDate);
	extern U8 adjustDate(U8* currentDate);
	extern U32 compareDate(U16 date1Year,U8 date1Month,U8 date1Day,U16 date2Year,U8 date2Month,U8 date2Day);
	extern U8 isLeapYear(U16 year);
	extern U8 check_day(U8 y,U8 m,U8 d);

	extern U8 open_fout_1s(void);
	extern U8 close_fout_1s(void);
#endif

