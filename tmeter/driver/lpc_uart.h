/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __LPC_UART_H
#define __LPC_UART_H



#define EVT_UART0_PROCESS		  	0x0001//串口0协议处理事件标志
#define EVT_UART1_PROCESS		  	0x0002//串口1协议处理事件标志
#define EVT_UART3_PROCESS       0x0004//串口3协议处理事件标志


#define PriorityUart0	6
#define PriorityUart1	7
#define PriorityUart2	7
#define PriorityUart3	8


#define UART3_POWER_ON  (LPC_SC->PCONP |= ((1<< 25)))

/* The micro are configurable */

#define FRAME_HEAD  0x68   //帧头
#define FRAME_TRAIL 0x16   //帧尾

#define	UART0_BUFSIZE	256
#define	UART1_BUFSIZE	256
//#define	UART2_BUFSIZE	256
#define	UART3_BUFSIZE	256

#define TXFIFODEEP  16		  //发送FIFO
#define RXFIFODEEP  8       //接收FIFO 

#define BD115200     115200
#define BD38400     38400
#define BD9600    9600

#define CR     0x0D
#define LF     0x0A

/* Uart line control register bit descriptions */
#define LCR_WORDLENTH_BIT         0
#define LCR_STOPBITSEL_BIT         2
#define LCR_PARITYENBALE_BIT         3
#define LCR_PARITYSEL_BIT         4
#define LCR_BREAKCONTROL_BIT         6
#define LCR_DLAB_BIT         7

/* Uart line status register bit descriptions */
#define LSR_RDR_BIT         0
#define LSR_OE_BIT         1
#define LSR_PE_BIT         2
#define LSR_FE_BIT         3
#define LSR_BI_BIT         4
#define LSR_THRE_BIT         5
#define LSR_TEMT_BIT         6
#define LSR_RXFE_BIT         7

/* Uart Interrupt Identification */
#define IIR_RSL         0x3
#define IIR_RDA        0x2
#define IIR_CTI         0x6
#define IIR_THRE      0x1

/* Uart Interrupt Enable Type*/
#define IER_RBR         0x1
#define IER_THRE       0x2
#define IER_RLS        0x4

/* Uart Receiver Errors */
#define RC_FIFO_OVERRUN_ERR       0x1
#define RC_OVERRUN_ERR            0x2
#define RC_PARITY_ERR             0x4
#define RC_FRAMING_ERR            0x8
#define RC_BREAK_IND              0x10
#define RC_OneFrame_ERR			  0x20



/* Device number */
typedef enum {
  UART0 = 0,
  UART1,
  UART2,
  UART3
} LPC_Uart_Channel_t;

/* Word Lenth type */
typedef enum {
    WordLength5 = 0,
    WordLength6,
    WordLength7,
    WordLength8
} LPC_Uart_WordLenth_t;

/* Parity Select type */
typedef enum {
    ParitySelOdd = 0,
    ParitySelEven,
    ParitySelStickHigh,
    ParitySelEvenLow
} LPC_Uart_ParitySelect_t;

 /* FIFO Rx Trigger Level type */
typedef enum {
    FIFORXLEV0 = 0,	// 0x1
    FIFORXLEV1,		// 0x4
    FIFORXLEV2,		// 0x8
    FIFORXLEV3		// 0xe			 //接收缓冲触发深度
} LPC_Uart_FIFORxTriggerLevel_t;

typedef struct {
	U8 *RxBuf;                //接收缓冲区
	U8 RxHeadPoint;           //接收的数据长度
	U8 RxTailPoint;           //接收的数据长度
	volatile U8 RxCount;      //一帧接收数据长度
	volatile U8 RxState;      //一帧接收状态
	U32 TimeReciveLastData;  //接收最后一个字节的时间
}Uart_RxBuffer_t;

typedef struct {

	U8 *TxBuf;	             //发送缓冲区
	U8 TxHeadPoint;          //发送帧头指针
	U8 TxTailPoint;          //发送帧头指针
	volatile U8 TxCount;     // 一帧发送数据长度
	volatile U8 TxState;     //一帧接收标志
	U32 TimeTransmitLastData;//发送最后一个数据的时间
}Uart_TxBuffer_t;

//提供给外部的接口函数
extern int UART_Init (LPC_Uart_Channel_t DevNum, unsigned long BaudRate,unsigned char divaddval,unsigned char mulval, 
		LPC_Uart_WordLenth_t WordLenth, BOOL TwoStopBitsSelect, 
		BOOL ParityEnable, LPC_Uart_ParitySelect_t ParitySelect, 
		BOOL BreakEnable, BOOL FIFOEnable, LPC_Uart_FIFORxTriggerLevel_t FIFORxTriggerLevel,
		BOOL IntEnable, unsigned long IntType);

extern int Send_645_Format(LPC_Uart_Channel_t DevNum,unsigned char *ADDR,unsigned char control,unsigned char *Buf,unsigned short datalong);

extern void Uart_DataTimeout(void);




#endif //__LPC_UART_H

