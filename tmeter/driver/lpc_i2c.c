/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#include <rtl.h>
#include <LPC17xx.H>

#include "LPC_I2C.h"
#include "config.h"
#include "lpc_timer.h"
#include "target_nvic.h"
#include "lcd.h"
#include "eeprom.h"


LPC_I2C_Msg_t	I2C0Msg,I2C1Msg;
/*********************************************************************************************************
** 函数名称: I2C_Init
** 功能描述: i2c 初始化
** 输　入: 	 DevNum   端口编号   好比与PC机通讯时候，端口号选择
**			 Mode	  模式
**			 BusSpeed  总线速度   ，任何通讯方式都会有总线速度，而总线速度是受控于时候驱动控制
**			 SlaveAddr 从机地址     ，iic通讯协议约定，每一个逻辑器件都有地址
** 输　出:   0  成功
**			 其他 错误编码
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
int I2C_Init (LPC_I2C_Channel_t DevNum,LPC_I2C_Mode_t Mode,U32 BusSpeed,U32 SlaveAddr)
{
	U16 I2C_I2SCLH,I2C_I2SCLL;

	if (BusSpeed > I2C_MAXSPEED)
		return 1;

	I2C_I2SCLH = (((Fpclk / BusSpeed) + 1) /2) ;    //通讯速率计算
	I2C_I2SCLL = (Fpclk / BusSpeed )/2 ;

	if (I2C_I2SCLH < 4 || I2C_I2SCLL < 4){
		return 1;
	}
	switch(DevNum)
	{
		case I2C0:
		{
			LPC_I2C0->I2SCLH = I2C_I2SCLH;     //将计算出来的速率之写入占空比寄存器高半字
			LPC_I2C0->I2SCLL = I2C_I2SCLL;      //将计算出来的速率之写入占空比寄存器低半字
			I2C0Msg.State = I2C_IDLE;
			I2C0Msg.DevNum =  I2C0;
			if(Mode == I2C_MASTER){
				I2C0Msg.Mode  =   I2C_MASTER;
			}else{
				I2C0Msg.Mode =   I2C_SLAVE;
				LPC_I2C0->I2ADR0 = SlaveAddr;
			}
			LPC_PINCON->PINSEL1 &= ~0x3c00000;    
			LPC_PINCON->PINSEL1 |= 0x1400000;

			NVIC_PrioritySet(I2C0_IRQn, PriorityI2C0);
			NVIC_EnableSet(I2C0_IRQn);              // enable irq in nvic 
			
		}
		return 0;
		case I2C1:
		{
			LPC_I2C1->I2SCLH = I2C_I2SCLH;
			LPC_I2C1->I2SCLL = I2C_I2SCLL;
			I2C1Msg.State = I2C_IDLE;
			I2C1Msg.DevNum =  I2C1;
			if(Mode == I2C_MASTER){
				I2C1Msg.Mode =   I2C_MASTER;
			}else{
				I2C1Msg.Mode =   I2C_SLAVE;
				LPC_I2C1->I2ADR0 = SlaveAddr;
			}
//			LPC_PINCON->PINSEL0 |= 0x0f;			//P0.0 && P0.1
			LPC_PINCON->PINSEL1 |= (3<<6);			//P0.19
			LPC_PINCON->PINSEL1 |= (3<<8);			//P0.20

			NVIC_PrioritySet(I2C1_IRQn, PriorityI2C1);
			NVIC_EnableSet(I2C1_IRQn);              // enable irq in nvic 
		}
		return 0;
		default :
			return 1;

	}
}
/*************************************************************************
 * Function Name: I2C_MasterWrite
 * Parameters: lpc_uint8 addr -- the slave address which you send message to
 *			  lpc_uint8 *pMsg -- the point to the message
 *			  lpc_uint32 numMsg -- the byte number of the message
 * Return: int 
 *             	0: success
 *			non-zero: error number
 *
 * Description: Transmit messages
 *
 *************************************************************************/
int I2C_MasterWrite (LPC_I2C_Channel_t DevNum,U8 addr, U8 *pMsg , U32 numMsg)
{
	return I2C_Transfer (DevNum,addr, pMsg , numMsg, WRITE, 0);
}


/*************************************************************************
 * Function Name: I2C_MasterRead
 * Parameters: lpc_uint8 addr -- the slave address which you send message to
 *			  lpc_uint8 *pMsg -- the point to the message
 *			  lpc_uint32 numMsg -- the byte number of the message
 * Return: int 
 *             	0: success
 *			non-zero: error number
 *
 * Description: Receive messages
 *
 *************************************************************************/
int I2C_MasterRead (LPC_I2C_Channel_t DevNum,U8 addr, U8 *pMsg , U32 numMsg)
{
	return I2C_Transfer (DevNum,addr, pMsg , numMsg, READ, 0);
}
/*********************************************************************************************************
** 函数名称: I2C_Transfer
** 功能描述: i2c传输函数
** 输　入: 	 DevNum   端口编号
**           addr 从机地址
**			 *pMsg 数据指针
**			 numMsg  操作的数据个数
**			 transMode 传输模式
**			 numWrite  写数据个数
** 输　出:   0  成功
**			 其他 错误编码
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
int I2C_Transfer (LPC_I2C_Channel_t DevNum,U8 addr, U8 *pMsg , U32 numMsg, 
	LPC_I2C_TransMode_t transMode, U32 numWrite)
{
	
	LPC_I2C_Msg_t *pConfig;

	switch(DevNum)
	{
		case I2C0:
			pConfig = &I2C0Msg;	
		break;
		case I2C1:
			pConfig = &I2C1Msg;	
		break;

		default:
		return 0xff;
	}
	if ( pConfig->Mode == I2C_MASTER){
		if (transMode == WRITETHENREAD){
			if (numWrite >= numMsg)
				return 1;
			else
				pConfig->nrWriteBytes = numWrite;
		}
		else
			pConfig->nrWriteBytes= 0;
	
		pConfig->buf = pMsg;
		pConfig->nrBytes= numMsg;
		pConfig->address = addr;
		pConfig->transMode= transMode;
		pConfig->dataCount = 0;
		pConfig->TimeTransferFirstData = TimeTick;

		pConfig->State = I2C_BUSY;	

		switch(DevNum)
		{
			case I2C0:
				I2C_EnableI2C(I2C0);
				__I2C_ClearFlag(I2C0,(I2C_MSG_START | I2C_MSG_SI | I2C_MSG_AA));
				__I2C_SetFlag(I2C0,I2C_MSG_START);
				
			break;
			case I2C1:
				I2C_EnableI2C(I2C1);
				__I2C_ClearFlag(I2C1,(I2C_MSG_START | I2C_MSG_SI | I2C_MSG_AA));
				__I2C_SetFlag(I2C1,I2C_MSG_START);
				
			break;

			default:
			return I2C_DEVICE_NOT_PRESENT;
		}	
	}
	while(pConfig->State){
		if(pConfig->State == I2C_BUSY){
			if(TimeTick>=pConfig->TimeTransferFirstData){
				if((TimeTick-pConfig->TimeTransferFirstData)>20){
					pConfig->State = I2C_TIME_OUT;
				}
			}else{
				if((0x0ffffffff-pConfig->TimeTransferFirstData+TimeTick)>20){
					pConfig->State = I2C_TIME_OUT;
				}
			}
		}else{
			break;
		}
	}
	return pConfig->State;
}

/*********************************************************************************************************
** 函数名称: I2C_HandleMasterState
** 功能描述: I2C 主机中断处理函数
** 输　入: 	 DevNum  端口编号
**
** 输　出:   
**
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
__inline void I2C_HandleMasterState(LPC_I2C_Channel_t DevNum)
{
	unsigned long I2Cstatus ;//= I2C_I2STAT & 0xF8;
	unsigned long ReadData ; // for debug
	LPC_I2C_Msg_t *pConfig;

//	switch(DevNum)
//	{
//		case I2C0:
//			I2Cstatus = LPC_I2C0->I2STAT & 0xF8;
//			pConfig = &I2C0Msg;
//		break;
//		case I2C1:
//			I2Cstatus = LPC_I2C1->I2STAT & 0xF8;
//			pConfig = &I2C1Msg;
//		break;
//		default:
//		return ;
//	}
	pConfig = ((DevNum)?(&I2C1Msg):(&I2C0Msg));
	I2Cstatus = ((DevNum)?(LPC_I2C1->I2STAT&0xF8):(LPC_I2C0->I2STAT&0xF8));
	switch (I2Cstatus)
	{
		case 0x08 : // start condition transmitted
		case 0x10 : // restart condition transmitted
			if (pConfig->transMode == WRITETHENREAD){
				if (pConfig->dataCount < pConfig->nrWriteBytes)
					__I2C_SendData( pConfig->DevNum,pConfig->address | WRITE );
				else
					__I2C_SendData( pConfig->DevNum,pConfig->address | READ );
			}else{
				__I2C_SendData( pConfig->DevNum,(pConfig->address | pConfig->transMode) );
			}
			__I2C_ClearFlag(pConfig->DevNum,I2C_MSG_SI);	// clear SI bit, STO bit clear automatically
			break;
			
		case 0x18 : // SLA+W transmitted and ACK received
			if ( pConfig->nrBytes == 0 ){
				__I2C_ClearFlag(pConfig->DevNum,I2C_MSG_SI | I2C_MSG_START);
				__I2C_SetFlag(pConfig->DevNum,I2C_MSG_STOP);
	    			pConfig->State = I2C_NO_DATA;
			}else{
    				// Send next data byte
				__I2C_SendData(pConfig->DevNum,pConfig->buf[pConfig->dataCount++]);	
				__I2C_ClearFlag(pConfig->DevNum,(I2C_MSG_SI | I2C_MSG_START)); // clear SI, STA bit, STO bit clear automatically
			}
			break;
			
		case 0x20 : // SLA+W transmitted, but no ACK received
			__I2C_ClearFlag(pConfig->DevNum,I2C_MSG_SI | I2C_MSG_START);
			__I2C_SetFlag(pConfig->DevNum,I2C_MSG_STOP);
			pConfig->State = I2C_NACK_ON_ADDRESS;
			break;
			
		case 0x28 : //Data byte transmitted and ACK received

			if (pConfig->transMode == WRITE){
				if (pConfig->dataCount < pConfig->nrBytes){
					__I2C_SendData(pConfig->DevNum,pConfig->buf[pConfig->dataCount++]);	
					__I2C_ClearFlag(pConfig->DevNum,I2C_MSG_SI | I2C_MSG_START);
				}else{
					__I2C_ClearFlag(pConfig->DevNum,I2C_MSG_SI | I2C_MSG_START);
					__I2C_SetFlag(pConfig->DevNum,I2C_MSG_STOP);	// data transmit finished, stop transmit
					pConfig->State = I2C_OK;
				}
			}else if (pConfig->transMode == WRITETHENREAD){
				if (pConfig->dataCount < pConfig->nrWriteBytes){
					__I2C_SendData(pConfig->DevNum,pConfig->buf[pConfig->dataCount++]);	
					__I2C_ClearFlag(pConfig->DevNum,I2C_MSG_SI | I2C_MSG_START);
				}else{	
					// send start condition
					__I2C_ClearFlag(pConfig->DevNum,I2C_MSG_SI);
					__I2C_SetFlag(pConfig->DevNum,I2C_MSG_START);
				}
				
			}else {	
				//补加
				__I2C_ClearFlag(pConfig->DevNum,I2C_MSG_SI | I2C_MSG_START | I2C_MSG_AA);
				__I2C_SetFlag(pConfig->DevNum,I2C_MSG_STOP);
				pConfig->State = I2C_ERROR;
			}
			break;
			
		case 0x30 : // no ACK for data byte
			__I2C_ClearFlag(pConfig->DevNum,I2C_MSG_SI | I2C_MSG_START);
			__I2C_SetFlag(pConfig->DevNum,I2C_MSG_STOP);
			pConfig->State = I2C_NACK_ON_DATA;
			break;
			
		case 0x38 : // arbitration lost in Slave Address or Data bytes
			//__I2C_ClearFlag(I2C_MSG_SI | I2C_MSG_START);	// Or stop and release bus???
			__I2C_ClearFlag(pConfig->DevNum,I2C_MSG_SI);				// Send start again
			__I2C_SetFlag(pConfig->DevNum,I2C_MSG_START);
			pConfig->State = I2C_ARBITRATION_LOST;
			break;
			
		// MASTER RECEIVER FUNCTIONS
		case 0x40 : // ACK for slave address + R
			if (pConfig->nrBytes>1){
				__I2C_ClearFlag(pConfig->DevNum,I2C_MSG_SI | I2C_MSG_START);
				__I2C_SetFlag(pConfig->DevNum,I2C_MSG_AA);	// send acknowledge byte
			}else{
				__I2C_ClearFlag(pConfig->DevNum,I2C_MSG_SI | I2C_MSG_START | I2C_MSG_AA); // return NACK
			}
			break;
			
		case 0x48 : // no ACK for slave address + R
			__I2C_ClearFlag(pConfig->DevNum,I2C_MSG_SI | I2C_MSG_START);
			__I2C_SetFlag(pConfig->DevNum,I2C_MSG_STOP);
			pConfig->State = I2C_NACK_ON_ADDRESS;
			break;
			
		case 0x50 : // ACK for data byte
			ReadData =  __I2C_ReceiveData(pConfig->DevNum);	// for debug
			pConfig->buf[pConfig->dataCount++] = ReadData & 0xFF;
			//添加一段以改进以适应I2C0的通讯协议
			if (pConfig->dataCount + 1 < pConfig->nrBytes){
				__I2C_ClearFlag(pConfig->DevNum,I2C_MSG_SI | I2C_MSG_START);
				__I2C_SetFlag(pConfig->DevNum,I2C_MSG_AA);	// send acknowledge byte
			}else{
				__I2C_ClearFlag(pConfig->DevNum,I2C_MSG_SI | I2C_MSG_START | I2C_MSG_AA); // return NACK
			}
			break;
			
		case 0x58 : // no ACK for data byte
			ReadData =  __I2C_ReceiveData(pConfig->DevNum);	// for debug
			pConfig->buf[pConfig->dataCount++] = ReadData & 0xFF;
			
			__I2C_ClearFlag(pConfig->DevNum,I2C_MSG_SI | I2C_MSG_START);
			__I2C_SetFlag(pConfig->DevNum,I2C_MSG_STOP);
			pConfig->State = I2C_OK;
			//添加一段以改进以适应I2C0的通讯协议
			break;
			
		default : // undefined error//补加
			__I2C_ClearFlag(pConfig->DevNum,I2C_MSG_SI | I2C_MSG_START | I2C_MSG_AA);
			__I2C_SetFlag(pConfig->DevNum,I2C_MSG_STOP);
			pConfig->State = I2C_ERROR;
			break;
	} // switch - I2C_I2STAT & 0xF8
	return ;
}
/*********************************************************************************************************
** 函数名称: I2C0_IRQHandler
** 功能描述: I2C0中断处理函数
** 输　入: 	 
**
** 输　出:   
**
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
__irq void I2C0_IRQHandler(void)
{

	I2C_HandleMasterState(I2C0);	// Master Mode

}
/*********************************************************************************************************
** 函数名称: I2C1_IRQHandler
** 功能描述: I2C1中断处理函数
** 输　入: 	 
**
** 输　出:   
**
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
__irq void I2C1_IRQHandler(void)
{

	I2C_HandleMasterState(I2C1);	// Master Mode

}



///*************************************************************************
// * Function Name: init_i2c_device
// * Parameters: void
// * Return: void
// *
// * Description: 初始化i2c器件
// *  
// *************************************************************************/
//void init_i2c_device(void)
//{
//
//	//IIC2 管脚链接
////	LPC_PINCON->PINSEL0 &= ~(1<<20);
////	LPC_PINCON->PINSEL0 |= (1<<21);
////	LPC_PINCON->PINSEL0 &= ~(1<<22);
////	LPC_PINCON->PINSEL0 |= (1<<23);
//
//	I2C_Init (I2C0,I2C_MASTER,100000,0x10);
//	I2C_Init (I2C1,I2C_MASTER,50000,0x10);
//	initWP();
//
////	TID_Eeprom = os_tsk_create (EepromTask,PRI_Eeprom);
//	TID_Lcd = os_tsk_create (LcdDisplay,PRI_LcdDisplay);
//
//}
