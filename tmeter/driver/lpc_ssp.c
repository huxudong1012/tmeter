/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#include <rtl.h>
#include <LPC17xx.h>
#include "LPC_SSP.h"
#include "lpc_uart.h"
#include "config.h"




//extern U32 TimeTick;                    /*系统定时器 1ms由定时器1产生*/

/*********************************************************************************************************
** 函数名称:             SSP_Init
** 功能描述:             初始化SSP外设
** 输　入:       DevNum  SSP编号
**				 Mode    主从机模式
**				 BusSpeed 总线速度
**				 CPHA     时钟相位
**				 CPOL     时钟极性
**				 LSBF     传输次序
**				 IntEnable 中断使能与否
**				 (* Fnpr)(void *) 中断调用函数
**				  FnprArg 中断调用数据
** 输　出:       0 成功
**				 1 参数错误
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
int SSP_Init (LPC_SSP_Channel_t DevNum, LPC_SSP_Mode_t Mode,LPC_SSP_Format_t Format, U32 BusSpeed,LPC_SSP_Datalong_t Datalong, LPC_SSP_CPHA_t  CPHA, LPC_SSP_CPOL_t  CPOL, LPC_SSP_LBM_t  LBM, LPC_SSP_ENSSP_t ENSSP,LPC_SSP_SSPSOD_t SSPSOD, int Speed_DVSR,BOOL IntEnable,unsigned short IntType, void (* Fnpr)(void *), void * FnprArg)
{
	unsigned long SPCCR_value;


	// Check parameter valid
	if (BusSpeed > SSP_MAXSPEED) //判断参数波特率是否合法
		return 1;
	SPCCR_value = Fpclk/BusSpeed/Speed_DVSR; //求取串行时钟速率值
	if (SPCCR_value < SSPCR0_MINVALUE)
		return 1;

	// Configure the registers
	switch(DevNum)
	{
		case SSP0:

			LPC_PINCON->PINSEL0 &= ~(0x0c0000000);		  //p0.15 sck
			LPC_PINCON->PINSEL0 |= (0x80000000);

			LPC_PINCON->PINSEL1 &= ~(3<<2);			  //p0.17 miso
			LPC_PINCON->PINSEL1 |= (2<<2);

			LPC_PINCON->PINSEL1 &= ~(3<<4);			 //p0.18 mosi
			LPC_PINCON->PINSEL1 |= (2<<4);

			SSP0_SS_SEL;							 //p0.16 ss
			SSP0_SS_DIR;
			SSP0_SS(1);

		//	LPC_PINCON->PINSEL1 &= ~(3);			  //ss
		//	LPC_PINCON->PINSEL1 |= (2);

			LPC_SSP0->CR0 = (SPCCR_value<<8)|(CPHA << 7) | (CPOL << 6) | (Format << 4) |(Datalong);
			LPC_SSP0->CR1 = (SSPSOD << 3) | (ENSSP << 1) | (Mode << 2) |(LBM);
			LPC_SSP0->CPSR = Speed_DVSR;
			
		break;
		case SSP1:

			SSP1_SS_SEL;
			SSP1_SS_DIR;
			SSP1_SS(1);


			LPC_PINCON->PINSEL0 &= ~(3<<14);		  //p0.7 sck
			LPC_PINCON->PINSEL0 |= (2<<14);

			LPC_PINCON->PINSEL0 &= ~(3<<16);		  //p0.8 miso
			LPC_PINCON->PINSEL0 |= (2<<16);

			LPC_PINCON->PINSEL0 &= ~(3<<18);		  //p0.9 mosi
			LPC_PINCON->PINSEL0 |= (2<<18);

		
			LPC_SSP1->CR0 = (SPCCR_value<<8)|(CPHA << 7) | (CPOL << 6) | (Format << 4) |(Datalong);
			LPC_SSP1->CR1 = (SSPSOD << 3) | (ENSSP << 1) | (Mode << 2) |(LBM);
			LPC_SSP1->CPSR = Speed_DVSR;
			
		break;
		default:
		return 1;
	}
	
	return 0;
}
/*********************************************************************************************************
** 函数名称:             SSP_AccessData
** 功能描述:             查询方式读写ssp数据
** 输　入:       DevNum  SSP编号
**				 RxBuf   接收的数据缓冲
**				 TxBuf   发送数据缓冲
**				 datalong    数据长度
** 输　出:       1 成功
**				 0 错误
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 SSP_AccessData(LPC_SSP_Channel_t DevNum,U8 *RxBuf,U8 *TxBuf,U16 datalong)
{
	U32 time_access = 0;
	U8 i;

	switch(DevNum)
	{
		case SSP0:
		{
			SSP0_SS(0);   //硬件p0[16]连接ADE_CS，从机为计量芯片，为0时选择从机
			for(i = 0;i<datalong;i++){
				LPC_SSP0->DR = *(TxBuf++);   //载入要发送的数据
				time_access = TimeTick;
				do{
					//超时判断
					if(TimeTick>=time_access){
						if((TimeTick-time_access)>3){
							SSP0_SS(1);   //从机失能
							return 0;
						}
					}else{
						if((0x0ffffffff-time_access+TimeTick)>3){
							SSP0_SS(1);
							 return 0;
						}
					}	
				}
				while((LPC_SSP0->SR)&SPSR_BSY);//等待发送完成
				*(RxBuf++) = LPC_SSP0->DR;
			}
			SSP0_SS(1);
		}
		return 1;
		case SSP1:
		{
			SSP1_SS(0);
			for(i = 0;i<datalong;i++){
				LPC_SSP1->DR = *(TxBuf++);
				time_access = TimeTick;
				do{
					//超时判断
					if(TimeTick>=time_access){
						if((TimeTick-time_access)>3){
							SSP1_SS(1);
							return 0;
						}
					}else{
						if((0x0ffffffff-time_access+TimeTick)>3){
							SSP1_SS(1);
							 return 0;
						}
					}	
				}
				while((LPC_SSP1->SR)&SPSR_BSY);
				*(RxBuf++) = LPC_SSP1->DR;
			}
			SSP1_SS(1);
		}
		return 1;
		default:
		return 0;
	}

}

