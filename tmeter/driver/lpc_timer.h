/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __LPC_TIMER_H
#define __LPC_TIMER_H


/* Interrupt source type */
#define TIMERMR0Int     0x01
#define TIMERMR1Int     0x02
#define TIMERMR2Int     0x04
#define TIMERMR3Int     0x08
#define TIMERCR0Int     0x10
#define TIMERCR1Int     0x20
#define TIMERCR2Int     0x40
#define TIMERCR3Int     0x80

#define PriorityTIMER1	29
#define PriorityTIMER2	30

extern void enable_timer1(void);
extern void disable_timer1(void);
extern void reset_timer1(void);
extern void init_timer1(U32 TimerInterval);

extern void init_timer0(void);
extern void enable_timer0(void);
extern void disable_timer0(void);
extern void reset_timer0(void);

extern void init_timer2(void);
extern void enable_timer2(void);
extern void disable_timer2(void);
extern void reset_timer2(void);


#endif /*__LPC_TIMER_H */
