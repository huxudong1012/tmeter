/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __LPC_SSP_H
#define __LPC_SSP_H



/* The micro are configurable */
#define SSP_RXBUFSIZE    256

/* 	
	SSP max speed: 7.5Mbits/s
 	SSP bus speed = Fpclk / SPCCR 
 */
#define SSP_MAXSPEED        7500000


#define SSPCR0_MINVALUE		1

/* SSP Status Type */
#define SPSR_TFE	0x1	   // 发送 FIFO 空。发送 FIFO 为空时该位为 1，反之为 0 
#define SPSR_TNF	0x2	   // 发送 FIFO 未满。Tx FIFO 满时该位为 0，反之为 1 
#define SPSR_RNE	0x4	   // 接收 FIFO 不为空。接收 FIFO为空时该位为 0，反之为 1 
#define SPSR_RFF	0x8	   // 接收 FIFO 满。接收 FIFO 满时该位为 1，反之为 0 
#define SPSR_BSY	0x10   // 忙。SSPn 控制器空闲时该位为 0，或当前正在发送/接收一帧数据和/或 Tx FIFO 不为空时该位为 1 	

/* SSP INT Status Type */
#define RORMIS	0x1		// 当 Rx FIFO 满时又接收到另一帧数据，并且中断被使能时该位置位
#define RTMIS	0x2	    // 当接收超时条件出现且中断被使能时该位置位。
#define RXMIS	0x4	    // 当 Rx FIFO 至少一半为满且中断被使能时该位置位 
#define TXMIS	0x8	    // 当 Tx FIFO 至少一半为空且中断被使能时该位置位 


/* SSP Interrupt Enable Type*/
#define SSP_INTRORIM      0x1 //当接收溢出时软件置位该位来使能中断 
#define SSP_INTRTIM       0x2 //接收超时，软件置位该位来使能中断
#define SSP_INTRXIM       0x4 // Rx FIFO 至少有一半为满
#define SSP_INTTXIM       0x8 // Tx FIFO 至少有一半为空



#define SSP_BUF_OVERFLOW_ERR       0x4

/* Device number */
typedef enum {
  SSP0 = 0,
  SSP1
} LPC_SSP_Channel_t;

/* Clock phase control */
typedef enum {
	SSP_SCK_FIRSTEDGE = 0,
	SSP_SCK_SECONDEDGE
} LPC_SSP_CPHA_t;

/* Clock polarity control */
typedef enum {
	SSP_SCK_HIGH = 0,
	SSP_SCK_LOW
} LPC_SSP_CPOL_t;

/*  LBM Mode controls */
typedef enum {
	SSP_LBMOFF = 0,		// Nomal mode
	SSP_LBMON			// Back mode
} LPC_SSP_LBM_t;

/* Master mode select */
typedef enum {
	SSP_MASTER = 0,
	SSP_SLAVE
} LPC_SSP_Mode_t;

/* SSP enable select */
typedef enum {
	SSP_UNENSSP = 0,
	SSP_ENSSP
} LPC_SSP_ENSSP_t;

/* SSP SOD  select */
typedef enum {
	SSP_UNENSSPSOD = 0,
	SSP_ENSSPSOD
} LPC_SSP_SSPSOD_t;
/* Format select */
typedef enum {
	SSP_SPI = 0,
	SSP_SSI,
	SSP_Microwire
} LPC_SSP_Format_t;

/* Datalong select */
typedef enum {
	SSP_Datalong4 = 3,
	SSP_Datalong5,
	SSP_Datalong6,
	SSP_Datalong7,
	SSP_Datalong8,
	SSP_Datalong9,
	SSP_Datalong10,
	SSP_Datalong11,
	SSP_Datalong12,
	SSP_Datalong13,
	SSP_Datalong14,
	SSP_Datalong15,
	SSP_Datalong16
} LPC_SSP_Datalong_t;



//ssp0 从机选择
#define SSP0_SS_SEL   (LPC_PINCON->PINSEL1 &= ~((3<< 0))) // 选择p0.16为GPIO  
#define SSP0_SS_DIR   (LPC_GPIO0->FIODIR |= (1<<16)) // 选择p0.16为输出
#define SSP0_SS(x)	  (x?(LPC_GPIO0->FIOSET |= (1<<16)):(LPC_GPIO0->FIOCLR |= (1<<16)))//设置p0.16为高或低

//CE 从机选择
#define SSP1_SS_SEL   (LPC_PINCON->PINSEL0 &= ~((3<< 12))) // 选择p0.6为GPIO  
#define SSP1_SS_DIR   (LPC_GPIO0->FIODIR |= (1<<6)) // 选择p0.6为输出
#define SSP1_SS(x)	  (x?(LPC_GPIO0->FIOSET |= (1<<6)):(LPC_GPIO0->FIOCLR |= (1<<6)))//设置p0.6为高或低

//ce1 从机选择
#define FALSH_CE1_SEL   (LPC_PINCON->PINSEL2 &= ~(0x0c0000000)) // 选择p1.15为GPIO  
#define FALSH_CE1_DIR   (LPC_GPIO1->FIODIR |= (1<<15)) // 选择p1.15为输出
#define FALSH_CE1(x)	  (x?(LPC_GPIO1->FIOSET |= (1<<15)):(LPC_GPIO1->FIOCLR |= (1<<15)))//设置p1.15为高或低

//wp 从机选择
#define FALSH_WP_SEL   (LPC_PINCON->PINSEL0 &= ~((3<< 10))) // 选择p0.5为GPIO  
#define FALSH_WP_DIR   (LPC_GPIO0->FIODIR |= (1<<5)) // 选择p0.5为输出
#define FALSH_WP(x)	  (x?(LPC_GPIO0->FIOSET |= (1<<5)):(LPC_GPIO0->FIOCLR |= (1<<5)))//设置p0.5为高或低

/* Declare functions */
extern int SSP_Init (LPC_SSP_Channel_t DevNum, LPC_SSP_Mode_t Mode,LPC_SSP_Format_t Format, U32 BusSpeed,LPC_SSP_Datalong_t Datalong, LPC_SSP_CPHA_t  CPHA, LPC_SSP_CPOL_t  CPOL, LPC_SSP_LBM_t  LBM, LPC_SSP_ENSSP_t ENSSP,LPC_SSP_SSPSOD_t SSPSOD, int Speed_DVSR,BOOL IntEnable,unsigned short IntType, void (* Fnpr)(void *), void * FnprArg);

extern U8 SSP_AccessData(LPC_SSP_Channel_t DevNum,U8 *RxBuf,U8 *TxBuf,U16 datalong);

 #endif  // __LPC_SSP_H
