/****************************************Copyright (c)**************************************************
**
**                                 许继电能仪表有限公司
**                                     
**                                       IE    科                                       
**
**                                 
**--------------文件信息--------------------------------------------------------------------------------
**文   件   名: event
**创   建   人: 
**最后修改日期: 
**描        述: 基于lpc17xx  操纵系统选用keil_RTX
**              						
**--------------历史版本信息----------------------------------------------------------------------------
** 创建人: 
** 版  本: 
** 日　期: 
** 描　述: 
**
**--------------当前版本修订------------------------------------------------------------------------------
** 修改人: 
** 日　期:
** 描　述: 
**		
**		
**		
**		
**		
**		
**		
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
#ifndef __EVEVT_H
#define __EVEVT_H


	//总次数，总累计时间
	typedef struct {
		U8 timesA[3];					//A相总次数
		U8 timeA[3];					//A相总累计时间
		U8 timesB[3];					//B相总次数
		U8 timeB[3];					//B相总累计时间
		U8 timesC[3];					//C相总次数
		U8 timeC[3];					//C相总累计时间

	}totalEvent_t;

	typedef union {
		U8 buf[18];
		totalEvent_t totalEvent;
	}totalEvent_u;

	//总次数，总累计时间(分)
	typedef struct {
		U8 times[3];					//总次数
		U8 time[3];						//总累计时间
	}totalTimes_t;

	typedef union {
		U8 buf[6];
		totalTimes_t totalTimes;
	}totalTimes_u;

	//总次数,用于需量超限
	typedef struct {
		U8 timesFP[3];					//正向有功需量超限总次数
		U8 timesRP[3];					//反向有功需量超限总次数
		U8 q1[3];						//第1象限无功需量超限总次数
		U8 q2[3];						//第2象限无功需量超限总次数
		U8 q3[3];						//第3象限无功需量超限总次数
		U8 q4[3];						//第4象限无功需量超限总次数

	}timesDemand_t;

	typedef union {
		U8 buf[6];
		timesDemand_t timesDemand;
	}timesDemand_u;

	//失压、欠压、过压和断相记录，使用同一数据结构
	typedef struct {
		U8 timeStart[6];					//发生时刻
		U8 timeEnd[6];						//结束时刻
		union{
			U8 fPIncrease[4];				//期间正向有功总电能增量
//			U32 fPI; 
		}fPI_u;

		union{
			U8 rPIncrease[4];				//期间反向有功总电能增量
//			U32 rPI; 
		}rPI_u;

		union{
			U8 combo1Increase[4];			//期间组合无功1总电能增量
//			U32 combo1;
		}combo1_u;

		union{
			U8 combo2Increase[4];			//期间组合无功2总电能增量
//			U32 combo2;
		}combo2_u;

		/*----------------------------a相------------------------------*/
		union{
			U8 aFPIncrease[4];				//期间A相正向有功电能增量
//			U32 aFPI;
		}aFPI_u;

		union{
			U8 aRPIncrease[4];				//期间A相反向有功电能增量
//			U32 aRPI;
		}aRPI_u;

		union{
			U8 aCombo1Increase[4];			//期间A相组合无功1总电能增量
//			U32 aCombo1;
		}aCombo1_u;

		union{
			U8 aCombo2Increase[4];			//期间A相组合无功2总电能增量
//			U32 aCombo2;
		}aCombo2_u;

		U8 ua[2];							//发生时刻A相电压
		U8 ia[3];							//发生时刻A相电流
		U8 aKw[3];							//发生时刻A相有功功率
		U8 aKvar[3];						//发生时刻A相无功功率
		U8 aCos[2];							//发生时刻A相功率因数

		/*----------------------------b相------------------------------*/
		union{
			U8 bFPIncrease[4];				//期间b相正向有功电能增量
//			U32 bFPI;
		}bFPI_u;

		union{
			U8 bRPIncrease[4];				//期间b相反向有功电能增量
//			U32 bRPI;
		}bRPI_u;

		union{
			U8 bCombo1Increase[4];			//期间b相组合无功1总电能增量
//			U32 bCombo1;
		}bCombo1_u;

		union{
			U8 bCombo2Increase[4];			//期间b相组合无功2总电能增量
//			U32 bCombo2;
		}bCombo2_u;

		U8 ub[2];							//发生时刻b相电压
		U8 ib[3];							//发生时刻b相电流
		U8 bKw[3];							//发生时刻b相有功功率
		U8 bKvar[3];						//发生时刻b相无功功率
		U8 bCos[2];							//发生时刻b相功率因数
		/*----------------------------c相------------------------------*/
		union{
			U8 cFPIncrease[4];				//期间c相正向有功电能增量
//			U32 cFPI;
		}cFPI_u;

		union{
			U8 cRPIncrease[4];				//期间c相反向有功电能增量
//			U32 cRPI;
		}cRPI_u;

		union{
			U8 cCombo1Increase[4];			//期间c相组合无功1总电能增量
//			U32 cCombo1;
		}cCombo1_u;

		union{
			U8 cCombo2Increase[4];			//期间c相组合无功2总电能增量
//			U32 cCombo2;
		}cCombo2_u;

		U8 uc[2];							//发生时刻c相电压
		U8 ic[3];							//发生时刻c相电流
		U8 cKw[3];							//发生时刻c相有功功率
		U8 cKvar[3];						//发生时刻c相无功功率
		U8 cCos[2];							//发生时刻c相功率因数
		/*------------------------------------------------------------*/
		union{
			U8 Ah[4];						//期间总安时数
//			U32 Ah32;
		}Ah_u;
		union{
			U8 aAh[4];						//期间A相安时数
//			U32 aAh32;
		}aAh_u;
		union{
			U8 bAh[4];						//期间B相安时数
//			U32 bAh32;
		}bAh_u;
		union{
			U8 cAh[4];						//期间C相安时数
//			U32 cAh32;
		}cAh_u;

		U8 sta;
	}lostVoltage_t;	

	typedef union {
		U8 buf[131];
		lostVoltage_t lostVoltage;
	}voltage_u;

	//全失压数据结构
	typedef struct {
		U8 timeStart[6];					//发生时刻 
		U8 i[3];							//电流
		U8 timeEnd[6];						//结束时刻
		

		U8 sta;
	}allLost_t;	

	typedef union {
		U8 buf[15];
		allLost_t allLost;
	}allLost_u;

	//失流、过流、断流记录，使用同一数据结构
	typedef struct {
		U8 timeStart[6];					//发生时刻
		U8 timeEnd[6];						//结束时刻
		union{
			U8 fPIncrease[4];				//期间正向有功总电能增量
//			U32 fPI; 
		}fPI_u;

		union{
			U8 rPIncrease[4];				//期间反向有功总电能增量
//			U32 rPI; 
		}rPI_u;

		union{
			U8 combo1Increase[4];			//期间组合无功1总电能增量
//			U32 combo1;
		}combo1_u;

		union{
			U8 combo2Increase[4];			//期间组合无功2总电能增量
//			U32 combo2;
		}combo2_u;

		/*----------------------------a相------------------------------*/
		union{
			U8 aFPIncrease[4];				//期间A相正向有功电能增量
//			U32 aFPI;
		}aFPI_u;

		union{
			U8 aRPIncrease[4];				//期间A相反向有功电能增量
//			U32 aRPI;
		}aRPI_u;

		union{
			U8 aCombo1Increase[4];			//期间A相组合无功1总电能增量
//			U32 aCombo1;
		}aCombo1_u;

		union{
			U8 aCombo2Increase[4];			//期间A相组合无功2总电能增量
//			U32 aCombo2;
		}aCombo2_u;

		U8 ua[2];							//发生时刻A相电压
		U8 ia[3];							//发生时刻A相电流
		U8 aKw[3];							//发生时刻A相有功功率
		U8 aKvar[3];						//发生时刻A相无功功率
		U8 aCos[2];							//发生时刻A相功率因数

		/*----------------------------b相------------------------------*/
		union{
			U8 bFPIncrease[4];				//期间b相正向有功电能增量
//			U32 bFPI;
		}bFPI_u;

		union{
			U8 bRPIncrease[4];				//期间b相反向有功电能增量
//			U32 bRPI;
		}bRPI_u;

		union{
			U8 bCombo1Increase[4];			//期间b相组合无功1总电能增量
//			U32 bCombo1;
		}bCombo1_u;

		union{
			U8 bCombo2Increase[4];			//期间b相组合无功2总电能增量
//			U32 bCombo2;
		}bCombo2_u;

		U8 ub[2];							//发生时刻b相电压
		U8 ib[3];							//发生时刻b相电流
		U8 bKw[3];							//发生时刻b相有功功率
		U8 bKvar[3];						//发生时刻b相无功功率
		U8 bCos[2];							//发生时刻b相功率因数
		/*----------------------------c相------------------------------*/
		union{
			U8 cFPIncrease[4];				//期间c相正向有功电能增量
//			U32 cFPI;
		}cFPI_u;

		union{
			U8 cRPIncrease[4];				//期间c相反向有功电能增量
//			U32 cRPI;
		}cRPI_u;

		union{
			U8 cCombo1Increase[4];			//期间c相组合无功1总电能增量
//			U32 cCombo1;
		}cCombo1_u;

		union{
			U8 cCombo2Increase[4];			//期间c相组合无功2总电能增量
//			U32 cCombo2;
		}cCombo2_u;

		U8 uc[2];							//发生时刻c相电压
		U8 ic[3];							//发生时刻c相电流
		U8 cKw[3];							//发生时刻c相有功功率
		U8 cKvar[3];						//发生时刻c相无功功率
		U8 cCos[2];							//发生时刻c相功率因数
		/*------------------------------------------------------------*/


		U8 sta;
	}current_t;	

	typedef union {
		U8 buf[131];
		current_t current;
	}current_u;

 	//潮流反向、过载、电压逆相序、电流逆相序使用同一数据结构
	typedef struct {
		U8 timeStart[6];					//发生时刻
		U8 timeEnd[6];						//结束时刻
		union{
			U8 fPIncrease[4];				//期间正向有功总电能增量
//			U32 fPI; 
		}fPI_u;

		union{
			U8 rPIncrease[4];				//期间反向有功总电能增量
//			U32 rPI; 
		}rPI_u;

		union{
			U8 combo1Increase[4];			//期间组合无功1总电能增量
//			U32 combo1;
		}combo1_u;

		union{
			U8 combo2Increase[4];			//期间组合无功2总电能增量
//			U32 combo2;
		}combo2_u;

		/*----------------------------a相------------------------------*/
		union{
			U8 aFPIncrease[4];				//期间A相正向有功电能增量
//			U32 aFPI;
		}aFPI_u;

		union{
			U8 aRPIncrease[4];				//期间A相反向有功电能增量
//			U32 aRPI;
		}aRPI_u;

		union{
			U8 aCombo1Increase[4];			//期间A相组合无功1总电能增量
//			U32 aCombo1;
		}aCombo1_u;

		union{
			U8 aCombo2Increase[4];			//期间A相组合无功2总电能增量
//			U32 aCombo2;
		}aCombo2_u;

		/*----------------------------b相------------------------------*/
		union{
			U8 bFPIncrease[4];				//期间b相正向有功电能增量
//			U32 bFPI;
		}bFPI_u;

		union{
			U8 bRPIncrease[4];				//期间b相反向有功电能增量
//			U32 bRPI;
		}bRPI_u;

		union{
			U8 bCombo1Increase[4];			//期间b相组合无功1总电能增量
//			U32 bCombo1;
		}bCombo1_u;

		union{
			U8 bCombo2Increase[4];			//期间b相组合无功2总电能增量
//			U32 bCombo2;
		}bCombo2_u;

		/*----------------------------c相------------------------------*/
		union{
			U8 cFPIncrease[4];				//期间c相正向有功电能增量
//			U32 cFPI;
		}cFPI_u;

		union{
			U8 cRPIncrease[4];				//期间c相反向有功电能增量
//			U32 cRPI;
		}cRPI_u;

		union{
			U8 cCombo1Increase[4];			//期间c相组合无功1总电能增量
//			U32 cCombo1;
		}cCombo1_u;

		union{
			U8 cCombo2Increase[4];			//期间c相组合无功2总电能增量
//			U32 cCombo2;
		}cCombo2_u;

		/*------------------------------------------------------------*/

		U8 sta;
	}powerEvent_t;	

	typedef union {
		U8 buf[76];
		powerEvent_t power;
	}powerEvent_u;

	//电压不平衡、电流不平衡使用同一数据结构
	typedef struct {
		U8 timeStart[6];					//发生时刻
		U8 timeEnd[6];						//结束时刻
		U8 imbalanceRate[2];				//最大不平衡率
		union{
			U8 fPIncrease[4];				//期间正向有功总电能增量
//			U32 fPI; 
		}fPI_u;

		union{
			U8 rPIncrease[4];				//期间反向有功总电能增量
//			U32 rPI; 
		}rPI_u;

		union{
			U8 combo1Increase[4];			//期间组合无功1总电能增量
//			U32 combo1;
		}combo1_u;

		union{
			U8 combo2Increase[4];			//期间组合无功2总电能增量
//			U32 combo2;
		}combo2_u;

		/*----------------------------a相------------------------------*/
		union{
			U8 aFPIncrease[4];				//期间A相正向有功电能增量
//			U32 aFPI;
		}aFPI_u;

		union{
			U8 aRPIncrease[4];				//期间A相反向有功电能增量
//			U32 aRPI;
		}aRPI_u;

		union{
			U8 aCombo1Increase[4];			//期间A相组合无功1总电能增量
//			U32 aCombo1;
		}aCombo1_u;

		union{
			U8 aCombo2Increase[4];			//期间A相组合无功2总电能增量
//			U32 aCombo2;
		}aCombo2_u;

		/*----------------------------b相------------------------------*/
		union{
			U8 bFPIncrease[4];				//期间b相正向有功电能增量
//			U32 bFPI;
		}bFPI_u;

		union{
			U8 bRPIncrease[4];				//期间b相反向有功电能增量
//			U32 bRPI;
		}bRPI_u;

		union{
			U8 bCombo1Increase[4];			//期间b相组合无功1总电能增量
//			U32 bCombo1;
		}bCombo1_u;

		union{
			U8 bCombo2Increase[4];			//期间b相组合无功2总电能增量
//			U32 bCombo2;
		}bCombo2_u;

		/*----------------------------c相------------------------------*/
		union{
			U8 cFPIncrease[4];				//期间c相正向有功电能增量
//			U32 cFPI;
		}cFPI_u;

		union{
			U8 cRPIncrease[4];				//期间c相反向有功电能增量
//			U32 cRPI;
		}cRPI_u;

		union{
			U8 cCombo1Increase[4];			//期间c相组合无功1总电能增量
//			U32 cCombo1;
		}cCombo1_u;

		union{
			U8 cCombo2Increase[4];			//期间c相组合无功2总电能增量
//			U32 cCombo2;
		}cCombo2_u;

		/*------------------------------------------------------------*/

		U8 sta;
	}imbalance_t;	

	typedef union {
		U8 buf[78];
		imbalance_t imbalance;
	}imbalance_u;

	//掉电记录、辅助电源失电
	typedef struct {
		U8 timeStart[6];				//发生时刻
		U8 timeEnd[6];					//结束时刻

		U8 sta;
	}powerDown_t;	

	typedef union {
		U8 buf[12];
		powerDown_t powerDown;
	}powerDown_u;

	//需量超限
	typedef struct {
		U8 timeStart[6];				//发生时刻
		U8 timeEnd[6];					//结束时刻
		U8 kw[3];						//需量超限期间正向有功最大需量及发生时间
		U8 timeHappen[6];
								
		U8 sta;
	}demandExceed_t;	

	typedef union {
		U8 buf[22];
		demandExceed_t demandExceed;
	}demandExceed_u;

	//编程记录
	typedef struct {
		U8 timeStart[6];				//发生时刻
		U8 c[4];						//操作者代码
		U8 di[10][4];					//编程的前10个数据标识码
		U8 sta;
	}program_t;	


	typedef union {
		U8 buf[51];
		program_t program;
	}program_u;

	//电表清零记录
	typedef struct {
		U8 timeStart[6];		//发生时刻
		U8 c[4];				//操作者代码

		U8 forwardKwh[4]; 		//正向有功电能数据
		U8 reverseKwh[4];  		//反向有功电能数据
		U8 q1Kvarh[4]; 			//第一象限无功电能数据
		U8 q2Kvarh[4];			//第二象限无功电能数据
		U8 q3Kvarh[4];			//第三象限无功电能数据
		U8 q4Kvarh[4];			//第四象限无功电能数据
		U8 aForward[4];			//电表清零前A相正向有功电能
		U8 aReverse[4];			//电表清零前A相反向有功电能
		U8 aQ1Kvarh[4];			//电表清零前A相第一象限无功电能
		U8 aQ2Kvarh[4];			//电表清零前A相第二象限无功电能
		U8 aQ3Kvarh[4];			//电表清零前A相第三象限无功电能
		U8 aQ4Kvarh[4];			//电表清零前A相第四象限无功电能
		U8 bForward[4];			//电表清零前B相正向有功电能
		U8 bReverse[4];			//电表清零前B相反向有功电能
		U8 bQ1Kvarh[4];			//电表清零前B相第一象限无功电能
		U8 bQ2Kvarh[4];			//电表清零前B相第二象限无功电能
		U8 bQ3Kvarh[4];			//电表清零前B相第三象限无功电能
		U8 bQ4Kvarh[4];			//电表清零前B相第四象限无功电能
		U8 cForward[4];			//电表清零前C相正向有功电能
		U8 cReverse[4];			//电表清零前C相反向有功电能
		U8 cQ1Kvarh[4];			//电表清零前C相第一象限无功电能
		U8 cQ2Kvarh[4];			//电表清零前C相第二象限无功电能
		U8 cQ3Kvarh[4];			//电表清零前C相第三象限无功电能
		U8 cQ4Kvarh[4];			//电表清零前C相第四象限无功电能

		U8 sta;
	}meterZero_t;
	
	typedef union {
		U8 buf[106];
		meterZero_t meterZero;
	}meterZero_u;


	//需量清零记录
	typedef struct {
		U8 timeStart[6];		//发生时刻
		U8 c[4];				//操作者代码

		U8 forwardKw[3]; 		//正向有功最大需量
		U8 forwardKwTime[5];		//发生时间
		U8 reverseKw[3];  		//反向有功最大需量
		U8 reverseKwhTime[5];	//发生时间
		U8 q1Kvar[3]; 			//第一象限无功最大需量
		U8 q1KvarTime[5];		//发生时间
		U8 q2Kvar[3];			//第二象限无功最大需量
		U8 q2KvarTime[5];		//发生时间
		U8 q3Kvar[3];			//第三象限无功最大需量
		U8 q3KvarTime[5];		//发生时间
		U8 q4Kvar[3];			//第四象限无功最大需量
		U8 q4KvarTime[5];		//发生时间
		U8 aForward[3];			//电表清零前A相正向最大需量
		U8 aForwardTime[5];		//发生时间
		U8 aReverse[3];			//电表清零前A相反向最大需量
		U8 aReverseTime[5];		//发生时间
		U8 aQ1Kvar[3];			//电表清零前A相第一象限无功最大需量
		U8 aQ1KvarTime[5];		//发生时间
		U8 aQ2Kvar[3];			//电表清零前A相第二象限无功最大需量
		U8 aQ2KvarTime[5];		//发生时间
		U8 aQ3Kvar[3];			//电表清零前A相第三象限无功最大需量
		U8 aQ3KvarTime[5];		//发生时间
		U8 aQ4Kvar[3];			//电表清零前A相第四象限无功最大需量
		U8 aQ4KvarTime[5];		//发生时间
		U8 bForward[3];			//电表清零前B相正向有功最大需量
		U8 bForwardTime[5];		//发生时间
		U8 bReverse[3];			//电表清零前B相反向有功最大需量
		U8 bReverseTime[5];		//发生时间
		U8 bQ1Kvar[3];			//电表清零前B相第一象限无功最大需量
		U8 bQ1KvarTime[5];		//发生时间
		U8 bQ2Kvar[3];			//电表清零前B相第二象限无功最大需量
		U8 bQ2KvarTime[5];		//发生时间
		U8 bQ3Kvar[3];			//电表清零前B相第三象限无功最大需量
		U8 bQ3KvarTime[5];		//发生时间
		U8 bQ4Kvar[3];			//电表清零前B相第四象限无功最大需量
		U8 bQ4KvarTime[5];		//发生时间
		U8 cForward[3];			//电表清零前C相正向有功最大需量
		U8 cForwardTime[5];		//发生时间
		U8 cReverse[3];			//电表清零前C相反向有功最大需量
		U8 cReverseTime[5];		//发生时间
		U8 cQ1Kvar[3];			//电表清零前C相第一象限无功最大需量
		U8 cQ1KvarTime[5];		//发生时间
		U8 cQ2Kvar[3];			//电表清零前C相第二象限无功最大需量
		U8 cQ2KvarTime[5];		//发生时间
		U8 cQ3Kvar[3];			//电表清零前C相第三象限无功最大需量
		U8 cQ3KvarTime[5];		//发生时间
		U8 cQ4Kvar[3];			//电表清零前C相第四象限无功最大需量
		U8 cQ4KvarTime[5];		//发生时间

		U8 sta;
	}demandZero_t;
	
	typedef union {
		U8 buf[202];
		demandZero_t demandZero;
	}demandZero_u;

	//事件清零记录
	typedef struct {
		U8 timeStart[6];		//发生时刻
		U8 c[4];				//操作者代码
		U8 di[4]; 				//事件清零标识码

		U8 sta;
	}eventZero_t;
	
	typedef union {
		U8 buf[24];
		eventZero_t eventZero;
	}eventZero_u;

	//校时记录
	typedef struct {
		U8 timeStart[6];		//发生时刻
		U8 c[4];				//操作者代码
		U8 di[4]; 				//事件清零标识码

		U8 sta;
	}adjTime_t;
	
	typedef union {
		U8 buf[16];
		adjTime_t adjTime;
	}adjTime_u;

	//时段表编程记录
	typedef struct {
		U8 timeStart[6];		//发生时刻
		U8 c[4];				//操作者代码
		U8 d[2][8][14][3]; 		//2套8日时段表

		U8 sta;
	}programHourTable_t;
	
	typedef union {
		U8 buf[682];
		programHourTable_t programHourTable;
	}programHourTable_u;

	//时区表编程记录
	typedef struct {
		U8 timeStart[6];		//发生时刻
		U8 c[4];				//操作者代码
		U8 d[2][14][3]; 		//2套14时区时区表

		U8 sta;
	}programDayTable_t;
	
	typedef union {
		U8 buf[94];
		programDayTable_t programDayTable;
	}programDayTable_u;

	//周休日编程记录
	typedef struct {
		U8 timeStart[6];		//发生时刻
		U8 c[4];				//操作者代码
		U8 num; 				//周休日编程前采用的日时段表号

		U8 sta;
	}programDay_t;
	
	typedef union {
		U8 buf[11];
		programDay_t programDay;
	}programDay_u;

	//节假日编程记录
	typedef struct {
		U8 timeStart[6];		//发生时刻
		U8 c[4];				//操作者代码
		U8 d[254][4]; 			//节假日编程前第1~254节假日数据

		U8 sta;
	}programHoliday_t;
	
	typedef union {
		U8 buf[1026];
		programHoliday_t programHoliday;
	}programHoliday_u;

	//有功组合方式编程记录
	typedef struct {
		U8 timeStart[6];		//发生时刻
		U8 c[4];				//操作者代码
		U8 pCombMode; 			//有功组合方式编程前的有功组合方式特征字

		U8 sta;
	}programPCombo_t;
	
	typedef union {
		U8 buf[11];
		programPCombo_t programPCombo;
	}programPCombo_u;

	//无功组合方式编程记录
	typedef struct {
		U8 timeStart[6];		//发生时刻
		U8 c[4];				//操作者代码
		U8 pCombMode; 			//无功组合方式编程前的有功组合方式特征字

		U8 sta;
	}programQCombo_t;
	
	typedef union {
		U8 buf[11];
		programQCombo_t programQCombo;
	}programQCombo_u;

	//结算日编程记录
	typedef struct {
		U8 timeStart[6];		//发生时刻
		U8 c[4];				//操作者代码
		U8 data[3][2]; 			//结算日编程前每月第1~3结算日数据

		U8 sta;
	}programSAD_t;
	
	typedef union {
		U8 buf[16];
		programSAD_t programSAD;
	}programSAD_u;

	//开表盖记录,适用于开尾盖
	typedef struct {
		U8 timeStart[6];		//发生时刻
		U8 timeEnd[6];			//结束时刻
		U8 c[4];				//操作者代码

		U8 forwardKwh[4]; 		//开盖前正向有功电能数据
		U8 reverseKwh[4];  		//开盖前反向有功电能数据
		U8 q1KvarhS[4]; 		//开盖前第一象限无功电能数据
		U8 q2KvarhS[4];			//开盖前第二象限无功电能数据
		U8 q3KvarhS[4];			//开盖前第三象限无功电能数据
		U8 q4KvarhS[4];			//开盖前第四象限无功电能数据

		U8 forwardKwhE[4]; 		//开盖后正向有功电能数据
		U8 reverseKwhE[4];  	//开盖后反向有功电能数据
		U8 q1KvarhE[4]; 		//开盖后第一象限无功电能数据
		U8 q2KvarhE[4];			//开盖后第二象限无功电能数据
		U8 q3KvarhE[4];			//开盖后第三象限无功电能数据
		U8 q4KvarhE[4];			//开盖后第四象限无功电能数据

		U8 sta;
	}openCover_t;
	
	typedef union {
		U8 buf[60];
		openCover_t openCover;
	}openCover_u;


	typedef struct {
		U8 DI2_01[3];
		U8 DI2_02[3];
		U8 DI2_03[3];
		U8 DI2_04[3];
		U8 DI2_0b[3];
		U8 DI2_0c[3];
		U8 DI2_0d[3];
		U8 DI2_0e[3];
		U8 DI2_0f[3];		  	//过载

		U8 DI2_05;			
		U8 DI2_06;			
		U8 DI2_07;			
		U8 DI2_08;			
		U8 DI2_09;			
		U8 DI2_0a;			//电流不平衡

		U8 DI3_10[3];   //失压
		U8 DI3_11[3];	//欠压
		U8 DI3_12[3];	//过压
		U8 DI3_13[3];	//断相
		U8 DI3_18[3];
		U8 DI3_19[3];
		U8 DI3_1a[3];
		U8 DI3_1b[3];
		U8 DI3_1c[3];	

		U8 DI3_14;			
		U8 DI3_15;
		U8 DI3_16;
		U8 DI3_17;
		U8 DI3_1f;	
	}saveEventSecond_u;

	extern saveEventSecond_u saveSecend;				//保存事件记录中未累加的秒值

	extern U8 creatVoltage(U8 phase,U8 time,U8 fun,U8 *clock);//失压、欠压、过压、断相记录
	extern U8 change_event_id(U8* di,U8 phase,U8 fun,U8 num,U8 mode);//转换循环存储位置，指向所要的下一个
	extern U8 save_event_num_time(U8 phase,U8 *event,U8 fun);
	extern U8 creatCurrent(U8 phase,U8 time,U8 fun,U8 *clock);
	extern U8 creatAllLost(U8 time);
	extern U8 creatImbalance(U8 time,U8 fun,U32 imblance);
	extern U8 creatPower(U8 phase,U8 time,U8 fun,U8 *clock);
	extern U8 creatPowerDown(U8 time,U8 fun,U8 *clock);
	extern U8 creatEventZero(U8 id0,U8 id1,U8 id2,U8 id3);


	extern programHourTable_u* programHourTableRec; 	//时段表编程记录
	extern programDayTable_u* programDayTableRec;		//时区表编程记录




	extern void initProgramm(void);
	extern U8 creatMeterZero(void);
	extern U8 creatDemandZero(void);
	
	extern U8 creatAdjTimeZero(U8 *later_time,U8 *after_time);
	extern U8 programDayInit(void);
	extern U8 programHolidayInit(void);//有问题
	extern U8 programPComboInit(void);
	extern U8 programQCombo1Init(void);
	extern U8 programQCombo2Init(void);
	extern U8 openTopCoverInit(U8 time);
	extern U8 openTailCoverInit(U8 time);
	extern U8 programSADInit(void);
	extern U8 creatConverse(U8 time,U8 fun);
	extern void eventRecoder(void);
	extern void programHourTableInit(void);
	extern void programDayTableInit(void);

	extern U8 state_creat_voltage(U8 phase,U8 time,U8 fun,U8 *clock);
	extern U8 state_power_factor_down(U8 fun,U8 phase,U8 time,U8 *clock);
	extern U8 state_open_close_gate(U8 fun,U32 handle_num,U8 *clock);
	extern U8 state_creat_imbalance(U8 time,U8 fun,U32 imbalance,U8 *clock);
	extern U8 state_creat_power(U8 phase,U8 time,U8 fun,U8 *clock);
	extern U8 state_creat_current(U8 phase,U8 time,U8 fun,U8 *clock);

	extern U8 state_save_event_num_time(U8 phase,U8 *event,U8 fun);
	extern U8 state_change_event_id(U8* di,U8 phase,U8 fun,U8 num,U8 mode);
#endif 

