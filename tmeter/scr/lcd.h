/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __LCD_H__
#define __LCD_H__
//	#define PRI_LcdDisplay  2

	#define PCF8576 0x70				//器件地址
	#define FIRSTSUBADDR	0x00		//第一片子地址
	#define SECONDSUBADDR	0x01		//第二片子地址

/*******************************************************************************************/	
	//pcf8576操作命令定义
	//本例未使用“显示存储器分区 ” 和 “闪烁”命令本文件不作定义
	//pcf8576工作方式定义
	typedef struct {
		U8 m0:1;					  	//显示驱动方式   背电极               M1   M0
		U8 m1:1;						//静态            BP0                 0    1
										//1：2            BP0，BP1            1    0
										//1：3            BP0，BP1，BP2       1    1
										//1：4            BP0，BP1，BP2，BP3  0    0
		
		U8 b:1;							//LCD驱动偏压设置  位B
										//1/3偏压          0
										//1/2偏压          1


		U8 e:1;							//显示状态设置    位E
										//关显示（空白）   0
										//开显示           1


		U8 bit4:1;						//未使用
		U8 bit5:1;						//const 0
		U8 bit6:1;						//const 1
		U8 c:1;							//C=0 表示为最后一个命令字节
										//C=1 表示下一个数据仍是命令字节
	}mode;

	typedef union {
		U8 u8Mode;
		mode bitMode;
	}Pcf8576Mode;

	//pcf8576地址指针设置
	typedef struct {
		U8 pointer:6;					//数据指针，对应段输出S0——S39的某一段
		U8 bit6:1;						//const 0
		U8 c:1;							//C=0 表示为最后一个命令字节
										//C=1 表示下一个数据仍是命令字节
	}dataPointer;

	typedef union {
		U8 u8DataPointer;
		dataPointer bitDataPointer;
	}Pcf8576DataPointer;

	//pcf8576器件选择
	typedef struct {
		U8 subAddr:3;					//3位立即数（位A2到A0）
		U8 settled:4;					//固定为 1100
		U8 c:1;							//C=0 表示为最后一个命令字节
										//C=1 表示下一个数据仍是命令字节
	}devicAddr;

	typedef union {
		U8 u8DevicAddr;
		devicAddr bitDevicAddr;
	}Pcf8576DevicAddr;

/*******************************************************************************************/	
	//pcf8576显示存储器数据缓冲区定义
	typedef struct {
		U8 bit0:1;					
		U8 bit1:1;					
		U8 bit2:1;					
		U8 bit3:1;					
		U8 bit4:1;					
		U8 bit5:1;					
		U8 bit6:1;					
		U8 bit7:1;					
									
	}dataU8; 							//一个字节的数据格式定义，包含两个段

 	typedef union {
		dataU8 dataBuf;					//8位
		U8 data;
	}DataBuf;

	typedef struct {
		DataBuf buf0[20];				//第一片的数据缓冲区 160bit = 40X4，对应液晶的下半部分和上半部分的前八段	
		DataBuf buf1[20];				//第二片的数据缓冲区 160bit = 40X4，对应液晶的上半部分的其余段
									
	}pcf8576Data; 	
/*******************************************************************************************/	
	//一个字段对应的数据结构   用于存储lcd真值表的数据格式
	typedef struct {
		char str[6];					//液晶上显示的字符
		U8	sn;							//段号
		U8  bit; 						//位号
	
	}iconConfig;

	extern __task void LcdDisplay (void);
	extern OS_TID TID_Lcd;
	
	typedef struct {
		U8 display_mode;	//显示模式     0 关显示 1循显  2键显
		U8	display_num;	//显示的屏号
		U8  cyc_time;		//循环显示间隔时间
		U8  cyc_period;		//循显周期数
		U32 display_time; 	//显示时间

		U32 dataID;
		U32 ID_num;
		U8 key_display_num;  //键显总屏数
		U8 cyc_display_num;	 //循显总屏数
	}lcd_control_t;


extern lcd_control_t  lcd_control;//显示控制
						
extern void check_display_item(U8 mode);
extern void lcd_all_clear(void);
extern void init_lcd(void);
				
#endif

