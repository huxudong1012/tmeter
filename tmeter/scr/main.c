/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#include <stdio.h>                    /* standard I/O .h-file                */
#include <ctype.h>                    /* character functions                 */
#include <string.h>                   /* string and memory functions         */
#include <RTL.h>                      /* RTX kernel functions & defines      */
#include <LPC17xx.H>                  /* LPC17xx definitions                 */

#include "meter_output.h"
#include "lpc_timer.h"
#include "uart_task.h"
#include "lpc_uart.h"
#include "ade7878.h"
#include "LPC_I2C.h"
#include "lcd.h"
#include "eeprom.h"
#include "input.h"
#include "demand.h"
#include "power_check.h"
#include "time_period_zone.h"
#include "voltage_check.h"
#include "load.h"
#include "firmware.h"
#include "lpc_pwm.h"
#include "err.h"
#include "lpc_spi.h"
/*****************************************************************************/

#if HAVE_ESAM == 1
	#include "esam_card.c"
#endif


static U64 uart_task_stack[3200/8];
static U64 event_task_stack[3200/8];

OS_TID uart_task;//串口数据处理任务ID


OS_TID TID_Lcd;	  //lcd显示及其实时时钟读取任务ID

OS_TID ade7878_task;//ade7878 处理任务ID

OS_TID TID_input;

OS_TID power_task;

OS_TID esam_task;

OS_MUT eeprom_handle;

/*** System 1ms timeTicks at run-time. ***/
U32 TimeTick; /*系统定时器 1ms由定时器1产生*/
/*******************************************************************/

extern void init_eeprom(void); 		//初始化eeprom

/*********************************************************************************************************
** 函数名称: former_task_init
** 功能描述: 本部分初始化需要任务运行起来之前执行
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void former_task_init(void){

	Init_Meter_Output();

	init_err_check();

	init_timer0(); //红外38K

	init_timer1(1000);
	enable_timer1(); //1ms 定时

	Init_spi();					//初始化spi

#if HAVE_ESAM == 1
	init_pwm1();//esam时钟信号
	init_timer2();//esam 一字节数据接收
	init_esam();
#endif
	
	I2C_Init (I2C0,I2C_MASTER,400000,0x10);
	initWP();				  //eeprom i2c0初始化
										  
	os_mut_init(eeprom_handle);	   //eeprom用互斥量初始化 
	os_mut_release(eeprom_handle); 
	
	init_eeprom();	   //eeprom 配置初始化

	important_parametrer_read();//重要数据读取	

	init_load();  //负荷初始化
}

/*********************************************************************************************************
** 函数名称: after_task_init
** 功能描述: 本部分初始化需要任务运行起来之后执行
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void after_task_init(void){
#if HAVE_ESAM == 1
	U8 databuf[8];
	reset_esam(databuf);//复位esam
	init_account_info();
#endif
}

/*********************************************************************************************************
** 函数名称: init
** 功能描述: 初始化任务
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 run_1000ms = 0;
__task void init (void) {																				 	
	


	former_task_init();
	
	ade7878_task = os_tsk_create (ade7878_process,6); //ade7878 irq0处理任务
	power_task = os_tsk_create (power_process,7); //掉电处理
	run_1000ms = 1;
 	os_dly_wait (80);
	run_1000ms = 0;
	uart_task = os_tsk_create_user(process_uart_protocol,3,&uart_task_stack,sizeof(uart_task_stack)); //串口数据处理

	TID_input = os_tsk_create (Input,2);//按键输入    10ms级任务

	TID_Lcd = os_tsk_create (LcdDisplay,2);//显示    100ms级任务

	os_tsk_create_user(check_voltage_task,2,&event_task_stack,sizeof(event_task_stack)); //电池电压采样	 1s级任务

	os_tsk_create (zone_period_check,2); //时区时段管理 10s级任务

#if HAVE_ESAM == 1
	esam_task = os_tsk_create (esam_one_frame_process,8); //esam一帧数据处理  根据标志进行一帧数据发送，接收 或延时几个etu
#endif
   	
	after_task_init();//任务启动之后需要初始化的事件

	for(;;)
	{
		meter_output();
		Uart_DataTimeout();
		start_ade7878_samp();
#if HAVE_ESAM == 1
		check_authen_time();
#endif
	}

}
/*********************************************************************************************************
** 函数名称: main
** 功能描述: c语言的主函数，由它启动多任务环境
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
int main (void) {                         /* program execution starts here   */

	
	SystemInit();                           /* initialize clocks               */

	os_sys_init (init);                     /* init and start with task 'INIT' */
}


/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
