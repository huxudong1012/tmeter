/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __ADE7878_H
#define __ADE7878_H


//三个任务
#define EVT_ADE7878_IRQ0		  	0x0001//ade7878 中断0
#define EVT_ADE7878_IRQ1		  	0x0002//ade7878 中断1
#define EVT_ADE7878_SAMP        0x0004//ade7878 定时采样

//ade7878复位控制
#define ADE7878_RST_SEL    (LPC_PINCON->PINSEL0 &= ~((3<< 2))) // 选择p0.1为GPIO
#define ADE7878_RST_DIR    (LPC_GPIO0->FIODIR |= (1<<1)) // 选择p0.1为输出
#define ADE7878_RST(x)	   (x?(LPC_GPIO0->FIOSET |= (1<<1)):(LPC_GPIO0->FIOCLR |= (1<<1)))//设置p0.1为高或低
//ade7878模式控制
#define ADE7878_PM0_SEL    (LPC_PINCON->PINSEL0 &= ~((3<< 20))) // 选择p0.10为GPIO
#define ADE7878_PM0_DIR    (LPC_GPIO0->FIODIR |= (1<<10)) // 选择p0.10为输出
#define ADE7878_PM0(x)	   (x?(LPC_GPIO0->FIOSET |= (1<<10)):(LPC_GPIO0->FIOCLR |= (1<<10)))//设置p0.10为高或低

#define ADE7878_PM1_SEL    (LPC_PINCON->PINSEL0 &= ~((3<< 22))) // 选择p0.11为GPIO
#define ADE7878_PM1_DIR    (LPC_GPIO0->FIODIR |= (1<<11)) // 选择p0.11为输出
#define ADE7878_PM1(x)	   (x?(LPC_GPIO0->FIOSET |= (1<<11)):(LPC_GPIO0->FIOCLR |= (1<<11)))//设置p0.11为高或低

#define MAX_ENERGY_TYPE 11
#define MAX_PHASE_ENERGY_TYPE 10

#define A_PHASE_ENERGY_ADDR 0x15
#define B_PHASE_ENERGY_ADDR 0x29
#define C_PHASE_ENERGY_ADDR 0x3d

#define ENERGY_PULSE_ADDR 0xff00	//掉电保存能量地址

typedef struct {
	//已a相参考
	U8 flag;	       //标志 0 电压逆相序  1 电流逆相序
	U16 b_voltage;      //b相电压相对夹角
	U16 c_voltage;      //c相电压相对夹角
	U16 b_current;	   //b相电流相对夹角
	U16 c_current;	   //c相电流相对夹角
	
}alternate_with_information_t;

typedef enum {
	combination_active = 0,
	positive_active,		
	negetive_active,		
	combination_reactive1,		
	combination_reactive2,	    
	first_quadrant_reactive,		
	second_quadrant_reactive,		
	third_quadrant_reactive,
	fourth_quadrant_reactive,
	positive_apparent,
	negetive_apparent	
}energy_type;

typedef enum {
	phase_positive_active = 0,		
	phase_negetive_active,		
	phase_combination_reactive1,		
	phase_combination_reactive2,	    
	phase_first_quadrant_reactive,		
	phase_second_quadrant_reactive,		
	phase_third_quadrant_reactive,
	phase_fourth_quadrant_reactive,
	phase_positive_apparent,
	phase_negetive_apparent	
}phase_energy_type;

typedef struct {

	U32 energy_data;	    //能量数据
	U16 pulse;             //脉冲数 能量方向
}energy_data_t;

typedef struct {
	U8 active_comb;
	U8 reactive_comb1;
	U8 reactive_comb2;
	energy_data_t total_energy[MAX_ENERGY_TYPE];
	energy_data_t moment_energy[MAX_ENERGY_TYPE];
	energy_data_t a_phase_energy[MAX_PHASE_ENERGY_TYPE];
	energy_data_t b_phase_energy[MAX_PHASE_ENERGY_TYPE];
	energy_data_t c_phase_energy[MAX_PHASE_ENERGY_TYPE];
	U16 total_energy_flag;
	U16 moment_energy_flag;
	U16 a_phase_energy_flag;
	U16 b_phase_energy_flag;
	U16 c_phase_energy_flag;

	//安时
	U32 positive_AH;
	U32 negetive_AH;
		
}energy_info_t;		  //能量相关数据结构体


typedef struct {

	U32 voltage;	       //电压
	U32 current;           //电流
	U32 power_factor;      //功率因数  总高位为符号位
	U32 active_power;	   //有功功率  总高位为符号位
	U32 reactive_power;	   //无功功率  总高位为符号位
	U32 apparent_power;	   //视在功率
	U16 angle;             //相角
	U16 voltage_mul;       //电压调整倍数，步进10
	U32 current_offset;	   //电流偏置

	//安时
	U32 positive_AH;
	U32 negetive_AH;
}phase_information_t;

typedef struct {

	U32 power_factor;      //功率因数  总高位为符号位
	U32 active_power;	   //有功功率  总高位为符号位
	U32 reactive_power;	   //无功功率  总高位为符号位
	U32 apparent_power;	   //无功功率

}sum_information_t;

extern phase_information_t  a_phase_info,b_phase_info,c_phase_info;
extern energy_info_t  energy_info;
extern sum_information_t sum_info;
extern U16 period_line;
extern U8 meter_clear;
extern alternate_with_information_t alternate_with_info;
extern U8 Ade7878_Init(void);

extern __task void ade7878_process(void);
extern void start_ade7878_samp(void);

extern U8 Ade7878_ReadData(U16 addr,U32 *data,U8 datalong);
extern U8 Ade7878_WriteData(U16 addr,U32 data,U8 datalong);
extern U8 read_one_energy_info(U8 num,U8 fee,U8 type,U8 *data,U8 *flag);
extern U8 read_energy(U8 type);
extern U8 save_energy(U8 type);
extern U8 save_moment_energy(U8 fee);
extern U8 save_energy_data(void);

extern U32 sign_calculate(U32 data1,U32 data2,U32 data3);

extern U8 chech_combination_active_mode(U8 mode);
extern U8 chech_combination_reactive_mode(U8 mode);
extern U8 modify_energy_data(void);

//ade7878 寄存器列表
#define AIGAIN  0x4380  //A相电流增益调整
#define AVGAIN  0x4381  //A相电压增益调整
#define BIGAIN  0x4382  //B相电流增益调整
#define BVGAIN  0x4383  //B相电压增益调整
#define CIGAIN  0x4384  //C相电流增益调整
#define CVGAIN  0x4385  //C相电压增益调整

#define NIGAIN  0x4386  //中线电流增益调整   ？

#define AIRMSOS 0x4387  //A相电流均方根偏置
#define AVRMSOS 0x4388  //A相电流均方根偏置
#define BIRMSOS 0x4389  //B相电流均方根偏置
#define BVRMSOS 0x438A  //B相电流均方根偏置
#define CIRMSOS 0x438B  //C相电流均方根偏置
#define CVRMSOS 0x438C  //C相电流均方根偏置

#define NIRMSOS  0x438D  //中线电流均方根偏置

#define AVAGAIN 0x438E  //A相视在功率增益调整   
#define BVAGAIN 0x438F  //B相视在功率增益调整
#define CVAGAIN 0x4390  //C相视在功率增益调整

#define AWGAIN  0x4391  //A相总有功增益调整
#define AWATTOS  0x4392  //A相总有功偏置调整
#define BWGAIN  0x4393  //B相总有功增益调整
#define BWATTOS  0x4394  //B相总有功偏置调整
#define CWGAIN  0x4395  //C相总有功增益调整
#define CWATTOS  0x4396  //C相总有功偏置调整

#define AVARGAIN  0x4397  //A相总无功增益调整
#define AVAROS	 0x4398	  //A相总无功偏置调整
#define BVARGAIN  0x4399  //B相总无功增益调整
#define BVAROS	 0x439A	  //B相总无功偏置调整
#define CVARGAIN  0x439B  //C相总无功增益调整
#define CVAROS	 0x439C	  //C相总无功偏置调整

#define AFWGAIN  0x439D  //A相基波有功增益调整
#define AFWATTOS  0x439E  //A相基波有功偏置调整
#define BFWGAIN  0x439F  //B相基波有功增益调整
#define BFWATTOS  0x43A0  //B相基波有功偏置调整
#define CFWGAIN  0x43A1  //C相基波有功增益调整
#define CFWATTOS  0x43A2  //C相基波有功偏置调整

#define AFVARGAIN  0x43A3  //A相基波无功增益调整
#define AFVAROS	 0x43A4	  //A相基波无功偏置调整
#define BFVARGAIN  0x43A5  //B相基波无功增益调整
#define BFVAROS	 0x43A6	  //B相基波无功偏置调整
#define CFVARGAIN  0x43A7  //C相基波无功增益调整
#define CFVAROS	 0x43A8	  //C相基波无功偏置调整

#define VATHR1	 0x43A9	  //视在功率阀值高24位
#define VATHR0	 0x43AA	  //视在功率阀值低24位

#define WTHR1	 0x43AB	  //总、基波有功阀值高24位
#define WTHR0	 0x43AC	  //总、基波有功阀值低24位

#define VARTHR1	 0x43AD	  //总、基波无功阀值高24位
#define VARTHR0	 0x43AE	  //总、基波无功阀值低24位

#define VANOLOAD 0x43B0   //视在功率无负载阀值
#define APNOLOAD 0x43B1   //总、基波有功无负载阀值
#define VARNOLOAD 0x43B2   //总、基波无功无负载阀值
#define VLEVEL 0x43B3   //计算基波有功无功

#define DICOEFF 0x43B5   //数字积分控制
#define HPFDIS 0x43B6   //电流高筒滤波器控制

#define ISUMLVL 0x43B8   //中性线与相电流比较阀值

#define ISUM 0x43BF    //ABC 相电流瞬时值和

#define AIRMS 0x43C0    //A相电流均方根
#define AVRMS 0x43C1    //A相电压均方根
#define BIRMS 0x43C2    //B相电流均方根
#define BVRMS 0x43C3    //B相电压均方根
#define CIRMS 0x43C4    //C相电流均方根
#define CVRMS 0x43C5    //C相电压均方根

#define NIRMS 0x43C5    //中线电流均方根

#define RUN 0xE228     //DSP运行控制

#define AWATTHR 0xE400  //a相总有功电能累计
#define BWATTHR 0xE401  //b相总有功电能累计
#define CWATTHR 0xE402  //c相总有功电能累计

#define AFWATTHR 0xE403  //a相基波有功电能累计
#define BFWATTHR 0xE404  //b相基波有功电能累计
#define CFWATTHR 0xE405  //c相基波有功电能累计

#define AVARHR  0xE406 //a相总无功电能累计
#define BVARHR  0xE407 //b相总无功电能累计
#define CVARHR  0xE408 //c相总无功电能累计

#define AFVARHR  0xE409 //a相基波无功电能累计
#define BFVARHR  0xE40A //b相基波无功电能累计
#define CFVARHR  0xE40B //c相基波无功电能累计

#define AVAHR 0xE40C  //a相视在电能累计
#define BVAHR 0xE40D  //b相视在电能累计
#define CVAHR 0xE40E  //c相视在电能累计

#define IPEAK 0xE500  //电流尖峰寄存器
#define VPEAK 0xE501  //电压尖峰寄存器

#define STATUS0 0xE502  //中断状态寄存器0
#define STATUS1 0xE503  //中断状态寄存器1

#define AIMAV 0xE504   //a相电流绝对平均值
#define BIMAV 0xE505   //b相电流绝对平均值
#define CIMAV 0xE506   //c相电流绝对平均值

#define OILVL 0xE507   //过电流阀值
#define OVLVL 0xE508   //过电压阀值
#define SAGLVL 0xE509   //电压SAG阀值

#define MASK0 0xE50A   //中断使能寄存器0
#define MASK1 0xE50B   //中断使能寄存器0

#define IAWV 0xE50C   //a相电流瞬时值
#define IBWV 0xE50D   //b相电流瞬时值
#define ICWV 0xE50E   //c相电流瞬时值
#define INWV 0xE50F   //中线电流瞬时值

#define VAWV 0xE510   //a相电压瞬时值
#define VBWV 0xE511   //b相电压瞬时值
#define VCWV 0xE512   //c相电压瞬时值

#define AWATT 0xE513   //a相总有功电能瞬时值
#define BWATT 0xE514   //b相总有功电能瞬时值
#define CWATT 0xE515   //c相总有功电能瞬时值

#define AVAR 0xE516   //a相总无功电能瞬时值
#define BVAR 0xE517   //b相总无功电能瞬时值
#define CVAR 0xE518   //c相总无功电能瞬时值

#define AVA 0xE519   //a相视在电能瞬时值
#define BVA 0xE51A   //b相视在电能瞬时值
#define CVA 0xE51B   //c相视在电能瞬时值

#define CHECKSUM 0xE51F //检查和寄存器
#define VNOM 0xE520 //相电压均方根

#define PHSTATUS 0xE600  //相尖峰寄存器
#define ANGLE0 0xE601  //时间延时0
#define ANGLE1 0xE602  //时间延时1
#define ANGLE2 0xE603  //时间延时2

#define PERIOD 0xE607 //网络周期
#define PHNOLOAD 0xE608 //相无负载寄存器
#define LINECYC	 0xE60C //线周期累计模式计数器
#define ZXTOUT	 0xE60D //过零超时计数器
#define COMPMODE 0xE60E //计算模式寄存器

#define GAIN 	0xE60F //PGA增益寄存器
#define CFMODE 0xE610  //CF模式寄存器
#define CF1DEN 0xE611 //CF1分母
#define CF2DEN 0xE612 //CF2分母
#define CF3DEN 0xE613 //CF3分母

#define APHCAL 0xE614 //a相相位校准寄存器
#define BPHCAL 0xE615 //b相相位校准寄存器
#define CPHCAL 0xE616 //c相相位校准寄存器

#define PHSIGN 0xE617 //功率符号
#define CONFIG  0xE618 //ade7878配置寄存器

#define MMODE   0xE700 //计量模式
#define ACCMODE   0xE701 //累加模式
#define LCYCMODE 0xE702		 //线性累加行为

#define PEAKCYC	0xE703 //尖峰检测半周期
#define SAGCYC	0xE704 //SAG检测半周期
#define CFCYC	0xE705 //CF周期
#define HSDC_CFG  0xE706 //HSDC配置
#define VERSION  0xE707 //die版本

#define LPOILVL 0xEC00  //模式2过电流阀值
#define CONFIG2 0xEC01 //模式1配置寄存器

extern U8 combination_reactive_calculate(U8 mode,U32 data1,U32 data2,U32 data3,U32 data4,U32 *result);
extern U8 combination_active_calculate(U32 data1,U32 data2,U32 *result);

#endif //__ADE7878_H

