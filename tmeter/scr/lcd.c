/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#include <RTL.h>                      /* RTX kernel functions & defines      */
#include <stdio.h>                    
#include <string.h>                    
#include <LPC17xx.H>                  /* LPC17xx definitions                 */
#include "lcd.h"
#include "LPC_I2C.h"
#include "RX8025T.h"
#include "meter_output.h"
#include "eeprom.h"
#include "ade7878.h"
#include "demand.h" 
#include "time_period_zone.h"
#include "power_check.h"
#include "voltage_check.h"
#include "uart_task.h"
#include "err.h"
#include "firmware.h"
#include "lpc_uart.h"
#include "balance.h"
#include "input.h"


extern OS_TID TID_Lcd;

//pcf8576显示存储器数据缓冲区					
pcf8576Data pcf8576DataBuf,pcf8576DataBufGlitter;				
//显示控制
lcd_control_t  lcd_control;
/*********************************************************************************************************
** 函数名称: lcd_all_show_
** 功能描述: lcd全显
** 输　入: 	 无
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void lcd_all_show(void){
	U8 i;
	for(i = 0;i<20;i++){
		pcf8576DataBuf.buf0[i].data = 0xff;
		pcf8576DataBuf.buf1[i].data = 0xff;
		
		pcf8576DataBufGlitter.buf0[i].data = 0xff;
		pcf8576DataBufGlitter.buf1[i].data = 0xff;	
	}
	
}
/*********************************************************************************************************
** 函数名称: lcd_all_clear
** 功能描述: lcd全显
** 输　入: 	 无
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void lcd_all_clear(void){
	U8 i;
	for(i = 0;i<20;i++){
		pcf8576DataBuf.buf0[i].data = 0;
		pcf8576DataBuf.buf1[i].data = 0;
		
		pcf8576DataBufGlitter.buf0[i].data = 0;
		pcf8576DataBufGlitter.buf1[i].data = 0;	
	}
	
}
/*********************************************************************************************************
** 函数名称: initPcf8576
** 功能描述: 初始化pcf8576
** 输　入: 	无
** 输　出:  U8 0 —— 失败 1 —— 成功
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 initPcf8576(void){
	Pcf8576Mode mode;
	U8 re = 0;
	U8 done = 0;

	lcd_all_show();
	
	mode.bitMode.m0 = 0;		  		//设置为1：4            BP0，BP1，BP2，BP3
	mode.bitMode.m1 = 0;
	mode.bitMode.b = 0;					//1/3偏压
	mode.bitMode.e = 1;					//开显示
	mode.bitMode.bit4 = 0;
	mode.bitMode.bit5 = 0;
	mode.bitMode.bit6 = 1;
	mode.bitMode.c = 1;					//最后一个命令字节

//	re = I2C_MasterWrite (I2C1,PCF8576,&(mode.u8Mode),1);
//	if(!re) return 1;	 	//设定工作模式成功
	while(re < 5){
		if(I2C_MasterWrite (I2C1,PCF8576,&(mode.u8Mode),1) == 0) re++;
		done++;
		if(done > 10) return 0;
	}

	return 1;	 	//设定工作模式成功
}


/*********************************************************************************************************
** 函数名称: update8576Buffer
** 功能描述: 根据要显示的内容，计算显示缓冲区的数据
** 输　入: 	const iconConfig icon ——　要显示的液晶段
**			U8 fun —— 显示方式  0：正常显示   
**								1：闪烁 
**								2：不显示 
** 输　出:  U8 0:失败 1：成功
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 update8576Buffer(const iconConfig icon,U8 fun){
	U8 s;
	if(icon.sn > 64) return 0;

	if(icon.sn <= 39 ){			// 使用第一片8576驱动
		s = icon.sn/2;			//字节获取缓冲区下标
		switch (icon.bit)
		{
			case 0:
				if(fun == 0)	{	   //正常显示
					pcf8576DataBuf.buf0[s].dataBuf.bit0 = 1;		//置位要显示的符号相应的位
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit0 = 1;		//置位要显示的符号相应的位
				}
				if(fun == 1)	{	   //闪烁
					pcf8576DataBuf.buf0[s].dataBuf.bit0 = 1;		//置位要显示的符号相应的位
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit0 = 0;		//置位要显示的符号相应的位
				}
				if(fun == 2)	{	   //不显示
					pcf8576DataBuf.buf0[s].dataBuf.bit0 = 0;		//置位要显示的符号相应的位
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit0 = 0;		//置位要显示的符号相应的位
				}
				
				break;
			case 1:
				if(fun == 0)	{	   //正常显示
					pcf8576DataBuf.buf0[s].dataBuf.bit1 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit1 = 1;		//置位要显示的符号相应的位
				}
				if(fun == 1)	{	   //闪烁
					pcf8576DataBuf.buf0[s].dataBuf.bit1 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit1 = 0;		//置位要显示的符号相应的位
				}
				if(fun == 2)	{	   //不显示
					pcf8576DataBuf.buf0[s].dataBuf.bit1 = 0;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit1 = 0;		//置位要显示的符号相应的位
				}
				break;
			case 2:
				if(fun == 0)	{	   //正常显示
					pcf8576DataBuf.buf0[s].dataBuf.bit2 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit2 = 1;		//置位要显示的符号相应的位
				}
				if(fun == 1)	{	   //闪烁
					pcf8576DataBuf.buf0[s].dataBuf.bit2 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit2 = 0;		//置位要显示的符号相应的位
				}
				if(fun == 2)	{	   //不显示
					pcf8576DataBuf.buf0[s].dataBuf.bit2 = 0;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit2 = 0;		//置位要显示的符号相应的位
				}
				break;
			case 3:
				if(fun == 0)	{	   //正常显示
					pcf8576DataBuf.buf0[s].dataBuf.bit3 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit3 = 1;		//置位要显示的符号相应的位
				}
				if(fun == 1)	{	   //闪烁
					pcf8576DataBuf.buf0[s].dataBuf.bit3 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit3 = 0;		//置位要显示的符号相应的位
				}
				if(fun == 2)	{	   //不显示
					pcf8576DataBuf.buf0[s].dataBuf.bit3 = 0;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit3 = 0;		//置位要显示的符号相应的位
				}
				break;
			case 4:
				if(fun == 0)	{	   //正常显示
					pcf8576DataBuf.buf0[s].dataBuf.bit4 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit4 = 1;		//置位要显示的符号相应的位
				}
				if(fun == 1)	{	   //闪烁
					pcf8576DataBuf.buf0[s].dataBuf.bit4 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit4 = 0;		//置位要显示的符号相应的位
				}
				if(fun == 2)	{	   //不显示
					pcf8576DataBuf.buf0[s].dataBuf.bit4 = 0;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit4 = 0;		//置位要显示的符号相应的位
				}
				break;
			case 5:
				if(fun == 0)	{	   //正常显示
					pcf8576DataBuf.buf0[s].dataBuf.bit5 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit5 = 1;		//置位要显示的符号相应的位
				}
				if(fun == 1)	{	   //闪烁
					pcf8576DataBuf.buf0[s].dataBuf.bit5 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit5 = 0;		//置位要显示的符号相应的位
				}
				if(fun == 2)	{	   //不显示
					pcf8576DataBuf.buf0[s].dataBuf.bit5 = 0;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit5 = 0;		//置位要显示的符号相应的位
				}
				break;
			case 6:
				if(fun == 0)	{	   //正常显示
					pcf8576DataBuf.buf0[s].dataBuf.bit6 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit6 = 1;		//置位要显示的符号相应的位
				}
				if(fun == 1)	{	   //闪烁
					pcf8576DataBuf.buf0[s].dataBuf.bit6 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit6 = 0;		//置位要显示的符号相应的位
				}
				if(fun == 2)	{	   //不显示
					pcf8576DataBuf.buf0[s].dataBuf.bit6 = 0;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit6 = 0;		//置位要显示的符号相应的位
				}
				break;
			case 7:
				if(fun == 0)	{	   //正常显示
					pcf8576DataBuf.buf0[s].dataBuf.bit7 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit7 = 1;		//置位要显示的符号相应的位
				}
				if(fun == 1)	{	   //闪烁
					pcf8576DataBuf.buf0[s].dataBuf.bit7 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit7 = 0;		//置位要显示的符号相应的位
				}
				if(fun == 2)	{	   //不显示
					pcf8576DataBuf.buf0[s].dataBuf.bit7 = 0;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf0[s].dataBuf.bit7 = 0;		//置位要显示的符号相应的位
				}
				break;
			default:
				break;
		}
	}else if(icon.sn > 39 && icon.sn <= 64){	//使用第二片8576驱动
		s = (icon.sn - 40)/2;			//字节获取缓冲区下标
		switch (icon.bit)
		{
			case 0:
				if(fun == 0)	{	   //正常显示
					pcf8576DataBuf.buf1[s].dataBuf.bit0 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit0 = 1;		//置位要显示的符号相应的位
				}
				if(fun == 1)	{	   //闪烁
					pcf8576DataBuf.buf1[s].dataBuf.bit0 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit0 = 0;		//置位要显示的符号相应的位
				}
				if(fun == 2)	{	   //不显示
					pcf8576DataBuf.buf1[s].dataBuf.bit0 = 0;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit0 = 0;		//置位要显示的符号相应的位
				}
				break;
			case 1:
				if(fun == 0)	{	   //正常显示
					pcf8576DataBuf.buf1[s].dataBuf.bit1 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit1 = 1;		//置位要显示的符号相应的位
				}
				if(fun == 1)	{	   //闪烁
					pcf8576DataBuf.buf1[s].dataBuf.bit1 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit1 = 0;		//置位要显示的符号相应的位
				}
				if(fun == 2)	{	   //不显示
					pcf8576DataBuf.buf1[s].dataBuf.bit1 = 0;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit1 = 0;		//置位要显示的符号相应的位
				}
				break;
			case 2:
				if(fun == 0)	{	   //正常显示
					pcf8576DataBuf.buf1[s].dataBuf.bit2 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit2 = 1;		//置位要显示的符号相应的位
				}
				if(fun == 1)	{	   //闪烁
					pcf8576DataBuf.buf1[s].dataBuf.bit2 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit2 = 0;		//置位要显示的符号相应的位
				}
				if(fun == 2)	{	   //不显示
					pcf8576DataBuf.buf1[s].dataBuf.bit2 = 0;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit2 = 0;		//置位要显示的符号相应的位
				}
				break;
			case 3:
				if(fun == 0)	{	   //正常显示
					pcf8576DataBuf.buf1[s].dataBuf.bit3 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit3 = 1;		//置位要显示的符号相应的位
				}
				if(fun == 1)	{	   //闪烁
					pcf8576DataBuf.buf1[s].dataBuf.bit3 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit3 = 0;		//置位要显示的符号相应的位
				}
				if(fun == 2)	{	   //不显示
					pcf8576DataBuf.buf1[s].dataBuf.bit3 = 0;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit3 = 0;		//置位要显示的符号相应的位
				}
				break;
			case 4:
				if(fun == 0)	{	   //正常显示
					pcf8576DataBuf.buf1[s].dataBuf.bit4 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit4 = 1;		//置位要显示的符号相应的位
				}
				if(fun == 1)	{	   //闪烁
					pcf8576DataBuf.buf1[s].dataBuf.bit4 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit4 = 0;		//置位要显示的符号相应的位
				}
				if(fun == 2)	{	   //不显示
					pcf8576DataBuf.buf1[s].dataBuf.bit4 = 0;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit4 = 0;		//置位要显示的符号相应的位
				}
				break;
			case 5:
				if(fun == 0)	{	   //正常显示
					pcf8576DataBuf.buf1[s].dataBuf.bit5 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit5 = 1;		//置位要显示的符号相应的位
				}
				if(fun == 1)	{	   //闪烁
					pcf8576DataBuf.buf1[s].dataBuf.bit5 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit5 = 0;		//置位要显示的符号相应的位
				}
				if(fun == 2)	{	   //不显示
					pcf8576DataBuf.buf1[s].dataBuf.bit5 = 0;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit5 = 0;		//置位要显示的符号相应的位
				}
				break;
			case 6:
				if(fun == 0)	{	   //正常显示
					pcf8576DataBuf.buf1[s].dataBuf.bit6 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit6 = 1;		//置位要显示的符号相应的位
				}
				if(fun == 1)	{	   //闪烁
					pcf8576DataBuf.buf1[s].dataBuf.bit6 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit6 = 0;		//置位要显示的符号相应的位
				}
				if(fun == 2)	{	   //不显示
					pcf8576DataBuf.buf1[s].dataBuf.bit6 = 0;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit6 = 0;		//置位要显示的符号相应的位
				}
				break;
			case 7:
				if(fun == 0)	{	   //正常显示
					pcf8576DataBuf.buf1[s].dataBuf.bit7 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit7 = 1;		//置位要显示的符号相应的位
				}
				if(fun == 1)	{	   //闪烁
					pcf8576DataBuf.buf1[s].dataBuf.bit7 = 1;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit7 = 0;		//置位要显示的符号相应的位
				}
				if(fun == 2)	{	   //不显示
					pcf8576DataBuf.buf1[s].dataBuf.bit7 = 0;		//置位要显示的符号相应的位		
					pcf8576DataBufGlitter.buf1[s].dataBuf.bit7 = 0;		//置位要显示的符号相应的位
				}
				break;
			default:
				break;
		}
	}

	return 1;
}

/*********************************************************************************************************
** 函数名称: selectDevice
** 功能描述: 生成选择器件的命令字节数据
** 输　入: 	 U8 ad —— A2A1A0 ,
**           U8 c  —— 0： 最后一个命令 1：不是最后一个命令
** 输　出:   U8 器件选择命令字节
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 selectDevice(U8 ad,U8 c){
	Pcf8576DevicAddr addr;
	addr.bitDevicAddr.subAddr = ad;	//第一片子地址
	addr.bitDevicAddr.settled = 0x0c;	//固定为1100
	addr.bitDevicAddr.c = c;			//固定为最后一条命令

	return addr.u8DevicAddr;		//器件选择命令字节;

}

/*********************************************************************************************************
** 函数名称: setAddrPointer
** 功能描述: 设置数据的地址指针，用于指定显示数据的存储位置
** 输　入: 	 U8 ad —— 0~39 ,
**           U8 c  —— 0： 最后一个命令 1：不是最后一个命令
** 输　出:   U8 数据的地址指针
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 setAddrPointer(U8 ad,U8 c){
	Pcf8576DataPointer ptr;
	ptr.bitDataPointer.pointer = ad;
	ptr.bitDataPointer.bit6 = 0;		//固定为零
	ptr.bitDataPointer.c =c;

	return ptr.u8DataPointer;			//数据的地址指针;

}

/*********************************************************************************************************
** 函数名称: flashLcd
** 功能描述: 刷新LCD显示
** 输　入: 	
** 输　出:   U8 0:失败 1：成功
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 flashLcd(pcf8576Data *pcf8576DataBuf){
	U8 lcdData[22],i,temp[2];

	//第一片pcf8576操作
	lcdData[0] = selectDevice(0,1);		//生成器件选择命令字节
	lcdData[1] = setAddrPointer(0,0);	//设置数据地址指针



	for(i = 0;i < 20;i++){				//生成包含器件选择命令字的显示数据
		lcdData[i+2] = pcf8576DataBuf->buf0[i].data;
	}
	temp[0] = I2C_MasterWrite (I2C1,PCF8576,lcdData,22);	 	//写入第一片的显示数据

	//第二片pcf8576操作
	lcdData[0] = selectDevice(1,1);		//生成器件选择命令字节
	lcdData[1] = setAddrPointer(0,0);	//设置数据地址指针
	for(i = 0;i < 20;i++){				//生成包含器件选择命令字的显示数据
		lcdData[i+2] = pcf8576DataBuf->buf1[i].data;
	}
	temp[1] = I2C_MasterWrite (I2C1,PCF8576,lcdData,22);	 	//写入第二片的显示数据

	if((temp[0]&&temp[1]) == 0) return 1;
		else return 0;

}

/*********************************************************************************************************
** 函数名称: shutLcd
** 功能描述: 关闭LCD显示
** 输　入: 	
** 输　出:   U8 0:失败 1：成功
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 shutLcd(void){
	Pcf8576Mode mode;

	mode.bitMode.m0 = 0;		  		//设置为1：4            BP0，BP1，BP2，BP3
	mode.bitMode.m1 = 0;
	mode.bitMode.b = 0;					//1/3偏压
	mode.bitMode.e = 0;					//开显示
	mode.bitMode.bit4 = 0;
	mode.bitMode.bit5 = 0;
	mode.bitMode.bit6 = 1;
	mode.bitMode.c = 1;					//最后一个命令字节
	if(I2C_MasterWrite (I2C1,PCF8576,&(mode.u8Mode),1)==0) return 1;	 	//设定工作模式成功

	return 0;
}



/*******************************JY09345真值表***************************/
/*******************************JY09345真值表***************************/
/*******************************JY09345真值表***************************/
/*******************************JY09345真值表***************************/
/*******************************JY09345真值表***************************/

const iconConfig TruthTables01[128] =		  //上半部分
{
    { " ",     32, 7},		//无关		TruthTables01[0]	
    { "1P",    32, 6},		//月份的十位  1P
    { "上",    32, 5},
    { "当前",  32, 4},
    { "N",     33, 3},
    { "C",     33, 2},
    { "B",     33, 1},
    { "A",     33, 0},

    { "1A",    34, 7},	   //TruthTables01[8]
    { "1F",    34, 6},
    { "1E",    34, 5},
    { "1D",    34, 4},
    { "1B",    35, 3},
    { "1G",    35, 2},
    { "1C",    35, 1},
    { "COS",   35, 0},

    { "阶价1", 36, 7},		//TruthTables01[16]
    { "相角",  36, 6},	  	//即 “Φ” ，为方便输入使用 “相角”代替
    { "月",    36, 5},
    { "组合",  36, 4},
    { "剩余",  37, 3},
    { "阶梯",  37, 2},
    { "正",    37, 1},
    { "反",    37, 0},

    { "需",    38, 7},		//TruthTables01[24]
    { "电",    38, 6},	  	//
    { "向",    38, 5},
    { "无",    38, 4},
    { "COL1",  39, 3},
    { "量",    39, 2},
    { "功",    39, 1},
    { "有",    39, 0},

    { "COL2",  40, 7},		//TruthTables01[32]
    { "费",    40, 6},	  	//
    { "T1",    40, 5},
    { "T2",    40, 4},
    { "价",    41, 3},
    { "失",    41, 2},
    { "T4",    41, 1},
    { "T3",    41, 0},

    { "压",	   42, 7},		//TruthTables01[40]
    { "流",    42, 6},	  	//
    { "总",    42, 5},
    { "尖",    42, 4},
    { "功率",  43, 3},
    { "时",    43, 2},
    { "平",    43, 1},
    { "峰",    43, 0},

    { "S37'-k",44, 7},		//kWAh 的k	  TruthTables01[48]
    { "段",    44, 6},	  	//
    { "间",    44, 5},
    { "谷",    44, 4},
    { "var",   45, 3},
    { "S45-A", 45, 2},		//kWAh 的A
    { "S38-h", 45, 1},		//kWAh 的h
    { "S43-h", 45, 0},		//kvarh的h

    { "S37-V", 46, 7},		//kWAh 中组成W的第一个V		 TruthTables01[56]
    { "X-V",   46, 6},	  	//kWAh 中组成W的第二个V
    { "万",    46, 5},
    { "元",    46, 4},
    { "囤积",  47, 3},
    { " ",     47, 2},	   	//无关
    { "负号",  47, 1},
    { "阶价2", 47, 0},

    { "9B",    48, 7},		//TruthTables01[64]
    { "9G",    48, 6},	  	
    { "9C",    48, 5},
    { "S41-k", 48, 4},	 	//kvarh 的k
    { "9A",    49, 3},
    { "9F",    49, 2},	   	
    { "9E",    49, 1},
    { "9D",    49, 0},

    { "8B",    50, 7},		//TruthTables01[72]
    { "8G",    50, 6},	  	
    { "8C",    50, 5},
    { "8P",    50, 4},	 
    { "8A",    51, 3},
    { "8F",    51, 2},	   	
    { "8E",    51, 1},
    { "8D",    51, 0},

    { "7B",    52, 7},		//TruthTables01[80]
    { "7G",    52, 6},	  	
    { "7C",    52, 5},
    { "7P",    52, 4},	 
    { "7A",    53, 3},
    { "7F",    53, 2},	   	
    { "7E",    53, 1},
    { "7D",    53, 0},

    { "6B",    54, 7},		//TruthTables01[88]
    { "6G",    54, 6},	  	
    { "6C",    54, 5},
    { "6P",    54, 4},	 
    { "6A",    55, 3},
    { "6F",    55, 2},	   	
    { "6E",    55, 1},
    { "6D",    55, 0},

    { "5B",    56, 7},		//TruthTables01[96]
    { "5G",    56, 6},	  	
    { "5C",    56, 5},
    { "5P",    56, 4},	 
    { "5A",    57, 3},
    { "5F",    57, 2},	   	
    { "5E",    57, 1},
    { "5D",    57, 0},

    { "4B",    58, 7},		//TruthTables01[104]
    { "4G",    58, 6},	  	
    { "4C",    58, 5},
    { "4P",    58, 4},	 
    { "4A",    59, 3},
    { "4F",    59, 2},	   	
    { "4E",    59, 1},
    { "4D",    59, 0},

    { "3B",    60, 7},		//TruthTables01[112]
    { "3G",    60, 6},	  	
    { "3C",    60, 5},
    { "3P",    60, 4},	 
    { "3A",    61, 3},
    { "3F",    61, 2},	   	
    { "3E",    61, 1},
    { "3D",    61, 0},

    { "2B",    62, 7},		//TruthTables01[120]
    { "2G",    62, 6},	  	
    { "2C",    62, 5},
    { "2P",    62, 4},	 
    { "2A",    63, 3},
    { "2F",    63, 2},	   	
    { "2E",    63, 1},
    { "2D",    63, 0},

};

const iconConfig TruthTables02[128] =		  //下半部分
{
    { " ",      0, 7},		//未用
    { " ",      0, 6},	  	//未用
    { " ",      0, 5},		//未用
    { "停超欠", 0, 4},
    { "时段1",  1, 3},
    { "时段2",  1, 2},		
    { "时钟欠", 1, 1},		
    { " ",      1, 0},		//未用

    { "Ic",     2, 7},		
    { "N9-",    2, 6},	  	//N9标示的负号
    { "拉闸",   2, 5},		
    { "透支",   2, 4},
    { "N7-",    3, 3},
    { "Ib",     3, 2},		
    { "失败",   3, 1},		
    { "请购电", 3, 0},		

    { "Ia",     4, 7},		
    { "N5-",    4, 6},	  	//N9标示的负号
    { "成功",   4, 5},		
    { "中",     4, 4},
    { "Uc",     5, 3},
    { "逆相序", 5, 2},		
    { "实验室", 5, 1},		//小房子符号	
    { "报警",   5, 0},		//报警铃铛符号

    { "Ub",     6, 7},		
    { "Ua",     6, 6},	  	//
    { "密码错", 6, 5},		//锁形符号	
    { "可编程", 6, 4},		//允许编程符号
    { "在线",   7, 3},		// 无线通信在线符号
    { "第1格",  7, 2},		//信号强度第一格
    { "第2格",  7, 1},		//信号强度第二格
    { "第3格",  7, 0},		//信号强度第三格

    { "17B",    8, 7},		//
    { "17G",    8, 6},	  	//
    { "17C",    8, 5},		//
    { "485-2",  8, 4},		//与电话符号一起显示标示第二路485
    { "17A",    9, 3},		//
    { "17F",    9, 2},		//
    { "17E",    9, 1},		//
    { "17D",    9, 0},		//

    { "16B",    10, 7},		//
    { "16G",    10, 6},	  	//
    { "16C",    10, 5},		//
    { "485-1",  10, 4},		//与电话符号一起显示标示第一路485，
    { "16A",    11, 3},		//
    { "16F",    11, 2},		//
    { "16E",    11, 1},		//
    { "16D",    11, 0},		//

    { "15B",    12, 7},		//
    { "15G",    12, 6},	  	//
    { "15C",    12, 5},		//
    { "15P",    12, 4},		//
    { "15A",    13, 3},		//
    { "15F",    13, 2},		//
    { "15E",    13, 1},		//
    { "15D",    13, 0},		//

    { "14B",    14, 7},		//
    { "14G",    14, 6},	  	//
    { "14C",    14, 5},		//
    { "电话",   14, 4},		//电话符号，标示红外通信
    { "14A",    15, 3},		//
    { "14F",    15, 2},		//
    { "14E",    15, 1},		//
    { "14D",    15, 0},		//

    { "13B",    16, 7},		//
    { "13G",    16, 6},	  	//
    { "13C",    16, 5},		//
    { "13P",    16, 4},		//
    { "13A",    17, 3},		//
    { "13F",    17, 2},		//
    { "13E",    17, 1},		//
    { "13D",    17, 0},		//

    { "12B",    18, 7},		//
    { "12G",    18, 6},	  	//
    { "12C",    18, 5},		//
    { "载波",   18, 4},		//电力符号
    { "12A",    19, 3},		//
    { "12F",    19, 2},		//
    { "12E",    19, 1},		//
    { "12D",    19, 0},		//

    { "11B",    20, 7},		//
    { "11G",    20, 6},	  	//
    { "11C",    20, 5},		//
    { "11P",    20, 4},		//
    { "11A",    21, 3},		//
    { "11F",    21, 2},		//
    { "11E",    21, 1},		//
    { "11D",    21, 0},		//

    { "10B",    22, 7},		//
    { "10G",    22, 6},	  	//
    { "10C",    22, 5},		//
    { "第4格",  22, 4},		//信号强度第四格
    { "10A",    23, 3},		//
    { "10F",    23, 2},		//
    { "10E",    23, 1},		//
    { "10D",    23, 0},		//

    { "19B",    24, 7},		//
    { "19G",    24, 6},	  	//
    { "19C",    24, 5},		//
    { " ",      24, 4},		//未使用
    { "19A",    25, 3},		//
    { "19F",    25, 2},		//
    { "19E",    25, 1},		//
    { "19D",    25, 0},		//

    { "18B",    26, 7},		//
    { "18G",    26, 6},	  	//
    { "18C",    26, 5},		//
    { "读卡",   26, 4},		//
    { "18A",    27, 3},		//
    { "18F",    27, 2},		//
    { "18E",    27, 1},		//
    { "18D",    27, 0},		//

    { "谷",    28, 7},		//
    { "平",    28, 6},	  	//
    { "峰",    28, 5},		//
    { "尖",    28, 4},		//
    { "第4阶", 29, 3},		//指示当前运行第“4”阶梯电价
    { "第3阶", 29, 2},		//指示当前运行第“3”阶梯电价
    { "第2阶", 29, 1},		//指示当前运行第“2”阶梯电价
    { "第1阶", 29, 0},		//指示当前运行第“1”阶梯电价

    { "Q1",    30, 7},		//
    { "Q4",    30, 6},	  	//
    { "Q3",    30, 5},		//
    { "Q2",    30, 4},		//
    { " ",    31, 3},		//未使用
    { "Q",    31, 2},		//
    { " ",    31, 1},		//未使用
    { " ",    31, 0},		//未使用

};


/*********************************************************************************************************
** 函数名称: displayNumber
** 功能描述: 显示0~F，
** 输　入: 	 U8 no —— 数码管编号 1~19，详见液晶资料
**           U8 content —— 要显示的数据0~9
**           U8 dp —— 是否显示小数点 0：不显示 1：显示
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
/*********************************七段数码管显示0~F真值表********************************/
//const U8 dis_code[11]={0x3f,0x06,0x5b,0x4f,0x66, 			// 0, 1, 2, 3，4
//     			       0x6d,0x7d,0x07,0x7f,0x6f,0x77	// 5, 6, 7, 8, 9, a
//					   0x7c,0x39,0x5e,0x79,0x71,0x00}; 	// b, c, d, e, f,off
void displayNumber(U8 no,U8 content,U8 dp){
	U8 b = 0;
	U8 t = 0;

	switch (content) 		//获取相应的真值表数据
	{
		case 0x00:	t = 0x3f; 	break;
		case 0x01:	t = 0x06; 	break;
		case 0x02:	t = 0x5b;	break;
		case 0x03:	t = 0x4f;	break;
		case 0x04:	t = 0x66;	break;
		case 0x05:	t = 0x6d;	break;
		case 0x06:	t = 0x7d;	break;
		case 0x07:	t = 0x07;	break;
		case 0x08:	t = 0x7f;	break;
		case 0x09:	t = 0x6f;	break;	//9
		case 0x0a:	t = 0x77;	break;
		case 0x0b:	t = 0x7c;	break;
		case 0x0c:	t = 0x39;	break;
		case 0x0d:	t = 0x5e;	break;
		case 0x0e:	t = 0x79;	break;
		case 0x0f:	t = 0x71;	break;
		case 'a':	t = 0x77;	break;
		case 'b':	t = 0x7c;	break;
		case 'c':	t = 0x39;	break;
		case 'd':	t = 0x5e;	break;
		case 'e':	t = 0x79;	break;
		case 'f':	t = 0x71;	break;
		case 'g':	t = 0x6f;	break;	//小写g
		case 'H':	t = 0x76;	break;	//大写H
		case 'p':	t = 0xf3;	break;	//小写p
		case 'o':	t = 0x5c;	break;	//小写o
		case 'r':	t = 0x50;	break;	//小写r
		case '-':	t = 0x40;	break;	//负号
		case 'n':	t = 0x54;	break;	//
		case ' ':	t = 0x00;	break;	//空格，不显示
		default:   	t = 0x00;	break;
	}

	
	switch (no)			   	//要显示的数据
	{
		case 1:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables01[8],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[8],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables01[12],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[12],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables01[14],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[14],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables01[11],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[11],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables01[10],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[10],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables01[9],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[9],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables01[13],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[13],2);
			break;
		case 2:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables01[124],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[124],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables01[120],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[120],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables01[122],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[122],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables01[127],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[127],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables01[126],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[126],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables01[125],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[125],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables01[121],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[121],2);
			if(dp)update8576Buffer(TruthTables01[123],0);	//小数点，如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[123],2);

			break;
		case 3:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables01[116],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[116],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables01[112],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[112],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables01[114],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[114],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables01[119],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[119],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables01[118],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[118],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables01[117],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[117],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables01[113],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[113],2);
			if(dp)update8576Buffer(TruthTables01[115],0);	//小数点，如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[115],2);

			break;
		case 4:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables01[108],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[108],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables01[104],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[104],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables01[106],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[106],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables01[111],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[111],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables01[110],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[110],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables01[109],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[109],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables01[105],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[105],2);
			if(dp)update8576Buffer(TruthTables01[107],0);	//小数点，如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[107],2);

			break;
		case 5:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables01[100],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[100],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables01[96],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[96],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables01[98],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[98],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables01[103],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[103],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables01[102],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[102],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables01[101],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[101],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables01[97],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[97],2);
			if(dp)update8576Buffer(TruthTables01[99],0);	//小数点，如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[99],2);

			break;
		case 6:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables01[92],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[92],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables01[88],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[88],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables01[90],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[90],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables01[95],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[95],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables01[94],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[94],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables01[93],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[93],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables01[89],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[89],2);
			if(dp)update8576Buffer(TruthTables01[91],0);	//小数点，如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[91],2);

			break;
		case 7:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables01[84],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[84],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables01[80],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[80],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables01[82],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[82],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables01[87],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[87],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables01[86],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[86],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables01[85],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[85],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables01[81],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[81],2);
			if(dp)update8576Buffer(TruthTables01[83],0);	//小数点，如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[83],2);

			break;
		case 8:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables01[76],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[76],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables01[72],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[72],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables01[74],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[74],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables01[79],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[79],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables01[78],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[78],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables01[77],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[77],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables01[73],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[73],2);
			if(dp)update8576Buffer(TruthTables01[75],0);	//小数点，如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[75],2);

			break;
		case 9:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables01[68],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[68],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables01[64],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[64],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables01[66],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[66],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables01[71],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[71],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables01[70],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[70],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables01[69],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[69],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables01[65],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables01[65],2);

			break;
		case 10:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables02[92],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[92],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables02[88],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[88],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables02[90],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[90],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables02[95],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[95],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables02[94],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[94],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables02[93],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[93],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables02[89],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[89],2);

			break;
		case 11:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables02[84],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[84],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables02[80],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[80],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables02[82],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[82],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables02[87],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[87],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables02[86],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[86],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables02[85],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[85],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables02[81],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[81],2);
			if(dp)update8576Buffer(TruthTables02[83],0);	//小数点，如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[83],2);

			break;
		case 12:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables02[76],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[76],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables02[72],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[72],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables02[74],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[74],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables02[79],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[79],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables02[78],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[78],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables02[77],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[77],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables02[73],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[73],2);

			break;
		case 13:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables02[68],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[68],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables02[64],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[64],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables02[66],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[66],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables02[71],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[71],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables02[70],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[70],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables02[69],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[69],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables02[65],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[65],2);
			if(dp)update8576Buffer(TruthTables02[67],0);	//小数点，如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[67],2);

			break;
		case 14:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables02[60],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[60],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables02[56],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[56],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables02[58],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[58],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables02[63],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[63],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables02[62],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[62],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables02[61],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[61],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables02[57],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[57],2);

			break;
		case 15:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables02[52],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[52],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables02[48],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[48],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables02[50],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[50],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables02[55],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[55],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables02[54],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[54],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables02[53],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[53],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables02[49],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[49],2);
			if(dp)update8576Buffer(TruthTables02[51],0);	//小数点，如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[51],2);

			break;
		case 16:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables02[44],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[44],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables02[40],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[40],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables02[42],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[42],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables02[47],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[47],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables02[46],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[46],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables02[45],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[45],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables02[41],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[41],2);

			break;
		case 17:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables02[36],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[36],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables02[32],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[32],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables02[34],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[34],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables02[39],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[39],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables02[38],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[38],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables02[37],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[37],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables02[33],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[33],2);

			break;
			
		case 18:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables02[108],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[108],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables02[104],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[104],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables02[106],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[106],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables02[111],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[111],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables02[110],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[110],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables02[109],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[109],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables02[105],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[105],2);

			break;
		case 19:
			b = t & 0x01 ;		//获取数码管A段对应的值
			if(b == 0x01)update8576Buffer(TruthTables02[100],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[100],2);
			b = t & 0x01<<1 ;		//获取数码管B段对应的值
			if(b == 0x02)update8576Buffer(TruthTables02[96],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[96],2);
			b = t & 0x01<<2 ;		//获取数码管C段对应的值
			if(b == 0x04)update8576Buffer(TruthTables02[98],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[98],2);
			b = t & 0x01<<3 ;		//获取数码管D段对应的值
			if(b == 0x08)update8576Buffer(TruthTables02[103],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[103],2);
			b = t & 0x01<<4 ;		//获取数码管E段对应的值
			if(b == 0x10)update8576Buffer(TruthTables02[102],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[102],2);
			b = t & 0x01<<5 ;		//获取数码管F段对应的值
			if(b == 0x20)update8576Buffer(TruthTables02[101],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[101],2);
			b = t & 0x01<<6 ;		//获取数码管G段对应的值
			if(b == 0x40)update8576Buffer(TruthTables02[97],0);		//如果该位为1，将显示缓冲区中该段相应的值置位
				else update8576Buffer(TruthTables02[97],2);

			break;					
		default:
			break;
	}

}

/*********************************************************************************************************
** 函数名称: clearTopData
** 功能描述: 清除液晶上半部分的显示数据
** 输　入: 	 pcf8576Data *dataBuf —— 指向数据缓冲区的指针
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void clearTopData(pcf8576Data *dataBuf){
//	U8 i;
//	for(i = 1;i<20;i++){
//		dataBuf->buf1[i].data = 0;	
//	}
//
//	dataBuf->buf0[19].data = 0;	
//	dataBuf->buf0[18].data = 0;	
//	dataBuf->buf0[17].data = 0;	
//	dataBuf->buf0[16].data = 0;	
//	pcf8576DataBufGlitter.buf0[19].data = 0;	
//	pcf8576DataBufGlitter.buf0[18].data = 0;	
//	pcf8576DataBufGlitter.buf0[17].data = 0;	
//	pcf8576DataBufGlitter.buf0[16].data = 0;

	U8 i;

	for(i = 0;i<20;i++){
//		pcf8576DataBuf.buf1[i].data = 0;
		pcf8576DataBuf.buf1[i].data = 0;	
	}

	pcf8576DataBuf.buf0[19].data = 0;	
	pcf8576DataBuf.buf0[18].data = 0;	
	pcf8576DataBuf.buf0[17].data = 0;	
	pcf8576DataBuf.buf0[16].data = 0;	

	for(i = 0;i<20;i++){
		pcf8576DataBufGlitter.buf1[i].data = 0;	
	}

	pcf8576DataBufGlitter.buf0[19].data = 0;	
	pcf8576DataBufGlitter.buf0[18].data = 0;	
	pcf8576DataBufGlitter.buf0[17].data = 0;	
	pcf8576DataBufGlitter.buf0[16].data = 0;	
	
}


/*********************************************************************************************************
** 函数名称: updateContent
** 功能描述: 更新液晶上半部分显示内容区域的值
** 输　入: 	 U8 d3 —— 最高字节   10进制数
**           U8 d2 —— 次高字节   10进制数
**           U8 d1 —— 次低字节   10进制数
**           U8 d0 —— 最低字节   10进制数
**           数据格式：XX  XX  XX  XX
**                     d3  d2  d1  d0
**           U8 dp —— 要显示的小数点
**           数据格式：X.X.X.X.X.X.X.X	  除最后一位外均可为1，为1对应的位，小数点显示
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateContent(U8 d3,U8 d2,U8 d1,U8 d0,U8 dp){
	U8 temp[7],i;

	//数据内容
	for(i = 0;i<7;i++) temp[i] = dp&(1<<(7-i));		//获取要显示的小数点
	if((d3/10 != 0)||temp[0])	{
		displayNumber(2,d3/10,temp[0]);		//最高位
		displayNumber(3,d3%10,temp[1]);		//次高位
		displayNumber(4,d2/10,temp[2]);		//	 .
		displayNumber(5,d2%10,temp[3]);		//	 .
		displayNumber(6,d1/10,temp[4]);		//	 .
		displayNumber(7,d1%10,temp[5]);		//	 .
		displayNumber(8,d0/10,temp[6]);		//次低位
		displayNumber(9,d0%10,0);			//最低位

	}else{
		if((d3%10 != 0)||temp[1])	{
			displayNumber(3,d3%10,temp[1]);		//最高位
			displayNumber(4,d2/10,temp[2]);		//	 .
			displayNumber(5,d2%10,temp[3]);		//	 .
			displayNumber(6,d1/10,temp[4]);		//	 .
			displayNumber(7,d1%10,temp[5]);		//	 .
			displayNumber(8,d0/10,temp[6]);		//次低位
			displayNumber(9,d0%10,0);			//最低位
		}else{
			if((d2/10 != 0)||temp[2])	{
				displayNumber(4,d2/10,temp[2]);		//最高位
				displayNumber(5,d2%10,temp[3]);		//  .
				displayNumber(6,d1/10,temp[4]);		//  .
				displayNumber(7,d1%10,temp[5]);		//  .
				displayNumber(8,d0/10,temp[6]);		//次低位
				displayNumber(9,d0%10,0);			//最低位
			}else{
				if((d2%10 != 0)||temp[3])	{
					displayNumber(5,d2%10,temp[3]);		//最高位
					displayNumber(6,d1/10,temp[4]);		//  .
					displayNumber(7,d1%10,temp[5]);		//  .
					displayNumber(8,d0/10,temp[6]);		//次低位
					displayNumber(9,d0%10,0);			//最低位
				}else{
					if((d1/10 != 0)||temp[4])	{
						displayNumber(6,d1/10,temp[4]);		//最高位
						displayNumber(7,d1%10,temp[5]);		//  .
						displayNumber(8,d0/10,temp[6]);		//次低位
						displayNumber(9,d0%10,0);			//最低位
					}else{
						if((d1%10 != 0)||temp[5]) {
							displayNumber(7,d1%10,temp[5]);		//最高位
							displayNumber(8,d0/10,temp[6]);		//次低位
							displayNumber(9,d0%10,0);		  //最低位
						}else{
							if((d1/10 != 0)||temp[6]){
								displayNumber(8,d0/10,temp[6]);		//次低位
								displayNumber(9,d0%10,0);
							}else{
								displayNumber(9,d0%10,0);
							}
						}
									
					}
				}
			}
		}
	}
}


/*********************************************************************************************************
** 函数名称: updateIdentifier
** 功能描述: 更新液晶下半部分数据标识和页码区域的值
** 输　入: 	 U8 di3 —— 最高字节 16进制数
**           U8 di2 —— 次高字节 16进制数
**           U8 di1 —— 次低字节 16进制数
**           U8 di0 —— 最低字节 16进制数
**           数据标识格式：XX   XX   XX   XX
**                         di3  di2  di1  di0
**           U8 page —— 页码    16进制数
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateIdentifier(U8 di3,U8 di2,U8 di1,U8 di0,U8 page){
	//数据标识
	displayNumber(10,di3/16,0);			//
	displayNumber(11,di3%16,0);			//
	displayNumber(12,di2/16,0);			//
	displayNumber(13,di2%16,0);			//
	displayNumber(14,di1/16,0);			//
	displayNumber(15,di1%16,0);			//
	displayNumber(16,di0/16,0);			//
	displayNumber(17,di0%16,0);			//
	//页码
	displayNumber(18,page/16,0);		//
	displayNumber(19,page%16,0);		//

}

/*******************************以上为循显和键显所使用的基本函数**********************************/


/*******************************以下为循显和键显项目**********************************/


/*********************************************************************************************************
** 函数名称: updateDate
** 功能描述: 更新缓冲区的日期数据，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式： XX. XX. XX
**                      d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateDate(U8 d3,U8 d2,U8 d1,U8 d0){

	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);

	//数据内容
	updateContent(0,d2,d1,d0,0x14);	//0x14 = 0001 0100 ,为1的位对应的小数点儿显示
	if(d2/10 == 0) displayNumber(4,0,0);
	//数据标识04 00 01 01  页码00
	updateIdentifier(0x04,0x00,0x01,0x01,0x00);

	//提示字符——无
}

/*********************************************************************************************************
** 函数名称: updateTime
** 功能描述: 更新缓冲区的时间数据，当前时间
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX. XX. XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateTime(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0,d2,d1,d0,0x14);	//0x14 = 0001 0100 ,为1的位对应的小数点儿显示
//	if(!d2) {
//		displayNumber(4,0,0);
//		displayNumber(5,0,1);
//		if(d1 < 10) displayNumber(6,0,0);
//	}
	if(d2/10 == 0) displayNumber(4,0,0);
	//数据标识04 00 01 02  页码00
	updateIdentifier(0x04,0x00,0x01,0x02,0x00);
	//提示字符
	update8576Buffer(TruthTables01[45],0);		//"时"
	update8576Buffer(TruthTables01[50],0);		//"间"
	//其它符号
	update8576Buffer(TruthTables01[28],0);		//"COL1"
	update8576Buffer(TruthTables01[32],0);		//"COL2"
}

/*********************************************************************************************************
** 函数名称: updateLeavingMoney
** 功能描述: 更新缓冲区的剩余金额，当前剩余金额
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLeavingMoney(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 90 01 00  页码00
	updateIdentifier(0x00,0x90,0x01,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[20],0);		//"剩余"
	update8576Buffer(TruthTables01[59],0);		//"元"

}

/*********************************************************************************************************
** 函数名称: updateComboPTotal
** 功能描述: 更新缓冲区的 当前组合有功总电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateComboPTotalKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 00 00 00  页码00
	updateIdentifier(0x00,0x00,0x00,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[19],0);	//"组合"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}
/*********************************************************************************************************
** 函数名称: updateComboPTotal
** 功能描述: 更新缓冲区的 当前组合有功总电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateCombo1QTotalKvarh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 00 00 00  页码00
	updateIdentifier(0x00,0x03,0x00,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[19],0);	//"组合"
	update8576Buffer(TruthTables01[27],0);	//"无"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[34],0);	//"T1"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[67],0);	//"S41-k"
	update8576Buffer(TruthTables01[52],0);	//"var"
	update8576Buffer(TruthTables01[55],0);	//"S43-h"
}
/*********************************************************************************************************
** 函数名称: updateComboPTotal
** 功能描述: 更新缓冲区的 当前组合有功总电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateCombo2QTotalKvarh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 00 00 00  页码00
	updateIdentifier(0x00,0x04,0x00,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[19],0);	//"组合"
	update8576Buffer(TruthTables01[27],0);	//"无"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[34],0);	//"T1"
	update8576Buffer(TruthTables01[35],0);	//"T2"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[67],0);	//"S41-k"
	update8576Buffer(TruthTables01[52],0);	//"var"
	update8576Buffer(TruthTables01[55],0);	//"S43-h"
}
/*********************************************************************************************************
** 函数名称: updateForwardPTotalKwh
** 功能描述: 更新缓冲区的 当前正向有功总电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateForwardPTotalKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 01 00 00  页码00
	updateIdentifier(0x00,0x01,0x00,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
	
}

/*********************************************************************************************************
** 函数名称: updateForwardPJianKwh
** 功能描述: 更新缓冲区的 当前正向有功尖电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateForwardPJianKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 01 01 00  页码00
	updateIdentifier(0x00,0x01,0x01,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[43],0);	//"尖"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateForwardPFengKwh
** 功能描述: 更新缓冲区的 当前正向有功峰电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateForwardPFengKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 01 02 00  页码00
	updateIdentifier(0x00,0x01,0x02,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[47],0);	//"峰"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateForwardPPingKwh
** 功能描述: 更新缓冲区的 当前正向有功平电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateForwardPPingKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 01 03 00  页码00
	updateIdentifier(0x00,0x01,0x03,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[46],0);	//"平"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateForwardPGuKwh
** 功能描述: 更新缓冲区的 当前正向有功谷电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateForwardPGuKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 01 04 00  页码00
	updateIdentifier(0x00,0x01,0x04,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[51],0);	//"谷"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateForwardPTatalMaxKw
** 功能描述: 更新缓冲区的 当前正向有功总最大需量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX. XX  XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateForwardPTatalMaxKw(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x10);	//0x10 = 0001 0000 ,为1的位对应的小数点儿显示
	//数据标识01 01 00 00  页码00
	updateIdentifier(0x01,0x01,0x00,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[24],0);	//"需"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
}

/*********************************************************************************************************
** 函数名称: updateForwardPTatalMaxKwDate
** 功能描述: 更新缓冲区的 当前正向有功总最大需量发生日期
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX. XX. XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateForwardPTatalMaxKwDate(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x14);	//0x14 = 0001 0100 ,为1的位对应的小数点儿显示
	if(d2/10 == 0) displayNumber(4,0,0);
	//数据标识01 01 00 00  页码01
	updateIdentifier(0x01,0x01,0x00,0x00,0x01);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[24],0);	//"需"
	update8576Buffer(TruthTables01[29],0);	//"量"

}

/*********************************************************************************************************
** 函数名称: updateForwardPTatalMaxKwTime
** 功能描述: 更新缓冲区的 当前正向有功总最大需量发生时间
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX. XX. XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateForwardPTatalMaxKwTime(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x14);	//0x14 = 0001 0100 ,为1的位对应的小数点儿显示
	if(d2/10 == 0) displayNumber(4,0,0);
	//数据标识01 01 00 00  页码02
	updateIdentifier(0x01,0x01,0x00,0x00,0x02);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[24],0);	//"需"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[45],0);	//"时"
	update8576Buffer(TruthTables01[50],0);	//"间"
	update8576Buffer(TruthTables01[28],0);	//"COL1"
	update8576Buffer(TruthTables01[32],0);	//"COL2"

}
/*********************************************************************************************************
** 函数名称: updateForwardPTatalMaxKwTime
** 功能描述: 更新缓冲区的 当前正向有功总最大需量发生时间
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX. XX. XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateClockBattUseTime(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x00);


	updateIdentifier(0x02,0x80,0x00,0x0a,0x00);
	//提示字符
	update8576Buffer(TruthTables01[45],0);	//"时"
	update8576Buffer(TruthTables01[50],0);	//"间"


}
/*********************************************************************************************************
** 函数名称: updateReversePTotalKwh
** 功能描述: 更新缓冲区的 当前反向有功总电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateReversePTotalKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 02 00 00  页码00
	updateIdentifier(0x00,0x02,0x00,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateReversePJianKwh
** 功能描述: 更新缓冲区的 当前反向有功尖电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateReversePJianKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 02 01 00  页码00
	updateIdentifier(0x00,0x02,0x01,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[43],0);	//"尖"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateReversePFengKwh
** 功能描述: 更新缓冲区的 当前反向有功峰电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateReversePFengKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 02 02 00  页码00
	updateIdentifier(0x00,0x02,0x02,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[47],0);	//"峰"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateReversePPingKwh
** 功能描述: 更新缓冲区的 当前反向有功平电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateReversePPingKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 02 03 00  页码00
	updateIdentifier(0x00,0x02,0x03,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[46],0);	//"平"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateReversePGuKwh
** 功能描述: 更新缓冲区的 当前反向有功谷电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateReversePGuKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 02 04 00  页码00
	updateIdentifier(0x00,0x02,0x04,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[51],0);	//"谷"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateReversePTatalMaxKw
** 功能描述: 更新缓冲区的 当前反向有功总最大需量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX. XX  XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateReversePTatalMaxKw(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x10);	//0x04 = 0001 0000 ,为1的位对应的小数点儿显示
	//数据标识01 02 00 00  页码00
	updateIdentifier(0x01,0x02,0x00,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[24],0);	//"需"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
}

/*********************************************************************************************************
** 函数名称: updateReversePTatalMaxKwDate
** 功能描述: 更新缓冲区的 当前反向有功总最大需量发生日期
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX. XX. XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateReversePTatalMaxKwDate(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x14);	//0x14 = 0001 0100 ,为1的位对应的小数点儿显示
	if(d2/10 == 0) displayNumber(4,0,0);
	//数据标识01 02 00 00  页码01
	updateIdentifier(0x01,0x02,0x00,0x00,0x01);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[24],0);	//"需"
	update8576Buffer(TruthTables01[29],0);	//"量"
}

/*********************************************************************************************************
** 函数名称: updateReversePTatalMaxKwTime
** 功能描述: 更新缓冲区的 当前正向有功总最大需量发生时间
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX. XX. XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateReversePTatalMaxKwTime(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x14);	//0x14 = 0001 0100 ,为1的位对应的小数点儿显示
	if(d2/10 == 0) displayNumber(4,0,0);
	//数据标识01 02 00 00  页码02
	updateIdentifier(0x01,0x02,0x00,0x00,0x02);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[24],0);	//"需"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[45],0);	//"时"
	update8576Buffer(TruthTables01[50],0);	//"间"
	update8576Buffer(TruthTables01[28],0);	//"COL1"
	update8576Buffer(TruthTables01[32],0);	//"COL2"
}

/*********************************************************************************************************
** 函数名称: updateQuadrant1QTotalKvarh
** 功能描述: 更新缓冲区的 当前第一象限无功总电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateQuadrant1QTotalKvarh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 05 00 00  页码00
	updateIdentifier(0x00,0x05,0x00,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);	//"当前"
	update8576Buffer(TruthTables01[34],0);	//"T1"
	update8576Buffer(TruthTables01[27],0);	//"无"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[67],0);	//"S41-k"
	update8576Buffer(TruthTables01[52],0);	//"var"
	update8576Buffer(TruthTables01[55],0);	//"S43-h"
	
}

/*********************************************************************************************************
** 函数名称: updateQuadrant2QTotalKvarh
** 功能描述: 更新缓冲区的 当前第二象限无功总电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateQuadrant2QTotalKvarh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 06 00 00  页码00
	updateIdentifier(0x00,0x06,0x00,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[34],0);	//"T1"
	update8576Buffer(TruthTables01[35],0);	//"T2"
	update8576Buffer(TruthTables01[27],0);	//"无"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[67],0);	//"S41-k"
	update8576Buffer(TruthTables01[52],0);	//"var"
	update8576Buffer(TruthTables01[55],0);	//"S43-h"

}

/*********************************************************************************************************
** 函数名称: updateQuadrant3QTotalKvarh
** 功能描述: 更新缓冲区的 当前第三象限无功总电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateQuadrant3QTotalKvarh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 07 00 00  页码00
	updateIdentifier(0x00,0x07,0x00,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[34],0);	//"T1"
	update8576Buffer(TruthTables01[35],0);	//"T2"
	update8576Buffer(TruthTables01[39],0);	//"T3"
	update8576Buffer(TruthTables01[27],0);	//"无"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[67],0);	//"S41-k"
	update8576Buffer(TruthTables01[52],0);	//"var"
	update8576Buffer(TruthTables01[55],0);	//"S43-h"
}

/*********************************************************************************************************
** 函数名称: updateQuadrant4QTotalKvarh
** 功能描述: 更新缓冲区的 当前第四象限无功总电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateQuadrant4QTotalKvarh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 08 00 00  页码00
	updateIdentifier(0x00,0x08,0x00,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[39],0);	//"T3"
	update8576Buffer(TruthTables01[38],0);	//"T4"
	update8576Buffer(TruthTables01[27],0);	//"无"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[67],0);	//"S41-k"
	update8576Buffer(TruthTables01[52],0);	//"var"
	update8576Buffer(TruthTables01[55],0);	//"S43-h"
}


/*********************************************************************************************************
** 函数名称: updatePreviousPTotalKwh
** 功能描述: 更新缓冲区的 上1月正向有功总电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousPTotalKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 01 00 01  页码00
	updateIdentifier(0x00,0x01,0x00,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"

}

/*********************************************************************************************************
** 函数名称: updatePreviousPJianKwh
** 功能描述: 更新缓冲区的 上1月正向有功尖电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousPJianKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 01 01 01  页码00
	updateIdentifier(0x00,0x01,0x01,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[43],0);	//"尖"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updatePreviousPFengKwh
** 功能描述: 更新缓冲区的 上1月正向有功峰电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousPFengKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 01 02 01  页码00
	updateIdentifier(0x00,0x01,0x02,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[47],0);	//"峰"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updatePreviousPPingKwh
** 功能描述: 更新缓冲区的 上1月正向有功平电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousPPingKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 01 03 01  页码00
	updateIdentifier(0x00,0x01,0x03,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[46],0);	//"平"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updatePreviousPGuKwh
** 功能描述: 更新缓冲区的 上1月正向有功谷电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousPGuKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 01 04 01  页码00
	updateIdentifier(0x00,0x01,0x04,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[51],0);	//"谷"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updatePreviousMaxKw
** 功能描述: 更新缓冲区的 上1月正向有功最大需量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousMaxKw(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x10);	//0x10 = 0001 0000 ,为1的位对应的小数点儿显示
	//数据标识01 01 00 01  页码00
	updateIdentifier(0x01,0x01,0x00,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[24],0);	//"需"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
}
/*********************************************************************************************************
** 函数名称: updatePreviousReverseMaxKw
** 功能描述: 更新缓冲区的 上1月反向有功最大需量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousReverseMaxKw(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x10);	//0x10 = 0001 0000 ,为1的位对应的小数点儿显示
	//数据标识01 01 00 01  页码00
	updateIdentifier(0x01,0x02,0x00,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[24],0);	//"需"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
}
/*********************************************************************************************************
** 函数名称: updatePreviousMaxKwDate
** 功能描述: 更新缓冲区的 上1月正向有功最大需量发生日期
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousMaxKwDate(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x14);	//0x14 = 0001 0100 ,为1的位对应的小数点儿显示
	if(d2/10 == 0) displayNumber(4,0,0);
	//数据标识01 01 00 01  页码01
	updateIdentifier(0x01,0x01,0x00,0x01,0x01);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[24],0);	//"需"
	update8576Buffer(TruthTables01[29],0);	//"量"
}
/*********************************************************************************************************
** 函数名称: updatePreviousMaxKwDate
** 功能描述: 更新缓冲区的 上1月正向有功最大需量发生日期
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousReverseMaxKwDate(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x14);	//0x14 = 0001 0100 ,为1的位对应的小数点儿显示
	if(d2/10 == 0) displayNumber(4,0,0);
	//数据标识01 01 00 01  页码01
	updateIdentifier(0x01,0x02,0x00,0x01,0x01);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[24],0);	//"需"
	update8576Buffer(TruthTables01[29],0);	//"量"
}
/*********************************************************************************************************
** 函数名称: updatePreviousMaxKwTime
** 功能描述: 更新缓冲区的 上1月正向有功最大需量发生时间
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousMaxKwTime(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x14);	//0x14 = 0001 0100 ,为1的位对应的小数点儿显示
	if(d2/10 == 0) displayNumber(4,0,0);
	//数据标识01 01 00 01  页码02
	updateIdentifier(0x01,0x01,0x00,0x01,0x02);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[24],0);	//"需"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[45],0);	//"时"
	update8576Buffer(TruthTables01[50],0);	//"间"
	update8576Buffer(TruthTables01[28],0);	//"COL1"
	update8576Buffer(TruthTables01[32],0);	//"COL2"
}
/*********************************************************************************************************
** 函数名称: updatePreviousMaxKwTime
** 功能描述: 更新缓冲区的 上1月正向有功最大需量发生时间
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousReverseMaxKwTime(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x14);	//0x14 = 0001 0100 ,为1的位对应的小数点儿显示
	if(d2/10 == 0) displayNumber(4,0,0);
	//数据标识01 01 00 01  页码02
	updateIdentifier(0x01,0x02,0x00,0x01,0x02);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[24],0);	//"需"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[45],0);	//"时"
	update8576Buffer(TruthTables01[50],0);	//"间"
	update8576Buffer(TruthTables01[28],0);	//"COL1"
	update8576Buffer(TruthTables01[32],0);	//"COL2"
}
/*********************************************************************************************************
** 函数名称: updatePreviousReversePTotalKwh
** 功能描述: 更新缓冲区的 上1月反向有功总电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousReversePTotalKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 02 00 01  页码00
	updateIdentifier(0x00,0x02,0x00,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updatePreviousReversePJianKwh
** 功能描述: 更新缓冲区的 上1月反向有功尖电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousReversePJianKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 02 01 01  页码00
	updateIdentifier(0x00,0x02,0x01,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[43],0);	//"尖"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updatePreviousReversePFengKwh
** 功能描述: 更新缓冲区的 上1月反向有功峰电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousReversePFengKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 02 02 01  页码00
	updateIdentifier(0x00,0x02,0x02,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[47],0);	//"峰"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updatePreviousReversePPingKwh
** 功能描述: 更新缓冲区的 上1月反向有功平电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousReversePPingKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 02 03 01  页码00
	updateIdentifier(0x00,0x02,0x03,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[46],0);	//"平"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updatePreviousReversePGuKwh
** 功能描述: 更新缓冲区的 上1月反向有功谷电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousReversePGuKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 02 04 01  页码00
	updateIdentifier(0x00,0x02,0x04,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[51],0);	//"谷"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updatePreviousQuadrant1QTotalKvarh
** 功能描述: 更新缓冲区的 上1月第一象限无功总电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousQuadrant1QTotalKvarh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 05 00 01  页码00
	updateIdentifier(0x00,0x05,0x00,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[34],0);	//"T1"
	update8576Buffer(TruthTables01[27],0);	//"无"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[67],0);	//"S41-k"
	update8576Buffer(TruthTables01[52],0);	//"var"
	update8576Buffer(TruthTables01[55],0);	//"S43-h"
}

/*********************************************************************************************************
** 函数名称: updatePreviousQuadrant2QTotalKvarh
** 功能描述: 更新缓冲区的 上1月第二象限无功总电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousQuadrant2QTotalKvarh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 06 00 01  页码00
	updateIdentifier(0x00,0x06,0x00,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[34],0);	//"T1"
	update8576Buffer(TruthTables01[35],0);	//"T2"
	update8576Buffer(TruthTables01[27],0);	//"无"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[67],0);	//"S41-k"
	update8576Buffer(TruthTables01[52],0);	//"var"
	update8576Buffer(TruthTables01[55],0);	//"S43-h"
}

/*********************************************************************************************************
** 函数名称: updatePreviousQuadrant3QTotalKvarh
** 功能描述: 更新缓冲区的 上1月第三象限无功总电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousQuadrant3QTotalKvarh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 07 00 01  页码00
	updateIdentifier(0x00,0x07,0x00,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[34],0);	//"T1"
	update8576Buffer(TruthTables01[35],0);	//"T2"
	update8576Buffer(TruthTables01[39],0);	//"T3"
	update8576Buffer(TruthTables01[27],0);	//"无"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[67],0);	//"S41-k"
	update8576Buffer(TruthTables01[52],0);	//"var"
	update8576Buffer(TruthTables01[55],0);	//"S43-h"
}

/*********************************************************************************************************
** 函数名称: updatePreviousQuadrant4QTotalKvarh
** 功能描述: 更新缓冲区的 上1月第四象限无功总电量，
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePreviousQuadrant4QTotalKvarh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 08 00 01  页码00
	updateIdentifier(0x00,0x08,0x00,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[2],0);		//"上"
	displayNumber(1,1,0);		//数码管1显示“1”
	update8576Buffer(TruthTables01[18],0);	//"月"
	update8576Buffer(TruthTables01[39],0);	//"T3"
	update8576Buffer(TruthTables01[38],0);	//"T4"
	update8576Buffer(TruthTables01[27],0);	//"无"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[67],0);	//"S41-k"
	update8576Buffer(TruthTables01[52],0);	//"var"
	update8576Buffer(TruthTables01[55],0);	//"S43-h"
}

/*********************************************************************************************************
** 函数名称: updateMeterNumberLow8
** 功能描述: 更新缓冲区的 电能表通信地址（标号）低8位
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX  XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateMeterNumberLow8(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x00);	//0x00 = 0000 0000 ,为1的位对应的小数点儿显示
	//数据标识04 00 04 01  页码00
	updateIdentifier(0x04,0x00,0x04,0x01,0x00);
}

/*********************************************************************************************************
** 函数名称: updateMeterNumberHigh4
** 功能描述: 更新缓冲区的 电能表通信地址（标号）高4位
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX
**                     d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateMeterNumberHigh4(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,0x00,d1,d0,0x00);	//0x00 = 0000 0000 ,为1的位对应的小数点儿显示
	//数据标识04 00 04 01  页码01
	updateIdentifier(0x04,0x00,0x04,0x01,0x01);
}

/*********************************************************************************************************
** 函数名称: updateBaudRate
** 功能描述: 更新缓冲区的 通信波特率
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateBaudRate(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x00);	//0x00 = 0000 0000 ,为1的位对应的小数点儿显示
	//数据标识04 00 07 03  页码00
	updateIdentifier(0x04,0x00,0x07,0x03,0x00);
}

/*********************************************************************************************************
** 函数名称: updateImpKwh
** 功能描述: 更新缓冲区的 有功脉冲常数
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateImpKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x00);	//0x00 = 0000 0000 ,为1的位对应的小数点儿显示
	//数据标识04 00 04 09  页码00
	updateIdentifier(0x04,0x00,0x04,0x09,0x00);
}

/*********************************************************************************************************
** 函数名称: updateImpKvarh
** 功能描述: 更新缓冲区的 无功脉冲常数
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateImpKvarh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x00);	//0x00 = 0000 0000 ,为1的位对应的小数点儿显示
	//数据标识04 00 04 0A  页码00
	updateIdentifier(0x04,0x00,0x04,0x0A,0x00);
}

/*********************************************************************************************************
** 函数名称: updateRtcBatterisUseTime
** 功能描述: 更新缓冲区的 时钟电池使用时间
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX  XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateRtcBatterisUseTime(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x00);	//0x00 = 0000 0000 ,为1的位对应的小数点儿显示
	//数据标识02 80 00 0A  页码00
	updateIdentifier(0x02,0x80,0x00,0x0A,0x00);
}

/*********************************************************************************************************
** 函数名称: updateLastTimeProgramDate
** 功能描述: 更新缓冲区的 最近一次编程日期
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX. XX. XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLastTimeProgramDate(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x14);	//0x14 = 0001 0100 ,为1的位对应的小数点儿显示
	if(d2/10 == 0) displayNumber(4,0,0);
	//数据标识03 30 00 01  页码00
	updateIdentifier(0x03,0x30,0x00,0x01,0x00);
}

/*********************************************************************************************************
** 函数名称: updateLastTimeProgramTime
** 功能描述: 更新缓冲区的 最近一次编程时间
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX. XX. XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLastTimeProgramTime(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x14);	//0x14 = 0001 0100 ,为1的位对应的小数点儿显示
	if(d2/10 == 0) displayNumber(4,0,0);
	//数据标识03 30 00 01  页码01
	updateIdentifier(0x03,0x30,0x00,0x01,0x01);
	//提示字符
	update8576Buffer(TruthTables01[45],0);	//"时"
	update8576Buffer(TruthTables01[50],0);	//"间"
	update8576Buffer(TruthTables01[28],0);	//"COL1"
	update8576Buffer(TruthTables01[32],0);	//"COL2"
}

/*********************************************************************************************************
** 函数名称: updateLostVoltageTotalTimes
** 功能描述: 更新缓冲区的 总失压次数
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLostVoltageTotalTimes(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x00);	//0x00 = 0000 0000 ,为1的位对应的小数点儿显示
	//数据标识10 00 00 01  页码00
	updateIdentifier(0x10,0x00,0x00,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[37],0);	//"失"	
	update8576Buffer(TruthTables01[40],0);	//"压"

}

/*********************************************************************************************************
** 函数名称: updateLostVoltageTotalTime
** 功能描述: 更新缓冲区的 总失压累计时间
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLostVoltageTotalTime(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x00);	//0x14 = 0000 0000 ,为1的位对应的小数点儿显示
	//数据标识10 00 00 02  页码00
	updateIdentifier(0x10,0x00,0x00,0x02,0x00);
	//提示字符
	update8576Buffer(TruthTables01[37],0);	//"失"	
	update8576Buffer(TruthTables01[40],0);	//"压"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[45],0);	//"时"
	update8576Buffer(TruthTables01[50],0);	//"间"
}

/*********************************************************************************************************
** 函数名称: updateLastLostVoltageStartDate
** 功能描述: 更新缓冲区的 最近一次失压起始日期
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX. XX. XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLastLostVoltageStartDate(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x14);	//0x14 = 0001 0100 ,为1的位对应的小数点儿显示
	if(d2/10 == 0) displayNumber(4,0,0);
	//数据标识10 00 01 01  页码00
	updateIdentifier(0x10,0x00,0x01,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[37],0);	//"失"	
	update8576Buffer(TruthTables01[40],0);	//"压"
}

/*********************************************************************************************************
** 函数名称: updateLastLostVoltageStartTime
** 功能描述: 更新缓冲区的 最近一次失压开始时间
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX. XX. XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLastLostVoltageStartTime(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x14);	//0x14 = 0001 0100 ,为1的位对应的小数点儿显示
	if(d2/10 == 0) displayNumber(4,0,0);
	//数据标识10 00 01 01  页码01
	updateIdentifier(0x10,0x00,0x01,0x01,0x01);
	//提示字符
	update8576Buffer(TruthTables01[37],0);	//"失"	
	update8576Buffer(TruthTables01[40],0);	//"压"
	update8576Buffer(TruthTables01[45],0);	//"时"
	update8576Buffer(TruthTables01[50],0);	//"间"
	update8576Buffer(TruthTables01[28],0);	//"COL1"
	update8576Buffer(TruthTables01[32],0);	//"COL2"
}

/*********************************************************************************************************
** 函数名称: updateLastLostVoltageEndDate
** 功能描述: 更新缓冲区的 最近一次失压结束日期
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX. XX. XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLastLostVoltageEndDate(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x14);	//0x14 = 0001 0100 ,为1的位对应的小数点儿显示
	if(d2/10 == 0) displayNumber(4,0,0);
	//数据标识10 00 02 01  页码00
	updateIdentifier(0x10,0x00,0x02,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[37],0);	//"失"	
	update8576Buffer(TruthTables01[40],0);	//"压"
}

/*********************************************************************************************************
** 函数名称: updateLastLostVoltageEndTime
** 功能描述: 更新缓冲区的 最近一次失压结束时间
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX. XX. XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLastLostVoltageEndTime(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x14);	//0x14 = 0001 0100 ,为1的位对应的小数点儿显示
	if(d2/10 == 0) displayNumber(4,0,0);
	//数据标识10 00 02 01  页码01
	updateIdentifier(0x10,0x00,0x02,0x01,0x01);
	//提示字符
	update8576Buffer(TruthTables01[37],0);	//"失"	
	update8576Buffer(TruthTables01[40],0);	//"压"
	update8576Buffer(TruthTables01[45],0);	//"时"
	update8576Buffer(TruthTables01[50],0);	//"间"
	update8576Buffer(TruthTables01[28],0);	//"COL1"
	update8576Buffer(TruthTables01[32],0);	//"COL2"
}

/*********************************************************************************************************
** 函数名称: updateLastLostStartAForwardPKwh
** 功能描述: 更新缓冲区的 最近一次A相失压起始时刻正向有功电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLastLostStartAForwardPKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识10 01 02 01  页码00
	updateIdentifier(0x10,0x01,0x02,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[7],0);		//"A"
	update8576Buffer(TruthTables01[37],0);	//"失"	
	update8576Buffer(TruthTables01[40],0);	//"压"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateLastLostEndAForwardPKwh
** 功能描述: 更新缓冲区的 最近一次A相失压结束时刻正向有功电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLastLostEndAForwardPKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识10 01 26 01  页码00
	updateIdentifier(0x10,0x01,0x26,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[7],0);		//"A"
	update8576Buffer(TruthTables01[37],0);	//"失"	
	update8576Buffer(TruthTables01[40],0);	//"压"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateLastLostStartAReversePKwh
** 功能描述: 更新缓冲区的 最近一次A相失压起始时刻反向有功电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLastLostStartAReversePKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识10 01 03 01  页码00
	updateIdentifier(0x10,0x01,0x03,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[7],0);		//"A"
	update8576Buffer(TruthTables01[37],0);	//"失"	
	update8576Buffer(TruthTables01[40],0);	//"压"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateLastLostEndAReversePKwh
** 功能描述: 更新缓冲区的 最近一次A相失压结束时刻反向有功电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLastLostEndAReversePKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识10 01 27 01  页码00
	updateIdentifier(0x10,0x01,0x27,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[7],0);		//"A"
	update8576Buffer(TruthTables01[37],0);	//"失"	
	update8576Buffer(TruthTables01[40],0);	//"压"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateLastLostStartBForwardPKwh
** 功能描述: 更新缓冲区的 最近一次B相失压起始时刻正向有功电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLastLostStartBForwardPKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识10 02 02 01  页码00
	updateIdentifier(0x10,0x02,0x02,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[6],0);		//"B"
	update8576Buffer(TruthTables01[37],0);	//"失"	
	update8576Buffer(TruthTables01[40],0);	//"压"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateLastLostEndBForwardPKwh
** 功能描述: 更新缓冲区的 最近一次B相失压结束时刻正向有功电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLastLostEndBForwardPKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识10 02 26 01  页码00
	updateIdentifier(0x10,0x02,0x26,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[6],0);		//"B"
	update8576Buffer(TruthTables01[37],0);	//"失"	
	update8576Buffer(TruthTables01[40],0);	//"压"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateLastLostStartBReversePKwh
** 功能描述: 更新缓冲区的 最近一次B相失压起始时刻反向有功电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLastLostStartBReversePKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识10 02 03 01  页码00
	updateIdentifier(0x10,0x02,0x03,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[6],0);		//"B"
	update8576Buffer(TruthTables01[37],0);	//"失"	
	update8576Buffer(TruthTables01[40],0);	//"压"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateLastLostEndBReversePKwh
** 功能描述: 更新缓冲区的 最近一次B相失压结束时刻反向有功电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLastLostEndBReversePKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识10 02 27 01  页码00
	updateIdentifier(0x10,0x02,0x27,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[6],0);		//"B"
	update8576Buffer(TruthTables01[37],0);	//"失"	
	update8576Buffer(TruthTables01[40],0);	//"压"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateLastLostStartCForwardPKwh
** 功能描述: 更新缓冲区的 最近一次C相失压起始时刻正向有功电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLastLostStartCForwardPKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识10 03 02 01  页码00
	updateIdentifier(0x10,0x03,0x02,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[5],0);		//"C"
	update8576Buffer(TruthTables01[37],0);	//"失"	
	update8576Buffer(TruthTables01[40],0);	//"压"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateLastLostEndCForwardPKwh
** 功能描述: 更新缓冲区的 最近一次C相失压结束时刻正向有功电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLastLostEndCForwardPKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识10 03 26 01  页码00
	updateIdentifier(0x10,0x03,0x26,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[5],0);		//"C"
	update8576Buffer(TruthTables01[37],0);	//"失"	
	update8576Buffer(TruthTables01[40],0);	//"压"
	update8576Buffer(TruthTables01[22],0);	//"正"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateLastLostStartCReversePKwh
** 功能描述: 更新缓冲区的 最近一次C相失压起始时刻反向有功电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLastLostStartCReversePKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识10 03 03 01  页码00
	updateIdentifier(0x10,0x03,0x03,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[5],0);		//"C"
	update8576Buffer(TruthTables01[37],0);	//"失"	
	update8576Buffer(TruthTables01[40],0);	//"压"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updateLastLostEndCReversePKwh
** 功能描述: 更新缓冲区的 最近一次C相失压结束时刻反向有功电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX  XX. XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateLastLostEndCReversePKwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识10 03 27 01  页码00
	updateIdentifier(0x10,0x03,0x27,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[5],0);		//"C"
	update8576Buffer(TruthTables01[37],0);	//"失"	
	update8576Buffer(TruthTables01[40],0);	//"压"
	update8576Buffer(TruthTables01[23],0);	//"反"
	update8576Buffer(TruthTables01[26],0);	//"向"
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
	update8576Buffer(TruthTables01[54],0);	//"S38-h"
}

/*********************************************************************************************************
** 函数名称: updatePhaseAVoltage
** 功能描述: 更新缓冲区的 A相电压
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  X.X
**                     d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePhaseAVoltage(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,0x00,d1,d0,0x02);	//0x04 = 0000 0010 ,为1的位对应的小数点儿显示
	//数据标识02 01 01 00  页码00
	updateIdentifier(0x02,0x01,0x01,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[7],0);		//"A"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[40],0);	//"压"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
}

/*********************************************************************************************************
** 函数名称: updatePhaseBVoltage
** 功能描述: 更新缓冲区的 B相电压
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  X.X
**                     d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePhaseBVoltage(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,0x00,d1,d0,0x02);	//0x04 = 0000 0010 ,为1的位对应的小数点儿显示
	//数据标识02 01 02 00  页码00
	updateIdentifier(0x02,0x01,0x02,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[6],0);		//"B"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[40],0);	//"压"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
}

/*********************************************************************************************************
** 函数名称: updatePhaseBVoltage
** 功能描述: 更新缓冲区的 C相电压
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  X.X
**                     d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePhaseCVoltage(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,0x00,d1,d0,0x02);	//0x04 = 0000 0010 ,为1的位对应的小数点儿显示
	//数据标识02 01 03 00  页码00
	updateIdentifier(0x02,0x01,0x03,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[5],0);		//"C"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[40],0);	//"压"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
}

/*********************************************************************************************************
** 函数名称: updatePhaseACurrent
** 功能描述: 更新缓冲区的 A相电流
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  X.X  XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePhaseACurrent(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x08);	//0x08 = 0000 1000 ,为1的位对应的小数点儿显示
	//数据标识02 02 01 00  页码00
	updateIdentifier(0x02,0x02,0x01,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[7],0);		//"A"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[41],0);	//"流"
	update8576Buffer(TruthTables01[53],0);	//"KWH-A"
}

/*********************************************************************************************************
** 函数名称: updatePhaseBCurrent
** 功能描述: 更新缓冲区的 B相电流
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  X.X  XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePhaseBCurrent(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x08);	//0x08 = 0000 1000 ,为1的位对应的小数点儿显示
	//数据标识02 02 02 00  页码00
	updateIdentifier(0x02,0x02,0x02,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[6],0);		//"B"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[41],0);	//"流"
	update8576Buffer(TruthTables01[53],0);	//"KWH-A"
}

/*********************************************************************************************************
** 函数名称: updatePhaseCCurrent
** 功能描述: 更新缓冲区的 C相电流
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  X.X  XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePhaseCCurrent(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x08);	//0x08 = 0000 1000 ,为1的位对应的小数点儿显示
	//数据标识02 02 03 00  页码00
	updateIdentifier(0x02,0x02,0x03,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[5],0);		//"C"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[41],0);	//"流"
	update8576Buffer(TruthTables01[53],0);	//"KWH-A"
}

/*********************************************************************************************************
** 函数名称: updateInstanTotalKw
** 功能描述: 更新缓冲区的 瞬时总有功功率
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX. XX  XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateInstanTotalPKw(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x10);	//0x10 = 0001 0000 ,为1的位对应的小数点儿显示
	//数据标识02 03 00 00  页码00
	updateIdentifier(0x02,0x03,0x00,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[44],0);	//"功率"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
}

/*********************************************************************************************************
** 函数名称: updateInstanAPKw
** 功能描述: 更新缓冲区的 瞬时A相有功功率
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX. XX  XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateInstanAPKw(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x10);	//0x10 = 0001 0000 ,为1的位对应的小数点儿显示
	//数据标识02 03 01 00  页码00
	updateIdentifier(0x02,0x03,0x01,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[7],0);		//"A"
	update8576Buffer(TruthTables01[44],0);	//"功率"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
}

/*********************************************************************************************************
** 函数名称: updateInstanBPKw
** 功能描述: 更新缓冲区的 瞬时B相有功功率
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX. XX  XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateInstanBPKw(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x10);	//0x10 = 0001 0000 ,为1的位对应的小数点儿显示
	//数据标识02 03 02 00  页码00
	updateIdentifier(0x02,0x03,0x02,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[6],0);		//"B"
	update8576Buffer(TruthTables01[44],0);	//"功率"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
}

/*********************************************************************************************************
** 函数名称: updateInstanCPKw
** 功能描述: 更新缓冲区的 瞬时C相有功功率
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX. XX  XX
**                     d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateInstanCPKw(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x10);	//0x10 = 0001 0000 ,为1的位对应的小数点儿显示
	//数据标识02 03 03 00  页码00
	updateIdentifier(0x02,0x03,0x03,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[31],0);	//"有"
	update8576Buffer(TruthTables01[30],0);	//"功"
	update8576Buffer(TruthTables01[5],0);		//"C"
	update8576Buffer(TruthTables01[44],0);	//"功率"
	update8576Buffer(TruthTables01[48],0);	//"S37'-k"
	update8576Buffer(TruthTables01[56],0);	//"S37-V"
	update8576Buffer(TruthTables01[57],0);	//"X-V"
}

/*********************************************************************************************************
** 函数名称: updateInstanTotalPowerFactor
** 功能描述: 更新缓冲区的 瞬时总功率因数
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：X.X  XX
**                     d1   d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateInstanTotalPowerFactor(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,0x00,d1,d0,0x08);	//0x08 = 0000 1000 ,为1的位对应的小数点儿显示
	//数据标识02 06 00 00  页码00
	updateIdentifier(0x02,0x06,0x00,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[42],0);	//"总"
	update8576Buffer(TruthTables01[15],0);	//"COS"
	update8576Buffer(TruthTables01[17],0);	//"相角",即 “Φ”
}

/*********************************************************************************************************
** 函数名称: updateInstanAPowerFactor
** 功能描述: 更新缓冲区的 瞬时A相功率因数
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：X.X  XX
**                     d1   d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateInstanAPowerFactor(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,0x00,d1,d0,0x08);	//0x08 = 0000 1000 ,为1的位对应的小数点儿显示
	//数据标识02 06 01 00  页码00
	updateIdentifier(0x02,0x06,0x01,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[7],0);		//"A"
	update8576Buffer(TruthTables01[15],0);	//"COS"
	update8576Buffer(TruthTables01[17],0);	//"相角",即 “Φ”
}

/*********************************************************************************************************
** 函数名称: updateInstanBPowerFactor
** 功能描述: 更新缓冲区的 瞬时B相功率因数
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：X.X  XX
**                     d1   d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateInstanBPowerFactor(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,0x00,d1,d0,0x08);	//0x08 = 0000 1000 ,为1的位对应的小数点儿显示
	//数据标识02 06 02 00  页码00
	updateIdentifier(0x02,0x06,0x02,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[6],0);		//"B"
	update8576Buffer(TruthTables01[15],0);	//"COS"
	update8576Buffer(TruthTables01[17],0);	//"相角",即 “Φ”
}

/*********************************************************************************************************
** 函数名称: updateInstanCPowerFactor
** 功能描述: 更新缓冲区的 瞬时C相功率因数
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：X.X  XX
**                     d1   d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateInstanCPowerFactor(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,0x00,d1,d0,0x08);	//0x08 = 0000 1000 ,为1的位对应的小数点儿显示
	//数据标识02 06 03 00  页码00
	updateIdentifier(0x02,0x06,0x03,0x00,0x00);
	//提示字符
	update8576Buffer(TruthTables01[5],0);		//"C"
	update8576Buffer(TruthTables01[15],0);	//"COS"
	update8576Buffer(TruthTables01[17],0);	//"相角",即 “Φ”
}

/*********************************************************************************************************
** 函数名称: updateJianPrice
** 功能描述: 更新缓冲区的 当前尖费率电价
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX. XX  XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateJianPrice(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x10);	//0x08 = 0001 0000 ,为1的位对应的小数点儿显示
	//数据标识04 05 01 01  页码00
	updateIdentifier(0x04,0x05,0x01,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[43],0);	//"尖"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[36],0);	//"价"
	update8576Buffer(TruthTables01[59],0);	//"元"
}

/*********************************************************************************************************
** 函数名称: updateFengPrice
** 功能描述: 更新缓冲区的 当前峰费率电价
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX. XX  XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateFengPrice(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x10);	//0x08 = 0001 0000 ,为1的位对应的小数点儿显示
	//数据标识04 05 01 02  页码00
	updateIdentifier(0x04,0x05,0x01,0x02,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[47],0);	//"峰"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[36],0);	//"价"
	update8576Buffer(TruthTables01[59],0);	//"元"
}

/*********************************************************************************************************
** 函数名称: updatePingPrice
** 功能描述: 更新缓冲区的 当前平费率电价
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX. XX  XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updatePingPrice(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x10);	//0x08 = 0001 0000 ,为1的位对应的小数点儿显示
	//数据标识04 05 01 03  页码00
	updateIdentifier(0x04,0x05,0x01,0x03,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[46],0);	//"平"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[36],0);	//"价"
	update8576Buffer(TruthTables01[59],0);	//"元"
}

/*********************************************************************************************************
** 函数名称: updateGuPrice
** 功能描述: 更新缓冲区的 当前谷费率电价
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：XX  XX. XX  XX
**                     d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateGuPrice(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x10);	//0x08 = 0001 0000 ,为1的位对应的小数点儿显示
	//数据标识04 05 01 04  页码00
	updateIdentifier(0x04,0x05,0x01,0x04,0x00);
	//提示字符
	update8576Buffer(TruthTables01[3],0);		//"当前"
	update8576Buffer(TruthTables01[51],0);	//"谷"
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[36],0);	//"价"
	update8576Buffer(TruthTables01[59],0);	//"元"
}


/*********************************************************************************************************
** 函数名称: updateStep1Kwh
** 功能描述: 更新缓冲区的 阶梯1电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：1- XX  XX. XX
**                        d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateStep1Kwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识04 06 00 01  页码00
	updateIdentifier(0x04,0x06,0x00,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[21],0);	//"阶梯"	
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
}

/*********************************************************************************************************
** 函数名称: updateStep2Kwh
** 功能描述: 更新缓冲区的 阶梯2电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：2- XX  XX. XX
**                        d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateStep2Kwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识04 06 00 02  页码00
	updateIdentifier(0x04,0x06,0x00,0x02,0x00);
	//提示字符
	update8576Buffer(TruthTables01[21],0);	//"阶梯"	
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
}
/*********************************************************************************************************
** 函数名称: updateStep3Kwh
** 功能描述: 更新缓冲区的 阶梯3电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：3- XX  XX. XX
**                        d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateStep3Kwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识04 06 00 03  页码00
	updateIdentifier(0x04,0x06,0x00,0x03,0x00);
	//提示字符
	update8576Buffer(TruthTables01[21],0);	//"阶梯"	
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
}
/*********************************************************************************************************
** 函数名称: updateStep4Kwh
** 功能描述: 更新缓冲区的 阶梯4电量
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：4- XX  XX. XX
**                        d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateStep4Kwh(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,d2,d1,d0,0x04);	//0x04 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识04 06 00 04  页码00
	updateIdentifier(0x04,0x06,0x00,0x04,0x00);
	//提示字符
	update8576Buffer(TruthTables01[21],0);	//"阶梯"	
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[29],0);	//"量"
}

/*********************************************************************************************************
** 函数名称: updateStep1Price
** 功能描述: 更新缓冲区的 阶梯1电价
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：1-XX  XX. XX  XX
**                       d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateStep1Price(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x10);	//0x10 = 0001 0000 ,为1的位对应的小数点儿显示
	//数据标识04 06 01 01  页码00
	updateIdentifier(0x04,0x06,0x01,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[21],0);	//"阶梯"	
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[36],0);	//"价"
}

/*********************************************************************************************************
** 函数名称: updateStep2Price
** 功能描述: 更新缓冲区的 阶梯2电价
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：2-XX  XX. XX  XX
**                       d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateStep2Price(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x10);	//0x10 = 0001 0000 ,为1的位对应的小数点儿显示
	//数据标识04 06 01 02  页码00
	updateIdentifier(0x04,0x06,0x01,0x02,0x00);
	//提示字符
	update8576Buffer(TruthTables01[21],0);	//"阶梯"	
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[36],0);	//"价"
}
/*********************************************************************************************************
** 函数名称: updateStep3Price
** 功能描述: 更新缓冲区的 阶梯3电价
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：3-XX  XX. XX  XX
**                       d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateStep3Price(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x10);	//0x10 = 0001 0000 ,为1的位对应的小数点儿显示
	//数据标识04 06 01 03  页码00
	updateIdentifier(0x04,0x06,0x01,0x03,0x00);
	//提示字符
	update8576Buffer(TruthTables01[21],0);	//"阶梯"	
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[36],0);	//"价"
}
/*********************************************************************************************************
** 函数名称: updateStep4Price
** 功能描述: 更新缓冲区的 阶梯4电价
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：4-XX  XX. XX  XX
**                       d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateStep4Price(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x10);	//0x10 = 0001 0000 ,为1的位对应的小数点儿显示
	//数据标识04 06 01 04  页码00
	updateIdentifier(0x04,0x06,0x01,0x04,0x00);
	//提示字符
	update8576Buffer(TruthTables01[21],0);	//"阶梯"	
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[36],0);	//"价"
}
/*********************************************************************************************************
** 函数名称: updateStep5Price
** 功能描述: 更新缓冲区的 阶梯5电价
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：5-XX  XX. XX  XX
**                       d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateStep5Price(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x10);	//0x10 = 0001 0000 ,为1的位对应的小数点儿显示
	//数据标识04 06 01 05  页码00
	updateIdentifier(0x04,0x06,0x01,0x05,0x00);
	//提示字符
	update8576Buffer(TruthTables01[21],0);	//"阶梯"	
	update8576Buffer(TruthTables01[25],0);	//"电"
	update8576Buffer(TruthTables01[36],0);	//"价"
}

/*********************************************************************************************************
** 函数名称: updateAlarmMoney1
** 功能描述: 更新缓冲区的 报警金额1
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：  XX  XX  XX. XX
**                       d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateAlarmMoney1(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x10 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识04 00 10 01  页码00
	updateIdentifier(0x04,0x00,0x10,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[59],0);	//"元"
}

/*********************************************************************************************************
** 函数名称: updateAlarmMoney2
** 功能描述: 更新缓冲区的 报警金额2
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：  XX  XX  XX. XX
**                       d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateAlarmMoney2(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x10 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识04 00 10 02  页码00
	updateIdentifier(0x04,0x00,0x10,0x02,0x00);
	//提示字符
	update8576Buffer(TruthTables01[59],0);	//"元"
}

/*********************************************************************************************************
** 函数名称: updateOverdraftMoney
** 功能描述: 更新缓冲区的 透支金额
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：  XX  XX  XX. XX
**                       d3  d2  d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateOverdraftMoney(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(d3,d2,d1,d0,0x04);	//0x10 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识00 90 02 01  页码00
	updateIdentifier(0x00,0x90,0x02,0x01,0x00);
	//提示字符
	update8576Buffer(TruthTables01[59],0);	//"元"
	update8576Buffer(TruthTables02[11],0);	//"透支"
}

/*********************************************************************************************************
** 函数名称: updateCashInDate
** 功能描述: 更新缓冲区的 结算日
** 输　入: 	 U8 d3 —— 最高字节
**           U8 d2 —— 次高字节
**           U8 d1 —— 次低字节
**           U8 d0 —— 最低字节
**           数据格式：  XX. XX
**                       d1  d0
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updateCashInDate(U8 d3,U8 d2,U8 d1,U8 d0){
	//清除液晶上半部分的显示数据
	clearTopData(&pcf8576DataBuf);
	clearTopData(&pcf8576DataBufGlitter);
	//数据内容
	updateContent(0x00,0x00,d1,d0,0x04);	//0x10 = 0000 0100 ,为1的位对应的小数点儿显示
	//数据标识04 00 0B 01  页码00
	updateIdentifier(0x04,0x00,0x0B,0x01,0x00);
}
/*********************************************************************************************************
** 函数名称: display_err_num
** 功能描述: 显示错误编码
** 输　入: 	 
**           
** 输　出:   
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void display_err_num(U8 num){
	//清除液晶上半部分的显示数据
//	clearTopData(&pcf8576DataBuf);
//	clearTopData(&pcf8576DataBufGlitter);
	lcd_all_clear();
	//数据内容
	displayNumber(4,'e',0);		
	displayNumber(5,'r',0);		
	displayNumber(6,'r',0);		
	displayNumber(7,'-',0);		
	displayNumber(8,num/10,0);		
	displayNumber(9,num%10,0);		
}
/*********************************************************************************************************
** 函数名称: updata_display_item
** 功能描述: 跟新显示项
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void updata_display_item(void)
{
	U8 buf[200],flag = 0;
	
	switch(lcd_control.dataID)
	{
		case 0x04000101://当前日期
		{
			updateDate(0,currentClock[5],currentClock[4],currentClock[3]);			
		}
		break;
		case 0x04000102://当前时间
		{
			updateTime(0,currentClock[2],currentClock[1],currentClock[0]);
		}
		break;
		case 0x00900200: //剩余金额
		{
			updateLeavingMoney(0,0,0,0);
		}
		break;
		case 0x0://当前组合有功总
		{
			if(read_one_energy_info(0,0,0,buf,&flag)){
				updateComboPTotalKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
				if(flag) update8576Buffer(TruthTables01[62],0);	//"负号"
					else  update8576Buffer(TruthTables01[62],2);	//"负号"

			}
		}
		break;
		case 0x00010000://当前有功总
		{
			if(read_one_energy_info(0,0,1,buf,&flag)){
				updateForwardPTotalKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00010100://当前有功 尖
		{ 
			if(read_one_energy_info(0,1,1,buf,&flag)){
				updateForwardPJianKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}	
		}
		break;
		case 0x00010200://当前有功 峰
		{
			if(read_one_energy_info(0,2,1,buf,&flag)){
				updateForwardPFengKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00010300://当前有功 平
		{
			if(read_one_energy_info(0,3,1,buf,&flag)){
				updateForwardPPingKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00010400://当前有功 谷
		{
			if(read_one_energy_info(0,4,1,buf,&flag)){
				updateForwardPGuKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x01010000: //当前最大需量
		{
			if(lcd_control.ID_num == 0){
				if(read_one_demand_info(0,0,1,buf,&flag)){
					updateForwardPTatalMaxKw(0,BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
				}
			}else if(lcd_control.ID_num == 1){
				if(read_one_demand_info(0,0,1,buf,&flag)){
					updateForwardPTatalMaxKwDate(0,BCDToDec(buf[7]),BCDToDec(buf[6]),BCDToDec(buf[5]));
				}
			}else if(lcd_control.ID_num == 2){
				if(read_one_demand_info(0,0,1,buf,&flag)){
					updateForwardPTatalMaxKwTime(0,BCDToDec(buf[4]),BCDToDec(buf[3]),0);
				}
			}
		}
		break;
		case 0x00020000:   //反向总
		{
			if(read_one_energy_info(0,0,2,buf,&flag)){
				updateReversePTotalKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00020100: //反向尖
		{
			if(read_one_energy_info(0,1,2,buf,&flag)){
				updateReversePJianKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00020200: //反向峰
		{
			if(read_one_energy_info(0,2,2,buf,&flag)){
			 	updateReversePFengKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00020300: //反向平
		{
			if(read_one_energy_info(0,3,2,buf,&flag)){
			 	updateReversePPingKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00020400:  //反向谷
		{
			if(read_one_energy_info(0,4,2,buf,&flag)){
				updateReversePGuKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x01020000:   //反向最大需量
		{
			if(lcd_control.ID_num == 0){
				if(read_one_demand_info(0,0,2,buf,&flag)){
					updateReversePTatalMaxKw(0,BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
				}
			}else if(lcd_control.ID_num == 1){
				if(read_one_demand_info(0,0,2,buf,&flag)){
					updateReversePTatalMaxKwDate(0,BCDToDec(buf[7]),BCDToDec(buf[6]),BCDToDec(buf[5]));
				}
			}else if(lcd_control.ID_num == 2){
				if(read_one_demand_info(0,0,2,buf,&flag)){
					updateReversePTatalMaxKwTime(0,BCDToDec(buf[4]),BCDToDec(buf[3]),0);
				}
			}
		}
		break;
		case 0x00030000://当前组合无功1
		{
			if(read_one_energy_info(0,0,3,buf,&flag)){
				updateCombo1QTotalKvarh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
				if(flag) update8576Buffer(TruthTables01[62],0);	//"负号"
					else  update8576Buffer(TruthTables01[62],2);	//"负号"

			}
		}
		break;
		case 0x00040000://当前组合无功2
		{
			if(read_one_energy_info(0,0,4,buf,&flag)){
				updateCombo2QTotalKvarh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
				if(flag) update8576Buffer(TruthTables01[62],0);	//"负号"
					else  update8576Buffer(TruthTables01[62],2);	//"负号"

			}
		}
		break;
		case 0x00050000://第一象限无功
		{
			if(read_one_energy_info(0,0,5,buf,&flag)){
				updateQuadrant1QTotalKvarh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00060000://第二象限无功
		{
			if(read_one_energy_info(0,0,6,buf,&flag)){
				updateQuadrant2QTotalKvarh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00070000://第三象限无功
		{
			if(read_one_energy_info(0,0,7,buf,&flag)){
				updateQuadrant3QTotalKvarh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00080000://第四象限无功
		{
			if(read_one_energy_info(0,0,8,buf,&flag)){
				updateQuadrant4QTotalKvarh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00010001://上1 正向有功总
		{
			if(read_one_energy_info(1,0,1,buf,&flag)){
				updatePreviousPTotalKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00010101://上1 正向有功尖
		{
	   		if(read_one_energy_info(1,1,1,buf,&flag)){
				updatePreviousPJianKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00010201://上1 正向有功峰
		{
			if(read_one_energy_info(1,2,1,buf,&flag)){
				updatePreviousPFengKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00010301://上1 正向有功平
		{
			if(read_one_energy_info(1,3,1,buf,&flag)){
				updatePreviousPPingKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00010401://上1 正向有功谷
		{
			if(read_one_energy_info(1,4,1,buf,&flag)){
				updatePreviousPGuKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x01010001: //上1 最大需量
		{
			if(lcd_control.ID_num == 0){
				if(read_one_demand_info(1,0,1,buf,&flag)){
					updatePreviousMaxKw(0,BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
				}
			}else if(lcd_control.ID_num == 1){
				if(read_one_demand_info(1,0,1,buf,&flag)){
					updatePreviousMaxKwDate(0,BCDToDec(buf[7]),BCDToDec(buf[6]),BCDToDec(buf[5]));
				}
			}else if(lcd_control.ID_num == 2){
				if(read_one_demand_info(1,0,1,buf,&flag)){
					updatePreviousMaxKwTime(0,BCDToDec(buf[4]),BCDToDec(buf[3]),0);
				}
			}
		}
		break;
		case 0x00020001://上1 反向有功
		{
			if(read_one_energy_info(1,0,2,buf,&flag)){
				updatePreviousReversePTotalKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00020101://上1 反向有功尖
		{
			if(read_one_energy_info(1,0,2,buf,&flag)){
				updatePreviousReversePJianKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00020201://上1 反向有功峰
		{
			if(read_one_energy_info(1,0,2,buf,&flag)){
				updatePreviousReversePFengKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00020301://上1 反向有功平
		{
			if(read_one_energy_info(1,0,2,buf,&flag)){
				updatePreviousReversePPingKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}	
		}
		break;
		case 0x00020401://上1 反向有功谷
		{
			if(read_one_energy_info(1,0,2,buf,&flag)){
				updatePreviousReversePGuKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x01020001:   //上1 反向最大需量
		{
			if(lcd_control.ID_num == 0){
				if(read_one_demand_info(1,0,2,buf,&flag)){
					updatePreviousReverseMaxKw(0,BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
				}
			}else if(lcd_control.ID_num == 1){
				if(read_one_demand_info(1,0,2,buf,&flag)){
					updatePreviousReverseMaxKwDate(0,BCDToDec(buf[7]),BCDToDec(buf[6]),BCDToDec(buf[5]));
				}
			}else if(lcd_control.ID_num == 2){
				if(read_one_demand_info(1,0,2,buf,&flag)){
					updatePreviousReverseMaxKwTime(0,BCDToDec(buf[4]),BCDToDec(buf[3]),0);
				}
			}
		}
		break;
		case 0x00050001://上1 第一象限无功
		{
			if(read_one_energy_info(1,0,5,buf,&flag)){
				updatePreviousQuadrant1QTotalKvarh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00060001://上1 第二象限无功
		{
			if(read_one_energy_info(1,0,6,buf,&flag)){
				updatePreviousQuadrant2QTotalKvarh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00070001://上1 第三象限无功
		{
			if(read_one_energy_info(1,0,7,buf,&flag)){
				updatePreviousQuadrant3QTotalKvarh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x00080001://上1 第四象限无功
		{
			if(read_one_energy_info(1,0,8,buf,&flag)){
				updatePreviousQuadrant4QTotalKvarh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x04000401: //通讯地址 2屏
		{
			if(lcd_control.ID_num == 0){
				buf[3] = BCDToDec(meter_state_info.meter_comm_addr[3]);
				buf[2] = BCDToDec(meter_state_info.meter_comm_addr[2]);
				buf[1] = BCDToDec(meter_state_info.meter_comm_addr[1]);
				buf[0] = BCDToDec(meter_state_info.meter_comm_addr[0]);
				updateMeterNumberLow8(buf[3],buf[2],buf[1],buf[0]);	
			}else if(lcd_control.ID_num == 1){
				buf[1] = BCDToDec(meter_state_info.meter_comm_addr[5]);
				buf[0] = BCDToDec(meter_state_info.meter_comm_addr[4]);
				updateMeterNumberHigh4(0,0,buf[1],buf[0]);	
			}
		}
		break;
		case 0x04000703: //通讯波特率
		{
			U32 speed = 0;
			speed = get_uart_speed(UART0,uart0_645frame.speed_state);
			buf[0] = speed%100;
			buf[1] = speed/100%100;
			buf[2] = speed/10000%100;
			updateBaudRate(0,buf[2],buf[1],buf[0]);
		}
		break;
		case 0x04000409: //有功常数
		{
			buf[0] = ACTIVE_PULSE_CONSTANT%100;
			buf[1] = ACTIVE_PULSE_CONSTANT/100%100;
			buf[2] = ACTIVE_PULSE_CONSTANT/10000%100;
			updateImpKwh(0,buf[2],buf[1],buf[0]);
		}
		break;
		case 0x0400040a: //无功常数
		{
			buf[0] = REACTIVE_PULSE_CONSTANT%100;
			buf[1] = REACTIVE_PULSE_CONSTANT/100%100;
			buf[2] = REACTIVE_PULSE_CONSTANT/10000%100;
			updateImpKvarh(0,buf[2],buf[1],buf[0]);
		}
		break;
		case 0x0280000a:  //时钟电池使用时间
		{
			if(!read_data(3,EVENT_STATE_ADDR+0x80,4,buf)){
				buf[3] = 0;
				buf[2] = 0;
				buf[1] = 0;
				buf[0] = 0;
			}
			if((buf[0]&buf[1]&buf[2]&buf[3])==0x0ff){
		   	buf[3] = 0;
				buf[2] = 0;
				buf[1] = 0;
				buf[0] = 0;
			}
			updateClockBattUseTime(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
		}
		break;
		case 0x03300001: //最近一次编程 2屏 日期 时间
		{
			if(com_read_event_data(UART0,0x30,0,1,buf,&flag) != 1){	
				buf[5] = 0;buf[4] = 0;buf[3] = 0;buf[2] = 0;buf[1] = 0;buf[0] = 0;
			}
			if(lcd_control.ID_num == 0){
				updateLastTimeProgramDate(0,BCDToDec(buf[5]),BCDToDec(buf[4]),BCDToDec(buf[3]));
			}else if(lcd_control.ID_num == 1){
				updateLastTimeProgramTime(0,BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
			}
		}
		break;
		case 0x10000001: //总失压次数	  国网规范 暂没做
		{
			if(com_read_state_event_data(UART0,0x10,0,0,1,buf,&flag) != 1){
				buf[3] = 0;
				buf[2] = 0;
				buf[1] = 0;
				buf[0] = 0;
			}
			updateLostVoltageTotalTimes(0,BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));	
		}
		break;
		case 0x10000002: //总失压累计时间
		{
			if(com_read_state_event_data(UART0,0x10,0,0,2,buf,&flag) != 1){
				buf[3] = 0;
				buf[2] = 0;
				buf[1] = 0;
				buf[0] = 0;
			}
			updateLostVoltageTotalTime(0,BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));	
		}
		break;
		case 0x10000101: //最近一次失压起始日期、时间 2屏
		{
			if(com_read_state_event_data(UART0,0x10,0,1,1,buf,&flag) != 1){
				buf[5] = 0;
				buf[4] = 0;
				buf[3] = 0;
				buf[2] = 0;
				buf[1] = 0;
				buf[0] = 0;
			}
			if(lcd_control.ID_num == 0){
				updateLastLostVoltageStartDate(0,BCDToDec(buf[5]),BCDToDec(buf[4]),BCDToDec(buf[3]));	
			}else if(lcd_control.ID_num == 1){
				updateLastLostVoltageStartTime(0,BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));	
			}
		}
		break;
		case 0x10000201: //最近一次失压结束日期、时间 2屏
		{
			
			if(com_read_state_event_data(UART0,0x10,0,0x2,1,buf,&flag) != 1){
				buf[5] = 0;
				buf[4] = 0;
				buf[3] = 0;
				buf[2] = 0;
				buf[1] = 0;
				buf[0] = 0;
			}
			if(lcd_control.ID_num == 0){
				updateLastLostVoltageEndDate(0,BCDToDec(buf[5]),BCDToDec(buf[4]),BCDToDec(buf[3]));	
			}else if(lcd_control.ID_num == 1){
				updateLastLostVoltageEndTime(0,BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));	
			}
		}
		break;
		case 0x10010201: //最近一次A相失压起始时刻正向有功电量
		{
			if(com_read_state_event_data(UART0,0x10,1,0x2,1,buf,&flag) != 1){
				buf[3] = 0;
				buf[2] = 0;
				buf[1] = 0;
				buf[0] = 0;
			}
			updateLastLostStartAForwardPKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
		}
		break;
		case 0x10012601: //最近一次A相失压结束时刻正向有功电量
		{
			if(com_read_state_event_data(UART0,0x10,1,0x26,1,buf,&flag) != 1){
				buf[3] = 0;
				buf[2] = 0;
				buf[1] = 0;
				buf[0] = 0;
			}
			updateLastLostEndAForwardPKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
		}
		break;
		case 0x10010301: //最近一次A相失压起始时刻反向有功电量
		{
			if(com_read_state_event_data(UART0,0x10,1,0x3,1,buf,&flag) != 1){
				buf[3] = 0;
				buf[2] = 0;
				buf[1] = 0;
				buf[0] = 0;
			}
			updateLastLostStartAReversePKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
		}
		break;
		case 0x10012701: //最近一次A相失压结束时刻反向有功电量
		{
			if(com_read_state_event_data(UART0,0x10,1,0x27,1,buf,&flag) != 1){
				buf[3] = 0;
				buf[2] = 0;
				buf[1] = 0;
				buf[0] = 0;
			}
			updateLastLostEndAReversePKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
		}
		break;
		case 0x10020201: //最近一次B相失压起始时刻正向有功电量
		{
			if(com_read_state_event_data(UART0,0x10,2,0x2,1,buf,&flag) != 1){
				buf[3] = 0;
				buf[2] = 0;
				buf[1] = 0;
				buf[0] = 0;
			}
			updateLastLostStartBForwardPKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
		}
		break;
		case 0x10022601: //最近一次B相失压结束时刻正向有功电量
		{
			if(com_read_state_event_data(UART0,0x10,2,0x26,1,buf,&flag) != 1){
				buf[3] = 0;
				buf[2] = 0;
				buf[1] = 0;
				buf[0] = 0;
			}
			updateLastLostEndBForwardPKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
		}
		break;
		case 0x10020301: //最近一次B相失压起始时刻反向有功电量
		{
			if(com_read_state_event_data(UART0,0x10,2,0x3,1,buf,&flag) != 1){
				buf[3] = 0;
				buf[2] = 0;
				buf[1] = 0;
				buf[0] = 0;
			}
			updateLastLostStartBReversePKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
		}
		break;
		case 0x10022701: //最近一次B相失压结束时刻反向有功电量
		{
			if(com_read_state_event_data(UART0,0x10,2,0x27,1,buf,&flag) != 1){
				buf[3] = 0;
				buf[2] = 0;
				buf[1] = 0;
				buf[0] = 0;
			}
			updateLastLostEndBReversePKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
		}
		break;
		case 0x10030201: //最近一次C相失压起始时刻正向有功电量
		{
			if(com_read_state_event_data(UART0,0x10,3,0x2,1,buf,&flag) != 1){
				buf[3] = 0;
				buf[2] = 0;
				buf[1] = 0;
				buf[0] = 0;
			}
			updateLastLostStartCForwardPKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
		}
		break;
		case 0x10032601: //最近一次C相失压结束时刻正向有功电量
		{
			if(com_read_state_event_data(UART0,0x10,3,0x26,1,buf,&flag) != 1){
				buf[3] = 0;
				buf[2] = 0;
				buf[1] = 0;
				buf[0] = 0;
			}
			updateLastLostEndCForwardPKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
		}
		break;
		case 0x10030301: //最近一次C相失压起始时刻反向有功电量
		{
			if(com_read_state_event_data(UART0,0x10,3,0x3,1,buf,&flag) != 1){
				buf[3] = 0;
				buf[2] = 0;
				buf[1] = 0;
				buf[0] = 0;
			}
			updateLastLostStartCReversePKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
		}
		break;
		case 0x10032701: //最近一次C相失压结束时刻反向有功电量
		{
			if(com_read_state_event_data(UART0,0x10,3,0x27,1,buf,&flag) != 1){
				buf[3] = 0;
				buf[2] = 0;
				buf[1] = 0;
				buf[0] = 0;
			}
			updateLastLostEndCReversePKwh(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
		}
		break;
		case 0x02010100://A相电压
		{
			com_read_variable_data(1,1,0,buf,&flag);
			updatePhaseAVoltage(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
		}
		break;
		case 0x02010200://B相电压
		{
			com_read_variable_data(1,2,0,buf,&flag);
			updatePhaseBVoltage(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
		}
		break;
		case 0x02010300://C相电压
		{
			com_read_variable_data(1,3,0,buf,&flag);
			updatePhaseCVoltage(BCDToDec(buf[3]),BCDToDec(buf[2]),BCDToDec(buf[1]),BCDToDec(buf[0]));
		}
		break;
		case 0x02020100://A相电流
		{
			com_read_variable_data(2,1,0,buf,&flag);
			updatePhaseACurrent(BCDToDec(buf[3]),BCDToDec(buf[2]&0x7f),BCDToDec(buf[1]),BCDToDec(buf[0]));
			if(buf[2] & 0x80)	update8576Buffer(TruthTables01[62],0);	//"负号"
				else  update8576Buffer(TruthTables01[62],2);	//"负号"
		}
		break;
		case 0x02020200://B相电流
		{
			com_read_variable_data(2,2,0,buf,&flag);
			updatePhaseBCurrent(BCDToDec(buf[3]),BCDToDec(buf[2]&0x7f),BCDToDec(buf[1]),BCDToDec(buf[0]));
			if(buf[2] & 0x80)	update8576Buffer(TruthTables01[62],0);	//"负号"
				else  update8576Buffer(TruthTables01[62],2);	//"负号"
		}
		break;
		case 0x02020300://C相电流
		{
			com_read_variable_data(2,3,0,buf,&flag);
			updatePhaseCCurrent(BCDToDec(buf[3]),BCDToDec(buf[2]&0x7f),BCDToDec(buf[1]),BCDToDec(buf[0]));
			if(buf[2] & 0x80)	update8576Buffer(TruthTables01[62],0);	//"负号"
				else  update8576Buffer(TruthTables01[62],2);	//"负号"
		}
		break;
		case 0x02030000: //瞬时总有功功率
		{
			com_read_variable_data(3,0,0,buf,&flag);
			updateInstanTotalPKw(0,BCDToDec(buf[2]&0x7f),BCDToDec(buf[1]),BCDToDec(buf[0]));
			if(buf[2] & 0x80)	update8576Buffer(TruthTables01[62],0);	//"负号"
				else  update8576Buffer(TruthTables01[62],2);	//"负号"
		}
		break;
		case 0x02030100: //瞬时A相有功功率
		{
			com_read_variable_data(3,1,0,buf,&flag);
			updateInstanAPKw(0,BCDToDec(buf[2]&0x7f),BCDToDec(buf[1]),BCDToDec(buf[0]));
			if(buf[2] & 0x80)	update8576Buffer(TruthTables01[62],0);	//"负号"
				else  update8576Buffer(TruthTables01[62],2);	//"负号"
		}
		break;
		case 0x02030200: //瞬时B相有功功率
		{
			com_read_variable_data(3,2,0,buf,&flag);
			updateInstanBPKw(0,BCDToDec(buf[2]&0x7f),BCDToDec(buf[1]),BCDToDec(buf[0]));
			if(buf[2] & 0x80)	update8576Buffer(TruthTables01[62],0);	//"负号"
				else  update8576Buffer(TruthTables01[62],2);	//"负号"
		}
		break;
		case 0x02030300: //瞬时C相有功功率
		{
			com_read_variable_data(3,3,0,buf,&flag);
			updateInstanCPKw(0,BCDToDec(buf[2]&0x7f),BCDToDec(buf[1]),BCDToDec(buf[0]));
			if(buf[2] & 0x80)	update8576Buffer(TruthTables01[62],0);	//"负号"
				else  update8576Buffer(TruthTables01[62],2);	//"负号"
		}
		break;
		case 0x02060000: //瞬时总功率因数
		{
			com_read_variable_data(6,0,0,buf,&flag);
			updateInstanTotalPowerFactor(0,0,BCDToDec(buf[1]&0x7f),BCDToDec(buf[0]));
			if(buf[1] & 0x80)	update8576Buffer(TruthTables01[62],0);	//"负号"
				else  update8576Buffer(TruthTables01[62],2);	//"负号"
		}
		break;
		case 0x02060100: //瞬时A相功率因数
		{
			com_read_variable_data(6,1,0,buf,&flag);
			updateInstanAPowerFactor(0,0,BCDToDec(buf[1]&0x7f),BCDToDec(buf[0]));
			if(buf[1] & 0x80)	update8576Buffer(TruthTables01[62],0);	//"负号"
				else  update8576Buffer(TruthTables01[62],2);	//"负号"
		}
		break;
		case 0x02060200: //瞬时B相功率因数
		{
			com_read_variable_data(6,2,0,buf,&flag);
			updateInstanBPowerFactor(0,0,BCDToDec(buf[1]&0x7f),BCDToDec(buf[0]));
			if(buf[1] & 0x80)	update8576Buffer(TruthTables01[62],0);	//"负号"
				else  update8576Buffer(TruthTables01[62],2);	//"负号"
		}
		break;
		case 0x02060300: //瞬时C相功率因数
		{
			com_read_variable_data(6,3,0,buf,&flag);
			updateInstanCPowerFactor(0,0,BCDToDec(buf[1]&0x7f),BCDToDec(buf[0]));
			if(buf[1] & 0x80)	update8576Buffer(TruthTables01[62],0);	//"负号"
				else  update8576Buffer(TruthTables01[62],2);	//"负号"
		}
		break;
//		case 0x04050101: //当前尖费率电价
//		{
//			buf[3] = 0;
//			buf[2] = 0;
//			buf[1] = 0;
//			buf[0] = 0;
//			updateJianPrice(buf[3],buf[2],buf[1],buf[0]);
//		}
//		break;
//		case 0x04050102: //当前峰费率电价
//		{
//			buf[3] = 0;
//			buf[2] = 0;
//			buf[1] = 0;
//			buf[0] = 0;
//			updateFengPrice(buf[3],buf[2],buf[1],buf[0]);
//		}
//		break;
//		case 0x04050103: //当前平费率电价
//		{
//			buf[3] = 0;
//			buf[2] = 0;
//			buf[1] = 0;
//			buf[0] = 0;
//			updatePingPrice(buf[3],buf[2],buf[1],buf[0]);
//		}
//		break;
//		case 0x04050104: //当前谷费率电价
//		{
//			buf[3] = 0;
//			buf[2] = 0;
//			buf[1] = 0;
//			buf[0] = 0;
//			updateGuPrice(buf[3],buf[2],buf[1],buf[0]);
//		}
//		break;
//		case 0x04060001: //阶梯1电量
//		{
//			buf[3] = 0;
//			buf[2] = 0;
//			buf[1] = 0;
//			buf[0] = 0;
//			updateStep1Kwh(buf[3],buf[2],buf[1],buf[0]);
//		}
//		break;
//		case 0x04060002: //阶梯2电量
//		{
//			buf[3] = 0;
//			buf[2] = 0;
//			buf[1] = 0;
//			buf[0] = 0;
//			updateStep2Kwh(buf[3],buf[2],buf[1],buf[0]);
//		}
//		break;
//		case 0x04060003: //阶梯3电量
//		{
//			buf[3] = 0;
//			buf[2] = 0;
//			buf[1] = 0;
//			buf[0] = 0;
//			updateStep3Kwh(buf[3],buf[2],buf[1],buf[0]);
//		}
//		break;
//		case 0x04060004: //阶梯4电量
//		{
//			buf[3] = 0;
//			buf[2] = 0;
//			buf[1] = 0;
//			buf[0] = 0;
//			updateStep4Kwh(buf[3],buf[2],buf[1],buf[0]);
//		}
//		break;
//		case 0x04060101: //阶梯1电价
//		{
//			buf[3] = 0;
//			buf[2] = 0;
//			buf[1] = 0;
//			buf[0] = 0;
//			updateStep1Price(buf[3],buf[2],buf[1],buf[0]);
//		}
//		break;
//		case 0x04060102: //阶梯2电价
//		{
//			buf[3] = 0;
//			buf[2] = 0;
//			buf[1] = 0;
//			buf[0] = 0;
//			updateStep2Price(buf[3],buf[2],buf[1],buf[0]);
//		}
//		break;
//		case 0x04060103: //阶梯3电价
//		{
//			buf[3] = 0;
//			buf[2] = 0;
//			buf[1] = 0;
//			buf[0] = 0;
//			updateStep3Price(buf[3],buf[2],buf[1],buf[0]);
//		}
//		break;
//		case 0x04060104: //阶梯4电价
//		{
//			buf[3] = 0;
//			buf[2] = 0;
//			buf[1] = 0;
//			buf[0] = 0;
//			updateStep4Price(buf[3],buf[2],buf[1],buf[0]);
//		}
//		break;
//		case 0x04060105: //阶梯5电价
//		{
//			buf[3] = 0;
//			buf[2] = 0;
//			buf[1] = 0;
//			buf[0] = 0;
//			updateStep5Price(buf[3],buf[2],buf[1],buf[0]);
//		}
//		break;
//		case 0x04001001: //报警金额1
//		{
//			buf[3] = 0;
//			buf[2] = 0;
//			buf[1] = 0;
//			buf[0] = 0;
//			updateAlarmMoney1(buf[3],buf[2],buf[1],buf[0]);
//		}
//		break;
//		case 0x04001002: //报警金额2
//		{
//			buf[3] = 0;
//			buf[2] = 0;
//			buf[1] = 0;
//			buf[0] = 0;
//			updateAlarmMoney2(buf[3],buf[2],buf[1],buf[0]);
//		}
//		break;
//		case 0x04001003: //透支金额
//		{
//			buf[3] = 0;
//			buf[2] = 0;
//			buf[1] = 0;
//			buf[0] = 0;
//			updateOverdraftMoney(buf[3],buf[2],buf[1],buf[0]);
//		}
//		break;
		case 0x04000b01: //结算日
		{
			buf[0] = balance_config[0].balance_time[0];
			buf[1] = balance_config[0].balance_time[1];
			updateCashInDate(0,0,buf[1],buf[0]);	
		}
		break;
		default:
		{
			updateTime(0,currentClock[2],currentClock[1],currentClock[0]);
		}
		break;
	}

	return ;
}

/*********************************************************************************************************
** 函数名称: check_display_item
** 功能描述: 跟新显示项
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void check_display_item(U8 mode)
{
	U16 datalong = 0;
	U32 data = 0;
	U8 databuf[5];

	if(mode == 1)
	{
		datalong = getEepromData(0x04,0x04,0x01,lcd_control.display_num,databuf);
	}else if(mode == 2){
		datalong = getEepromData(0x04,0x04,0x02,lcd_control.display_num,databuf);
	}else{
		return ;
	}
	data = (U32)((databuf[3])<<24)+(U32)((databuf[2])<<16)+(U32)((databuf[1])<<8)+((databuf[0]));
	if((datalong == 5)&&((data) != 0x0ffffffff)){
		lcd_control.dataID = data;
		lcd_control.ID_num = databuf[4];	
		
	}else{
		lcd_control.display_num = 1;	
	}
}
/*********************************************************************************************************
** 函数名称: meter_state_display
** 功能描述: 电表一些状态量显示
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
#define COMM_CHANNEL_TYPE 0 //0 4851
   							//1 无线
							//2 载波
U8 comm_display_state = 0; //通讯状态显示    bit1bit0    红外
							//				 bit3bit2    4851
							// 				 bit5bit4    4852/无线/载波
 
U8 meter_state_diaplay(void)
{
	static U32 UART3_comm_time = 0,UART1_comm_time = 0,UART0_comm_time = 0;
	
	//继电器状态
	if(meter_state_info.meter_run_state[2]&0x10){
		update8576Buffer(TruthTables02[10],0);	
	}else{
		update8576Buffer(TruthTables02[10],2);
	}
	//编程状态
	if(meter_state_info.meter_run_state[2]&0x8){
		update8576Buffer(TruthTables02[27],0);	
	}else{
   		update8576Buffer(TruthTables02[27],2);
	}
	//当前运行时段
	if(meter_state_info.meter_run_state[2]&0x1){
		update8576Buffer(TruthTables02[5],0);
		update8576Buffer(TruthTables02[4],2);	
	}else{
   		update8576Buffer(TruthTables02[5],2);
		update8576Buffer(TruthTables02[4],0);
	}

	//逆向序
	if((meter_state_info.meter_run_state[6]&0x1)||(meter_state_info.meter_run_state[6]&0x2)){
		update8576Buffer(TruthTables02[21],0);	
	}else{
   		update8576Buffer(TruthTables02[21],2);
	}

	//四象限
	update8576Buffer(TruthTables02[125],0);
	if((meter_state_info.meter_run_state[0]&0x20)&&(meter_state_info.meter_run_state[0]&0x10)){
		update8576Buffer(TruthTables02[120],2);
		update8576Buffer(TruthTables02[121],2);
		update8576Buffer(TruthTables02[122],0);
		update8576Buffer(TruthTables02[123],2);
	}else if((meter_state_info.meter_run_state[0]&0x10)&&(!(meter_state_info.meter_run_state[0]&0x20))){
		update8576Buffer(TruthTables02[120],2);
		update8576Buffer(TruthTables02[121],2);
		update8576Buffer(TruthTables02[122],2);
		update8576Buffer(TruthTables02[123],0);
	}else if((meter_state_info.meter_run_state[0]&0x20)&&(!(meter_state_info.meter_run_state[0]&0x10))){
   		update8576Buffer(TruthTables02[120],2);
		update8576Buffer(TruthTables02[121],0);
		update8576Buffer(TruthTables02[122],2);
		update8576Buffer(TruthTables02[123],2);
	}else{
		update8576Buffer(TruthTables02[120],0);
		update8576Buffer(TruthTables02[121],2);
		update8576Buffer(TruthTables02[122],2);
		update8576Buffer(TruthTables02[123],2);
	}
	//费率
	if(time_zone_con.fee_num == 1){
		update8576Buffer(TruthTables02[112],2);
		update8576Buffer(TruthTables02[113],2);
		update8576Buffer(TruthTables02[114],2);
		update8576Buffer(TruthTables02[115],0);
	}else if(time_zone_con.fee_num == 2){
		update8576Buffer(TruthTables02[112],2);
		update8576Buffer(TruthTables02[113],2);
		update8576Buffer(TruthTables02[114],0);
		update8576Buffer(TruthTables02[115],2);
	}else if(time_zone_con.fee_num == 3){
		update8576Buffer(TruthTables02[112],2);
		update8576Buffer(TruthTables02[113],0);
		update8576Buffer(TruthTables02[114],2);
		update8576Buffer(TruthTables02[115],2);
	}else if(time_zone_con.fee_num == 4){
   		update8576Buffer(TruthTables02[112],0);
		update8576Buffer(TruthTables02[113],2);
		update8576Buffer(TruthTables02[114],2);
		update8576Buffer(TruthTables02[115],2);
	}else{
		update8576Buffer(TruthTables02[112],2);
		update8576Buffer(TruthTables02[113],2);
		update8576Buffer(TruthTables02[114],2);
		update8576Buffer(TruthTables02[115],2);
	}

	//时钟电池欠压
	if(meter_state_info.meter_run_state[0]&0x4){
		update8576Buffer(TruthTables02[6],0);	
	}else{
   		update8576Buffer(TruthTables02[6],2);
	}
	//停抄电池欠压
	if(meter_state_info.meter_run_state[0]&0x8){
		update8576Buffer(TruthTables02[3],0);	
	}else{
   		update8576Buffer(TruthTables02[3],2);
	}
	//报警
	if(Display_Alarm_Flag){
		update8576Buffer(TruthTables02[23],0);
	}else{
		update8576Buffer(TruthTables02[23],2);
	}
	//厂内模式
	if(input[KeyTop]){
		update8576Buffer(TruthTables02[22],0);
	}else{
		update8576Buffer(TruthTables02[22],2);
	}
	
	//A相状态指示
	//Ua断相
//	if(meter_state_info.meter_run_state[3]){
		if(meter_state_info.meter_run_state[3]&0x80){
			update8576Buffer(TruthTables02[25],2);
		}else if(meter_state_info.meter_run_state[3]&0x1){
			update8576Buffer(TruthTables02[25],1);
		}else{
			update8576Buffer(TruthTables02[25],0);
		}
		// -Ia
		if((meter_state_info.meter_run_state[3]&0x1000)||(meter_state_info.meter_run_state[3]&0x80)){
			update8576Buffer(TruthTables02[16],2);
			update8576Buffer(TruthTables02[17],2);
		}else if(meter_state_info.meter_run_state[3]&0x8){
			update8576Buffer(TruthTables02[16],1);
// 			if(meter_state_info.meter_run_state[3]&0x40){
			if(a_phase_info.active_power&0x80000000){
				update8576Buffer(TruthTables02[17],1);
			}else{
				update8576Buffer(TruthTables02[17],2);
			}
		}else{
			update8576Buffer(TruthTables02[16],0);
// 			if(meter_state_info.meter_run_state[3]&0x40){
			if(a_phase_info.active_power&0x80000000){
				update8576Buffer(TruthTables02[17],0);
			}else{
				update8576Buffer(TruthTables02[17],2);
			}
		}
//	}
	
	//B相状态指示
	//Ub断相
//	if(meter_state_info.meter_run_state[4]){
		if(meter_state_info.meter_run_state[4]&0x80){
			update8576Buffer(TruthTables02[24],2);
		}else if(meter_state_info.meter_run_state[4]&0x1){
			update8576Buffer(TruthTables02[24],1);
		}else{
			update8576Buffer(TruthTables02[24],0);
		}
		// -Ib
		if((meter_state_info.meter_run_state[4]&0x1000)||(meter_state_info.meter_run_state[4]&0x80)){
			update8576Buffer(TruthTables02[13],2);
			update8576Buffer(TruthTables02[12],2);
		}else if(meter_state_info.meter_run_state[4]&0x8){
			update8576Buffer(TruthTables02[13],1);
// 			if(meter_state_info.meter_run_state[4]&0x40){
			if(b_phase_info.active_power&0x80000000){
				update8576Buffer(TruthTables02[12],1);
			}else{
				update8576Buffer(TruthTables02[12],2);
			}
		}else{
			update8576Buffer(TruthTables02[13],0);
// 			if(meter_state_info.meter_run_state[4]&0x40){
			if(b_phase_info.active_power&0x80000000){
				update8576Buffer(TruthTables02[12],0);
			}else{
				update8576Buffer(TruthTables02[12],2);
			}
		}
//	}
	
	//c相状态指示
	//Uc断相
//	if(meter_state_info.meter_run_state[5]){
		if(meter_state_info.meter_run_state[5]&0x80){
			update8576Buffer(TruthTables02[20],2);
		}else if(meter_state_info.meter_run_state[5]&0x1){
			update8576Buffer(TruthTables02[20],1);
		}else{
			update8576Buffer(TruthTables02[20],0);
		}
		// -Ic
		if((meter_state_info.meter_run_state[5]&0x1000)||(meter_state_info.meter_run_state[5]&0x80)){
			update8576Buffer(TruthTables02[8],2);
			update8576Buffer(TruthTables02[9],2);
		}else if(meter_state_info.meter_run_state[5]&0x8){
			update8576Buffer(TruthTables02[8],1);
// 			if(meter_state_info.meter_run_state[5]&0x40){
			if(c_phase_info.active_power&0x80000000){
				update8576Buffer(TruthTables02[9],1);
			}else{
				update8576Buffer(TruthTables02[9],2);
			}
		}else{
			update8576Buffer(TruthTables02[8],0);
// 			if(meter_state_info.meter_run_state[5]&0x40){
			if(c_phase_info.active_power&0x80000000){
				update8576Buffer(TruthTables02[9],0);
			}else{
				update8576Buffer(TruthTables02[9],2);
			}
		}
//	}
	//红外
	if(comm_display_state&0x3){
		if((comm_display_state&0x3) == 1){
			update8576Buffer(TruthTables02[59],0);
			comm_display_state &= ~0x3;
			comm_display_state |= 0x2;
			UART3_comm_time = TimeTick;	
		}else if((comm_display_state&0x3) == 2){
			if(check_timeout(TimeTick,UART3_comm_time,5000)){
				comm_display_state &= ~0x3;
				update8576Buffer(TruthTables02[59],2);
			}
		}
	}
	// 4851
	if(comm_display_state&0xc){
		if((comm_display_state&0xc) == 0x4){
			update8576Buffer(TruthTables02[59],0);
			update8576Buffer(TruthTables02[43],0);
			comm_display_state &= ~0xc;
			comm_display_state |= 0x8;
			UART0_comm_time = TimeTick;	
		}else if((comm_display_state&0xc) == 0x8){
			if(check_timeout(TimeTick,UART0_comm_time,5000)){
				comm_display_state &= ~0xc;
				update8576Buffer(TruthTables02[59],2);
				update8576Buffer(TruthTables02[43],2);
			}
		}
	}
	// 4852/无线、载波
	if(comm_display_state&0x30){
		switch(COMM_CHANNEL_TYPE)
		{
			case 0:
			{
				if((comm_display_state&0x30) == 0x10){
					update8576Buffer(TruthTables02[59],0);
					update8576Buffer(TruthTables02[35],0);
					comm_display_state &= ~0x30;
					comm_display_state |= 0x20;
					UART1_comm_time = TimeTick;	
				}else if((comm_display_state&0xc) == 0x20){
					if(check_timeout(TimeTick,UART1_comm_time,5000)){
						comm_display_state &= ~0x30;
						update8576Buffer(TruthTables02[59],2);
						update8576Buffer(TruthTables02[35],2);
					}
				}
			}
			break;
			case 1:
			case 2:
			{
				if((comm_display_state&0x30) == 0x10){
					update8576Buffer(TruthTables02[75],0);
					comm_display_state &= ~0x30;
					comm_display_state |= 0x20;
					UART1_comm_time = TimeTick;	
				}else if((comm_display_state&0xc) == 0x20){
					if(check_timeout(TimeTick,UART1_comm_time,5000)){
						comm_display_state &= ~0x30;
						update8576Buffer(TruthTables02[75],2);
					}
				}
			}
			break;
			default:
			break;
		}
		
	}
	return 1;
}
U8 clear_state_diaplay(void)
{
	update8576Buffer(TruthTables02[10],2);
	update8576Buffer(TruthTables02[75],2);
	update8576Buffer(TruthTables02[59],2);
	update8576Buffer(TruthTables02[35],2);
	update8576Buffer(TruthTables02[43],2);
	update8576Buffer(TruthTables02[9],2);
	update8576Buffer(TruthTables02[8],2);
	update8576Buffer(TruthTables02[20],2);
	update8576Buffer(TruthTables02[13],2);
	update8576Buffer(TruthTables02[12],2);
	update8576Buffer(TruthTables02[24],2);
	update8576Buffer(TruthTables02[16],2);
	update8576Buffer(TruthTables02[17],2);
	update8576Buffer(TruthTables02[25],2);
	update8576Buffer(TruthTables02[22],2);
	update8576Buffer(TruthTables02[23],2);
	update8576Buffer(TruthTables02[3],2);
	update8576Buffer(TruthTables02[6],2);
	update8576Buffer(TruthTables02[112],2);
	update8576Buffer(TruthTables02[113],2);
	update8576Buffer(TruthTables02[114],2);
	update8576Buffer(TruthTables02[115],2);
	update8576Buffer(TruthTables02[21],2);
	update8576Buffer(TruthTables02[5],2);
	update8576Buffer(TruthTables02[4],2);
	update8576Buffer(TruthTables02[27],2);
	update8576Buffer(TruthTables02[120],2);
	update8576Buffer(TruthTables02[121],2);
	update8576Buffer(TruthTables02[122],2);
	update8576Buffer(TruthTables02[123],2);
	update8576Buffer(TruthTables02[125],2);
	return 1;
}
/*********************************************************************************************************
** 函数名称: display_item
** 功能描述: 显示项
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void display_item(void)
{
	switch(lcd_control.display_mode){
		case 0://初始化全显
		{
			if(TimeTick>=(40*100)){
				lcd_all_clear();
				lcd_control.display_num = 1;
				lcd_control.display_mode = 1;
				check_display_item(lcd_control.display_mode);
				lcd_control.display_time = TimeTick;
				lcd_control.display_num++;
				
				check_err_type();	
			}
		}
		break;
		case 1:	 //循显模式
		{
			if(check_timeout(TimeTick,lcd_control.display_time,lcd_control.cyc_time*1000)){	
				
				
				if((lcd_control.display_num == 1)&&(err_control.err_mode == 0x82)){
					err_control.err_mode &= 0x7f;
				}
				check_err_type();
				lcd_control.display_time = TimeTick;
				if((err_control.err_mode == 0)||(err_control.err_mode == 0x82)){
				
					check_display_item(lcd_control.display_mode);
					lcd_control.display_num++;
					if(lcd_control.display_num>lcd_control.cyc_display_num){
						lcd_control.display_num = 1;
					}	
				}
			}
			//跟新内容
			switch(err_control.err_mode)
			{
				case 0:
				{
					updata_display_item();
					if(power_state){
						meter_state_diaplay();//状态量显示
					}else{
						clear_state_diaplay();
					}
				}
				break;
				case 1:
				{
					display_err_num(err_control.err_num);  //显示err—XX
				}
				break;
				case 2:
				{
					display_err_num(err_control.err_num);  //显示err—XX
				}
				break;
				case 0x82:
				{
					updata_display_item();
					if(power_state){
						meter_state_diaplay();//状态量显示
					}else{
						clear_state_diaplay();
					}
				}
				break;
				default:
				{
					updata_display_item();
					if(power_state){
						meter_state_diaplay();//状态量显示
					}else{
						clear_state_diaplay();
					}
				}
				break;
			}	
		}
		break;
		case 2:	 //键显模式
		{
			if((TimeTick - lcd_control.display_time)>20*1000){
				BG_Ctrl_Flag = 0;
				lcd_control.display_mode = 1;
				lcd_control.display_num = 1;
				check_display_item(lcd_control.display_mode);
			}else{
				if(power_state){	   //正常模式
				   	BG_Ctrl_Flag = 1;
				}else{
					BG_Ctrl_Flag = 0;
				}
				updata_display_item();//跟新内容
				if(power_state){
					meter_state_diaplay();//状态量显示
				}else{
					clear_state_diaplay();
				}
			}
		}
		break;
		default:  //清显示
		{
			lcd_all_clear();
		}
		break;
	}
	
		
	
}
/*********************************************************************************************************
** 函数名称: init_lcd
** 功能描述: 液晶显示
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void init_lcd(void){
	U8 datalong = 0;
	U8 databuf[10];



	I2C_Init (I2C1,I2C_MASTER,50000,0x10);
	for(datalong = 0;datalong<3;datalong++){
		if(initPcf8576()){
			break;
		}
	}
	lcd_control.display_mode = 0;//循显模式

	datalong = getEepromData(4,0,3,2,databuf);
	if(datalong == 1){
		lcd_control.cyc_time = BCDToDec(databuf[0]);
	}
	datalong = getEepromData(4,0,3,1,databuf);
	if(datalong == 1){
		lcd_control.cyc_display_num = BCDToDec(databuf[0]);//循显总显示屏数
	}
	datalong = getEepromData(4,0,3,5,databuf); 
	if(datalong == 1){
		lcd_control.key_display_num = BCDToDec(databuf[0]);//键显总显示屏数
	}
}
/*********************************************************************************************************
** 函数名称: batControl
** 功能描述: 停抄电池控制，用于控制LCD的显示时间
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
extern U8 power_Down_Flag;			//正常掉电置位

void batControl(void){
	static U32 timeWakeUp = 0;		//用于唤醒CPU后的计时
	static U8 flagPushKey = 0;		//用于唤醒CPU后是否有按键按下的标志
	static U8 batState = 0;			//是否用停抄电池供电状态

	if(power_Down_Flag) 
	{			
		return; //直接掉电
	}	

	
	if(!power_state){	//停抄电池供电
		if(!batState){
			Main_Mcu_Flag = 1;	//停抄电池供电
			batState = 1;		//当前为使用停抄电池供电状态
		}
		if(lcd_control.display_mode == 1){	//循显模式
			if(flagPushKey) {				//按键唤醒后有按键按下
			 	if(TimeTick - timeWakeUp >= 30000/3){
					Main_Mcu_Flag = 0;	//关闭停抄电池供电
					flagPushKey = 0;
				}
			}else if((lcd_control.display_num == lcd_control.cyc_display_num) && batState){
				Main_Mcu_Flag = 0;	//关闭停抄电池供电
			}

		}else{			//键显模式
			if(lcd_control.display_mode == 2){	//键显模式	
				timeWakeUp = TimeTick;	   	//重新计时
				flagPushKey = 1;			//按键按下标志
			}
		}
	}else{
	  	if(batState == 1) {
			batState = 0;
			Main_Mcu_Flag = 0;	//关闭停抄电池供电
			flagPushKey = 0;
		}

	}

}

/*********************************************************************************************************
** 函数名称: LcdDisplay
** 功能描述: 液晶显示
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
__task void LcdDisplay (void) {

	U32 timeLcd = TimeTick;
	U8 flagContent = 0;		//0:显示第一个缓冲区的内容，1显示第二个缓冲区的内容

	

	for(;;){
		getCurrentTDW();//读取时间
		
		calculate_demand_power();//计算需量

		check_err_state();//更新报警状态

		display_item();//查询显示项目及其跟新数据

		check_power_mode();

		batControl();		//停抄电池控制
		

		if(!flagContent){		  //刷新液晶屏
			flashLcd(&pcf8576DataBuf);
			if(check_timeout(TimeTick,timeLcd,500)){
				timeLcd = TimeTick;
				flagContent = 1;
			}
		}else{
			flashLcd(&pcf8576DataBufGlitter);
			if(check_timeout(TimeTick,timeLcd,500)){
				timeLcd = TimeTick;
				flagContent = 0;
			}
			
		}
  	os_dly_wait (20);
	}
}

