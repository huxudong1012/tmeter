/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __VOLTAGE_CHECK_H__
#define __VOLTAGE_CHECK_H__

	#define	AddBatAdClock		0
	#define	AddBatAdStop		1

	#define BAT_AD_CLOCK 	30			//P1.30,时钟电池电压检测输入
	#define BAT_AD_STOP 	31			//P1.31，停抄电池电压检测输入
	#define BAT_AD_CON		2			//P3.25, 电池检测控制管脚

	#define lowPowerBAT_CLOCK 	427		//时钟电池电压检测点为1.25V，时钟电池电压为3V
	#define lowPowerBAT_STOP 	0x450		//停抄电池电压检测点为0.82V，停抄电池电压为5V


	extern U16 voltageClockBat;			//用于保存电池电压，16进制数表示
	extern U16 voltageStopBat;			//用于保存电池电压，16进制数表示

	#define EVENT_STATE_ADDR 0xff00

	extern void init_voltage_adc(void);
	extern U8 save_event_state(void);
	extern unsigned long adcSampling(const U8 add);

	extern __task void check_voltage_task (void);


	extern U16 voltageClockBat;			//用于保存电池电压，16进制数表示
	extern U16 voltageStopBat;			//用于保存电池电压，16进制数表示

	typedef struct {
		U32 voltage_upper_limit;
		U32 voltage_renew_lower_limit;	
		U32 current_lower_limit;
		U8 time_limit;
		U8 event_state[3]; //三相状态
	}lost_voltage_t;
	typedef struct {
		U32 voltage_upper_limit;	
		U8 time_limit;
		U8 event_state[3]; //三相状态
	}lack_voltage_t;
	typedef struct {
		U32 voltage_lower_limit;	
		U8 time_limit;
		U8 event_state[3]; //三相状态
	}over_voltage_t;
	typedef struct {
		U32 voltage_upper_limit;
		U32 current_upper_limit;	
		U8 time_limit;
		U8 event_state[3]; //三相状态
	}break_off_t;
	typedef struct {
		U32 voltage_lower_limit;
		U32 current_upper_limit;	
		U32 current_lower_limit;
		U8 time_limit;
		U8 event_state[3];	//三相状态
	}lost_current_t;
	typedef struct {
		U32 voltage_upper_limit;	
		U32 current_lower_limit;
		U8 time_limit;
		U8 event_state;
	}all_lost_voltage_t;
	typedef struct {
		U32 no_balance_limit;	
		U8 time_limit;
		U8 event_state;
	}voltage_no_balance_t;
	typedef struct {
		U32 no_balance_limit;	
		U8 time_limit;
		U8 event_state;
	}current_no_balance_t;
	typedef struct {
		U32 current_lower_limit;	
		U8 time_limit;
		U8 event_state[3];
	}over_current_t;
	typedef struct {
		U32 voltage_lower_limit;
		U32 current_upper_limit;	
		U8 time_limit;
		U8 event_state[3];
	}break_current_t;
	typedef struct {
		U32 active_power_lower_limit;
		U8 time_limit;
		U8 event_state[3];
	}power_converse_t;
	typedef struct {
		U32 voltage_upper_limit;
		U32 voltage_lower_limit;
		U8 event_state;
	}voltage_regular_t;
	typedef struct {
		U32 active_demand_lower_limit;
		U32 reactive_demand_lower_limit;
		U8 time_limit;
		U8 event_state[6];
	}demand_limit_t;
	typedef struct {
		U32 power_factor_lower_limit;
		U8 time_limit;
		U8 event_state;
	}power_factor_limit_t;
	typedef struct {
		U16 current_no_balance_limit;
		U8 time_limit;
		U8 event_state;
	}current_sever_no_balance_t;
	typedef struct {
		U32 handler_code;
		U8 save_ID_num;
		U8 event_state;
	}programme_note_t;
	typedef struct {
		lost_voltage_t lost_voltage;	//失压
		lack_voltage_t lack_voltage;	//欠压
		over_voltage_t over_voltage;	//过压
		break_off_t break_off;			//断相
		all_lost_voltage_t all_lost_voltage;//全失压
		voltage_no_balance_t voltage_no_balance;//电压不平衡
		current_no_balance_t current_no_balance;//电流不平衡
		lost_current_t lost_current;	//失流
		over_current_t over_current;    //过流
		break_current_t break_current;	//断流
		power_converse_t power_converse; //潮流反向
		power_converse_t over_power;	 //过载
		voltage_regular_t voltage_regular;//电压合格 
		demand_limit_t demand_limit;//需量超限
		power_factor_limit_t power_factor_limit;//功率因数超限
		current_sever_no_balance_t current_sever_no_balance; //电流严重超限
		programme_note_t programme_note;//编程记录控制
		U8 top_cover_event_state;//开顶盖记录状态
		U8 tail_cover_event_state;//开尾盖记录状态
		U8 voltage_converse_event_state;//电压逆相序事件状态
		U8 current_converse_event_state;//电流逆相序事件状态
	}evevt_config_t;


	typedef struct {
		U8 flag;
		U8 state;		
	}meter_clear_zero_t;
	typedef struct {
		U8 flag;
		U8 state;		
	}demand_clear_zero_t;
	typedef struct {
		U8 flag;
		U8 state;
		U8 data_id[4];		
	}event_clear_zero_t;
	typedef struct {
		meter_clear_zero_t meter_clear_zero;
		demand_clear_zero_t demand_clear_zero;
		event_clear_zero_t 	event_clear_zero;	
	}clear_zero_t;
	extern clear_zero_t clear_zero;
	extern evevt_config_t event_config;

	extern U8 save_programe_note(U32 handler_code,U32 data_id);
	extern U8 event_clear_zero(U8 id0,U8 id1,U8 id2,U8 id3,U8 mode);
	extern U8 demand_clear_zero(void);

	extern U8 check_event_state(void);

#endif

