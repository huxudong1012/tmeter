/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述: 2时区，8时段表，14时段，8费率                                     **/
/**			1 定时冻结：按照指定的时刻、时间间隔冻结电能量数据，每个冻结量至少保存12 次。**/
/**			2 瞬时冻结：在非正常情况下，冻结当前的所有电量数据、日历和时间以及重要的测量数**/
/**			  据；瞬时冻结量保存最后3 次数据。**/
/**			3 约定冻结：在新老两种费率/时段转换、阶梯电价转换或电力公司认为有特殊要求时， **/
/**			  冻结约定时刻的电量以及其他重要数据。 **/
/**			4 日冻结：存储每天零点的电能量，可存储2 个月的数据量。**/
/**			5 冻结内容及标识符应符合DL/T 645—2007 及其备案文件要求。**/
/*****************************************************************************/

#ifndef __FREEZE_H
#define __FREEZE_H

/**********************************冻结数据模式字********************************************/
/*Bit7 |  Bit6	       |  Bit5	       |   Bit4	   |   Bit3   |  Bit2   |   Bit1  |	  Bit0	|*/
/*-------------------------------------------------------------------------------------------*/
/*变量 |反向有功最大   |正向有功最大   | 四象限无  |  组合无  |  组合无	|  反向有 |	正向有	|*/
/*	   |需量及发生时间 |需量及发生时间 | 功电能	   | 功2电能  |功1电能	|  功电能 |	功电能	|*/
/*------------------------------------------------------------------------------------------|*/
	extern U8 freezeMode;

	#define maxHourFreezeNum		254
	#define	maxDayFreezeNum			62
	#define	maxJieTiSwitchNum		2
	#define	maxDingShiNum			12
	#define	maxShunShiNum			3
	#define	maxShiQuSwitchNum		2
	#define	maxShiDuanSwitcheNum	2
	

	typedef enum {
		timing_freeze = 0,		//定时冻结
		instantanrous_freeze,	//瞬时冻结
		zones_freeze,			//两套时区表切换冻结
		periods_freeze,			//两套日时段表切换冻结
		whole_hours_freeze,		//整点冻结
		fees_freeze,			//两套费率冻结
		days_freeze,			//日冻结
		electrovalence_freeze	//两套阶梯电价

	}freeze_type_t;		

	#define MAX_FREEZE_MODE_TYPE 5

	typedef struct{
		U8 freeze_mode[MAX_FREEZE_MODE_TYPE];//各相冻结模式字
		U8 hour_freeze_start_time[5];		 //整点冻结开始时间
		U8 hour_freeze_interval;			 //整点冻结时间间隔
		U16 last_hour_freeze;				 //上一次整点冻结时间
		U8 day_freeze_time[2];				 //日冻结时间
		U8 last_day_freeze;					 //上一次日冻结时间
		U8 dingshi_freeze_time[4];			 //定时冻结时间
		U8 last_dingshi_freeze_time[5];		 //上一次定时冻结时间
	}freeze_config_t;

	extern U8 timings_freeze(void);
	extern U8 day_freeze(void);
	extern U8 whole_hour_freeze(void);
	extern U8 instantaneous_freeze(void);
	extern U8 init_freeze(void);
	extern U8 check_all_same(U8 *time,U8 data,U8 datalong);
	extern U8 check_minute_timeout(U32 timenew,U32 timeold,U32 interval);
   	extern U8 electrovalence_exchange_freeze(void);
	extern U8 periods_exchange_freeze(void);
	extern U8 zones_exchange_freeze(void);
	extern U8 fees_exchange_freeze(void);

	extern freeze_config_t freeze_config;
#endif 

