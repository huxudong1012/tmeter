/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __EEPROM_H__
#define __EEPROM_H__
	//输入管脚定义
	#define EE_WP	26
	#define EE_WP1	25

	//EEPROM地址
	#define LC51200	0xa4
	#define LC51201	0xa6
	#define LC51202	0xa8
	#define LC51203	0xa2
//	#define LC51204	0xa8
	#define LC512_PAGE_SIZE  0x80
	typedef enum {
	    EEWP = 0,			//0
	    EEWP1,	   			//1			
	} WP;				 	//写保护管脚编号

	extern void initWP(void);
	extern void setValue_EEWP(WP u,U8 value);
	

	extern U8 getEepromData(U8 di3,U8 di2,U8 di1,U8 di0,U8* data);
	extern U8 writeItem(U8 di3,U8 di2,U8 di1,U8 di0,U8* data);

	extern U8 i2c_page_write(LPC_I2C_Channel_t DevNum,U8 addr, U8 *pMsg , U32 numMsg);
	extern U8 i2c_page_read(LPC_I2C_Channel_t DevNum,U8 addr, U8 *pMsg , U32 numMsg);

	extern U8 read_data(U8 chip,U16 addr,U8 datalong,U8 *databuf);
	extern U8 save_data(U8 chip,U16 addr,U8 datalong,U8 *databuf);
	extern U8 check_data_first_addr(U8 di3,U8 di2,U8 di1,U8 di0,U16 *addr,U32 *length);

	extern U8 	getTableData1(U8 di3,U8 di2,U8 di1,U8 di0,U8 *addr);
	

#endif

