/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __SECURITY_PASSWORD_H__
#define __SECURITY_PASSWORD_H__

#define MAX_PASSWORD_NUM 5
typedef enum {
	PASSWORD0 = 0,	//密码0
	PASSWORD1,		//密码1
	PASSWORD2,		//密码2
	PASSWORD3,	    //密码3
	PASSWORD4,		//密码4
	PASSWORD5,		//密码5
	PASSWORD6,		//密码6
	PASSWORD7,
	PASSWORD8,
	PASSWORD9
}password_rate;

typedef struct {
	U8	rate;	 	//等级
	U32 password_code;	//密码
}password_t;

typedef struct {
	password_t password[MAX_PASSWORD_NUM]; 
	U32 handler_num;	//操作者代码
}password_handler_t;


extern U8 amend_password(U8 id0,U8 id1,U8 id2,U8 id3,U8 *data0,U8 rate0,U8 *data1,U8 rate1);
extern U8 check_password(U8 *data,U8 rate);
extern U8 read_password(void);
extern U8 amend_handler_num(U8 *data);
extern password_handler_t password_handler;
#endif

