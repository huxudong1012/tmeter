/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __ERR_H__
#define __ERR_H__

//错误类型定义
#define CONTROL_ERR 1//控制回路错误
#define ESAM_ERR 2//ESAM错误
#define INSISE_MEM_ERR 3//内存错误
//#define CLOCK_BETT_ERR 4//时钟电池低错误
#define INSIDE_PROG_ERR 5//内部程序
#define OUTSIDE_MEM_ERR 6//储存器错误
#define CLOCK_ERR 7//时钟错误

#define OVER_LOAD_ERR 51 //过载
#define CURRENT_NO_BALANCE_ERR 52 //电流不平衡
#define OVER_VOLTAGE_ERR 53 //过压
#define POWER_FACTOR_ERR 54 //功率因数超限
#define ACTIVE_DEMAND_ERR 55 //有功需量超限
#define ACTIVE_ENERGY_ERR 56 //有功电能方向

#define LOST_VOLTAGE_ERR 57 //失压
#define BREAK_PHASE_ERR 58 //断相
#define LOST_CURRENT_ERR 59 //失流
#define ATHWART_ERR 60 //逆向序
#define STOP_BETT_ERR 61 //停抄电池电压低
#define CLOCK_BETT_ERR 62//时钟电池低错误
#define OVERDRAFT_ERR 63//透支状态错误
#define COEMPTION_ERR 64//购电囤积错误

//IC卡错误

typedef struct {
	U8 err_mode;//报警类型 0 不需显示err 1 需长显 2 加如循显第一项之前
	U8 err_count;//报警总数
	U8 err_num;	 //报警编号
	U8 err_buf[8];//报警内容
}err_control_t;

extern err_control_t err_control;

extern U8 check_err_type(void);
extern U8 err_check(void);
extern void init_err_check(void);
extern U8 set_one_err(U8 type);
extern U8 clear_one_err(U8 type);
extern void check_err_state(void);

#endif
