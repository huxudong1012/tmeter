/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __DEMAND_H
#define __DEMAND_H

#define MAX_SLIDING_NUM 60
#define MAX_DEMAND_TYPE 10

typedef struct {
	U32 sliding_time_eneygy[MAX_SLIDING_NUM]; //每个滑差周期中的能量
	U32 previous_energy_data;	              //上一个滑差时间开始能量数
	U32 max_demand_value;			          //最大需量值
	U8 max_demand_time[5];			          //最大需量出现周期的结束时间
	U32	demand_value;						  //当前需量
	U32	sliding_demand_value;		 //滑差需量
}demand_unity_t;                 //需量个体

typedef struct {

	U8 total_demand_moment;  //总需量滑差计数
	U8 moment_demand_moment;  //时段需量滑差计数
	U8 demand_interval;	       //需量周期 多少分钟
	U8 sliding_time;           //滑差时间 多少分钟
	U32 previous_total_demand_start_time;	   //上一个滑差时间开始时间 一天为时间界限
	U32 previous_moment_demand_start_time;	   //上一个滑差时间开始时间 一天为时间界限

	demand_unity_t total_demand[MAX_DEMAND_TYPE];//总的需量信息
	demand_unity_t moment_demand[MAX_DEMAND_TYPE];//各费率需量信息

	demand_unity_t a_phase_demand[MAX_DEMAND_TYPE];//a的需量信息
	demand_unity_t b_phase_demand[MAX_DEMAND_TYPE];//b的需量信息
	demand_unity_t c_phase_demand[MAX_DEMAND_TYPE];//c的需量信息

	U8 interval;//需量计算最小时间间隔 1s
	U32 active_demand;
	U32 reactive_demand;
	U32 apparent_demand;
	
}demand_info_t;

extern U8 read_demand(U8 type);
extern U8 save_moment_demand(U8 type);
extern U8 read_one_demand_info(U8 num,U8 fee,U8 type,U8 *databuf,U8 *flag);
extern U8 init_demand(void);
extern U8 calculate_demand(void);
extern demand_info_t demand_info;
extern U8 calculate_demand_power(void);

#endif /*__DEMAND_H */


