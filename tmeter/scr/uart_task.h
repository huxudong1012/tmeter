/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __UART_TASK_H
#define __UART_TASK_H

#include "lpc_uart.h"

#define UART0_645DATALONG  200
#define UART1_645DATALONG  200
#define UART3_645DATALONG  200
typedef struct {
	U8 speed_state;			  //速率特征字
	U8 Control;	              //控制码
	U8 Datalong;              //数据长度
	U8 *DataBuf;              //数据域指针
	volatile U8 HandleState;  //操作状态
	U32 TimeReciveFrameTrail;

	U8 last_frame_ID[4]; //上一读后续帧ID
	U8 last_frame_seq; //上一读后续帧帧序号
	U8 chip; //读取的芯片
	U16 addr;
	U32 last_frame_datalong;//后续数据帧总长度
}Uart_645Data_t;




extern Uart_645Data_t uart0_645frame,uart1_645frame,uart3_645frame; 

extern __task void process_uart_protocol(void);

typedef struct {
	U8 meter_comm_addr[6];	//电表通讯地址
	U8 meter_number[6];	     //表号
	U16 meter_run_state[7];  //电表运行状态字
	U8 meter_err;              //电表错误字
}meter_state_info_t;

extern meter_state_info_t meter_state_info;


extern U8 check_bcd_data(U8 *buf,U8 datalong);
extern U16 com_read_variable_data(U8 id2,U8 id1,U8 id0,U8 *buf,U8 *datalong);
extern U32 get_uart_speed(LPC_Uart_Channel_t DevNum,U8 speed_state);
extern U8 com_read_state_event_data(LPC_Uart_Channel_t DevNum,U8 id3,U8 id2,U8 id1,U8 id0,U8 *buf,U8 *datalong);
extern U8 com_read_event_data(LPC_Uart_Channel_t DevNum,U8 id2,U8 id1,U8 id0,U8 *buf,U8 *datalong);
#endif //__UART_TASK_H

