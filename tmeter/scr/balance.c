/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#include <RTL.h>                      /* RTX kernel functions & defines      */

#include "balance.h"
#include "lpc_i2c.h"
#include "eeprom.h"
#include "rx8025t.h"
#include "time_period_zone.h"
#include "freeze.h"
#include "ade7878.h"
#include "demand.h"
#include "firmware.h"


balance_config_t balance_config[MAX_BALANCE_NUM];
/*********************************************************************************************************
** 函数名称: init_balance
** 功能描述: 结算相关初始化
** 输　入: 
** 		   		   
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 init_balance(void)
{
	U8 datalong,databuf[10];
	
	
	datalong = getEepromData(4,0,0x0b,1,databuf);  //第一结算日
	if(datalong == 2){
		balance_config[first_balance].balance_time[0] = BCDToDec(databuf[0]);	
		balance_config[first_balance].balance_time[1] = BCDToDec(databuf[1]);
	}
	

	datalong = getEepromData(4,0,0x0b,2,databuf);  //第二结算日
	if(datalong == 2){
		balance_config[second_balance].balance_time[0] = BCDToDec(databuf[0]);	
		balance_config[second_balance].balance_time[1] = BCDToDec(databuf[1]);
	}
	

	datalong = getEepromData(4,0,0x0b,3,databuf);  //第三结算日
	if(datalong == 2){
		balance_config[third_balance].balance_time[0] = BCDToDec(0x99);	
		balance_config[third_balance].balance_time[1] = BCDToDec(0x99);
	}
	


	return 1;
}
/*********************************************************************************************************
** 函数名称: check_balance
** 功能描述: 结算日相关处理
** 输　入: 
** 		   		   
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 check_balance(void)
{
	U8 i,j,k,energy_ID0 = 0,demand_ID0 = 0;
	U8 result = 0,result1 = 0;
	U8 databuf[10],flag = 0;
	U16 result2 = 0;


	for(k = 0;k<MAX_BALANCE_NUM;k++){
		if(!check_all_same(balance_config[k].balance_time,99,2)){
			if(balance_config[k].last_balance_month != currentClock[Month]){
				if(check_zone_timeout(currentClock+2,balance_config[k].balance_time,2)){
					result1++;

					//冻结开始
					//保存能量
					if(change_cyc_save_position(ENERGY,1,&energy_ID0,1)){
						for(j=0;j<11;j++){		//总能量
							for(i=0;i<5;i++){
								if(read_one_energy_info(0,i,j,databuf,&flag)){
									if((j==0)||(j==3)||(j==4)){
										if(flag){
											databuf[3] |= 0x80;
										}else{
											databuf[3] &= 0x7f;
										}
									}
									result2 += writeItem(0,j,i,energy_ID0,databuf);	
								}
							}
						}
						for(j=0;j<10;j++){		//a相能量
						
							if(read_one_energy_info(0,0,j+A_PHASE_ENERGY_ADDR,databuf,&flag)){
								if((j==3)||(j==2)){
									if(flag){
										databuf[3] |= 0x80;
									}else{
										databuf[3] &= 0x7f;
									}
								}
								result2 += writeItem(0,j+A_PHASE_ENERGY_ADDR,0,energy_ID0,databuf);
							}
						}
						for(j=0;j<10;j++){		//b相能量
	
							if(read_one_energy_info(0,0,j+B_PHASE_ENERGY_ADDR,databuf,&flag)){
								if((j==3)||(j==2)){
									if(flag){
										databuf[3] |= 0x80;	
									}else{
										databuf[3] &= 0x7f;
									}
								}
								result2 += writeItem(0,j+B_PHASE_ENERGY_ADDR,0,energy_ID0,databuf);
							}	
						}
						for(j=0;j<10;j++){		//c相能量
							if(read_one_energy_info(0,0,j+C_PHASE_ENERGY_ADDR,databuf,&flag)){
								if((j==3)||(j==2)){
									if(flag){
										databuf[3] |= 0x80;	   
									}else{
										databuf[3] &= 0x7f;
									}
								}
								result2 += writeItem(0,j+C_PHASE_ENERGY_ADDR,0,energy_ID0,databuf);
							}
						}
					}
					//保存需量
					if(change_cyc_save_position(DEMAND,1,&demand_ID0,1)){
						for(j=0;j<10;j++){		//总需量
							for(i=0;i<5;i++){
								if(read_one_demand_info(0,i,j+1,databuf,&flag)){
									if((j==3)||(j==2)){
										if(flag){
											databuf[2] |= 0x80;
										}else{
											databuf[2] &= 0x7f;
										}
									}
									result2 += writeItem(1,j+1,i,demand_ID0,databuf);
								}
							}
						}
						for(j=0;j<10;j++){		//a相需量
						
							if(read_one_demand_info(0,0,j+A_PHASE_ENERGY_ADDR,databuf,&flag)){
								if((j==3)||(j==2)){
									if(flag){
										databuf[2] |= 0x80;
									}else{
										databuf[2] &= 0x7f;
									}
								}
								result2 += writeItem(1,j+A_PHASE_ENERGY_ADDR,0,demand_ID0,databuf);
							}
						}
						for(j=0;j<10;j++){		//b相需量
	
							if(read_one_demand_info(0,0,j+B_PHASE_ENERGY_ADDR,databuf,&flag)){
								if((j==3)||(j==2)){
									if(flag){
										databuf[2] |= 0x80;
									}else{
										databuf[2] &= 0x7f;
									}
								}
								result2 += writeItem(1,j+B_PHASE_ENERGY_ADDR,0,demand_ID0,databuf);
							}	
						}
						for(j=0;j<10;j++){		//c相需量
							if(read_one_demand_info(0,0,j+C_PHASE_ENERGY_ADDR,databuf,&flag)){
								if((j==3)||(j==2)){
									if(flag){
										databuf[2] |= 0x80;
									}else{
										databuf[2] &= 0x7f;
									}
								}
								result2 += writeItem(1,j+C_PHASE_ENERGY_ADDR,0,demand_ID0,databuf);
							}
						}
						
					}
					if(result2 == 165){
						start_position[ENERGY]++;
						if(start_position[ENERGY]>MAX_ENERGY_NUM){
							start_position[ENERGY] = 1;
						}
						start_position[DEMAND]++;
						if(start_position[DEMAND]>MAX_DEMAND_NUM){
							start_position[DEMAND] = 1;
						}
						result++;
						balance_config[k].last_balance_month = 	currentClock[Month];
					}

					result2 = 0;
				}
			}
		}
	}
	if(result1 == result){
		return 1;
	}
	return 0;
}

