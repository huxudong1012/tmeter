/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#include <rtl.h>

#include "firmware.h"
#include "time_period_zone.h"
#include "freeze.h"
#include "load.h"
#include "lpc_i2c.h"
#include "eeprom.h"
#include "crc.h"
#include "balance.h"
#include "input.h"
#include "meter_output.h"
#include "voltage_check.h"
#include "ade7878.h"

U32 start_position[MAX_CYC_SAVE_NUM]; //循环存储开始相位置
/*********************************************************************************************************
** 函数名称: important_parameter_save
** 功能描述: 重要参数存储  
** 输　入: 	 
**           
** 输　出:     0 错误
**			   1 成功
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 important_parametrer_save(void)
{
	U8 databuf[130],i;

	databuf[0] = (U8)(IMPORTANT_PARAMETER_ADDR>>8);
	databuf[1] = (U8)(IMPORTANT_PARAMETER_ADDR);
	
	databuf[2] = time_zone_con.previous_fee_num;
	databuf[3] = time_zone_con.zones_num;
	databuf[4] = time_zone_con.periods_num;
	databuf[5] = 0;
	databuf[6] = 0;
	databuf[7] = freeze_config.dingshi_freeze_time[0];
	databuf[8] = freeze_config.dingshi_freeze_time[1];
	databuf[9] = freeze_config.dingshi_freeze_time[2];
	databuf[10] = freeze_config.dingshi_freeze_time[3];
	//负荷存储条数
	databuf[11] = load_config.load_save_config[first_kind_load].load_save_num;
	databuf[12] = (load_config.load_save_config[first_kind_load].load_save_num>>8);
	databuf[13] = (load_config.load_save_config[first_kind_load].load_save_num>>16);
	databuf[14] = (load_config.load_save_config[first_kind_load].load_save_num>>24);

	databuf[15] = load_config.load_save_config[second_kind_load].load_save_num;
	databuf[16] = (load_config.load_save_config[second_kind_load].load_save_num>>8);
	databuf[17] = (load_config.load_save_config[second_kind_load].load_save_num>>16);
	databuf[18] = (load_config.load_save_config[second_kind_load].load_save_num>>24);

	databuf[19] = load_config.load_save_config[third_kind_load].load_save_num;
	databuf[20] = (load_config.load_save_config[third_kind_load].load_save_num>>8);
	databuf[21] = (load_config.load_save_config[third_kind_load].load_save_num>>16);
	databuf[22] = (load_config.load_save_config[third_kind_load].load_save_num>>24);

	databuf[23] = load_config.load_save_config[fourth_kind_load].load_save_num;
	databuf[24] = (load_config.load_save_config[fourth_kind_load].load_save_num>>8);
	databuf[25] = (load_config.load_save_config[fourth_kind_load].load_save_num>>16);
	databuf[26] = (load_config.load_save_config[fourth_kind_load].load_save_num>>24);

	databuf[27] = load_config.load_save_config[fifth_kind_load].load_save_num;
	databuf[28] = (load_config.load_save_config[fifth_kind_load].load_save_num>>8);
	databuf[29] = (load_config.load_save_config[fifth_kind_load].load_save_num>>16);
	databuf[30] = (load_config.load_save_config[fifth_kind_load].load_save_num>>24);

	databuf[31] = load_config.load_save_config[sixth_kind_load].load_save_num;
	databuf[32] = (load_config.load_save_config[sixth_kind_load].load_save_num>>8);
	databuf[33] = (load_config.load_save_config[sixth_kind_load].load_save_num>>16);
	databuf[34] = (load_config.load_save_config[sixth_kind_load].load_save_num>>24);
	//上一次计算日的月份
	databuf[35] = balance_config[0].last_balance_month;
	databuf[36] = balance_config[1].last_balance_month;
	databuf[37] = balance_config[2].last_balance_month;

	databuf[38] = 0;
	databuf[39] = 0;

	for(i=0;i<MAX_CYC_SAVE_NUM;i++){
		databuf[40+4*i] = start_position[i];
		databuf[41+4*i] = (start_position[i]>>8);
		databuf[42+4*i] = (start_position[i]>>16);
		databuf[43+4*i] = (start_position[i]>>24);
	}
	//按键有效时间
	databuf[80] = key_program_time;
	databuf[81] = (key_program_time>>8);
	databuf[82] = (key_program_time>>16);
	databuf[83] = (key_program_time>>24);
	//远程报警状态
	databuf[84] = output_control.alarm_deal;
	//远程保电状态
	databuf[85] = output_control.keep_deal;
	//继电器操作状态
	databuf[86] = output_control.relay_deal;
	//编程事件状态
	databuf[87] = event_config.programme_note.event_state;
	//a相安时数据
	databuf[88] = a_phase_info.positive_AH;
	databuf[89] = (a_phase_info.positive_AH>>8);
	databuf[90] = (a_phase_info.positive_AH>>16);
	databuf[91] = (a_phase_info.positive_AH>>24);

	databuf[92] = a_phase_info.negetive_AH;
	databuf[93] = (a_phase_info.negetive_AH>>8);
	databuf[94] = (a_phase_info.negetive_AH>>16);
	databuf[95] = (a_phase_info.negetive_AH>>24);
	//b相安时数据
	databuf[96] = b_phase_info.positive_AH;
	databuf[97] = (b_phase_info.positive_AH>>8);
	databuf[98] = (b_phase_info.positive_AH>>16);
	databuf[99] = (b_phase_info.positive_AH>>24);

	databuf[100] = b_phase_info.negetive_AH;
	databuf[101] = (b_phase_info.negetive_AH>>8);
	databuf[102] = (b_phase_info.negetive_AH>>16);
	databuf[103] = (b_phase_info.negetive_AH>>24);
	//c相安时数据
	databuf[104] = c_phase_info.positive_AH;
	databuf[105] = (c_phase_info.positive_AH>>8);
	databuf[106] = (c_phase_info.positive_AH>>16);
	databuf[107] = (c_phase_info.positive_AH>>24);

	databuf[108] = c_phase_info.negetive_AH;
	databuf[109] = (c_phase_info.negetive_AH>>8);
	databuf[110] = (c_phase_info.negetive_AH>>16);
	databuf[111] = (c_phase_info.negetive_AH>>24);
	for(i=0;i<15;i++){
		databuf[112+i] = 0;
	}
	databuf[129]= CAL_CRC(databuf+2,127);

	setValue_EEWP(EEWP,0);
	if(!i2c_page_write(I2C0,LC51201,databuf,130)){
		setValue_EEWP(EEWP,1);
		return 1;
	}
	setValue_EEWP(EEWP,1);
	return 0;
}
/*********************************************************************************************************
** 函数名称: important_parameter_save
** 功能描述: 重要参数存储  
** 输　入: 	 
**           
** 输　出:     0 错误
**			   1 成功
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 important_parametrer_read(void)
{
	U8 databuf[130],i;

	databuf[0] = (U8)(IMPORTANT_PARAMETER_ADDR>>8);
	databuf[1] = (U8)(IMPORTANT_PARAMETER_ADDR);

	if(i2c_page_read(I2C0,LC51201, databuf, 130)){
		if(databuf[129] == CAL_CRC(databuf+2,127)){

		 	time_zone_con.previous_fee_num = databuf[2];
			if((databuf[3] == 1)||(databuf[3] == 2)){
				time_zone_con.zones_num = databuf[3];
			}else{
				time_zone_con.zones_num = 1;
			}
			if((databuf[4]<=8)&&(databuf[4] != 0)){
				time_zone_con.periods_num = databuf[4];

			}else{
				time_zone_con.periods_num = 1;
			}
			freeze_config.dingshi_freeze_time[0] = databuf[7];
			freeze_config.dingshi_freeze_time[1] = databuf[8];
			freeze_config.dingshi_freeze_time[2] = databuf[9];
			freeze_config.dingshi_freeze_time[3] = databuf[10];
			//负荷存储条数
			load_config.load_save_config[first_kind_load].load_save_num = databuf[11]+(databuf[12]<<8)+(databuf[12]<<16)+(databuf[12]<<24);
			load_config.load_save_config[second_kind_load].load_save_num = databuf[15]+(databuf[16]<<8)+(databuf[17]<<16)+(databuf[18]<<24);
			load_config.load_save_config[third_kind_load].load_save_num = databuf[19]+(databuf[20]<<8)+(databuf[21]<<16)+(databuf[22]<<24);
			load_config.load_save_config[fourth_kind_load].load_save_num = databuf[23]+(databuf[24]<<8)+(databuf[25]<<16)+(databuf[26]<<24);
			load_config.load_save_config[fifth_kind_load].load_save_num = databuf[27]+(databuf[28]<<8)+(databuf[29]<<16)+(databuf[30]<<24);
			load_config.load_save_config[sixth_kind_load].load_save_num = databuf[31]+(databuf[32]<<8)+(databuf[33]<<16)+(databuf[34]<<24);
			//上一次结算日发生的月份
			balance_config[0].last_balance_month = databuf[35];
			balance_config[1].last_balance_month = databuf[36];
			balance_config[2].last_balance_month = databuf[37];

			for(i=0;i<MAX_CYC_SAVE_NUM;i++){
				start_position[i] = databuf[40+4*i]+(databuf[41+4*i]<<8)+(databuf[42+4*i]<<16)+(databuf[43+4*i]<<24);
			}
			//按键有效时间
			key_program_time = 	databuf[80]+(databuf[81]<<8)+(databuf[82]<<16)+(databuf[83]<<24);
			if(key_program_time){
				key_program = 1;
			}
			//远程报警状态
	  		output_control.alarm_deal = databuf[84];
			//远程保电状态
			output_control.keep_deal = databuf[85];
			//继电器操作状态
			output_control.relay_deal = databuf[86];
			//编程事件状态
			event_config.programme_note.event_state = databuf[87];

			a_phase_info.positive_AH  = databuf[88]+(databuf[89]<<8)+(databuf[90]<<16)+(databuf[91]<<24);
			a_phase_info.negetive_AH  = databuf[92]+(databuf[93]<<8)+(databuf[94]<<16)+(databuf[95]<<24);

			b_phase_info.positive_AH  = databuf[96]+(databuf[97]<<8)+(databuf[98]<<16)+(databuf[99]<<24);
			b_phase_info.negetive_AH  = databuf[100]+(databuf[101]<<8)+(databuf[102]<<16)+(databuf[103]<<24);

			c_phase_info.positive_AH  = databuf[104]+(databuf[105]<<8)+(databuf[106]<<16)+(databuf[107]<<24);
			c_phase_info.negetive_AH  = databuf[108]+(databuf[109]<<8)+(databuf[110]<<16)+(databuf[111]<<24);
			return 1;
		}else{

			time_zone_con.previous_fee_num = 1;
			time_zone_con.zones_num = 1;
			time_zone_con.periods_num = 1;
			freeze_config.dingshi_freeze_time[0] = 99;
			freeze_config.dingshi_freeze_time[1] = 99;
			freeze_config.dingshi_freeze_time[2] = 99;
			freeze_config.dingshi_freeze_time[3] = 99;
			//负荷存储条数
			load_config.load_save_config[first_kind_load].load_save_num = 0;
			load_config.load_save_config[second_kind_load].load_save_num = 0;
			load_config.load_save_config[third_kind_load].load_save_num = 0;
			load_config.load_save_config[fourth_kind_load].load_save_num = 0;
			load_config.load_save_config[fifth_kind_load].load_save_num = 0;
			load_config.load_save_config[sixth_kind_load].load_save_num = 0;
			for(i=0;i<MAX_CYC_SAVE_NUM;i++){
				start_position[i] = 0;
			}
	
			key_program_time = 0;
			output_control.alarm_deal = 0;
			output_control.keep_deal = 0;
			output_control.relay_deal = 0;

			a_phase_info.positive_AH  = 0;
			a_phase_info.negetive_AH  = 0;

			b_phase_info.positive_AH  = 0;
			b_phase_info.negetive_AH  = 0;

			c_phase_info.positive_AH  = 0;
			c_phase_info.negetive_AH  = 0;
		}
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称: change_cyc_save_position
** 功能描述: 转换一个循环存储项位置
** 输　入:  item  存储项编号
**			num   原本的条数
**			buf   转换后的数据
** 输　出: 1 成功
**		   0 失败
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 change_cyc_save_position(cyc_save_item item,U8 num,U8 *buf,U8 mode){	
	U8 id = 0;
	switch(item)
	{
		case ENERGY:		//1到12
		case DEMAND:
		case TIMING_FREEZE:		   //1-12
		{
			if((start_position[item] >= num)||(mode)){
				id = start_position[item]%0x0c;
				if(num<=(id+1)){
					id = id+1-(num-1);
				}else{
					id = 0x0d-(num-1)+id;
				}
				*buf = id;
				return 1;
			}
		}
		break;
		case INSTANTANEOUS_FREEZE:	 //1-3
		{
			if((start_position[item] >= num)||(mode)){
				id = start_position[item]%0x03;
				if(num<=(id+1)){
					id = id+1-(num-1);
				}else{
					id = 0x04-(num-1)+id;
				}
				*buf = id;
				return 1;
			}
		}
		break;
		case ZONES_CHANGE_ENERGY:	 //1-2
		case PERIODS_CHANGE_ENERGY:
		case ELECTROVALENCE_FREEZE:
		case FEES_FREEZE:
		{
			if((start_position[item] >= num)||(mode)){
				id = start_position[item]%0x02;
				if(num<=(id+1)){
					id = id+1-(num-1);
				}else{
					id = 0x03-(num-1)+id;
				}
				*buf = id;
				return 1;
			}
		}
		break;
		case WHOLE_HOUR_FREEZE:
		{
			if((start_position[item] >= num)||(mode)){
				id = start_position[item]%MAX_WHOLE_HOUR_FREEZE_NUM;
				if(num<=(id+1)){
					id = id+1-(num-1);
				}else{
					id = 0xff-(num-1)+id;
				}
				*buf = id;
				return 1;
			}
		}
		break;
		case DAY_FREEZE:
		{
			if((start_position[item] >= num)||(mode)){
				id = start_position[item]%MAX_DAY_FREEZE_NUM;
				if(num<=(id+1)){
					id = id+1-(num-1);
				}else{
					id = 63-(num-1)+id;
				}
				*buf = id;
				return 1;
			}
		}
		break;
		default:
		break;
	}
	return 0;
}
