/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#include <rtl.h>
#include <LPC17xx.h>
#include <math.h>
#include "config.h"
#include "ade7878.h"
#include "ade7878_adjust.h"
#include "firmware.h"
#include "lpc_i2c.h"
#include "eeprom.h"
#include "crc.h"
/*****************************************************************************/

U8 ade7878_adjust_state = 0; //0x55 正在进行调零
/*********************************************************************************************************
** 函数名称:     ade7878_adjust_read_base
** 功能描述:     计量校准读取base值 
** 输　入:       
**				 
**				 
** 输　出:       1 成功				 
** 				 0 失败
** 全局变量: 				   
** 调用模块: 
**
** 作　者: 
** 日　期: 				
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 ade7878_adjust_read_base(U8 *databuf,U8 *datalong)
{
	U8 i,buf[101];

	buf[0] = (U8)(ADJUST_BASE_DATA_ADDR>>8);
	buf[1] = (U8)ADJUST_BASE_DATA_ADDR;
   	if(i2c_page_read(I2C0,LC51200, buf,ADJUST_BASE_DATA_NUM+3)){
		if(CAL_CRC(buf+2,ADJUST_BASE_DATA_NUM)== buf[ADJUST_BASE_DATA_NUM+2]){
			for(i=0;i<ADJUST_BASE_DATA_NUM;i++){
				*(databuf+i) = buf[i+2];
			}
			*datalong = ADJUST_BASE_DATA_NUM;
			return 1;
		}	
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称:     ade7878_adjust_write_base
** 功能描述:     计量校准写入base值 
** 输　入:       
**				 
**				 
** 输　出:       1 成功				 
** 				 0 失败
** 全局变量: 				   
** 调用模块: 
**
** 作　者: 
** 日　期: 				
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 ade7878_adjust_write_base(U8 *databuf,U8 datalong)
{
	U8 i,buf[101];

	if(datalong<=ADJUST_BASE_DATA_NUM){

		buf[0] = (U8)(ADJUST_BASE_DATA_ADDR>>8);
		buf[1] = (U8)ADJUST_BASE_DATA_ADDR;
	
		for(i=0;i<datalong;i++){
			buf[i+2] = *(databuf+i);
		}
	   	
		buf[datalong+2] = CAL_CRC(buf+2,datalong);
		setValue_EEWP(EEWP1,0);
	   	if(!i2c_page_write(I2C0,LC51200,buf,datalong+3)){
			setValue_EEWP(EEWP1,1);
			return 1;
	   	}
		setValue_EEWP(EEWP1,1);

	}
	return 0;

}  
/*********************************************************************************************************
** 函数名称:     ade7878_adjust_gain
** 功能描述:     ade7878增益 
** 输　入:       
**				 
**				 
** 输　出:       1 成功				 
** 				 0 失败
** 全局变量: 				   
** 调用模块: 
**
** 作　者: 
** 日　期: 				进入本函数有三个条件，主机发送的命令正确，短路环短接，编程键按下
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 ade7878_adjust_gain(U8 *buf,U8 datalong)
{
	U32 ade7878_adjust_data1 = 0;
	U8 flag_adjust1 = 0;
	 
	if(datalong == 98){
		//a相参数调整
		//a相电流增益
		ade7878_adjust_data1 = (*(buf+43)<<8)+*(buf+42);
		if(ade7878_adjust_data1&0x8000){
			ade7878_adjust_data1  = (((~ade7878_adjust_data1)+1)&0x07fff);
			ade7878_adjust_data1 = (U32)(((float)ade7878_adjust_data1)*0x800000/MAX_CURRENT);
			ade7878_adjust_data1 = ((~(ade7878_adjust_data1)+1)&0x0fffffff);
		}else{
			ade7878_adjust_data1 = (U32)(((float)ade7878_adjust_data1)*0x800000/MAX_CURRENT);
		}
		if(Ade7878_WriteData(AIGAIN,ade7878_adjust_data1,4)){
			flag_adjust1++;
		}

		//a相电压增益
		ade7878_adjust_data1 = (*(buf+37)<<8)+*(buf+36);
		if(ade7878_adjust_data1&0x8000){
			ade7878_adjust_data1  = (((~ade7878_adjust_data1)+1)&0x07fff);
			ade7878_adjust_data1 = (U32)(((float)ade7878_adjust_data1)*0x800000/RATING_VOLTAGE);
			ade7878_adjust_data1 = ((~(ade7878_adjust_data1)+1)&0x0fffffff);	
		}else{
			ade7878_adjust_data1 = (U32)(((float)ade7878_adjust_data1)*0x800000/RATING_VOLTAGE);
		}
		if(Ade7878_WriteData(AVGAIN,ade7878_adjust_data1,4)){
			flag_adjust1++;
		}

		//b相参数调整
		//b相电流增益
		ade7878_adjust_data1 = (*(buf+45)<<8)+*(buf+44);
		if(ade7878_adjust_data1&0x8000){
			ade7878_adjust_data1  = (((~ade7878_adjust_data1)+1)&0x07fff);
			ade7878_adjust_data1 = (U32)(((float)ade7878_adjust_data1)*0x800000/MAX_CURRENT);
			ade7878_adjust_data1 = ((~(ade7878_adjust_data1)+1)&0x0fffffff);
		}else{
	   		ade7878_adjust_data1 = (U32)(((float)ade7878_adjust_data1)*0x800000/MAX_CURRENT);
		}
		if(Ade7878_WriteData(BIGAIN,ade7878_adjust_data1,4)){
			flag_adjust1++;
		}
		//b相电压增益
		ade7878_adjust_data1 = (*(buf+39)<<8)+*(buf+38);
		if(ade7878_adjust_data1&0x8000){
			ade7878_adjust_data1  = (((~ade7878_adjust_data1)+1)&0x07fff);
			ade7878_adjust_data1 = (U32)(((float)ade7878_adjust_data1)*0x800000/RATING_VOLTAGE);
			ade7878_adjust_data1 = ((~(ade7878_adjust_data1)+1)&0x0fffffff);	
		}else{
			ade7878_adjust_data1 = (U32)(((float)ade7878_adjust_data1)*0x800000/RATING_VOLTAGE);
		}
		if(Ade7878_WriteData(BVGAIN,ade7878_adjust_data1,4)){
			flag_adjust1++;
		}
		
//		//c相参数调整
//		//c相电流增益
		ade7878_adjust_data1 = (*(buf+47)<<8)+*(buf+46);
		if(ade7878_adjust_data1&0x8000){
			ade7878_adjust_data1  = (((~ade7878_adjust_data1)+1)&0x07fff);
			ade7878_adjust_data1 = (U32)(((float)ade7878_adjust_data1)*0x800000/MAX_CURRENT);
			ade7878_adjust_data1 = ((~(ade7878_adjust_data1)+1)&0x0fffffff);
		}else{
			ade7878_adjust_data1 = (U32)(((float)ade7878_adjust_data1)*0x800000/MAX_CURRENT);
		}
		if(Ade7878_WriteData(CIGAIN,ade7878_adjust_data1,4)){
			flag_adjust1++;
		}
		//c相电压增益
		ade7878_adjust_data1 = (*(buf+41)<<8)+*(buf+40);
		if(ade7878_adjust_data1&0x8000){
			ade7878_adjust_data1  = (((~ade7878_adjust_data1)+1)&0x07fff);
			ade7878_adjust_data1 = (U32)(((float)ade7878_adjust_data1)*0x800000/RATING_VOLTAGE);
			ade7878_adjust_data1 = ((~(ade7878_adjust_data1)+1)&0x0fffffff);
		}else{
			ade7878_adjust_data1 = (U32)(((float)ade7878_adjust_data1)*0x800000/RATING_VOLTAGE);
		}
		if(Ade7878_WriteData(CVGAIN,ade7878_adjust_data1,4)){
			flag_adjust1++;
		}
		
		//a相电流偏置
		a_phase_info.current_offset = (((*buf+(*(buf+1)<<8)+(*(buf+2)<<16)+(*(buf+3)<<24))));
//		a_phase_info.current_offset = (((*buf+(*(buf+1)<<8)+(*(buf+2)<<16)+(*(buf+3)<<24))&0x0fffffff));
//		if(Ade7878_WriteData(AIRMSOS,ade7878_adjust_data1,4)){
			flag_adjust1++;
//		}

//		ade7878_adjust_data1 = (((~0xb00)+1)&0x0fffffff);
//		if(Ade7878_WriteData(AVRMSOS,ade7878_adjust_data1,4)){
//			flag_adjust1++;
//		}
		//b相电流偏置
		b_phase_info.current_offset = (((*(buf+4)+(*(buf+5)<<8)+(*(buf+6)<<16)+(*(buf+7)<<24))));
//		b_phase_info.current_offset = (((*(buf+4)+(*(buf+5)<<8)+(*(buf+6)<<16)+(*(buf+7)<<24))&0x0fffffff));
//		if(Ade7878_WriteData(BIRMSOS,ade7878_adjust_data1,4)){
			flag_adjust1++;
//		}
//		ade7878_adjust_data1 = 0;
//		if(Ade7878_WriteData(BVRMSOS,ade7878_adjust_data1,4)){
//			flag_adjust1++;
//		}
		//c相电流偏置
		c_phase_info.current_offset = (((*(buf+8)+(*(buf+9)<<8)+(*(buf+10)<<16)+(*(buf+11)<<24))));
//		c_phase_info.current_offset = (((*(buf+8)+(*(buf+9)<<8)+(*(buf+10)<<16)+(*(buf+11)<<24))&0x0fffffff));
//		if(Ade7878_WriteData(CIRMSOS,ade7878_adjust_data1,4)){
			flag_adjust1++;
//		}

//		ade7878_adjust_data1 = 0;
//		if(Ade7878_WriteData(CVRMSOS,ade7878_adjust_data1,4)){
//			flag_adjust1++;
//		}
		//a相相位
		ade7878_adjust_data1 = (*(buf+49)<<8)+*(buf+48);
		if(ade7878_adjust_data1&0x8000){
			ade7878_adjust_data1 = ((~ade7878_adjust_data1)+1)&0x07fff;
			ade7878_adjust_data1 = (U32)((ade7878_adjust_data1)*0.001/0.0176+512);
		}else{
			ade7878_adjust_data1 = (U32)((ade7878_adjust_data1)*0.001/0.0176);
		}
		if(Ade7878_WriteData(APHCAL,ade7878_adjust_data1,2)){
			flag_adjust1++;
		}
		//b相相位
		ade7878_adjust_data1 = (*(buf+51)<<8)+*(buf+50);
		if(ade7878_adjust_data1&0x8000){
			ade7878_adjust_data1 = ((~ade7878_adjust_data1)+1)&0x07fff;
			ade7878_adjust_data1 = (U32)((ade7878_adjust_data1)*0.001/0.0176+512);
			
		}else{
			ade7878_adjust_data1 = (U32)((ade7878_adjust_data1)*0.001/0.0176);
		}
		if(Ade7878_WriteData(BPHCAL,ade7878_adjust_data1,2)){
			flag_adjust1++;
		}
		//c相相位
		ade7878_adjust_data1 = (*(buf+53)<<8)+*(buf+52);
		if(ade7878_adjust_data1&0x8000){
			ade7878_adjust_data1 = ((~ade7878_adjust_data1)+1)&0x07fff;
			ade7878_adjust_data1 = (U32)((ade7878_adjust_data1)*0.001/0.0176+512);
		}else{
			ade7878_adjust_data1 = (U32)((ade7878_adjust_data1)*0.001/0.0176);
		}
		if(Ade7878_WriteData(CPHCAL,ade7878_adjust_data1,2)){
			flag_adjust1++;
		}
		//a相电压
		a_phase_info.voltage_mul = (*(buf+55)<<8)+*(buf+54);
		//b相电压
		b_phase_info.voltage_mul = (*(buf+57)<<8)+*(buf+56);
		//c相电压
		c_phase_info.voltage_mul = (*(buf+59)<<8)+*(buf+58);
		
		
		if(flag_adjust1 == 12){
			return 1;
		}	
	}
	return 0;	
}
/*********************************************************************************************************
** 函数名称:     ade7878_adjust_zero
** 功能描述:     ade7878调零
** 输　入:       
**				 
**				 
** 输　出:       无				 
** 				 无
** 全局变量: 				   Ade7878_ReadData(U16 addr,U32 *data,U8 datalong)
** 调用模块: 
**
** 作　者: 
** 日　期: 				进入本函数有三个条件，主机发送的命令正确，短路环短接，编程键按下
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
__task void ade7878_adjust_zero(void)
{
	U32 ade7878_adjust_data = 0;
	U16 ade7878_adjust_count = 0;
	U8 databuf[ADJUST_BASE_DATA_NUM],i,datalong = 0;

	ade7878_adjust_t a_phase_current_zero,b_phase_current_zero,c_phase_current_zero;

	a_phase_current_zero.data_sum = 0;
	a_phase_current_zero.data_count = 0;

	b_phase_current_zero.data_sum = 0;
	b_phase_current_zero.data_count = 0;

	c_phase_current_zero.data_sum = 0;
	c_phase_current_zero.data_count = 0;

	ade7878_adjust_state = 0x55;

	

	if(!ade7878_adjust_read_base(databuf,&datalong)){
		for(i=0;i<ADJUST_BASE_DATA_NUM;i++){
			databuf[i] = 0;
		}
	}

	for(;;){
		U8 flag_adjust = 0,i;

		ade7878_adjust_state = ADJUST_ZERO_MODE;
		ade7878_adjust_count++;
		//电流调零
		if(Ade7878_ReadData(AIRMS,&ade7878_adjust_data,4)){
	   		if(ade7878_adjust_data&0x800000){
				ade7878_adjust_data |= 0x0ff000000;
			}
			if((fabs((S32)ade7878_adjust_data))<= MAX_ADJUST_CURRENT_ZERO){
				a_phase_current_zero.data_sum += ade7878_adjust_data;
				a_phase_current_zero.data_count++;	
			}
		}
		if(Ade7878_ReadData(BIRMS,&ade7878_adjust_data,4)){
	   		if(ade7878_adjust_data&0x800000){
				ade7878_adjust_data |= 0x0ff000000;
			}
			if((fabs((S32)ade7878_adjust_data))<= MAX_ADJUST_CURRENT_ZERO){
				b_phase_current_zero.data_sum += ade7878_adjust_data;
				b_phase_current_zero.data_count++;	
			}
		}
		if(Ade7878_ReadData(CIRMS,&ade7878_adjust_data,4)){
	   		if(ade7878_adjust_data&0x800000){
				ade7878_adjust_data |= 0x0ff000000;
			}
			if((fabs((S32)ade7878_adjust_data))<= MAX_ADJUST_CURRENT_ZERO){
				c_phase_current_zero.data_sum += ade7878_adjust_data;
				c_phase_current_zero.data_count++;	
			}
		}
		//电压调零      


		if(ade7878_adjust_count>=1000){
			for(i = 0;i<3;i++){
				flag_adjust = 0;
				a_phase_current_zero.data_sum /= a_phase_current_zero.data_count;
				ade7878_adjust_data = (U32)(a_phase_current_zero.data_sum*a_phase_current_zero.data_sum/128.0);
				ade7878_adjust_data = ((~ade7878_adjust_data)+1);
				
//				if(Ade7878_WriteData(AIRMSOS,ade7878_adjust_data,4)){
					a_phase_info.current_offset = ade7878_adjust_data;
					*(databuf+0) = (U8)ade7878_adjust_data;
					*(databuf+1) = (U8)(ade7878_adjust_data>>8);
					*(databuf+2) = (U8)(ade7878_adjust_data>>16);
					*(databuf+3) = (U8)(ade7878_adjust_data>>24);
					flag_adjust++;
//				}
				b_phase_current_zero.data_sum /= b_phase_current_zero.data_count;
				ade7878_adjust_data = (U32)(b_phase_current_zero.data_sum*b_phase_current_zero.data_sum/128.0);
				ade7878_adjust_data = ((~ade7878_adjust_data)+1);
				
//				if(Ade7878_WriteData(BIRMSOS,ade7878_adjust_data,4)){
					b_phase_info.current_offset = ade7878_adjust_data;
					*(databuf+4) = (U8)ade7878_adjust_data;
					*(databuf+5) = (U8)(ade7878_adjust_data>>8);
					*(databuf+6) = (U8)(ade7878_adjust_data>>16);
					*(databuf+7) = (U8)(ade7878_adjust_data>>24);
					flag_adjust++;
//				}
				c_phase_current_zero.data_sum /= c_phase_current_zero.data_count;
				ade7878_adjust_data = (U32)(c_phase_current_zero.data_sum*c_phase_current_zero.data_sum/128.0);
				ade7878_adjust_data = ((~ade7878_adjust_data)+1);
				
//				if(Ade7878_WriteData(CIRMSOS,ade7878_adjust_data,4)){
					c_phase_info.current_offset = ade7878_adjust_data;
					*(databuf+8) = (U8)ade7878_adjust_data;
					*(databuf+9) = (U8)(ade7878_adjust_data>>8);
					*(databuf+10) = (U8)(ade7878_adjust_data>>16);
					*(databuf+11) = (U8)(ade7878_adjust_data>>24);
					flag_adjust++;
//				}
				
				if(flag_adjust == 3){
					if(ade7878_adjust_write_base(databuf,ADJUST_BASE_DATA_NUM)){
						ade7878_adjust_state = 0;
						break;
					}
					
				}
			}
			if(i==3){
				//ade7878 调零出错
				ade7878_adjust_state = 0xee;
			}	
		   	os_tsk_delete_self ();
		}
	
	   os_dly_wait(10);			//每100ms读取一次按键
	}

}

