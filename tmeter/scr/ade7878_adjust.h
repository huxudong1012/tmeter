/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __ADE7878_ADJUST_H__
#define __ADE7878_ADJUST_H__

#define ADJUST_ZERO_MODE 0x55
#define MAX_ADJUST_CURRENT_ZERO 0x1000

#define ADJUST_BASE_DATA_ADDR 0x1964
#define ADJUST_BASE_DATA_NUM   98

typedef struct {

	S32 data_sum;  	 		//数据和
	U16 data_count;	       	//有效数据个数
	
}ade7878_adjust_t;


#define VOLTAGE_MUL ((RATING_ADE7878_VOLTAGE*ADE7878_500MV_VALUE*14.14)/500.0/RATING_VOLTAGE)
#define CURRENT_MUL ((RATING_ADE7878_CURRENT*ADE7878_500MV_VALUE*14.14)/500.0/RATING_CURRENT)


extern U8 ade7878_adjust_gain(U8 *buf,U8 datalong);
extern U8 ade7878_adjust_gain_b(U8 *buf,U8 datalong);
extern __task void ade7878_adjust_zero(void);
extern U8 ade7878_adjust_read_base(U8 *databuf,U8 *datalong);
extern U8 ade7878_adjust_write_base(U8 *databuf,U8 datalong);       
#endif
