/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __FIRMWARE_H
#define __FIRMWARE_H

#define IMPORTANT_PARAMETER_ADDR 0xff00

/**************************************************************************/
#define HAVE_ESAM 1					//是否使用ESAM  1：有ESAM  0： 无ESAM
#define CPU_CARD  0					//是否使用CPU卡 1： 有卡   0：无卡

#define UN100_1_5_6_0		0		//100V 三相三线1.5(6)
#define UN220_1_5_6_0		1		//220V 三相四线1.5(6)
#define UN220_10_40			0		//220V 三相四线10(40)

#define THREE_PHASE_METER_TYPE 1    //1代表 3相4线  0: 3相3线

#define TEST_VERSION 1				//是否为测试版，1：是 ，0： 不是
/****************************************************************************/

#if UN100_1_5_6_0 == 1
	#define RATING_ADE7878_CURRENT   82.0 		//mV  ade7878 额定电流对应值
	#define RATING_ADE7878_VOLTAGE   271.0 		//mV  ade7878 额定电压对应值
	#define  ACTIVE_PULSE_CONSTANT 20000	 	//有功脉冲常数
	#define  REACTIVE_PULSE_CONSTANT 20000 		//无功脉冲常数
	#define  APPARENT_PULSE_CONSTANT 20000 		//视在脉冲常数
	#define RATING_VOLTAGE   100000    			//mV   额定电压 100  线电压
	#define RATING_CURRENT   1500   			//mA	  额定电流 1.5A
	#define MAX_CURRENT      6000   			//mA   最大电流 6A
#endif

#if UN220_1_5_6_0 == 1
	#define RATING_ADE7878_CURRENT   82.0 //mV  ade7878 额定电流对应值
	#define RATING_ADE7878_VOLTAGE   286.77 //mV  ade7878 额定电压对应值
	//#define RATING_ADE7878_VOLTAGE   287.73 //mV  ade7878 额定电压对应值	
	#define  ACTIVE_PULSE_CONSTANT 6400	  //有功脉冲常数
	#define  REACTIVE_PULSE_CONSTANT 6400 //无功脉冲常数
	#define  APPARENT_PULSE_CONSTANT 6400 //视在脉冲常数
	#define RATING_VOLTAGE   220000    //mV   额定电压 2200  相电压
	#define RATING_CURRENT   1500   //mA	  额定电流 1.5A
	#define MAX_CURRENT      6000   //mA   最大电流 6A
#endif

#if UN220_10_40 == 1
	#define RATING_ADE7878_CURRENT   40.8 //mV  ade7878 额定电流对应值
	#define RATING_ADE7878_VOLTAGE   286.77 //mV  ade7878 额定电压对应值
	//#define RATING_ADE7878_VOLTAGE   287.73 //mV  ade7878 额定电压对应值#define RATING_VOLTAGE   220000    //mV   额定电压 220  相
	#define  ACTIVE_PULSE_CONSTANT 600	  //有功脉冲常数
	#define  REACTIVE_PULSE_CONSTANT 600 //无功脉冲常数
	#define  APPARENT_PULSE_CONSTANT 600 //视在脉冲常数
	#define RATING_VOLTAGE   220000    //mV   额定电压 2200  相电压
	#define RATING_CURRENT   10000   //mA	  额定电流 10A
	#define MAX_CURRENT      40000   //mA   最大电流 40A
#endif

#define ADE7878_500MV_VALUE 4191910

#define NET_FREQUENCY  50 //电网频率
#define Pi 3.1415926535898

#define UFS  (500/(RATING_ADE7878_VOLTAGE*1.414)*RATING_VOLTAGE)
#define IFS  (500/(RATING_ADE7878_CURRENT*1.414)*RATING_CURRENT)

#define PMAX_LOW24BIT   0x0ff6a6b
#define PMAX_HIGHT24BIT 0x1

#define CF1DEN_VALUE  ((8000.0*3600.0*1000.0)/(UFS/1000.0)/(IFS/1000.0)/ACTIVE_PULSE_CONSTANT)	 //总有功
#define CF2DEN_VALUE  ((8000.0*3600.0*1000.0)/(UFS/1000.0)/(IFS/1000.0)/REACTIVE_PULSE_CONSTANT) //总无功
#define CF3DEN_VALUE  ((8000.0*3600.0*1000.0)/(UFS/1000.0)/(IFS/1000.0)/APPARENT_PULSE_CONSTANT) //总视在


extern U8 important_parametrer_read(void);
extern U8 important_parametrer_save(void);

//重复储存位置

	//能量
	#define MAX_ENERGY_NUM   12   //1到12   上1次为1
	//需量
	#define MAX_DEMAND_NUM   12   //1到12   上1次为1
	//记录
	#define MAX_A_PHASE_LOSS_VOLTAGE_NUM   10   //1到10  上1次为1
	#define MAX_B_PHASE_LOSS_VOLTAGE_NUM   10   //1到10  上1次为1
	#define MAX_C_PHASE_LOSS_VOLTAGE_NUM   10   //1到10  上1次为1

	#define MAX_A_PHASE_OWE_VOLTAGE_NUM   10   //1到10  上1次为1
	#define MAX_B_PHASE_OWE_VOLTAGE_NUM   10   //1到10  上1次为1
	#define MAX_C_PHASE_OWE_VOLTAGE_NUM   10   //1到10  上1次为1

	#define MAX_A_PHASE_OVER_VOLTAGE_NUM   10   //1到10  上1次为1
	#define MAX_B_PHASE_OVER_VOLTAGE_NUM   10   //1到10  上1次为1
	#define MAX_C_PHASE_OVER_VOLTAGE_NUM   10   //1到10  上1次为1

	#define MAX_A_PHASE_BREAK_VOLTAGE_NUM   10   //1到10  上1次为1
	#define MAX_B_PHASE_BREAK_VOLTAGE_NUM   10   //1到10  上1次为1
	#define MAX_C_PHASE_BREAK_VOLTAGE_NUM   10   //1到10  上1次为1

	#define MAX_ALL_LOSS_VOLTAGE_NUM   10   //1到10  上1次为1

	#define MAX_ASSISTANT_POWER_DOWN_NUM   10   //1到10  上1次为1

	#define MAX_VOLTAGE_CONVERSE_NUM   10   //1到10  上1次为1

	#define MAX_CURRENT_CONVERSE_NUM   10   //1到10  上1次为1

	#define MAX_VOLTAGE_NO_BALANCE_NUM   10   //1到10  上1次为1

	#define MAX_CURRENT_NO_BALANCE_NUM   10   //1到10  上1次为1

	#define MAX_A_PHASE_LOSS_CURRENT_NUM   10   //1到10  上1次为1
	#define MAX_B_PHASE_LOSS_CURRENT_NUM   10   //1到10  上1次为1
	#define MAX_C_PHASE_LOSS_CURRENT_NUM   10   //1到10  上1次为1

	#define MAX_A_PHASE_OVER_CURRENT_NUM   10   //1到10  上1次为1
	#define MAX_B_PHASE_OVER_CURRENT_NUM   10   //1到10  上1次为1
	#define MAX_C_PHASE_OVER_CURRENT_NUM   10   //1到10  上1次为1

	#define MAX_A_PHASE_BREAK_CURRENT_NUM   10   //1到10  上1次为1
	#define MAX_B_PHASE_BREAK_CURRENT_NUM   10   //1到10  上1次为1
	#define MAX_C_PHASE_BREAK_CURRENT_NUM   10   //1到10  上1次为1

	#define MAX_A_PHASE_POWER_CONVERSE_NUM   10   //1到10  上1次为1
	#define MAX_B_PHASE_POWER_CONVERSE_NUM   10   //1到10  上1次为1
	#define MAX_C_PHASE_POWER_CONVERSE_NUM   10   //1到10  上1次为1

	#define MAX_A_PHASE_POWER_OVER_NUM   10   //1到10  上1次为1
	#define MAX_B_PHASE_POWER_OVER_NUM   10   //1到10  上1次为1
	#define MAX_C_PHASE_POWER_OVER_NUM   10   //1到10  上1次为1

	#define MAX_VOLTAGE_REGULAR_RATIO_NUM   12   //1到12  上1次为1

	#define MAX_A_VOLTAGE_REGULAR_RATIO_NUM   12   //1到12  上1次为1
	#define MAX_B_VOLTAGE_REGULAR_RATIO_NUM   12   //1到12  上1次为1
	#define MAX_C_VOLTAGE_REGULAR_RATIO_NUM   12   //1到12  上1次为1

	#define MAX_POWER_DOWN_TIME_NUM   10   //1到10  上1次为1

	#define MAX_POSITIVE_ACTIVE_DEMAND_NUM   10   //1到10  上1次为1
	#define MAX_NEGETIVE_ACTIVE_DEMAND_NUM   10   //1到10  上1次为1
	#define MAX_FIRST_QUADRANT_REACTIVE_DEMAND_NUM   10   //1到10  上1次为1
	#define MAX_SECOND_QUADRANT_REACTIVE_DEMAND_NUM   10   //1到10  上1次为1
	#define MAX_THIRD_QUADRANT_REACTIVE_DEMAND_NUM   10   //1到10  上1次为1
	#define MAX_FOURTH_QUADRANT_REACTIVE_DEMAND_NUM   10   //1到10  上1次为1

	#define MAX_PROGRAM_NOTE_NUM   10   //1到10  上1次为1
	#define MAX_METER_CLEAR_NUM   10   //1到10  上1次为1
	#define MAX_DEMAND_CLEAR_NUM   10   //1到10  上1次为1
	#define MAX_EVENT_CLEAR_NUM   10   //1到10  上1次为1
	#define MAX_VERIFY_TIME_NUM   10   //1到10  上1次为1

	#define MAX_PERIODS_PROGRAM_NUM   10   //1到10  上1次为1
	#define MAX_ZONES_PROGRAM_NUM   10   //1到10  上1次为1
	#define MAX_WEEKDAY_PROGRAM_NUM   10   //1到10  上1次为1
	#define MAX_HOLIDAY_PROGRAM_NUM   10   //1到10  上1次为1
//	#define MAX_ZONES_PROGRAM_NUM   10   //1到10  上1次为1
	#define MAX_ACTIVE_MODE_PROGRAM_NUM   10   //1到10  上1次为1
	#define MAX_REACTIVE_MODE1_PROGRAM_NUM   10   //1到10  上1次为1
	#define MAX_REACTIVE_MODE2_PROGRAM_NUM   10   //1到10  上1次为1

	#define MAX_CALCULATE_DAY_PROGRAM_NUM   10   //1到10  上1次为1
	#define MAX_OPEN_METER_SHELL_NUM   10   //1到10  上1次为1
	#define MAX_OPEN_METER_END_SHELL_NUM   10   //1到10  上1次为1


	//冻结
	#define MAX_TIMING_FREEZE_NUM   12   //1到12  上1次为1
	#define MAX_INSTANTANEOUS_FREEZE_NUM   3   //1到3  上1次为1
	#define MAX_ZONES_CHANGE_ENERGY_NUM   2   //1到2  上1次为1
	#define MAX_PERIODS_CHANGE_ENERGY_NUM   2   //1到2  上1次为1
	#define MAX_WHOLE_HOUR_FREEZE_NUM   254   //1到2  上1次为1
	#define MAX_DAY_FREEZE_NUM   62   //1到62  上1次为1


	#define MAX_CYC_SAVE_NUM 10 

	/* 循环储存项定义 */
	typedef enum {
	  ENERGY = 0,		   //	能量
	  DEMAND,			   //	 需量
	  TIMING_FREEZE,		  //定时冻结
	  INSTANTANEOUS_FREEZE,	 //瞬时冻结能量
	  ZONES_CHANGE_ENERGY,	 //时区表切换能量
	  PERIODS_CHANGE_ENERGY,	//时段表切换能量
	  WHOLE_HOUR_FREEZE,		//整时冻结
	  DAY_FREEZE,				//日冻结
	  ELECTROVALENCE_FREEZE,   //两套阶梯电价
	  FEES_FREEZE			   //两套阶梯费率
	  			
	} cyc_save_item;

extern U8 change_cyc_save_position(cyc_save_item item,U8 num,U8 *buf,U8 mode);

extern U32 start_position[MAX_CYC_SAVE_NUM];

#endif //__FIRMWARE_H
