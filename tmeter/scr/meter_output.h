/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __METER_OUTPUT_H
#define __METER_OUTPUT_H


//有功脉冲和无功脉冲

//有功脉冲 p2.8 高脉冲80ms
#define P_PULSE_SEL    (LPC_PINCON->PINSEL2 &= 0x3fffffff) // 选择p2.8为GPIO  p1.15
#define P_PULSE_DIR    (LPC_GPIO1->FIODIR |= (1<<15)) // 选择p2.8为输出
#define P_PULSE(x)	   (x?(LPC_GPIO1->FIOSET |= (1<<15)):(LPC_GPIO1->FIOCLR |= (1<<15)))//设置p2.8为高或低

//无功脉冲 p2.7	高脉冲80ms   
#define Q_PULSE_SEL    (LPC_PINCON->PINSEL2 &= ~((3<< 28))) // 选择p2.7为GPIO	  p1.14
#define Q_PULSE_DIR    (LPC_GPIO1->FIODIR |= (1<<14)) // 选择p2.7为输出
#define Q_PULSE(x)	   (x?(LPC_GPIO1->FIOSET |= (1<<14)):(LPC_GPIO1->FIOCLR |= (1<<14)))//设置p2.7为高或低

//继电器状态指示 p1.19 低电平
#define DISPLAY_ARK_SEL    (LPC_PINCON->PINSEL0 &= ~(3)) // 选择p1.19为GPIO				 p0.0
#define DISPLAY_ARK_DIR    (LPC_GPIO0->FIODIR |= 1) // 选择p1.19为输出
#define DISPLAY_ARK(x)	   (x?(LPC_GPIO0->FIOSET |= 1):(LPC_GPIO0->FIOCLR |= 1))//设置p1.19为高或低

//报警状态指示p2.6	 低电平
#define DISPLAY_ALARM_SEL    (LPC_PINCON->PINSEL3 &= ~((3<< 20))) // 选择p2.6为GPIO		 p1.26
#define DISPLAY_ALARM_DIR    (LPC_GPIO1->FIODIR |= (1<<26)) // 选择p2.6为输出
#define DISPLAY_ALARM(x)	   (x?(LPC_GPIO1->FIOSET |= (1<<26)):(LPC_GPIO1->FIOCLR |= (1<<26)))//设置p2.6为高或低

//lcd背光控制 p1.22	 高电平
#define BG_CTRL_SEL    (LPC_PINCON->PINSEL3 &= ~((3<< 16))) // 选择p1.22为GPIO			  p1.24
#define BG_CTRL_DIR    (LPC_GPIO1->FIODIR |= (1<<24)) // 选择p1.22为输出
#define BG_CTRL(x)	   (x?(LPC_GPIO1->FIOSET |= (1<<24)):(LPC_GPIO1->FIOCLR |= (1<<24)))//设置p1.22为高或低

//磁保持继电器开p1.18  高脉冲100ms
#define RELAY_ON_SEL    (LPC_PINCON->PINSEL3 &= ~((3<< 24))) // 选择p1.18为GPIO			  //无用 p1.28
#define RELAY_ON_DIR    (LPC_GPIO1->FIODIR |= (1<<28)) // 选择p1.18为输出
#define RELAY_ON(x)	   (x?(LPC_GPIO1->FIOSET |= (1<<28)):(LPC_GPIO1->FIOCLR |= (1<<28)))//设置p1.18为高或低

//磁保持继电器关p1.25（与2路电磁继电器公用）高脉冲100ms								  p1.10
//2路电磁继电器p1.25 高电平
#define RELAY_OFF_SEL    (LPC_PINCON->PINSEL2 &= ~((3<< 20))) // 选择p1.25为GPIO
#define RELAY_OFF_DIR    (LPC_GPIO1->FIODIR |= (1<<10)) // 选择p1.25为输出
#define RELAY_OFF(x)	 (x?(LPC_GPIO1->FIOSET |= (1<<10)):(LPC_GPIO1->FIOCLR |= (1<<10)))//设置p1.25为高或低

//1路电磁继电器p1.24 高电平
#define RELAY_ALARM_SEL    (LPC_PINCON->PINSEL2 &= ~((3<< 18))) // 选择p1.24为GPIO		 p1.9
#define RELAY_ALARM_DIR    (LPC_GPIO1->FIODIR |= (1<<9)) // 选择p1.24为输出
#define RELAY_ALARM(x)	 (x?(LPC_GPIO1->FIOSET |= (1<<9)):(LPC_GPIO1->FIOCLR |= (1<<9)))//设置p1.24为高或低

//喂狗控制	P0.23  每1S出现一次状态转换，出现上升沿或下降沿
#define WDI_MCU_SEL    (LPC_PINCON->PINSEL1 &= ~((3<< 14))) // 选择p0.23为GPIO
#define WDI_MCU_DIR    (LPC_GPIO0->FIODIR |= (1<<23)) // 选择p0.23为输出
#define WDI_MCU(x)	 (x?(LPC_GPIO0->FIOSET |= (1<<23)):(LPC_GPIO0->FIOCLR |= (1<<23)))//设置p0.23为高或低

//停抄电源控制 p1.1	  连续的低脉冲
#define MAINCPU_SEL    (LPC_PINCON->PINSEL3 &= ~(3)) // 选择p1.1为GPIO	 p1.16
#define MAINCPU_DIR    (LPC_GPIO1->FIODIR |= (1<<16)) // 选择p1.1为输出
#define MAINCPU(x)	 (x?(LPC_GPIO1->FIOSET |= (1<<16)):(LPC_GPIO1->FIOCLR |= (1<<16)))//设置p1.1为高或低

//时钟与多功能输出选择 p1.4 高时多功能输出端子输出时钟，低时输出其他			 p1.23
#define FOE_SEL    (LPC_PINCON->PINSEL3 &= ~((3<< 14))) // 选择p1.4为GPIO
#define FOE_DIR    (LPC_GPIO1->FIODIR |= (1<<23)) // 选择p1.4为输出
#define FOE(x)	   (x?(LPC_GPIO1->FIOSET |= (1<<23)):(LPC_GPIO1->FIOCLR |= (1<<23)))//设置p1.4为高或低


//多功能输出 p1.26 低或者高脉冲
#define MULTIPLE_PORT_SEL    (LPC_PINCON->PINSEL2 &= ~((3<< 16))) // 选择p1.26为GPIO	  p1.8
#define MULTIPLE_PORT_DIR    (LPC_GPIO1->FIODIR |= (1<<8)) // 选择p1.26为输出
#define MULTIPLE_PORT(x)	 (x?(LPC_GPIO1->FIOSET |= (1<<8)):(LPC_GPIO1->FIOCLR |= (1<<8)))//设置p1.26为高或低

//蜂鸣器输出 p2.5 高电平
#define SOUND_ALARM_SEL    (LPC_PINCON->PINSEL3 &= ~((3<< 18))) // 选择p2.5为GPIO		p1.25
#define SOUND_ALARM_DIR    (LPC_GPIO1->FIODIR |= (1<<25)) // 选择p2.5为输出
#define SOUND_ALARM(x)	   (x?(LPC_GPIO1->FIOSET |= (1<<25)):(LPC_GPIO1->FIOCLR |= (1<<25)))//设置p2.5为高或低

extern U8 P_Pulse_Flag;//默认不产生脉冲，非零产生一个80ms高脉冲
extern U8 Q_Pulse_Flag;//默认不产生脉冲，非零产生一个80ms高脉冲
extern U8 Display_Ark_Flag;//0 不亮  1常亮   2闪烁1Hz
extern U8 Display_Alarm_Flag;//0 不亮  1常亮
extern U8 BG_Ctrl_Flag;//0 不亮  1常亮
extern U8 Hold_Relay_Flag;//0 不处理  1打开   2 关闭
extern U8 Electromagnetism_Relay_Flag;//0 不通  1通
extern U8 Sound_Alarm_Flag;//0 不响  1响
extern U8 Main_Mcu_Flag;//0 不通  1通
extern U8 FOE_Flag;//0 时钟  1其他功能指示
extern U8 Multiple_Port_Flag;//0 无  有1个切换点输出80ms低脉冲

extern void Init_Meter_Output(void);
extern  void meter_output (void);

typedef struct {
	U8 relay_deal;//继电器操作指令
	U8 alarm_deal;//报警操作指令
	U8 keep_deal;//保电操作指令	
}output_control_t;

extern output_control_t output_control;

extern U32 P_Pulse_Creat_time;	//开始产生的时间
extern U32 Q_Pulse_Creat_time;	//开始产生的时间


#endif /*__METER_OUTPUT_H */
