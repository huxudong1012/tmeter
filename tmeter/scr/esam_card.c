/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#include <RTL.h>                      /* RTX kernel functions & defines      */
#include <lpc17xx.h>

#include "lpc_pwm.h"
#include "esam_card.h"
#include "lpc_timer.h"
#include "config.h"
#include "time_period_zone.h"
#include "rx8025t.h"
#include "uart_task.h"

esam_card_one_byte_t esam_one_byte,card_one_byte;	//esam或cpu card 一字节数据处理
esam_card_one_frame_t esam_one_frame;			   //esam或cpu card 一帧节数据处理

extern_control_t extern_control;  //用户身份控制
account_control_t account_control; //用户控制

extern OS_TID esam_task;

/*********************************************************************************************************
** 函数名称:     init_esam
** 功能描述:     esam相关操作初始化
** 输　入:       无
**
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
#if HAVE_ESAM == 1
U8 init_esam(void)
{
	U8 i;
	// 数据管脚
	EASM_OUT_SEL;
	EASM_OUT(1);
	EASM_IN_DIR;
	//复位控制
	EASM_RST_SEL;
	EASM_RST(0);
	EASM_RST_DIR;
	//时钟控制
	EASM_CLK(0);

	//esam 相关参数初始化
	esam_one_byte.data = 0;
	esam_one_byte.state = 0;
	esam_one_byte.flag = 0;

	esam_one_frame.data_len = 0;
	esam_one_frame.state = 0;
	esam_one_frame.flag = 0;
	for(i=0;i<MAX_ESAM_CARD_NUM;i++){
		esam_one_frame.data_buf[i] = 0;
	}
	EASM_CLK(1);//加时钟

	//设置认证状态
	extern_control.flag = 0;
	extern_control.esam_authen_minutes = 5;
	extern_control.esam_authen_time = 0;

	return 1;
}
#endif
/*********************************************************************************************************
** 函数名称:     init_account_info
** 功能描述:     初始化用户信息
** 输　入:       无
**
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 init_account_info(void)
{
	U8 databuf[10],i;
	U16 result = 0;
	//用户控制信息
   	account_control.account_flag = 0; //开户信息
	//户名
	result = esam_read_binaryfile(0x82,32,databuf,6);
	if(result == 0x9000){
   		for(i=0;i<6;i++){
			account_control.account_num[i] = databuf[6-1-i];
		}

		//取充值次数剩余金额
		account_control.crush_num = 0;
		account_control.crush_money = 0;
		return 1;
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称:     esam_one_frame_process
** 功能描述:     esam 一帧数据发送与接收
** 输　入:       无
**
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
__task void esam_one_frame_process(void)
{

	for(;;)
	{
		U8 i = 0,error_num = 0;
		U32 esam_one_byte_time = 0;

		os_evt_wait_or(EVT_ESAM,0xffff);

		tsk_lock();//不允许任务切换

		switch(esam_one_frame.flag)
		{
			case ESAM_READ_STATE:
			{
			   	for(i=0;i<esam_one_frame.data_len;i++){
					for(error_num = 0;error_num<3;error_num++){
						esam_one_byte_time = TimeTick;
						while((EASM_IN)){
							if(check_timeout(TimeTick,esam_one_byte_time,960)){
								esam_one_frame.state = ESAM_FRAME_TIME_OUT; //超时
								break;	
							}
						}
						esam_one_byte.flag = ESAM_READ_STATE;
						esam_one_byte.state = 0;
						reset_timer2();
						enable_timer2();
						esam_one_byte.data = 0;
						esam_one_byte_time = TimeTick;
						while((esam_one_byte.flag == ESAM_READ_STATE)){
							if(check_timeout(TimeTick,esam_one_byte_time,3)){
								esam_one_frame.state = ESAM_FRAME_TIME_OUT; //超时
								break;	
							}
						}
						if(esam_one_byte.flag == ESAM_COMPLETE_STATE){
							if(esam_one_byte.state == ESAM_SUCCEED_STATE){
								esam_one_frame.data_buf[esam_one_frame.data_count+i] = esam_one_byte.data;
								break;
							}
						}
						if(esam_one_frame.state == ESAM_FRAME_TIME_OUT){
							break;
						}				
					}
					if(error_num == 3){
						esam_one_frame.state = ESAM_FRAME_READ_ERR; //读错误
						break;
					}
					if(esam_one_frame.state == ESAM_FRAME_TIME_OUT){
						break;
					}			
				}
				esam_one_frame.flag = ESAM_COMPLETE_STATE;
				esam_one_frame.data_count += esam_one_frame.data_len;	
			}
			break;
			case ESAM_WRITE_STATE:
			{
				EASM_OUT_DIR;
				for(i=0;i<esam_one_frame.data_len;i++){
					for(error_num = 0;error_num<3;error_num++){
						esam_one_byte.data = esam_one_frame.data_buf[esam_one_frame.data_count+i];
						esam_one_byte.state = 0;
						esam_one_byte.flag = ESAM_WRITE_STATE;
						reset_timer2();
						enable_timer2();
						//等待操作完成
						esam_one_byte_time = TimeTick;
						while((esam_one_byte.flag == ESAM_WRITE_STATE)){
							if(check_timeout(TimeTick,esam_one_byte_time,3)){
								esam_one_frame.state = ESAM_FRAME_TIME_OUT; //超时
								break;	
							}
						}
						if(esam_one_byte.flag == ESAM_COMPLETE_STATE){
							if(esam_one_byte.state == ESAM_SUCCEED_STATE){
								break;
							}
						}
						if(esam_one_frame.state == ESAM_FRAME_TIME_OUT){
							break;
						} 			
					}
					if(error_num == 3){
						esam_one_frame.state = ESAM_FRAME_WRITE_ERR; //写错误
						break;
					}
					if(esam_one_frame.state == ESAM_FRAME_TIME_OUT){
						break;
					}
								
				}
				esam_one_frame.flag = ESAM_COMPLETE_STATE;
				esam_one_frame.data_count += esam_one_frame.data_len;
				EASM_IN_DIR;
			}
			break;
			case ESAM_DELAY_STATE:
			{
				esam_one_byte.data = esam_one_frame.data_len;
				esam_one_byte.state = 0;
				reset_timer2();
				enable_timer2();
				esam_one_byte_time = TimeTick;
				while((esam_one_byte.flag == ESAM_WRITE_STATE)){
					if(check_timeout(TimeTick,esam_one_byte_time,10)){
						esam_one_frame.state = ESAM_FRAME_TIME_OUT; //超时
						break;	
					}
				}
				if(esam_one_byte.flag == ESAM_COMPLETE_STATE){
					if(esam_one_byte.state != ESAM_SUCCEED_STATE){
						esam_one_frame.state = ESAM_FRAME_TIME_OUT; //超时
					}
				}
				esam_one_frame.flag = ESAM_COMPLETE_STATE;	
			}
			break;
			default:
			break;
		}

		tsk_unlock();//允许任务切换
	}
}
/*********************************************************************************************************
** 函数名称:     reset_esam
** 功能描述:     复位esam
** 输　入:       无
**
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U16 reset_esam(U8 *id)
{
	U8 i;
	//复位
	EASM_RST(0);
	//延时大于400f
	esam_one_frame.data_len = 4;
   	esam_one_frame.flag = ESAM_DELAY_STATE;
	esam_one_frame.state = 0;
	os_evt_set(EVT_ESAM,esam_task);
	EASM_RST(1);
	//复位完成那个

	esam_one_frame.data_len = 13;	  //读取13字节数据
	esam_one_frame.data_count = 0;
   	esam_one_frame.flag = ESAM_READ_STATE;
	esam_one_frame.state = 0;
	os_evt_set(EVT_ESAM,esam_task);

	//数据正确否
	if((esam_one_frame.flag == ESAM_COMPLETE_STATE)&&(!esam_one_frame.state)){
		for(i=0;i<8;i++){
			*(id+i) = esam_one_frame.data_buf[5+i];
		}
		return 0x9000;
	}

	return 0;
}
/*********************************************************************************************************
** 函数名称:     comm_esam_send_data
** 功能描述:     esam一次通讯过程处理
** 输　入:       send_data_len 发送数据域数据个数
**				 receive_type 期望接收的数据类型 0 无数据 2字节应答信号  其他需要接收数据
** 				 receive_data_len 期望接收的数据个数
** 输　出:       0 操作错误
**				 其他 操作状态
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U16 comm_esam_send_data(U8 send_data_len,U8 receive_type,U8 receive_data_len)
{
	esam_one_frame.data_len = 5;	  //发送5个字节
	esam_one_frame.data_count = 0;	  //从缓冲首地址开始
   	esam_one_frame.flag = ESAM_WRITE_STATE;
	esam_one_frame.state = 0;
	esam_send_data();
	if((esam_one_frame.flag == ESAM_COMPLETE_STATE)&&(!esam_one_frame.state)){
		esam_one_frame.data_len = 1;	  //接收1个字节命令头应答
		esam_one_frame.data_count = 0;	  //从缓冲首地址开始
		esam_one_frame.flag = ESAM_READ_STATE;
		esam_one_frame.state = 0;
		esam_send_data();
		if((esam_one_frame.flag == ESAM_COMPLETE_STATE)&&(!esam_one_frame.state)&&(esam_one_frame.data_buf[1] == esam_one_frame.data_buf[0])){

			if(send_data_len){		//还需要发送数据
				esam_one_frame.data_len = send_data_len;	  //发送数据域数据
				esam_one_frame.data_count = 5;	  //从缓冲第5个字节开始
			   	esam_one_frame.flag = ESAM_WRITE_STATE;
				esam_one_frame.state = 0;
				esam_send_data();
				if((esam_one_frame.flag == ESAM_COMPLETE_STATE)&&(!esam_one_frame.state)){
					if(!receive_type){
						esam_one_frame.data_len = 2;
						receive_data_len = 0;
					}else{
						esam_one_frame.data_len = 2+receive_data_len;
					}
					esam_one_frame.data_count = 0;	  //从缓冲首地址开始
				   	esam_one_frame.flag = ESAM_READ_STATE;
					esam_one_frame.state = 0;
					esam_send_data();
					if((esam_one_frame.flag == ESAM_COMPLETE_STATE)&&(!esam_one_frame.state)){
						return ((esam_one_frame.data_buf[receive_data_len]<<8)|(esam_one_frame.data_buf[receive_data_len+1]));
					}
				}	
			}else{
				if(!receive_type){
					esam_one_frame.data_len = 2;
					receive_data_len = 0;
				}else{
					esam_one_frame.data_len = 2+receive_data_len;
				}
				esam_one_frame.data_count = 0;	  //从缓冲首地址开始
				esam_one_frame.flag = ESAM_READ_STATE;
				esam_one_frame.state = 0;
				esam_send_data();
				if((esam_one_frame.flag == ESAM_COMPLETE_STATE)&&(!esam_one_frame.state)){
					return ((esam_one_frame.data_buf[receive_data_len]<<8)|(esam_one_frame.data_buf[receive_data_len+1]));
				}
			}
		}	
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称:     set_esam_data_content
** 功能描述:     设置esam数据域
** 输　入:       cla 命令第一字节
**				 ins 指令
** 				 p1 参数1
**				 p2 参数2
**				 len 命令第5字节
**				 buf 数据域
**				 buf_len 数据域长度
** 输　出:       0 失败
**				 1 成功
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 set_esam_data_content(U8 cla,U8 ins,U8 p1,U8 p2,U8 len,U8 *buf,U8 buf_len)
{
	U8 i;
		
	if((buf_len+5)<=MAX_ESAM_CARD_NUM){
		for(i=0;i<MAX_ESAM_CARD_NUM;i++){	  //清缓冲
			esam_one_frame.data_buf[i]  = 0;
		}
		esam_one_frame.data_len = 0;
		esam_one_frame.data_count = 0;
		esam_one_frame.flag = 0;
		esam_one_frame.state = 0;
		
		esam_one_frame.data_buf[0] = cla;	  //填充数据
		esam_one_frame.data_buf[1] = ins;
		esam_one_frame.data_buf[2] = p1;
		esam_one_frame.data_buf[3] = p2;
		esam_one_frame.data_buf[4] = len;
		for(i=0;i<buf_len;i++){
			esam_one_frame.data_buf[5+i] = *(buf+i);
		}
		return 1;	
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称:     get_esam_response
** 功能描述:     取回esam返回的数据和MAC
** 输　入:       无
**
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U16 get_esam_response(U8 *buf,U8 datalen)
{
	U8 i;
	U16 result = 0;

	if(set_esam_data_content(0x00,0xc0,0,0,datalen,buf,0)){
		result = comm_esam_send_data(0,1,datalen);
		for( i =0;i<datalen;i++){
			*(buf+i) = esam_one_frame.data_buf[i];
		}
	}
	
	return result;	
}
/*********************************************************************************************************
** 函数名称:     esam_extern_authen
** 功能描述:     esam外部认证
** 输　入:       input 输入8字节密码信息
**				 key_id 所用的密钥标志
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U16 esam_extern_authen(U8 *input,U8 key_id)
{

	if(set_esam_data_content(0x00,0x82,0,key_id,0x08,input,0x08)){
		return comm_esam_send_data(0x08,0,0);
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称:     get_esam_random
** 功能描述:     取随机数
** 输　入:       无
**
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U16 get_esam_random(U8 *buf,U8 len)
{
	U8 i;
	U16 result = 0;

	if((len == 8)||(len == 4)){
		if(set_esam_data_content(0,0x84,0,0,len,buf,0)){
			result = comm_esam_send_data(0,1,len);
			if(result == 0x9000){
				for(i=0;i<len;i++){
					*(buf+i) = esam_one_frame.data_buf[i];
				}
			}
		}
	}
	return result;
}
/*********************************************************************************************************
** 函数名称:     esam_secret_key_detract
** 功能描述:     esam执行密钥分散
** 输　入:       buf分散因子
**				 len 缓冲中数据长度
**				 key_id 用于分散的密钥id
** 输　出:       操作状态码
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U16 esam_secret_key_detract(U8 *buf,U8 len,U8 key_id)
{	
	if(len == 8){
		if(set_esam_data_content(0x80,0xfa,0,key_id,8,buf,len)){
			return comm_esam_send_data(8,0,0);
		}
	}
	return 0;	
}
/*********************************************************************************************************
** 函数名称:     esam_encrypt_random
** 功能描述:     esam加密随机数
** 输　入:       data 随机数
**				 len 缓冲中的数据长度
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U16 esam_encrypt_random(U8 *data,U8 len)
{
	if(len == 8){
		if(set_esam_data_content(0x80,0xfa,0,0,8,data,len)){
			return comm_esam_send_data(8,0,0);
		}
	}
	return 0;	
}
/*********************************************************************************************************
** 函数名称:     esam_secret_key_update
** 功能描述:     esam密钥更新
** 输　入:       data 密钥首地址
**				 len 密钥长度
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U16 esam_secret_key_update(U8 type,U8 *key)
{
	if(set_esam_data_content(0x84,0xd4,0x01,type,0x20,key,0x20)){
		return comm_esam_send_data(0x20,0,0);
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称:     esam_secret_key_info_update
** 功能描述:     esam本地密钥信息更新
** 输　入:       
**				 info 密钥信息首地址
**				 
** 输　出:       状态码
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U16 esam_key_info_update(U8 *info)
{

	if(set_esam_data_content(0x00,0xd6,0x93,0x00,0x04,info,0x04)){
		return comm_esam_send_data(0x04,0,0);
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称:     esam_read_binaryfile_mac
** 功能描述:     待随机数和离散因子 读esam二进制文件
** 输　入:       file_id 文件id
**				 offset 文件偏移地址
**				 random 随机数首地址
**				 scatter 离散因子
**               data 信息输出首地址
**				 len 所读的数据长度
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U16 esam_read_binaryfile_mac(U8 file_id,U8 offset,U8 *data,U8 len,U8 *datalong)
{
	U8 i,databuf[20];	
	U16 result = 0;

	if(len > 0){
		//整理数据数据域数据
		for(i=0;i<4;i++){
			databuf[i] = extern_control.authen_random[i];
		}
		databuf[4] = 0x04;
		databuf[5] = 0xd6;
		databuf[6] = 0x86;
		databuf[7] = offset;
		databuf[8] = len+0x0c;
		for(i=0;i<8;i++){
			databuf[i+9] = extern_control.authen_detract[i];
		}

		if(set_esam_data_content(0x04,0xb0,file_id,offset,0x11,databuf,0x11)){
			result = comm_esam_send_data(0x11,0,0);
			if(((result&0x0ff00) == 0x6100)&&(((result&0x0ff) >= (len+4))||(result == 0x6100))){
				*datalong = len+4;
				result = get_esam_response(data,len+4);
			}
		}
	}
	return result;
}
/*********************************************************************************************************
** 函数名称:     esam_read_binaryfile_mac
** 功能描述:     待随机数和离散因子 读esam二进制文件
** 输　入:       file_id 文件id
**				 offset 文件偏移地址
**				 
**				 
**               data 信息输出首地址
**				 len 所读的数据长度
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U16 esam_read_binaryfile(U8 file_id,U8 offset,U8 *data,U8 len)
{	
	U16 result = 0;
	U8 i;
	if(len > 0){
		if(set_esam_data_content(0x00,0xb0,file_id,offset,len,data,0)){
			result = comm_esam_send_data(0,1,len);
			if((((result&0x0ff00) == 0x6100)&&(((result&0x0ff) >= (len))||(result == 0x6100)))||(result == 0x9000)){
				for(i=0;i<len;i++){
			   		*(data+i) = esam_one_frame.data_buf[i];
			   	}
				return ((esam_one_frame.data_buf[len]<<8)|esam_one_frame.data_buf[len+1]);
			}
		}
	}
	return result;
}
/*********************************************************************************************************
** 函数名称:     esam_write_binaryfile_mac
** 功能描述:     写带线路加密和MAC保护的二进制文件
** 输　入:       file_id  文件名称 含有数据类型信息
**				 
**				 parameter  参数缓冲首地址
**				 parameter_len
**				 offset 偏移
**				 mac 标识
** 输　出:       状态编码
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U16 esam_write_binaryfile_mac(U8 file_id,U8 *parameter,U8 parameter_len,U8 offset,U8 *mac)
{
	U8 i;
	U8 databuf[110];

	if(parameter_len != 0){
		for(i=0;i<parameter_len;i++){
			databuf[i] = *(parameter+i);
		}
		for(i=0;i<4;i++){
			databuf[i+parameter_len] = *(mac+i);
		}
		if(set_esam_data_content(0x04,0xd6,file_id,offset,parameter_len+4,databuf,parameter_len+4)){
			return comm_esam_send_data(parameter_len+4,0,0);
		}
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称:     esam_write_binaryfile_mac
** 功能描述:     写带线路加密和MAC保护的二进制文件
** 输　入:       file_id  文件名称 含有数据类型信息
**				 
**				 parameter  参数缓冲首地址
**				 parameter_len
**				 offset 偏移
**				 
** 输　出:       状态编码
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U16 esam_write_binaryfile(U8 file_id,U8 *parameter,U8 parameter_len,U8 offset)
{
	U8 i;
	U8 databuf[110];

	if(parameter_len != 0){
		for(i=0;i<parameter_len;i++){
			databuf[i] = *(parameter+i);
		}
		if(set_esam_data_content(0x00,0xd6,file_id,offset,parameter_len,databuf,parameter_len)){
			return comm_esam_send_data(parameter_len,0,0);
		}
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称:     esam_read_purse_mac
** 功能描述:     读取钱包信息 明文+mac
** 输　入:       				 
**				 type 钱包 1 剩余金额   3 购电次数
**				 recieve_buf 接收数据缓冲
**               receive_len 接收数据长度
**				 
** 输　出:       状态
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U16 esam_read_purse_mac(U8 type,U8 *recieve_buf,U8 receive_len)
{
	U8 i,databuf[20];	
	U16 result = 0;

	if(((type == 1)||(type == 3))&&(receive_len == 4)){
		//整理数据数据域数据
		for(i=0;i<4;i++){
			databuf[i] = extern_control.authen_random[i];
		}
		databuf[4] = 0x04;
		databuf[5] = 0xd6;
		databuf[6] = 0x86;
		databuf[7] = 0x00;
		databuf[8] = 0x10;
		for(i=0;i<8;i++){
			databuf[i+9] = extern_control.authen_detract[i];
		}

		if(set_esam_data_content(0x04,0xb2,type,0x0c,0x11,databuf,0x11)){
			result = comm_esam_send_data(0x11,0,0);
			if(result == 0x6108){
				result = get_esam_response(recieve_buf,receive_len+4);
			}
		}
	}
	return result;	
}
/*********************************************************************************************************
** 函数名称:     extern_authen
** 功能描述:     远程安全认证
** 输　入:       detract_data 分散数据
**				 random_data 随机数
**				 cryptograph_data1 密文数据1
** 输　出:       0 认证失败
**				 1 认证成功
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 remote_authen(U8 *detract_data,U8 *random_data,U8 *cryptograph_data1,U8 *response_data)
{
	U8 databuf[8],i;
	U8 esam_id[8];
	U16 result =0;

	for(i=0;i<8;i++){
		databuf[i] = 0;
		esam_id[i] = 0;
	}

	result =reset_esam(esam_id);
	if(result == 0x9000){
		//离散密钥
		// 重组数据
		for(i=0;i<8;i++){
			databuf[i] = *(detract_data+8-1-i);
		}
		result = esam_secret_key_detract(databuf,8,3);//发送分散指令
		if(result == 0x9000){
			//加密随机数
			// 重组数据
			for(i=0;i<8;i++){
				databuf[i] = *(random_data+8-1-i);
			}
			result = esam_encrypt_random(databuf,8); //加密随机数
			if((result == 0x9000)||((result&0x0ff00) == 0x6100)){
				//取加密数
				result = get_esam_response(databuf,8);
				if(result == 0x9000){
					for(i=0;i<8;i++){
						if(databuf[8-1-i] != *(cryptograph_data1+i)){
							return 0;
						}
					}
					os_dly_wait(1);
					result = get_esam_random(databuf,4); //取4字节随机数
					if(result == 0x9000){
						for(i=0;i<4;i++){
							*(response_data+i) = databuf[4-1-i];
						}
						for(i=0;i<8;i++){
							*(response_data+i+4) = esam_id[8-1-i];
						}
						//设置身份验证成功
						extern_control.flag = IDENTITY_AUTHEN_BIT;
						//设置认证时效
					   	extern_control.esam_authen_time = TimeTick;
						extern_control.flag |= IDENTITY_AUTHEN_TIME_BIT;
						//取认证成功的随机数和分散因子
						for(i=0;i<8;i++){
							extern_control.authen_random[i] = *(random_data+8-1-i);
							extern_control.authen_detract[i] = *(detract_data+8-1-i);
						}
						return 1;
					}
				}
			}
		}
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称:     remote_authen_ActiveDly
** 功能描述:     远程安全时效设置
** 输　入:       
**				 cryptograph_data 密文数据 6 字节 2字节数据+4字节mac
** 输　出:       0 mac验证失败
**				 1 mac验证成功
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 remote_authen_ActiveDly(U8 *cryptograph_data)
{
	U16 result = 0;
	U8 databuf[2],mac[4];
	
	
	databuf[0] = *(cryptograph_data+1);//取时间信息
	databuf[1] = *(cryptograph_data);

	mac[0] = *(cryptograph_data+5);//取MAC信息	//数据导向
	mac[1] = *(cryptograph_data+4);
	mac[2] = *(cryptograph_data+3);
	mac[3] = *(cryptograph_data+2);

	result = esam_write_binaryfile_mac(0x82,databuf,2,43,mac);

	if(result == 0x9000){ //操作正确
		//将设置的值保存到eeprom
		
		//设置变量信息
		extern_control.esam_authen_minutes = BCDToDec(*(cryptograph_data))+BCDToDec(*(cryptograph_data+1))*100;		
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称:     remote_authen_disable
** 功能描述:     身份认证失效设置
** 输　入:       
**				 
** 输　出:       0 失败
**				 1 证成功
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 remote_authen_disable(U8 *buf)
{
	U8 i = 0,databuf[4];
	U16 result = 0;

	//读取所要的数据
   	for(i=0;i<6;i++){
		*(buf+i) = account_control.account_num[i];	//客户编号
	}
	for(i=0;i<4;i++){
		*(buf+i+6) = 0;	//剩余金额
	}
	for(i=0;i<4;i++){
		*(buf+i+10) = 0;	//购电次数
	}
	result = esam_read_binaryfile(0x86,0,databuf,4);//取密钥状态
	if((result == 0x9000)||((result&0x0ff00) == 0x6100)){
		for(i=0;i<4;i++){
			*(buf+i+14) = databuf[4-1-i];	//购电次数
		}	
		extern_control.flag = 0;	//设置身份时效状态
		return 1;
	}
	
	return 0;
}
/*********************************************************************************************************
** 函数名称:     esam_work_info_update
** 功能描述:     esam运行信息更新
** 输　入:       list_id 目录标识
**				 file_id 文件标志
**				 offset 偏移量
**				 data_len 回抄数据个数
** 输　出:       0 错误
**				 1 成功
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U16 esam_work_info_update(void)
{
	U8 databuf[50],i,sum = 0;
	U16 result = 0;
	//电表运行信息更新
	databuf[0] = 0x68;
	databuf[1] = 0x11;
	databuf[2] = 0x00;
	databuf[3] = 0x2b;
	//用户类型
	databuf[4] = 0x0;
	//电流互感器变比
	databuf[5] = 0x0;
	databuf[6] = 0x0;
	databuf[7] = 0x1;
	//电压互感器变比
	databuf[8] = 0x0;
	databuf[9] = 0x0;
	databuf[10] = 0x1;
	//表号
	for(i=0;i<6;i++){
		databuf[11+i] = meter_state_info.meter_number[6-1-i];
	}
	//客户编号
	for(i=0;i<6;i++){
		databuf[17+i] = account_control.account_num[i];
	}
	//剩余金额
	databuf[23] = 0;
	databuf[24] = 0;
	databuf[25] = 0;
	databuf[26] = 0;
	//购电次数
	databuf[27] = 0;
	databuf[28] = 0;
	databuf[29] = 0;
	databuf[30] = 0;
	//透支金额
	databuf[31] = 0;
	databuf[32] = 0;
	databuf[33] = 0;
	databuf[34] = 0;

	//密钥状态		//远程或本地
	databuf[35] = 0;
	databuf[36] = 0;
	databuf[37] = 0;
	databuf[38] = 0;
	//非法卡插入次数
	databuf[39] = 0;
	databuf[40] = 0;
	databuf[41] = 0;
	//反写日期
	databuf[42] = DecToBCD(currentClock[5]);
	databuf[43] = DecToBCD(currentClock[4]);
	databuf[44] = DecToBCD(currentClock[3]);
	databuf[45] = DecToBCD(currentClock[2]);
	databuf[46] = DecToBCD(currentClock[1]);

	for(i=0;i<47;i++){
		sum += databuf[i];
	}

	databuf[47] = sum;
	databuf[48] = 0x16;

	//写入文件7
	result = esam_write_binaryfile(0x87,databuf,49,0);

	return result;
}
/*********************************************************************************************************
** 函数名称:     remote_read_data
** 功能描述:     esam数据回抄
** 输　入:       list_id 目录标识
**				 file_id 文件标志
**				 offset 偏移量
**				 data_len 回抄数据个数
** 输　出:       0 错误
**				 1 成功
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 remote_read_data(U16 list_id,U16 file_id,U16 offset,U16 data_len,U8 *data,U8 *len)
{
	U16 result = 0;
	U8 databuf[100],i,datalong = 0;
	//判断目录
	if(!list_id){
		switch(file_id)
		{
			case 0x0001://钱包文件
			{
				if(offset == 0){
					if((data_len == 8)||(data_len == 4)){
						//读取剩余金额
						result = esam_read_purse_mac(1,databuf,4);
						if(result == 0x9000){
							for(i=0;i<4;i++){
								*(data+i) = databuf[4-1-i];
							}
							for(i=0;i<4;i++){
								*(data+i+4) = databuf[8-1-i];
							}
							result = esam_read_purse_mac(3,databuf,4);
							if(result == 0x9000){
								for(i=0;i<4;i++){
									*(data+i+8) = databuf[4-1-i];
								}
								for(i=0;i<4;i++){
									*(data+i+12) = databuf[8-1-i];
								}
								*len = 8+8+8;
								return 1;
							}	
						}
					}
				}
			}
			break;
			case 0x0002://参数信息
			{
				if(((data_len+offset)<= 100)){
					result = esam_read_binaryfile_mac(0x82,offset,databuf,data_len,&datalong);
					if(((result&0x0ff00) == 0x6100)||(result == 0x9000)){
						for(i=0;i<(datalong-4);i++){
					   		*(data+i) = databuf[datalong-4-1-i];
						}
						for(i=0;i<4;i++){
					   		*(data+i+datalong-4) = databuf[datalong-1-i];
						}
						*len = 8+datalong;
						return 1;
					}
				}
					
			}
			break;
			case 0x0003://第一套费率
			case 0x0004://第二套费率
			{
				if((data_len<110)&&((data_len+offset)<= 258)){
					result = esam_read_binaryfile_mac(0x80|(file_id&0x0ff),offset,databuf,data_len,&datalong);
					if(((result&0x0ff00) == 0x6100)||(result == 0x9000)){
						for(i=0;i<(datalong-4);i++){
					   		*(data+i) = databuf[datalong-4-1-i];
						}
						for(i=0;i<4;i++){
					   		*(data+i+datalong-4) = databuf[datalong-1-i];
						}
						*len = 8+datalong;
						return 1;
					}
				}
			}
			break;
			case 0x0005://本地密钥信息
			{
				if((data_len<=4)&&((data_len+offset)<= 4)){
					result = esam_read_binaryfile_mac(0x85,offset,databuf,data_len,&datalong);
					if(((result&0x0ff00) == 0x6100)||(result == 0x9000)){
						for(i=0;i<(datalong-4);i++){
					   		*(data+i) = databuf[datalong-4-1-i];
						}
						for(i=0;i<4;i++){
					   		*(data+i+datalong-4) = databuf[datalong-1-i];
						}
						*len = 8+datalong;
						return 1;
					}
				}
			}
			break;
			case 0x0006://远程密钥信息
			{
				if((data_len<=4)&&((data_len+offset)<= 4)){
					result = esam_read_binaryfile_mac(0x86,offset,databuf,data_len,&datalong);
					if(((result&0x0ff00) == 0x6100)||(result == 0x9000)){
						for(i=0;i<(datalong-4);i++){
					   		*(data+i) = databuf[datalong-4-1-i];
						}
						for(i=0;i<4;i++){
					   		*(data+i+datalong-4) = databuf[datalong-1-i];
						}
						*len = 8+datalong;
						return 1;
					}
				}
			}
			break;
			case 0x0007://运行信息
			{
				//更新控制命令文件信息
				result = esam_work_info_update();
				//读取相关信息
				if((data_len<=49)&&((data_len+offset)<= 49)){
					result = esam_read_binaryfile_mac(0x87,offset,databuf,data_len,&datalong);
					if((result == 0x9000)||((result&0x0ff00) == 0x6100)){
						for(i=0;i<(datalong-4);i++){
					   		*(data+i) = databuf[datalong-4-1-i];
						}
						for(i=0;i<4;i++){
					   		*(data+i+datalong-4) = databuf[datalong-1-i];
						}
						*len = 8+datalong;
						return 1;
					}
				}	
			}
			break;
			default:
			break;
		}
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称:     remote_read_state
** 功能描述:     状态查询
** 输　入:       
**				 
**				 data 返回的数据指针
**				 
** 输　出:       0 错误
**				 1 成功
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 remote_read_state(U8 *data)
{
	U8 i,databuf[50];
	U16 result = 0;
	//读取esam中的剩余金额
	result = esam_read_purse_mac(1,databuf,4);
	if(result == 0x9000){
		//金额
		for(i=0;i<4;i++){
			*(data+i) = databuf[4-1-i];
		}//mac
		for(i=0;i<4;i++){
			*(data+4+i) = databuf[8-1-i];
		}
	}else{
		return 0;
	}
	result = esam_read_purse_mac(3,databuf,4);
	if(result == 0x9000){
		//购电次数
		for(i=0;i<4;i++){
			*(data+8+i) = databuf[4-1-i];
		}//mac
		for(i=0;i<4;i++){
			*(data+12+i) = databuf[8-1-i];
		}
	}else{
		return 0;
	}
	//读取客户编号
	for(i=0;i<6;i++){
		*(data+16+i) = account_control.account_num[i];
	}
	result = esam_read_binaryfile(0x86,0,databuf,4);
	if((result == 0x9000)||((result&0x0ff00) == 0x6100)){
		for(i=0;i<4;i++){
			*(data+22+i) = databuf[4-1-i];
		}
	}else{
		return 0;
	}
	return 1;
		
}
/*********************************************************************************************************
** 函数名称:     esam_crush_money_mac
** 功能描述:     明文+mac 充值
** 输　入:       无
**
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U16 esam_increase_money_mac(U8 *crush_info)
{
		
	if(set_esam_data_content(0x84,0x32,0x01,0x0c,0x0c,crush_info,0x0c)){
		return comm_esam_send_data(0x0c,0,0);
	}
	return 0;

}
/*********************************************************************************************************
** 函数名称:     Cmp_String
** 功能描述:     比较两组字符串
** 输　入:       
**				 
**				 
** 输　出:       0 不等
**				 1 相等
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 Cmp_String(U8 *buf1,U8 *buf2,U8 len)
{
	U8 i;

	for(i=0;i<len;i++){
		if(*(buf1+i) != *(buf2+i)){
			return 0;
		}
	}
	return 1;
}
/*********************************************************************************************************
** 函数名称:     remote_crush_money
** 功能描述:     远程开户充值
** 输　入:       mode 1 开户 0 充值
**				 buf 输入的数据首地址
**				 data_len 输入的数据长度
** 输　出:       0 成功
**				 其他 错误码
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 remote_crush_money(U8 mode,U8 *buf,U8 data_len)
{
	U8 account_info[10],crush_info[12],i;
	U16 result= 0;
	
	if(data_len == 22){	 //数据长度正确否
		
		for(i=0;i<4;i++){	  //金额
			crush_info[i] = *(buf+4-1-i);
		}
		for(i=0;i<4;i++){	  //充值次数
			crush_info[i+4] = *(buf+8-1-i);	
		}
		for(i=0;i<4;i++){	 //mac
			crush_info[i+8] = *(buf+12-1-i);
		}
		for(i=0;i<6;i++){	  //用户信息
			account_info[i] = *(buf+18-1-i);
		}
		for(i=0;i<4;i++){	 //mac
			account_info[i+6] = *(buf+22-1-i);	
		}
		switch(mode)
		{
			case 0://充值
			{
				if(account_control.account_flag == 0x5a){
					if(Cmp_String(buf+12,account_control.account_num,6)){ //开过户 且户号正确
						//充值次数大于1
						if((crush_info[7]+(crush_info[6]<<8)+(crush_info[5]<<16)+(crush_info[4]<<24)) == account_control.crush_num){
							return 4;
						}
						if(((crush_info[7]+(crush_info[6]<<8)+(crush_info[5]<<16)+(crush_info[4]<<24))>account_control.crush_num)&&(((crush_info[7]+(crush_info[6]<<8)+(crush_info[5]<<16)+(crush_info[4]<<24))- account_control.crush_num) == 1 )){
							//充值金额不大于囤积量
							if(((crush_info[3]+(crush_info[2]<<8)+(crush_info[1]<<16)+(crush_info[0]<<24))+ account_control.crush_money)<= 0x7fffffff){
								result = esam_increase_money_mac(crush_info);
								if(result == 0x9000){
									account_control.crush_num++;
									account_control.crush_money += ((crush_info[0]<<24)|(crush_info[1]<<16)|(crush_info[2]<<8)|crush_info[3]);
									return 0;
								}		
							}else{
								return 2;
							}
						}else{
							return 3;
						}			
					}else{
						return 1;
					}
				}	
			}
			break;
			case 1:	//开户
			{
				if((account_control.account_flag == 0)&&((crush_info[7]+(crush_info[6]<<8)+(crush_info[5]<<16)+(crush_info[4]<<24)) == 1)){ //没有开过户且充值次数为1
					//充值金额不大于囤积量
					if(((crush_info[3]+(crush_info[2]<<8)+(crush_info[1]<<16)+(crush_info[0]<<24))+ account_control.crush_money)<= 0x7fffffff){
						
						result = esam_write_binaryfile_mac(0x82,account_info,6,36,account_info+6);
						if(result == 0x9000){
							result = esam_increase_money_mac(crush_info);
							if(result == 0x9000){
								//开户成功
								account_control.account_flag = 0x5a;
								for(i=0;i<6;i++){
							   		account_control.account_num[i] = *(buf+i+12);
								}
								//金额和开户次数更新
								account_control.crush_num = 1;
								account_control.crush_money = ((crush_info[0]<<24)|(crush_info[1]<<16)|(crush_info[2]<<8)|crush_info[3]);
								//保存开户状态


								return 0;
							}
						}		
					}else{
						return 2;
					}			
				}
			}
			break;
			default:
			break;
		}
	}
	return 0x0ff;
}
/*********************************************************************************************************
** 函数名称:     check_authen_time
** 功能描述:     检测认证时间
** 输　入:       
**				 
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void check_authen_time(void)
{
	if((extern_control.flag&(IDENTITY_AUTHEN_BIT|IDENTITY_AUTHEN_TIME_BIT)) == (IDENTITY_AUTHEN_BIT|IDENTITY_AUTHEN_TIME_BIT)){
		if(check_timeout(TimeTick,extern_control.esam_authen_time,(extern_control.esam_authen_minutes)*60*1000)){
			extern_control.flag = 0;
		}
	}
}
/*********************************************************************************************************
** 函数名称:     remote_control
** 功能描述:     远程控制 
** 输　入:       
**				 buf 输入的数据首地址
**				 len 输入的数据长度
** 输　出:       0 成功
**				 其他 错误码
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
*******************************************************************************************************/
/*********************************************************************************************************
** 函数名称:     remote_control
** 功能描述:     远程控制 
** 输　入:       
**				 buf 输入的数据首地址
**				 len 输入的数据长度
**				 outbuf 输出数据首地址
**				 outlen 输出数据长度
** 输　出:       0 失败
**				 1 成功
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
*******************************************************************************************************/
U8 remote_control(U8 *buf,U8 len,U8 *outbuf,U8 *outlen)
{
	U8 databuf[20],mac[4],i;
	U16 result= 0;
	
	if(len == 0x14){	 
		
		for(i=0;i<0x14;i++){	  //密文
			databuf[i] = *(buf+0x14-1-i);
		}
		for(i=0;i<4;i++){	  	//mac
			mac[i] = databuf[i+16];	
		}
		
		result = esam_write_binaryfile_mac(0x88,databuf,0x10,0,mac);
		if(((result&0x0ff00) != 0x6100)&&((result) != 0x9000)){
			return 1;
		}
		result = esam_read_binaryfile(0x88,0,databuf,8);
		if((result) != 0x9000){
			return 2;
		}
		//处理数据
		*outlen = 8;
		for(i=0;i<8;i++){
			*(outbuf+i) = databuf[i];
		}
		return 1;
	}
	return 0;	
}
/*********************************************************************************************************
** 函数名称:     decode_98_mac
** 功能描述:     解密98级密文 
** 输　入:       
**				 di2   数据标志
**               inbuf 输入的数据首地址
**				 inlen 输入的数据长度
**				 outbuf	输出的数据首地址
**				 outlen	输出的数据首地址
**				 mode 0  正常倒序  1 3字节倒序 时区时段表
** 输　出:       0 失败
**				 1 成功
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
*******************************************************************************************************/
U8 decode_98_mac(U8 di2,U8 *inbuf,U8 inlen,U8 *outbuf,U8 *outlen,U8 mode)
{
	U8 file_no = 0,i,re_len;
	U8 databuf[110],mac[4];
	U16 result = 0;

	//整理数据
	if(di2%5){
		file_no = di2%5+0x0f;
	}else{
		file_no = 0x09;
	}
	for(i=0;i<(inlen-4);i++){
		databuf[i] = *(inbuf+(inlen-4)-1-i);
	}
	for(i=0;i<(4);i++){
		mac[i] = *(inbuf+(inlen)-1-i);
	}
	result = esam_write_binaryfile_mac(0x80|file_no,databuf,inlen-4,0,mac);  //发送密文信息
	if((result == 0x9000)||((result&0x0ff00) == 0x6100)){
		result = esam_read_binaryfile(0x80|file_no,0,databuf,1);  //读取解密信息长度
		if((result == 0x9000)||((result&0x0ff00) == 0x6100)){
			re_len = databuf[0];
			result = esam_read_binaryfile(0x80|file_no,0,databuf,re_len+1);	 //读取解密所有数据
			if((result == 0x9000)||((result&0x0ff00) == 0x6100)){
				
				
				//整理数据
				if(mode == 0){
					*outlen = re_len-4;
					for(i=0;i<(re_len-4);i++){
						*(outbuf+i) = databuf[re_len-i];	
					}
					return 1;
				}else{
					*outlen = re_len-4;
					if((re_len-4) == 3*14){
					   for(i=0;i<14;i++){
							*(outbuf+0+3*i) = databuf[5+2+3*i];
							*(outbuf+1+3*i) = databuf[5+1+3*i];
							*(outbuf+2+3*i) = databuf[5+0+3*i];	
						}
						return 1;
					}
				}				
			}
		}
	}	
	return 0;
}
/*********************************************************************************************************
** 函数名称:     remote_modify_key
** 功能描述:     远程修改密钥
** 输　入:       
**				 key_id 密钥类型
**				 data 输入的数据缓冲
**				 len 输入的数据长度
**				 
** 输　出:       0 成功
**				 其他  错误编码 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 								 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 remote_modify_key(U8 id1,U8 key_id,U8 *data,U8 len)
{
	U8 key_buf[32],key_info_buf[8],i;
	U8 old_key_info[4];
	U16 result = 0;
	if(len == 40){
		//整理数据
		for(i=0;i<32;i++){
			key_buf[i] = *(data+40-1-i);
		}for(i=0;i<8;i++){
			key_info_buf[i] = *(data+8-1-i);
		}

		//判断是否满足密钥更新条件
		if((key_info_buf[0]+(key_info_buf[1]<<8)+(key_info_buf[2]<<16)+(key_info_buf[3]<<24))==0){
			return 1;//未知错误 
		}
		result = esam_read_binaryfile(0x86,0,old_key_info,4);
		if(result != 0x9000){
			return 1;
		}
		if(key_info_buf[3]<old_key_info[3]){
		 	return 1;
		}
		if(id1 == 1){
			if(key_info_buf[2] != 2){
				return 4;//esam校验失败
			}
		}else if(id1 == 2){
			if(key_info_buf[2] != 1){
				return 4;//esam校验失败
			}
		}else if(id1 == 3){
			if(key_info_buf[2] != 3){
				return 4;//esam校验失败
			}
		}else if(id1 == 4){
			if(key_info_buf[2] != 4){
				return 4;//esam校验失败
			}
		}else{
			return 4; //esam校验失败
		}
		if(key_info_buf[1] != 1){
			return 4; //esam校验失败
		}

		if(key_id){
			key_id = 0x0ff;
		}
		//更新密钥
		result = esam_secret_key_update(key_id,key_buf);
		if((result != 0x9000)&&(result&0x0ff00 != 0x6100)){
	   		return 4; //esam校验失败
		}

		//更新密钥信息
		result = esam_write_binaryfile_mac(0x86,key_info_buf,4,0,key_info_buf+4);
		if((result != 0x9000)&&(result&0x0ff00 != 0x6100)){
	   		return 4; //esam校验失败
		}

		//保存更新后的密钥信息；


		return 0;

	}
	return 0;
}
