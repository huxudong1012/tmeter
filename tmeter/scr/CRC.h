#ifndef __CRC_H__
#define __CRC_H__



 /****************************************系统时钟设定*********************************************/
	#define CRC_8X  0X07	  //x8+x2+x+1
	extern unsigned char CAL_SUM(unsigned char *ptr, unsigned short len);
	extern unsigned char CAL_CRC(unsigned char *ptr, unsigned short len);
	extern unsigned char CRC_CHECK(unsigned char *p,unsigned short NUM);	
	extern unsigned char SUM_CHECK(unsigned char *p,unsigned short NUM);

#endif
