/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __TIME_PERIOD_ZONE_H__
#define __TIME_PERIOD_ZONE_H__



typedef struct {
	U8 zones_change_time[5];	//两套时区切换时间
	U8 periods_change_time[5];  //两套时段切换时间

	U8 zones_num;		//第几套时区1,2
	U8 periods_num;		//第几套时段1,2

	U8 zone_now;		//现在所处时区号1-14
	U8 data_table_now;	//现在所处的   1-8 日时段表数
	U8 period_now;		//现在所处时段号1-14
	U8 holiday_now;   	//本次假日序号

	U8 fee_num;		//非零编号1-63
	U8 previous_fee_num;

	U8 weekday_mode;	//周休日特征字
	U8 weekday_data_table;//周休日时段表号

	U8 max_zone_num;	//最大时区数
	U8 max_data_table; //最大日时段表数
	U8 max_period_num; //最大时段数
	U8 max_fee_num;     //最大费率数
	U16 max_holiday;	//最大节假日
}time_zone_t;

extern time_zone_t time_zone_con; //时区时段控制

extern U8 check_timeout(U32 time1,U32 time2,U32 interval);

extern U8 init_zone_period(void);
extern __task void zone_period_check(void);
extern U8 check_zone_timeout(U8 *time1,U8 *time2,U8 datalong);
extern U8 check_all_zero(U8 *databuf,U8 datalong);
extern U8 get_now_fee(void);
extern U8 check_current_fee(void);


#endif

