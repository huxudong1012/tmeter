#include"CRC.h"

/****************************************
* Function Name: unsigned char cal_crc(unsigned char *ptr, unsigned char len)
 * Parameters: *ptr//数据区
 *			   len//长度
 * Return: CRC
 *
 * Description: CRC计算
 CRC_8X  0X07	  //x8+x2+x+1
*****************************************/
unsigned char CAL_CRC(unsigned char *ptr, unsigned short len) 
{
	unsigned char i;
	unsigned char crc=0;
		while(len--!=0) 
		{
			for(i=0x80; i!=0; i/=2) 
			{
				if((crc&0x80)!=0) 
				{
					crc*= 2; 
					crc^= CRC_8X;   /* 余式CRC 乘以2 再求CRC */
				} 
				else 
				{
					crc*= 2;
				}
				if((*ptr&i)!=0) 
				{
					crc^= CRC_8X; /* 再加上本位的CRC */
				}
			}
			ptr++;
		}
		return(crc);
}
/****************************************
* Function Name: CRC_CHECK
 * Parameters: unsigned cahr *P 校验的数据区
               unsigned char NUM 数据长度
 *
 * Return: unsigned char
            1:校验成功
			0：不成功
 *
 * Description: check 一帧数据

*****************************************/

unsigned char CRC_CHECK(unsigned char *p,unsigned short NUM)
{
	

	if(CAL_CRC(p,NUM) == (*(p+NUM)))//CRC校验成功
	{
		return 1;
	}else
	{
		return 0;
	}	
	
}
/****************************************
* Function Name: unsigned char cal_crc(unsigned char *ptr, unsigned char len)
 * Parameters: *ptr//数据区
 *			   len//长度
 * Return: SUM
 *
 * Description: CRC计算
 CRC_8X  0X07	  //x8+x2+x+1
*****************************************/
unsigned char CAL_SUM(unsigned char *ptr, unsigned short len) 
{
	unsigned char SUM=0;
		while(len--!=0) 
		{
			SUM += *(ptr);
			ptr++;
		}
		return(SUM);
}
/****************************************
* Function Name: CRC_CHECK
 * Parameters: unsigned cahr *P 校验的数据区
               unsigned char NUM 数据长度
 *
 * Return: unsigned char
            1:校验成功
			0：不成功
 *
 * Description: check 一帧数据

*****************************************/

unsigned char SUM_CHECK(unsigned char *p,unsigned short NUM)
{
	

	if(CAL_SUM(p,NUM) == (*(p+NUM)))//和校验成功
	{
		return 1;
	}else
	{
		return 0;
	}	
	
}
