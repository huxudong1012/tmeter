/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __INPUT_H__
#define __INPUT_H__
	//输入管脚定义
	#define	pinNumberKeyUp		30		//P1.28，上翻按键	 P0.30
	#define	pinNumberKeyDown	29  	//p1.29，下翻按键	 p0.29
	#define	pinNumberKeyTop		0  	//p0.21，表盖按键	 p1.0
	#define	pinNumberKeyTRAIL	4  	//p0.22，尾盖按键	 p1.4
	#define	pinNumberKeyProgram	1  	//p1.14，编程按键  p1.1
	#define	pinNumberKeyIc		23  	//p1.23，IC卡常开触点	 无		  p0.22
	#define	pinNumberRelayState	26  	//p0.26，继电器状态输入			  p2.3
	#define	pinNumberLBT_OUT	 0		//p0.0	电池失效电压检测输入	  p0.26


	#define READ_KEYUP			(LPC_GPIO0->FIOPIN & (1<<pinNumberKeyUp))
	#define READ_KEYDOWN		(LPC_GPIO0->FIOPIN & (1<<pinNumberKeyDown))
	#define READ_KEYTOP			(LPC_GPIO1->FIOPIN & (1<<pinNumberKeyTop))
	#define READ_KEYTRAIL		(LPC_GPIO1->FIOPIN & (1<<pinNumberKeyTRAIL))
	#define READ_KEYIC			(LPC_GPIO0->FIOPIN & (1<<pinNumberKeyIc))
	#define READ_KEYPROGRAM		(LPC_GPIO1->FIOPIN & (1<<pinNumberKeyProgram))
	#define READ_RELATSTATE		(LPC_GPIO2->FIOPIN & (1<<pinNumberRelayState))
	#define READ_LOWBATTEST		(LPC_GPIO0->FIOPIN & (1<<pinNumberLBT_OUT))


	#define	pinFAC_STA	 		4				//p2.4	eeprom初始化输入
	#define READ_FAC_STA		(LPC_GPIO2->FIOPIN & (1<<pinFAC_STA))


	//任务优先级定义
//	#define PRI_Input			2

	//硬件通道号相关定义
	#define AddKeyUp 			1
	#define	AddKeyDown			2	
	#define	AddKeyTop			3
	#define	AddKeyTrail			4
	#define	AddKeyProgram		5
	#define	AddIcCard			6
	#define	AddRelayState		7


	//事件编号相关定义
	#define EventKeyUp 			1
	#define	EventKeyDown		2	
	#define	EventKeyTop			3
	#define	EventKeyTrail		4
	#define	EventKeyProgram		5
	#define	EventIcCard			6
	#define	EventRelayState		7


	//事件优先级相关定义
	#define PriKeyUp 			1
	#define	PriKeyDown			2	
	#define	PriKeyTop			3
	#define	PriKeyTrail			4
	#define	PriKeyProgram		5
	#define	PriIcCard			6
	#define	PriRelayState		7

 
	typedef enum {
	    KeyUp = 0,				//0
	    KeyDown,	   			//1			
	    KeyTop,	   				//2
	    KeyTrail,   			//3

		KeyProgram,	   			//4
		IcCard,	   				//5
		RelayState,	   			//6
	} Input_t;				 	//输入编号

	typedef enum {
	    counterKeyUp = 0,				//0
	    counterKeyDown,	   				//1			
	    counterKeyTop,	   				//2
	    counterKeyTrail,   				//3

		counterKeyProgram,	   			//4
		counterIcCard,	   				//5
		counterRelayState,	   			//6
	} CounterNumber_t;				 //用于检测外部开关量输入时对各个开关输入计数

	typedef enum {
	    timesKeyUp = 0,				//0
	    timesKeyDown,	   			//1			
	} Times_t;				 		//用于表示记录上翻按键或下翻按键的自加次数



	extern U8 input[7];					//用于标示各个输入按键的状态
	extern U32 key_program_time;		//按键有效时间
	extern U8 key_program;
	extern __task void Input (void);

#endif
