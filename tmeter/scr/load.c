/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#include <RTL.h>                      /* RTX kernel functions & defines      */
#include "LPC_I2C.h"
#include "lpc_spi.h"
#include "load.h"
#include "RX8025T.h"
#include "ade7878.h"
#include "demand.h"
#include "crc.h"
#include "eeprom.h"
#include "time_period_zone.h"
#include "freeze.h"

/***************************负荷记录模式字********************************************/
/*Bit7 |  Bit6	|  Bit5	    |   Bit4	|   Bit3   |  Bit2   |   Bit1  |	  Bit0	|*/
/*-----------------------------------------------------------------------------------*/
/*保留 |  保留  | 当前需量  | 四象限无  |  有无功  |功率因数 | 有、无  |电压、电流、|*/
/*	   |        |           | 功总电能	|  总电能  |         | 功功率  |    频率	|*/
/*----------------------------------------------------------------------------------|*/

load_config_t load_config;

/*********************************************************************************************************
** 函数名称: init_load
** 功能描述: 初始化
** 输　入: 
** 		   		   
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 init_load(void)
{
	U8 i,datalong,databuf[40];

	

	datalong = getEepromData(4,0,9,1,databuf);  //负荷模式字
	if(datalong == 1){
		load_config.lode_mode = databuf[0];
	}
	
	datalong = getEepromData(4,0,10,1,databuf);  //负荷开始时间
	if(datalong == 4){
		for(i=0;i<4;i++){
			load_config.load_start_time[i] = BCDToDec(databuf[i]);	
		}
	}
	
	datalong = getEepromData(4,0,10,2,databuf);  //第1类负荷间隔时间
	if(datalong == 2){
		load_config.load_save_config[first_kind_load].load_interval = BCDToDec(databuf[0])+BCDToDec(databuf[1])*100;
	}
	
	datalong = getEepromData(4,0,10,3,databuf);  //第2类负荷间隔时间
	if(datalong == 2){
		load_config.load_save_config[second_kind_load].load_interval = BCDToDec(databuf[0])+BCDToDec(databuf[1])*100;
	}
	
	datalong = getEepromData(4,0,10,4,databuf);  //第3类负荷间隔时间
	if(datalong == 2){
		load_config.load_save_config[third_kind_load].load_interval = BCDToDec(databuf[0])+BCDToDec(databuf[1])*100;
	}
	
	datalong = getEepromData(4,0,10,5,databuf);  //第4类负荷间隔时间
	if(datalong == 2){
		load_config.load_save_config[fourth_kind_load].load_interval = BCDToDec(databuf[0])+BCDToDec(databuf[1])*100;
	}
	
	datalong = getEepromData(4,0,10,6,databuf);  //第5类负荷间隔时间
	if(datalong == 2){
		load_config.load_save_config[fifth_kind_load].load_interval = BCDToDec(databuf[0])+BCDToDec(databuf[1])*100;
	}
	
	datalong = getEepromData(4,0,10,7,databuf);  //第6类负荷间隔时间
	if(datalong == 2){
		load_config.load_save_config[sixth_kind_load].load_interval = BCDToDec(databuf[0])+BCDToDec(databuf[1])*100;
	}
	

	
	//读取上一次负荷记录发生时间
	//实验部分
//	load_config.load_save_config[first_kind_load].load_save_num = 0;
//	spi_EraseSector(FIRST_KIND_LOAD_ADDR);
//	load_config.load_save_config[second_kind_load].load_save_num = 0;
//	spi_EraseSector(SECOND_KIND_LOAD_ADDR);
//	load_config.load_save_config[third_kind_load].load_save_num = 0;
//	spi_EraseSector(THIRD_KIND_LOAD_ADDR);
//	load_config.load_save_config[fourth_kind_load].load_save_num = 0;
//	spi_EraseSector(FOURTH_KIND_LOAD_ADDR);
//	load_config.load_save_config[fifth_kind_load].load_save_num = 0;
//	spi_EraseSector(FIFTH_KIND_LOAD_ADDR);
//	load_config.load_save_config[sixth_kind_load].load_save_num = 0;
//	spi_EraseSector(SIXTH_KIND_LOAD_ADDR);
	if(load_config.load_save_config[first_kind_load].load_save_num == 0){
		for(i = 0;i<4;i++){
			load_config.load_save_config[first_kind_load].last_load_save_time[i] = load_config.load_start_time[i];
		}
		load_config.load_save_config[first_kind_load].last_load_save_time[4] = currentClock[Year];
	}else{
		if(read_load_data(databuf,2,1)){
			for(i=0;i<5;i++){
				load_config.load_save_config[first_kind_load].last_load_save_time[i] = BCDToDec(databuf[i+3]);
			}	
		}	
	}
	if(load_config.load_save_config[second_kind_load].load_save_num == 0){
		for(i = 0;i<4;i++){
			load_config.load_save_config[second_kind_load].last_load_save_time[i] = load_config.load_start_time[i];
		}
		load_config.load_save_config[second_kind_load].last_load_save_time[4] = currentClock[Year];
	}else{
		if(read_load_data(databuf,2,2)){
			for(i=0;i<5;i++){
				load_config.load_save_config[second_kind_load].last_load_save_time[i] = BCDToDec(databuf[i+3]);	
			}
		}	
	}
	if(load_config.load_save_config[third_kind_load].load_save_num == 0){
		for(i = 0;i<4;i++){
			load_config.load_save_config[third_kind_load].last_load_save_time[i] = load_config.load_start_time[i];
		}
		load_config.load_save_config[third_kind_load].last_load_save_time[4] = currentClock[Year];
	}else{
		if(read_load_data(databuf,2,3)){
			for(i=0;i<5;i++){
				load_config.load_save_config[third_kind_load].last_load_save_time[i] = BCDToDec(databuf[i+3]);	
			}
		}	
	}
	if(load_config.load_save_config[fourth_kind_load].load_save_num == 0){
		for(i = 0;i<4;i++){
			load_config.load_save_config[fourth_kind_load].last_load_save_time[i] = load_config.load_start_time[i];
		}
		load_config.load_save_config[fourth_kind_load].last_load_save_time[4] = currentClock[Year];
	}else{
		if(read_load_data(databuf,2,4)){
			for(i=0;i<5;i++){
				load_config.load_save_config[fourth_kind_load].last_load_save_time[i] = BCDToDec(databuf[i+3]);	
			}
		}	
	}
	if(load_config.load_save_config[fifth_kind_load].load_save_num == 0){
		for(i = 0;i<4;i++){
			load_config.load_save_config[fifth_kind_load].last_load_save_time[i] = load_config.load_start_time[i];
		}
		load_config.load_save_config[fifth_kind_load].last_load_save_time[4] = currentClock[Year];
	}else{
		if(read_load_data(databuf,2,5)){
			for(i=0;i<5;i++){
				load_config.load_save_config[fifth_kind_load].last_load_save_time[i] = BCDToDec(databuf[i+3]);	
			}
		}	
	}
	if(load_config.load_save_config[sixth_kind_load].load_save_num == 0){
		for(i = 0;i<4;i++){
			load_config.load_save_config[sixth_kind_load].last_load_save_time[i] = load_config.load_start_time[i];
		}
		load_config.load_save_config[sixth_kind_load].last_load_save_time[4] = currentClock[Year];
	}else{
		if(read_load_data(databuf,2,6)){
			for(i=0;i<5;i++){
				load_config.load_save_config[sixth_kind_load].last_load_save_time[i] = BCDToDec(databuf[i+3]);	
			}
		}	
	}
	return 1;
}
/*********************************************************************************************************
** 函数名称: read_one_load_data
** 功能描述: 读取负荷数据，读取给定时间记录数据时需提供时间数据和负荷记录类型（0、1、2）
** 输　入: loaddata 	指向要保存读取数据的符合记录指针
** 		    n				所要读取的记录类型   
**         					0：最早记录块
**		   					1：给定时间记录块
**		   					2：最近一个记录块
**			type			
** 输　出: U8  0： 出错，未找到该记录文件或者给定时间记录数据 1： 读取成功
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 read_one_load_data(U8 *time,U8 n,load_type_t type,U8 *databuf)
{
   U32 num = 0,minute = 0,num1 = 0,count = 0;
   U32 lode_note_count = 0;
   U32 note_block_num = 0;
   U32 note_block_addr = 0;
   U8 interval = 0;

   	switch(type)
	{
		case first_kind_load:
		{
			lode_note_count = load_config.load_save_config[first_kind_load].load_save_num;
		   	note_block_num = FIRST_KIND_BLOCK;
		   	note_block_addr = FIRST_KIND_LOAD_ADDR;
			interval = 	load_config.load_save_config[first_kind_load].load_interval;
		}
		break;
		case second_kind_load:
		{
			lode_note_count = load_config.load_save_config[second_kind_load].load_save_num;
		   	note_block_num = SECOND_KIND_BLOCK;
		   	note_block_addr = SECOND_KIND_LOAD_ADDR;
			interval = 	load_config.load_save_config[second_kind_load].load_interval;	
		}
		break;
		case third_kind_load:
		{
			lode_note_count = load_config.load_save_config[third_kind_load].load_save_num;
		   	note_block_num = SECOND_KIND_BLOCK;
		   	note_block_addr = SECOND_KIND_LOAD_ADDR;
			interval = 	load_config.load_save_config[third_kind_load].load_interval;
		}
		break;
		case fourth_kind_load:
		{
			lode_note_count = load_config.load_save_config[fourth_kind_load].load_save_num;
		   	note_block_num = FOURTH_KIND_BLOCK;
		   	note_block_addr = FOURTH_KIND_LOAD_ADDR;
			interval = 	load_config.load_save_config[fourth_kind_load].load_interval;
		}
		break;
		case fifth_kind_load:
		{
			lode_note_count = load_config.load_save_config[fifth_kind_load].load_save_num;
		   	note_block_num = FIFTH_KIND_BLOCK;
		   	note_block_addr = FIFTH_KIND_LOAD_ADDR;
			interval = 	load_config.load_save_config[fifth_kind_load].load_interval;
		}
		break;
		case sixth_kind_load:
		{
			lode_note_count = load_config.load_save_config[sixth_kind_load].load_save_num;
		   	note_block_num = SIXTH_KIND_BLOCK;
		   	note_block_addr = SIXTH_KIND_LOAD_ADDR;
			interval = 	load_config.load_save_config[sixth_kind_load].load_interval;
		}
		break;
		default:
		return 0; 
	}
	count = lode_note_count; 
   if(n == 0){
		if(lode_note_count>0){
			if(!spi_ReadData(note_block_addr,note_block_num,databuf)){
				if(CAL_CRC(databuf,note_block_num-1) == databuf[note_block_num-1]){
					return 1;
				}
			}
		}
	}else if(n == 2){
		if(lode_note_count>0){
			num = (lode_note_count -1);
			if(!spi_ReadData(note_block_addr+num*note_block_num,note_block_num,databuf)){
				if(CAL_CRC(databuf,note_block_num -1) == databuf[note_block_num -1]){
					return 1;
				}
			}
		}
	}else if(n == 1){
		//按时间查询
		if(lode_note_count>0){
			num1 = lode_note_count;
			if(!spi_ReadData(note_block_addr+(num1-1)*note_block_num,note_block_num,databuf)){
				if(CAL_CRC(databuf,note_block_num-1) == databuf[note_block_num-1]){
							
					do{
						num = compareDate(BCDToDec(databuf[4]),BCDToDec(databuf[3]),BCDToDec(databuf[2]),BCDToDec(time[4]),BCDToDec(time[3]),BCDToDec(time[2]));
						if(num!=0x0ffffffff){
							minute = num*24*60+BCDToDec(databuf[1])*60+BCDToDec(databuf[0]) - BCDToDec(time[1])*60-BCDToDec(time[0]);
							minute = minute/interval;
							if(minute == 0){
								return 1;
							}else{
								//后查
								if(minute<=num1){
									num1 = num1-minute;
								}else{
									num1 = lode_note_count-count;
								}
								if(spi_ReadData(note_block_addr+(num1)*note_block_num,note_block_num,databuf)){
									break;
								}
								if(CAL_CRC(databuf,note_block_num-1) != databuf[note_block_num-1]){
									break;		
								}
										
							}
						}else{
							num = compareDate(BCDToDec(time[4]),BCDToDec(time[3]),BCDToDec(time[2]),BCDToDec(databuf[4]),BCDToDec(databuf[3]),BCDToDec(databuf[2]));
							minute = num*24*60+ BCDToDec(time[1])*60+BCDToDec(time[0])- BCDToDec(databuf[1])*60-BCDToDec(databuf[0]);
							minute = minute/interval;
							if(minute == 0){
								return 1;
							}else{
								//前查
								if(minute<=(lode_note_count-num1)){
									num1 = num1+minute;
								}else{
									num1 = count-1;
								}
								if(spi_ReadData(note_block_addr+(num1)*note_block_num,note_block_num,databuf)){
									break;
								}
								if(CAL_CRC(databuf,note_block_num-1) != databuf[note_block_num-1]){
									break;		
								}
							}
						}
					}while(count--);
				}
			}
		}
	}
	return 0;
}		
/*********************************************************************************************************
** 函数名称: read_load_data
** 功能描述: 读取负荷数据，读取给定时间记录数据时需提供时间数据和负荷记录类型（0、1、2）
** 输　入: loaddata 	指向要保存读取数据的符合记录指针 (包含给定的时间信息)
** 		    n				所要读取的记录类型   
**         					0：最早记录块
**		   					1：给定时间记录块
**		   					2：最近一个记录块
**			type			0 总的
**							1  一类
**						    2  二类
** 输　出: U8  0： 出错
**             其他： 数据长度
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U32 read_load_data(U8 *loaddata,U8 n,U8 type)
{
	U8 time[5] = {0,0,0,0,0},buf[40],i;
	U8 datalong = 0;
	U8 flag = 0;
	if(n == 1){
		for(i = 0;i<5;i++){
			time[i] = *(loaddata+i);
		}
	}
	switch(type)
	{
		case 0:
		{
			*loaddata = 0xa0;
			*(loaddata +1)	= 0xa0;
			datalong = 3;
			for(i=0;i<5;i++){
				*(loaddata +3+i) = time[i];
				datalong++;
			}
			if(load_config.lode_mode&1){
				if(read_one_load_data(time,n,first_kind_load,buf)){
					for(i=0;i<17;i++){
						*(loaddata +8+i) = buf[5+i];
						datalong++;
					}
					for(i=0;i<5;i++){
						time[i] = buf[i];
					}
					flag = 1;
					*(loaddata +datalong) = 0xaa;
					datalong++;
				}else{
					return 0;
				}
			}else{
				*(loaddata +8) = 0xaa;
				datalong++;	
			}
			if(load_config.lode_mode&2){
				if(read_one_load_data(time,n,second_kind_load,buf)){
					for(i=0;i<24;i++){
						*(loaddata +datalong) = buf[5+i];
						datalong++;
					}
					if(!flag){
						for(i=0;i<5;i++){
							time[i] = buf[i];
						}
						flag = 1;
					}
					*(loaddata +datalong) = 0xaa;
					datalong++;	
				}else{
					return 0;
				}
			}else{
				*(loaddata +datalong) = 0xaa;
				datalong++;
			}
			if(load_config.lode_mode&4){
				if(read_one_load_data(time,n,third_kind_load,buf)){
					for(i=0;i<8;i++){
						*(loaddata +datalong) = buf[5+i];
						datalong++;
					}
					if(!flag){
						for(i=0;i<5;i++){
							time[i] = buf[i];
						}
						flag = 1;
					}
					*(loaddata +datalong) = 0xaa;
					datalong++;	
				}else{
					return 0;
				}
			}else{
				*(loaddata +datalong) = 0xaa;
				datalong++;
			}
			if(load_config.lode_mode&8){
				if(read_one_load_data(time,n,fourth_kind_load,buf)){
					for(i=0;i<16;i++){
						*(loaddata +datalong) = buf[5+i];
						datalong++;
					}
					if(!flag){
						for(i=0;i<5;i++){
							time[i] = buf[i];
						}
						flag = 1;
					}
					*(loaddata +datalong) = 0xaa;
					datalong++;	
				}else{
					return 0;
				}
			}else{
				*(loaddata +datalong) = 0xaa;
				datalong++;
			}
			if(load_config.lode_mode&0x10){
				if(read_one_load_data(time,n,fifth_kind_load,buf)){
					for(i=0;i<16;i++){
						*(loaddata +datalong) = buf[5+i];
						datalong++;
					}
					if(!flag){
						for(i=0;i<5;i++){
							time[i] = buf[i];
						}
						flag = 1;
					}
					*(loaddata +datalong) = 0xaa;
					datalong++;	
				}else{
					return 0;
				}
			}else{
				*(loaddata +datalong) = 0xaa;
				datalong++;
			}
			if(load_config.lode_mode&0x20){
				if(read_one_load_data(time,n,sixth_kind_load,buf)){
					for(i=0;i<6;i++){
						*(loaddata +datalong) = buf[5+i];
						datalong++;
					}
					if(!flag){
						for(i=0;i<5;i++){
							time[i] = buf[i];
						}
						flag = 1;
					}
					*(loaddata +datalong) = 0xaa;
					datalong++;	
				}else{
					return 0;
				}
			}else{
				*(loaddata +datalong) = 0xaa;
				datalong++;
			}
			
			if(n != 1){
				for(i=0;i<5;i++){
					*(loaddata +3+i) = time[i];
				}
			}
			*(loaddata +2)	= datalong - 3;
			*(loaddata +datalong) = CAL_SUM(loaddata,datalong);
			datalong++;
			*(loaddata +datalong) = 0xe5;
			datalong++;
			
			return datalong;	
		}
		case 1:
		{
			if((read_one_load_data(time,n,first_kind_load,buf))&&(load_config.lode_mode&1)){
				//组帧
				*loaddata = 0xa0;
				*(loaddata +1)	= 0xa0;
				*(loaddata +2)	= 0x1c;
				for(i=0;i<(5+17);i++)
				{
					*(loaddata +3+i) = buf[i];	
				}
				for(i=0;i<(6);i++){
			   		*(loaddata +25+i) = 0xaa;
				}
				*(loaddata +31) = CAL_SUM(loaddata,31);
				*(loaddata +32) = 0xe5;
				return 33;
			}
		}
		break;
		case 2:
		{
			if((read_one_load_data(time,n,second_kind_load,buf))&&(load_config.lode_mode&2)){
				//组帧
				*loaddata = 0xa0;
				*(loaddata +1)	= 0xa0;
				*(loaddata +2)	= 0x23;
				for(i=0;i<(5);i++)
				{
					*(loaddata +3+i) = buf[i];	
				}
				*(loaddata +8) = 0xaa;
				for(i=0;i<(24);i++)
				{
					*(loaddata +9+i) = buf[i+5];	
				}
				for(i=0;i<(5);i++){
			   		*(loaddata +33+i) = 0xaa;
				}
				*(loaddata +38) = CAL_SUM(loaddata,38);
				*(loaddata +39) = 0xe5;
				return 40;
			}
		}
		break;
		case 3:
		{
			if((read_one_load_data(time,n,third_kind_load,buf))&&(load_config.lode_mode&4)){
				//组帧
				*loaddata = 0xa0;
				*(loaddata +1)	= 0xa0;
				*(loaddata +2)	= 0x13;
				for(i=0;i<(5);i++)
				{
					*(loaddata +3+i) = buf[i];	
				}
				*(loaddata +8) = 0xaa;
				*(loaddata +9) = 0xaa;
				for(i=0;i<(8);i++)
				{
					*(loaddata +10+i) = buf[i+5];	
				}
				for(i=0;i<(4);i++){
			   		*(loaddata +18+i) = 0xaa;
				}
				*(loaddata +22) = CAL_SUM(loaddata,22);
				*(loaddata +23) = 0xe5;
				return 24;
			}
		}
		break;
		case 4:
		{
			if((read_one_load_data(time,n,fourth_kind_load,buf))&&(load_config.lode_mode&8)){
				//组帧
				*loaddata = 0xa0;
				*(loaddata +1)	= 0xa0;
				*(loaddata +2)	= 0x1b;
				for(i=0;i<(5);i++)
				{
					*(loaddata +3+i) = buf[i];	
				}
				*(loaddata +8) = 0xaa;
				*(loaddata +9) = 0xaa;
				*(loaddata +10) = 0xaa;
				for(i=0;i<(16);i++)
				{
					*(loaddata +11+i) = buf[i+5];	
				}
				for(i=0;i<(3);i++){
			   		*(loaddata +27+i) = 0xaa;
				}
				*(loaddata +30) = CAL_SUM(loaddata,30);
				*(loaddata +31) = 0xe5;
				return 32;
			}
		}
		break;
		case 5:
		{
			if((read_one_load_data(time,n,fifth_kind_load,buf))&&(load_config.lode_mode&0x10)){
				//组帧
				*loaddata = 0xa0;
				*(loaddata +1)	= 0xa0;
				*(loaddata +2)	= 0x1b;
				for(i=0;i<(5);i++)
				{
					*(loaddata +3+i) = buf[i];	
				}
				*(loaddata +8) = 0xaa;
				*(loaddata +9) = 0xaa;
				*(loaddata +10) = 0xaa;
				*(loaddata +11) = 0xaa;
				for(i=0;i<(16);i++)
				{
					*(loaddata +12+i) = buf[i+5];	
				}
				for(i=0;i<(2);i++){
			   		*(loaddata +28+i) = 0xaa;
				}
				*(loaddata +30) = CAL_SUM(loaddata,30);
				*(loaddata +31) = 0xe5;
				return 32;
			}
		}
		break;
		case 6:
		{
			if((read_one_load_data(time,n,sixth_kind_load,buf))&&(load_config.lode_mode&0x20)){
				//组帧
				*loaddata = 0xa0;
				*(loaddata +1)	= 0xa0;
				*(loaddata +2)	= 0x1b;
				for(i=0;i<(5);i++)
				{
					*(loaddata +3+i) = buf[i];	
				}
				*(loaddata +8) = 0xaa;
				*(loaddata +9) = 0xaa;
				*(loaddata +10) = 0xaa;
				*(loaddata +11) = 0xaa;
				*(loaddata +12) = 0xaa;
				for(i=0;i<(6);i++)
				{
					*(loaddata +13+i) = buf[i+5];	
				}
			   	*(loaddata +19) = 0xaa;
				*(loaddata +20) = CAL_SUM(loaddata,20);
				*(loaddata +21) = 0xe5;
				return 22;
			}
		}
		break;
		default:
		break;
	}

	return 0;
}													 
/*********************************************************************************************************
** 函数名称: save_onetype_load
** 功能描述: 存储一类负荷
** 输　入: type 负荷记录编号
**
** 输　出: U8  0： 出错 1： 存储成功
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 save_onetype_load(load_type_t type)
{
	U8 databuf[50],flag;

	databuf[4] = DecToBCD(currentClock[Year]);		//修改负荷记录保存的时间
	databuf[3] = DecToBCD(currentClock[Month]);
	databuf[2] = DecToBCD(currentClock[Date]);
	databuf[1] = DecToBCD(currentClock[Hour]);
	databuf[0] = DecToBCD(currentClock[Minute]);

	switch(type)
	{
		case first_kind_load:
		{
	   		//修改负荷记录的三相电压值
			databuf[5] =  DecToBCD((a_phase_info.voltage/100)%100);
			databuf[6] =  DecToBCD((a_phase_info.voltage/100)/100%100);
			databuf[7] =  DecToBCD((b_phase_info.voltage/100)%100);
			databuf[8] =  DecToBCD((b_phase_info.voltage/100)/100%100);
			databuf[9] =  DecToBCD((c_phase_info.voltage/100)%100);
			databuf[10] =  DecToBCD((c_phase_info.voltage/100)/100%100);
			//修改负荷记录的三相电流值
			databuf[11] =  DecToBCD((a_phase_info.current&0x7fffffff)%100);
			databuf[12] =  DecToBCD((a_phase_info.current&0x7fffffff)/100%100);
			databuf[13] =  DecToBCD((a_phase_info.current&0x7fffffff)/10000%100);
			if(a_phase_info.current&0x80000000){
				databuf[13] |= 0x80;
			}
			databuf[14] =  DecToBCD((b_phase_info.current&0x7fffffff)%100);
			databuf[15] =  DecToBCD((b_phase_info.current&0x7fffffff)/100%100);
			databuf[16] =  DecToBCD((b_phase_info.current&0x7fffffff)/10000%100);
			if(b_phase_info.current&0x80000000){
				databuf[16] |= 0x80;
			}
			databuf[17] =  DecToBCD((c_phase_info.current&0x7fffffff)%100);
			databuf[18] =  DecToBCD((c_phase_info.current&0x7fffffff)/100%100);
			databuf[19] =  DecToBCD((c_phase_info.current&0x7fffffff)/10000%100);
			if(c_phase_info.current&0x80000000){
				databuf[19] |= 0x80;
			}
			//电网频率
			databuf[20] =  DecToBCD((period_line)%100);
			databuf[21] =  DecToBCD((period_line)/100%100);

			databuf[22]= CAL_CRC(databuf,FIRST_KIND_BLOCK-1);

			if(!spi_ProgramPage(FIRST_KIND_LOAD_ADDR+load_config.load_save_config[first_kind_load].load_save_num*FIRST_KIND_BLOCK,FIRST_KIND_BLOCK,databuf)){
				return 1;
			}	
		}
		break;
		case second_kind_load:
		{
			databuf[5] = DecToBCD((sum_info.active_power&0x7fffffff)%100);	 	
			databuf[6] = DecToBCD((sum_info.active_power&0x7fffffff)/100%100);	 	
			databuf[7] = DecToBCD((sum_info.active_power&0x7fffffff)/10000%100);	 	
			if(sum_info.active_power&0x80000000){
				databuf[7] |= 0x80;
			}
			databuf[8] = DecToBCD((a_phase_info.active_power&0x7fffffff)%100);	 	
			databuf[9] = DecToBCD((a_phase_info.active_power&0x7fffffff)/100%100);	 	
			databuf[10] = DecToBCD((a_phase_info.active_power&0x7fffffff)/10000%100);	 	
			if(a_phase_info.active_power&0x80000000){
				databuf[10] |= 0x80;
			}
			databuf[11] = DecToBCD((b_phase_info.active_power&0x7fffffff)%100);	 	
			databuf[12] = DecToBCD((b_phase_info.active_power&0x7fffffff)/100%100);	 	
			databuf[13] = DecToBCD((b_phase_info.active_power&0x7fffffff)/10000%100);	 	
			if(b_phase_info.active_power&0x80000000){
				databuf[13] |= 0x80;
			}
			databuf[14] = DecToBCD((c_phase_info.active_power&0x7fffffff)%100);	 	
			databuf[15] = DecToBCD((c_phase_info.active_power&0x7fffffff)/100%100);	 	
			databuf[16] = DecToBCD((c_phase_info.active_power&0x7fffffff)/10000%100);	 
			if(c_phase_info.active_power&0x80000000){
				databuf[16] |= 0x80;
			}

			databuf[17] = DecToBCD((sum_info.reactive_power&0x7fffffff)%100);	 	
			databuf[18] = DecToBCD((sum_info.reactive_power&0x7fffffff)/100%100);	 	
			databuf[19] = DecToBCD((sum_info.reactive_power&0x7fffffff)/10000%100);	 	
			if(sum_info.reactive_power&0x80000000){
				databuf[19] |= 0x80;
			}
			databuf[20] = DecToBCD((a_phase_info.reactive_power&0x7fffffff)%100);	 	
			databuf[21] = DecToBCD((a_phase_info.reactive_power&0x7fffffff)/100%100);	 	
			databuf[22] = DecToBCD((a_phase_info.reactive_power&0x7fffffff)/10000%100);	 	
			if(a_phase_info.reactive_power&0x80000000){
				databuf[22] |= 0x80;
			}
			databuf[23] = DecToBCD((b_phase_info.reactive_power&0x7fffffff)%100);	 	
			databuf[24] = DecToBCD((b_phase_info.reactive_power&0x7fffffff)/100%100);	 	
			databuf[25] = DecToBCD((b_phase_info.reactive_power&0x7fffffff)/10000%100);	 	
			if(b_phase_info.reactive_power&0x80000000){
				databuf[25] |= 0x80;
			}
			databuf[26] = DecToBCD((c_phase_info.reactive_power&0x7fffffff)%100);	 	
			databuf[27] = DecToBCD((c_phase_info.reactive_power&0x7fffffff)/100%100);	 	
			databuf[28] = DecToBCD((c_phase_info.reactive_power&0x7fffffff)/10000%100);	 
			if(c_phase_info.reactive_power&0x80000000){
				databuf[28] |= 0x80;
			}
			databuf[29]= CAL_CRC(databuf,SECOND_KIND_BLOCK-1);

			if(!spi_ProgramPage(SECOND_KIND_LOAD_ADDR+load_config.load_save_config[second_kind_load].load_save_num*SECOND_KIND_BLOCK,SECOND_KIND_BLOCK,databuf)){
				return 1;
			}
			
		}
		break;
		case third_kind_load:
		{
		 	databuf[5] = DecToBCD((sum_info.power_factor&0x7fffffff)%100);	 	//总功率因数
			databuf[6] = DecToBCD((sum_info.power_factor&0x7fffffff)/100%100);
			if(sum_info.power_factor&0x80000000){
				databuf[6] |= 0x80;
			}
			
			databuf[7] = DecToBCD((a_phase_info.power_factor&0x7fffffff)%100);	 	//A相功率因数
			databuf[8] = DecToBCD((a_phase_info.power_factor&0x7fffffff)/100%100);
			if(a_phase_info.power_factor&0x80000000){
				databuf[8] |= 0x80;
			}
			databuf[9] = DecToBCD((b_phase_info.power_factor&0x7fffffff)%100);	 	//B相功率因数
			databuf[10] = DecToBCD((b_phase_info.power_factor&0x7fffffff)/100%100);
			if(b_phase_info.power_factor&0x80000000){
				databuf[10] |= 0x80;
			}
			databuf[11] = DecToBCD((c_phase_info.power_factor&0x7fffffff)%100);	 	//C相功率因数
			databuf[12] = DecToBCD((c_phase_info.power_factor&0x7fffffff)/100%100);
			if(c_phase_info.power_factor&0x80000000){
				databuf[12] |= 0x80;
			}
			databuf[13]= CAL_CRC(databuf,THIRD_KIND_BLOCK-1);
			if(!spi_ProgramPage(THIRD_KIND_LOAD_ADDR+load_config.load_save_config[third_kind_load].load_save_num*THIRD_KIND_BLOCK,THIRD_KIND_BLOCK,databuf)){
				return 1;
			}
		}
		break;
		case fourth_kind_load:
		{
			if(!read_one_energy_info(0,0,positive_active,databuf+5,&flag)){
		   		return 0;
			}
			if(!read_one_energy_info(0,0,negetive_active,databuf+9,&flag)){
		   		return 0;
			}
			if(!read_one_energy_info(0,0,combination_reactive1,databuf+13,&flag)){
		   		return 0;
			}
			if(flag){
				databuf[16] |= 0x80;
			}
			if(!read_one_energy_info(0,0,combination_reactive2,databuf+17,&flag)){
		   		return 0;
			}
			if(flag){
				databuf[20] |= 0x80;
			}
			databuf[21]= CAL_CRC(databuf,FOURTH_KIND_BLOCK-1);
			if(!spi_ProgramPage(FOURTH_KIND_LOAD_ADDR+load_config.load_save_config[fourth_kind_load].load_save_num*FOURTH_KIND_BLOCK,FOURTH_KIND_BLOCK,databuf)){
				return 1;
			}
		}
		break;
		case fifth_kind_load:
		{
			if(!read_one_energy_info(0,0,first_quadrant_reactive,databuf+5,&flag)){
		   		return 0;
			}
			if(!read_one_energy_info(0,0,second_quadrant_reactive,databuf+9,&flag)){
		   		return 0;
			}
			if(!read_one_energy_info(0,0,third_quadrant_reactive,databuf+13,&flag)){
		   		return 0;
			}
			if(!read_one_energy_info(0,0,fourth_quadrant_reactive,databuf+17,&flag)){
		   		return 0;
			}
			databuf[21]= CAL_CRC(databuf,FIFTH_KIND_BLOCK-1);
			if(!spi_ProgramPage(FIFTH_KIND_LOAD_ADDR+load_config.load_save_config[fifth_kind_load].load_save_num*FIFTH_KIND_BLOCK,FIFTH_KIND_BLOCK,databuf)){
				return 1;
			}
		}
		break;
		case sixth_kind_load:
		{
			//当前有功需量
			databuf[5] = DecToBCD((demand_info.active_demand&0x7fffffff)%100);
			databuf[6] = DecToBCD((demand_info.active_demand&0x7fffffff)/100%100);
			databuf[7] = DecToBCD((demand_info.active_demand&0x7fffffff)/10000%100);
			if(demand_info.active_demand&0x80000000){
				databuf[7] |= 0x80;
			}
		
			//当前无功需量
			databuf[8] = DecToBCD((demand_info.reactive_demand&0x7fffffff)%100);
			databuf[9] = DecToBCD((demand_info.reactive_demand&0x7fffffff)/100%100);
			databuf[10] = DecToBCD((demand_info.reactive_demand&0x7fffffff)/10000%100);
			if(demand_info.reactive_demand&0x80000000){
				databuf[10] |= 0x80;
			}
			databuf[11]= CAL_CRC(databuf,SIXTH_KIND_BLOCK-1);
			if(!spi_ProgramPage(SIXTH_KIND_LOAD_ADDR+load_config.load_save_config[sixth_kind_load].load_save_num*SIXTH_KIND_BLOCK,SIXTH_KIND_BLOCK,databuf)){
				return 1;
			}
		}
		break;
		default:
		break;
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称: check_data_timeout_minute
** 功能描述: 检查时间是否超过间隙 分
** 输　入: 无
** 输　出: 1 超过
**		   0 没
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 check_data_timeout_minute(U8* timenew,U8* timeold,U32 interval)
{
	U32 day = 0;
	
	if(((*(timeold+2)) == 0)||((*(timeold+3)) == 0)){
   		return 0;
	}
	day = compareDate(*(timeold+4)+2000,*(timeold+3),*(timeold+2),*(timenew+4)+2000,*(timenew+3),*(timenew+2));

	if(day != 0x0ffffffff){
		if(((day*24*60+(*(timenew+1))*60+(*timenew)) -((*(timeold+1))*60+(*timeold)))>= interval){
			return 1;
		} 	
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称: check_load
** 功能描述: 检查存储负荷
** 输　入:
**
** 输　出: U8  0： 出错 1：存储成功
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 check_load(void)
{
	U8 i;

	if(!check_all_same(load_config.load_start_time,99,4)){
		if(check_zone_timeout(currentClock+1,load_config.load_start_time,4)){
			//第一类负荷
			if(check_data_timeout_minute(currentClock+1,load_config.load_save_config[first_kind_load].last_load_save_time,load_config.load_save_config[first_kind_load].load_interval)){
				if(save_onetype_load(first_kind_load)){
					for(i=0;i<5;i++){
						load_config.load_save_config[first_kind_load].last_load_save_time[i] = currentClock[Minute+i];
					}
					load_config.load_save_config[first_kind_load].load_save_num++;
				}	
			}
			//第二类负荷
			if(check_data_timeout_minute(currentClock+1,load_config.load_save_config[second_kind_load].last_load_save_time,load_config.load_save_config[second_kind_load].load_interval)){
				if(save_onetype_load(second_kind_load)){
					for(i=0;i<5;i++){
						load_config.load_save_config[second_kind_load].last_load_save_time[i] = currentClock[Minute+i];
					}
					load_config.load_save_config[second_kind_load].load_save_num++;
				}	
			}
			//第三类负荷
			if(check_data_timeout_minute(currentClock+1,load_config.load_save_config[third_kind_load].last_load_save_time,load_config.load_save_config[third_kind_load].load_interval)){
				if(save_onetype_load(third_kind_load)){
					for(i=0;i<5;i++){
						load_config.load_save_config[third_kind_load].last_load_save_time[i] = currentClock[Minute+i];
					}
					load_config.load_save_config[third_kind_load].load_save_num++;
				}	
			}
			//第四类负荷
			if(check_data_timeout_minute(currentClock+1,load_config.load_save_config[fourth_kind_load].last_load_save_time,load_config.load_save_config[fourth_kind_load].load_interval)){
				if(save_onetype_load(fourth_kind_load)){
					for(i=0;i<5;i++){
						load_config.load_save_config[fourth_kind_load].last_load_save_time[i] = currentClock[Minute+i];
					}
					load_config.load_save_config[fourth_kind_load].load_save_num++;
				}	
			}
			//第五类负荷
			if(check_data_timeout_minute(currentClock+1,load_config.load_save_config[fifth_kind_load].last_load_save_time,load_config.load_save_config[fifth_kind_load].load_interval)){
				if(save_onetype_load(fifth_kind_load)){
					for(i=0;i<5;i++){
						load_config.load_save_config[fifth_kind_load].last_load_save_time[i] = currentClock[Minute+i];
					}
					load_config.load_save_config[fifth_kind_load].load_save_num++;
				}	
			}
			//第六类负荷
			if(check_data_timeout_minute(currentClock+1,load_config.load_save_config[sixth_kind_load].last_load_save_time,load_config.load_save_config[sixth_kind_load].load_interval)){
				if(save_onetype_load(sixth_kind_load)){
					for(i=0;i<5;i++){
						load_config.load_save_config[sixth_kind_load].last_load_save_time[i] = currentClock[Minute+i];
					}
					load_config.load_save_config[sixth_kind_load].load_save_num++;
				}	
			}
		}
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称: check_load_position
** 功能描述: 检查负荷位置，如下一个位置在下一个扇区，擦除下一个扇区   以扇区进行
** 输　入:
**
** 输　出: U8  0： 出错 1：存储成功
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 check_load_position(void)
{
	static U32 last_erase_poaition[6] = {0,0,0,0,0,0};

	U32 data1,data2;

	//第一类负荷储存位置擦除
	data1 = load_config.load_save_config[first_kind_load].load_save_num*FIRST_KIND_BLOCK/0x1000;
	data2 = (load_config.load_save_config[first_kind_load].load_save_num+1)*FIRST_KIND_BLOCK/0x1000;
	if((data1 != data2)&&(last_erase_poaition[0] != data2)){
		if(!spi_EraseSector(data2&0x0fffff000)){
			last_erase_poaition[0] = data2; 
		}	
	} 
	//第二类负荷储存位置擦除
	data1 = load_config.load_save_config[second_kind_load].load_save_num*SECOND_KIND_BLOCK/0x1000;
	data2 = (load_config.load_save_config[second_kind_load].load_save_num+1)*SECOND_KIND_BLOCK/0x1000;
	if((data1 != data2)&&(last_erase_poaition[1] != data2)){
		if(!spi_EraseSector(data2&0x0fffff000)){
			last_erase_poaition[1] = data2; 
		}	
	}
	//第三类负荷储存位置擦除
	data1 = load_config.load_save_config[third_kind_load].load_save_num*THIRD_KIND_BLOCK/0x1000;
	data2 = (load_config.load_save_config[third_kind_load].load_save_num+1)*THIRD_KIND_BLOCK/0x1000;
	if((data1 != data2)&&(last_erase_poaition[2] != data2)){
		if(!spi_EraseSector(data2&0x0fffff000)){
			last_erase_poaition[2] = data2; 
		}	
	}
	//第四类负荷储存位置擦除
	data1 = load_config.load_save_config[fourth_kind_load].load_save_num*FOURTH_KIND_BLOCK/0x1000;
	data2 = (load_config.load_save_config[fourth_kind_load].load_save_num+1)*FOURTH_KIND_BLOCK/0x1000;
	if((data1 != data2)&&(last_erase_poaition[3] != data2)){
		if(!spi_EraseSector(data2&0x0fffff000)){
			last_erase_poaition[3] = data2; 
		}	
	}
	//第五类负荷储存位置擦除
	data1 = load_config.load_save_config[fifth_kind_load].load_save_num*FIFTH_KIND_BLOCK/0x1000;
	data2 = (load_config.load_save_config[fifth_kind_load].load_save_num+1)*FIFTH_KIND_BLOCK/0x1000;
	if((data1 != data2)&&(last_erase_poaition[4] != data2)){
		if(!spi_EraseSector(data2&0x0fffff000)){
			last_erase_poaition[4] = data2; 
		}	
	}
	//第六类负荷储存位置擦除
	data1 = load_config.load_save_config[sixth_kind_load].load_save_num*SIXTH_KIND_BLOCK/0x1000;
	data2 = (load_config.load_save_config[sixth_kind_load].load_save_num+1)*SIXTH_KIND_BLOCK/0x1000;
	if((data1 != data2)&&(last_erase_poaition[5] != data2)){
		if(!spi_EraseSector(data2&0x0fffff000)){
			last_erase_poaition[5] = data2; 
		}	
	}
	return 0;
}

/*********************************************************************************************************
** 函数名称: get_load_position
** 功能描述: 取一个时间对应的负荷位置
** 输　入:	 time1 时间点  
**			 last_time 最新一条记录时间
**			 addr_start 记录起始地址
**			 note_long	记录长度
**			 note_count 记录条数
** 输　出: U8  0： 出错 
**             1： 成功
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U32 get_load_position(U8 *time1,U8 *last_time,U32 time_interval,U32 addr_start,U32 note_long,U32 note_count)
{
	U8 timenew[5],i;
	U32 days = 0;
	U32 count = note_count,num1 = note_count;
	U8 databuf[40];

	for(i=0;i<5;i++){
		timenew[i] = *(last_time+i);
	}

	if(note_count){
		  if(!check_zone_timeout(time1,timenew,5)){
			do{
				days = compareDate(*(time1+4),*(time1+3),*(time1+2),timenew[4],timenew[3],timenew[2]);
				if(days != 0x0ffffffff){
					days = days*24*60+timenew[1]*60+timenew[0] - time1[1]*60-time1[0]; 
			
					days = days/time_interval;
			
					if(days == 0){
						return num1;
					}else{
						//前查
						if(days<(num1)){
							num1 = num1-days-1;
						}else{
							num1 = note_count - count;
						}
						if(spi_ReadData(addr_start+(num1)*note_long,note_long,databuf)){
							break;
						}
						if(CAL_CRC(databuf,note_long-1) != databuf[note_long-1]){
							break;		
						}
						for(i=0;i<5;i++){
							timenew[i] = BCDToDec(databuf[i]);
						}	
					}
				}else{
					days = compareDate(timenew[4],timenew[3],timenew[2],*(time1+4),*(time1+3),*(time1+2));
					days = days*24*60+ time1[1]*60+time1[0] - timenew[1]*60-timenew[0]; 
			
					days = days/time_interval;
			
					if(days == 0){
						return num1;
					}else{
						//后查
						if(days<(note_count-(num1))){
							num1 = num1+days;
						}else{
							num1 = count-1;
						}
						if(spi_ReadData(addr_start+(num1)*note_long,note_long,databuf)){
							break;
						}
						if(CAL_CRC(databuf,note_long-1) != databuf[note_long-1]){
							break;		
						}
						for(i=0;i<5;i++){
							timenew[i] = BCDToDec(databuf[i]);
						}	
					}
				}	
			}while(count--);
		}	
	}
	return 0x0ffffffff;
}
/*********************************************************************************************************
** 函数名称: copy_sector_data
** 功能描述: 将一个扇区的内容拷贝到另一扇区
** 输　入:	 source_addr 源地址  
**			 goal_addr 目标地址
**			 datalong 数据长度
**
** 输　出: U8  0： 出错 
**             1： 成功
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 copy_sector_data(U32 source_addr,U32 goal_addr,U32 datalong)
{
	U8 databuf[256],i;
	U8 count = datalong/256,trail = datalong%256;

	if(!spi_EraseSector(goal_addr)){
		for(i=0;i<count;i++){
			if(spi_ReadData(source_addr+i*256,256,databuf)){
				return 0;
			}
			if(spi_ProgramPage(goal_addr+i*256,256,databuf)){
				return 0;		
			}	
		}
		if(trail){
			if(spi_ReadData(source_addr+i*256,trail,databuf)){
				return 0;
			}
			if(spi_ProgramPage(goal_addr+i*256,trail,databuf)){
				return 0;		
			}
			
		}
		return 1;		
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称:erase_load_data
** 功能描述: 删除部分
** 输　入:	 
**
**
** 输　出: U8  0： 出错 
**             1： 成功
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 				//修改时间时调用
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 erase_load_data(void)
{
	static U8 exchange_sector = 0;
	U32 num;
	U8 data[40],i = 0;

	//第一类负荷
	num = get_load_position(currentClock+1,load_config.load_save_config[first_kind_load].last_load_save_time,load_config.load_save_config[first_kind_load].load_interval,FIRST_KIND_LOAD_ADDR,FIRST_KIND_BLOCK,load_config.load_save_config[first_kind_load].load_save_num);
	if(num != 0x0ffffffff){
		if(copy_sector_data((FIRST_KIND_LOAD_ADDR+FIRST_KIND_BLOCK*(num - 1))&0x0fffff000,0x3d0000+exchange_sector*0x1000,(FIRST_KIND_LOAD_ADDR+FIRST_KIND_BLOCK*(num - 1))%0x1000)){
			if(copy_sector_data(0x3d0000+exchange_sector*0x1000,(FIRST_KIND_LOAD_ADDR+FIRST_KIND_BLOCK*(num - 1))&0x0fffff000,(FIRST_KIND_LOAD_ADDR+FIRST_KIND_BLOCK*(num - 1))%0x1000)){
				load_config.load_save_config[first_kind_load].load_save_num = num - 1;
				//修改最后一条时间
				if(load_config.load_save_config[first_kind_load].load_save_num == 0){
					for(i = 0;i<4;i++){
						load_config.load_save_config[first_kind_load].last_load_save_time[i] = load_config.load_start_time[i];
					}
					load_config.load_save_config[first_kind_load].last_load_save_time[4] = currentClock[Year];
				}else{
					if(read_load_data(data,2,1)){
						load_config.load_save_config[first_kind_load].last_load_save_time[i] = BCDToDec(data[i]);	
					}	
				}	
			}
		}	
	}
	//第二类负荷
	num = get_load_position(currentClock+1,load_config.load_save_config[second_kind_load].last_load_save_time,load_config.load_save_config[second_kind_load].load_interval,SECOND_KIND_LOAD_ADDR,SECOND_KIND_BLOCK,load_config.load_save_config[second_kind_load].load_save_num);
	if(num != 0x0ffffffff){
		if(copy_sector_data((SECOND_KIND_LOAD_ADDR+SECOND_KIND_BLOCK*(num - 1))&0x0fffff000,0x3d0000+exchange_sector*0x1000,(SECOND_KIND_LOAD_ADDR+SECOND_KIND_BLOCK*(num - 1))%0x1000)){
			if(copy_sector_data(0x3d0000+exchange_sector*0x1000,(SECOND_KIND_LOAD_ADDR+SECOND_KIND_BLOCK*(num - 1))&0x0fffff000,(SECOND_KIND_LOAD_ADDR+SECOND_KIND_BLOCK*(num - 1))%0x1000)){
				load_config.load_save_config[second_kind_load].load_save_num = num - 1;
				//修改最后一条时间
				if(load_config.load_save_config[second_kind_load].load_save_num == 0){
					for(i = 0;i<4;i++){
						load_config.load_save_config[second_kind_load].last_load_save_time[i] = load_config.load_start_time[i];
					}
					load_config.load_save_config[second_kind_load].last_load_save_time[4] = currentClock[Year];
				}else{
					if(read_load_data(data,2,1)){
						load_config.load_save_config[second_kind_load].last_load_save_time[i] = BCDToDec(data[i]);	
					}	
				}	
			}
		}	
	}
	//第三类负荷
	num = get_load_position(currentClock+1,load_config.load_save_config[third_kind_load].last_load_save_time,load_config.load_save_config[third_kind_load].load_interval,SECOND_KIND_LOAD_ADDR,SECOND_KIND_BLOCK,load_config.load_save_config[third_kind_load].load_save_num);
	if(num != 0x0ffffffff){
		if(copy_sector_data((THIRD_KIND_LOAD_ADDR+THIRD_KIND_BLOCK*(num - 1))&0x0fffff000,0x3d0000+exchange_sector*0x1000,(THIRD_KIND_LOAD_ADDR+THIRD_KIND_BLOCK*(num - 1))%0x1000)){
			if(copy_sector_data(0x3d0000+exchange_sector*0x1000,(THIRD_KIND_LOAD_ADDR+THIRD_KIND_BLOCK*(num - 1))&0x0fffff000,(THIRD_KIND_LOAD_ADDR+THIRD_KIND_BLOCK*(num - 1))%0x1000)){
				load_config.load_save_config[third_kind_load].load_save_num = num - 1;
				//修改最后一条时间
				if(load_config.load_save_config[third_kind_load].load_save_num == 0){
					for(i = 0;i<4;i++){
						load_config.load_save_config[third_kind_load].last_load_save_time[i] = load_config.load_start_time[i];
					}
					load_config.load_save_config[third_kind_load].last_load_save_time[4] = currentClock[Year];
				}else{
					if(read_load_data(data,2,1)){
						load_config.load_save_config[third_kind_load].last_load_save_time[i] = BCDToDec(data[i]);	
					}	
				}	
			}
		}	
	}
	//第四类负荷
	num = get_load_position(currentClock+1,load_config.load_save_config[fourth_kind_load].last_load_save_time,load_config.load_save_config[fourth_kind_load].load_interval,FOURTH_KIND_LOAD_ADDR,FOURTH_KIND_BLOCK,load_config.load_save_config[fourth_kind_load].load_save_num);
	if(num != 0x0ffffffff){
		if(copy_sector_data((FOURTH_KIND_LOAD_ADDR+FOURTH_KIND_BLOCK*(num - 1))&0x0fffff000,0x3d0000+exchange_sector*0x1000,(FOURTH_KIND_LOAD_ADDR+FOURTH_KIND_BLOCK*(num - 1))%0x1000)){
			if(copy_sector_data(0x3d0000+exchange_sector*0x1000,(FOURTH_KIND_LOAD_ADDR+FOURTH_KIND_BLOCK*(num - 1))&0x0fffff000,(FOURTH_KIND_LOAD_ADDR+FOURTH_KIND_BLOCK*(num - 1))%0x1000)){
				load_config.load_save_config[fourth_kind_load].load_save_num = num - 1;
				//修改最后一条时间
				if(load_config.load_save_config[fourth_kind_load].load_save_num == 0){
					for(i = 0;i<4;i++){
						load_config.load_save_config[fourth_kind_load].last_load_save_time[i] = load_config.load_start_time[i];
					}
					load_config.load_save_config[fourth_kind_load].last_load_save_time[4] = currentClock[Year];
				}else{
					if(read_load_data(data,2,1)){
						load_config.load_save_config[fourth_kind_load].last_load_save_time[i] = BCDToDec(data[i]);	
					}	
				}	
			}
		}	
	}
	//第五类负荷
	num = get_load_position(currentClock+1,load_config.load_save_config[fifth_kind_load].last_load_save_time,load_config.load_save_config[fifth_kind_load].load_interval,FIFTH_KIND_LOAD_ADDR,FIFTH_KIND_BLOCK,load_config.load_save_config[fifth_kind_load].load_save_num);
	if(num != 0x0ffffffff){
		if(copy_sector_data((FIFTH_KIND_LOAD_ADDR+FIFTH_KIND_BLOCK*(num - 1))&0x0fffff000,0x3d0000+exchange_sector*0x1000,(FIFTH_KIND_LOAD_ADDR+FIFTH_KIND_BLOCK*(num - 1))%0x1000)){
			if(copy_sector_data(0x3d0000+exchange_sector*0x1000,(FIFTH_KIND_LOAD_ADDR+FIFTH_KIND_BLOCK*(num - 1))&0x0fffff000,(FIFTH_KIND_LOAD_ADDR+FIFTH_KIND_BLOCK*(num - 1))%0x1000)){
				load_config.load_save_config[fifth_kind_load].load_save_num = num - 1;
				//修改最后一条时间
				if(load_config.load_save_config[fifth_kind_load].load_save_num == 0){
					for(i = 0;i<4;i++){
						load_config.load_save_config[fifth_kind_load].last_load_save_time[i] = load_config.load_start_time[i];
					}
					load_config.load_save_config[fifth_kind_load].last_load_save_time[4] = currentClock[Year];
				}else{
					if(read_load_data(data,2,1)){
						load_config.load_save_config[fifth_kind_load].last_load_save_time[i] = BCDToDec(data[i]);	
					}	
				}	
			}
		}	
	}
	//第六类负荷
	num = get_load_position(currentClock+1,load_config.load_save_config[sixth_kind_load].last_load_save_time,load_config.load_save_config[sixth_kind_load].load_interval,SIXTH_KIND_LOAD_ADDR,SIXTH_KIND_BLOCK,load_config.load_save_config[sixth_kind_load].load_save_num);
	if(num != 0x0ffffffff){
		if(copy_sector_data((SIXTH_KIND_LOAD_ADDR+SIXTH_KIND_BLOCK*(num - 1))&0x0fffff000,0x3d0000+exchange_sector*0x1000,(SIXTH_KIND_LOAD_ADDR+SIXTH_KIND_BLOCK*(num - 1))%0x1000)){
			if(copy_sector_data(0x3d0000+exchange_sector*0x1000,(SIXTH_KIND_LOAD_ADDR+SIXTH_KIND_BLOCK*(num - 1))&0x0fffff000,(SIXTH_KIND_LOAD_ADDR+SIXTH_KIND_BLOCK*(num - 1))%0x1000)){
				load_config.load_save_config[sixth_kind_load].load_save_num = num - 1;
				//修改最后一条时间
				if(load_config.load_save_config[sixth_kind_load].load_save_num == 0){
					for(i = 0;i<4;i++){
						load_config.load_save_config[sixth_kind_load].last_load_save_time[i] = load_config.load_start_time[i];
					}
					load_config.load_save_config[sixth_kind_load].last_load_save_time[4] = currentClock[Year];
				}else{
					if(read_load_data(data,2,1)){
						load_config.load_save_config[sixth_kind_load].last_load_save_time[i] = BCDToDec(data[i]);	
					}	
				}	
			}
		}	
	}
	return 0;

}
