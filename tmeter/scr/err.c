/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#include <RTL.h>                      /* RTX kernel functions & defines      */
#include <LPC17xx.H>                  /* LPC17xx definitions                 */

#include "err.h"
#include "meter_output.h"
#include "uart_task.h"
#include "power_check.h"

err_control_t err_control;
/*********************************************************************************************************
** 函数名称: init_err_check
** 功能描述: 检查错误
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void init_err_check(void)
{
	U8 i;

	for(i=0;i<8;i++){
		err_control.err_buf[i] = 0;
	}
	err_control.err_num = 0;
	err_control.err_count = 0;
	err_control.err_mode = 0;
}
/*********************************************************************************************************
** 函数名称: set_one_err
** 功能描述: 设置一种错误
** 输　入: type 错误类型
** 输　出: 0 错误
**		   1 失败
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 set_one_err(U8 type)
{
	U8 a,b;
		
	if((type>64)||(type == 0)){
		return 0;
	}
	a = (type-1)/8;
	b = (type-1)%8;

	err_control.err_buf[a] |= (1<<b);

	return 1;
}
/*********************************************************************************************************
** 函数名称: clear_one_err
** 功能描述: 清一种错误
** 输　入: type 错误类型
** 输　出: 0 错误
**		   1 失败
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 clear_one_err(U8 type)
{
	U8 a,b;
		
	if((type>64)||(type == 0)){
		return 0;
	}
	a = (type-1)/8;
	b = (type-1)%8;

	err_control.err_buf[a] &= (~(1<<b));

	return 1;
}
/*********************************************************************************************************
** 函数名称: check_err_state
** 功能描述: 检查错误状态
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void check_err_state(void)
{
	//1类错误
	//控制线路错误
	if(((meter_state_info.meter_run_state[2]&0x50) == 0x50)||((meter_state_info.meter_run_state[2]&0x50) == 0x0)){
		clear_one_err(CONTROL_ERR);
	}else{
		set_one_err(CONTROL_ERR);
	}
	//esam错误

	//内存

	//内部程序

	//外部储存器

	//时钟

	//2类错误
	//过载
	if((meter_state_info.meter_run_state[3]&0x20)||(meter_state_info.meter_run_state[4]&0x20)||(meter_state_info.meter_run_state[5]&0x20)){
		set_one_err(OVER_LOAD_ERR);
	}else{
		clear_one_err(OVER_LOAD_ERR);
	}
	//电流不平衡
	if((meter_state_info.meter_run_state[6]&0x08)){
		set_one_err(CURRENT_NO_BALANCE_ERR);
	}else{
		clear_one_err(CURRENT_NO_BALANCE_ERR);
	}
	//过压
	if((meter_state_info.meter_run_state[3]&0x04)||(meter_state_info.meter_run_state[4]&0x04)||(meter_state_info.meter_run_state[5]&0x04)){
		set_one_err(OVER_VOLTAGE_ERR);
	}else{
		clear_one_err(OVER_VOLTAGE_ERR);
	}
	//功率因素超限
	if((0)){
		set_one_err(POWER_FACTOR_ERR);
	}else{
		clear_one_err(POWER_FACTOR_ERR);
	}
	//有功需量超限
	if((meter_state_info.meter_run_state[6]&0x40)){
		set_one_err(ACTIVE_DEMAND_ERR);
	}else{
		clear_one_err(ACTIVE_DEMAND_ERR);
	}
	//有功电能反向
	if((meter_state_info.meter_run_state[0]&0x10)){
		set_one_err(ACTIVE_ENERGY_ERR);
	}else{
		clear_one_err(ACTIVE_ENERGY_ERR);
	}


	//状态检测 3类 液晶显示
	//失压
	if((meter_state_info.meter_run_state[3]&1)||(meter_state_info.meter_run_state[4]&1)||(meter_state_info.meter_run_state[5]&1)){
		set_one_err(LOST_VOLTAGE_ERR);
	}else{
		clear_one_err(LOST_VOLTAGE_ERR);
	}
	//断相
	if((meter_state_info.meter_run_state[3]&0x80)||(meter_state_info.meter_run_state[4]&0x80)||(meter_state_info.meter_run_state[5]&0x80)){
		set_one_err(BREAK_PHASE_ERR);
	}else{
		clear_one_err(BREAK_PHASE_ERR);
	}
	//失流
	if((meter_state_info.meter_run_state[3]&0x8)||(meter_state_info.meter_run_state[4]&0x8)||(meter_state_info.meter_run_state[5]&0x8)){
		set_one_err(LOST_CURRENT_ERR);
	}else{
		clear_one_err(LOST_CURRENT_ERR);
	}
	//逆向序
	if((meter_state_info.meter_run_state[6]&0x1)||(meter_state_info.meter_run_state[6]&0x2)){
		set_one_err(ATHWART_ERR);
	}else{
		clear_one_err(ATHWART_ERR);
	}
	//时钟电池欠压
	if(meter_state_info.meter_run_state[0]&0x4){
		set_one_err(CLOCK_BETT_ERR);	
	}else{
   		clear_one_err(CLOCK_BETT_ERR);
	}
	//停抄电池欠压
	if(meter_state_info.meter_run_state[0]&0x8){
		set_one_err(STOP_BETT_ERR);	
	}else{
   		clear_one_err(STOP_BETT_ERR);
	}
	//透支

	//购电囤积


}
/*********************************************************************************************************
** 函数名称: err_check
** 功能描述: 检查错误
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 err_check(void)
{
	U8 i,j;
	U8 count = 0,flag = 0;

	for(i=0;i<8;i++){
		for(j=0;j<8;j++){
			if(err_control.err_buf[i]&(1<<j)){
				count++;
			}
		}
	}
	err_control.err_count = count;

	if((count>0)||(output_control.alarm_deal == 0x2a)){	//远程报警处理
		Display_Alarm_Flag = 1;
		Electromagnetism_Relay_Flag = 1;
	}else{
		Display_Alarm_Flag = 0;
		Electromagnetism_Relay_Flag = 0;
	}
	if(count>0){	 //检查错误类型
		for(i=0;i<8;i++){
			if(err_control.err_buf[0]&(1<<i)){
				flag++;
			}
		}
		if(flag){
			err_control.err_mode = 1;
			return 1;
		}
		for(i=0;i<6;i++){
			for(j=0;j<8;j++){
				if(err_control.err_buf[i+1]&(1<<j)){
					flag++;
				}
			}
		}
		if(flag){
			err_control.err_mode |= 2;
			return 1;
		}
		err_control.err_mode = 0;
	}else{
		err_control.err_mode = 0;
	}
	return 1;
}
/*********************************************************************************************************
** 函数名称: check_err_type
** 功能描述: 检查错误
** 输　入: 无
** 输　出: 0  没有报警
** 		   其他 报警编号
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 check_err_type(void)
{

	if(power_state){

		err_check();
		
		switch(err_control.err_mode)
		{
			case 1:
			{
		   		U8 i,b;
				if(err_control.err_num>8){
					err_control.err_num = 0;
				}
				for(i=0;i<8;i++){
					b = ((i+err_control.err_num)%8);
					if(err_control.err_buf[0]&(1<<b)){
						err_control.err_num = (b+1);
						return 1;
					}	
				}	
			}
			break;
			case 2:
			{
		   		U8 i,a,b;
				if(err_control.err_num<9){
					err_control.err_num = 8;
				}
				for(i=0;i<48;i++){
					a = ((i+err_control.err_num)%56)/8;
					b = ((i+err_control.err_num)%56)%8;
					
					if(err_control.err_buf[a]&(1<<b)){
						err_control.err_num = (((i+err_control.err_num)%56)+1);
						return 1;
					}
					if((i+err_control.err_num)>56){
						err_control.err_mode |= 0x80;
						return 0; 	
					}	
				}
			}
			break;
			case 0x82:
			{
				err_control.err_num = 8;
			}
			break;
			default:
			{
				err_control.err_num = 0;
			}
			break;
		}
	}else{
		init_err_check();
	}
	return 0;	
}
