/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#include <rtl.h>
#include <LPC17xx.h>
#include "LPC_I2C.h"
#include "lpc_uart.h"
#include "uart_task.h"
#include "eeprom.h"
#include "rx8025t.h"
#include "lcd.h"
#include "input.h"
#include "ade7878.h"
#include "demand.h"
#include "time_period_zone.h"
#include "security_password.h"
#include "load.h"
#include "freeze.h"
#include "balance.h"
#include "firmware.h"
#include "voltage_check.h"
#include "ade7878_adjust.h"
#include "event.h"
#include "esam_card.h"
#include "meter_output.h"

//通讯645结构
Uart_645Data_t uart0_645frame,uart1_645frame,uart3_645frame;
U8 uart0_645data[UART0_645DATALONG],uart1_645data[UART1_645DATALONG],uart3_645data[UART3_645DATALONG];

//电表运行状态
meter_state_info_t meter_state_info = {{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0x80,0x80,0x80,0},0};
//额定电压
#if RATING_VOLTAGE == 220000
	const U8 rate_voltage[6] = {0,0,'V','0','2','2'};
#else 
	#if	(RATING_VOLTAGE == 100000)
	const U8 rate_voltage[6] = {0,0,'V','0','0','1'};
	#endif
#endif

//额定电流
#if RATING_CURRENT == 10000 
	const U8 rate_current[6] = {0,0,0,'A','0','1'};
#else 
	#if RATING_CURRENT == 1500
	const U8 rate_current[6] = {0,0,'A','5','.','1'};
	#endif
#endif
//最大电流
#if MAX_CURRENT == 40000 
	const U8 max_rate_current[6] = {0,0,0,'A','0','4'};
#else 
	#if MAX_CURRENT == 6000 
	const U8 max_rate_current[6] = {0,0,0,0,'A','6'};
	#endif
#endif

//有功精度
const U8 active_precision_grade[4] = {0,'0','.','1'};
//有功精度
const U8 reactive_precision_grade[4] = {0,'0','.','2'};
#if TEST_VERSION == 1
	//厂商软件版本
	const U8 manufacturer_software_ver[32] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	//厂商硬件版本
	const U8 manufacturer_hardware_ver[32] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	//厂商编号
	const U8 manufacturer_serial_number[32] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	//电表型号
	const U8 meter_type[10] = {0,0,0,0,0,0,0,0,0,0};
	//协议版本,0
	const U8 protocol_edition[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
#else
	//厂商软件版本
	const U8 manufacturer_software_ver[32] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	//厂商硬件版本
	const U8 manufacturer_hardware_ver[32] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	//厂商编号 ,厂家编码：73193317-0
	const U8 manufacturer_serial_number[32] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'0','-','7','1','3','3','9','1','3','7'};
	//电表型号
	#if RATING_VOLTAGE == 57700 || RATING_VOLTAGE == 220000
	const U8 meter_type[10] = {0,0,0,0,'5','2','7','Z','T','D'};
	#endif
	#if RATING_VOLTAGE == 100000 || RATING_VOLTAGE == 380000
	const U8 meter_type[10] = {0,0,0,0,'5','2','7','Z','S','D'};
	#endif

	//协议版本,DL/T 645-2007
	const U8 protocol_edition[16] = {0,0,0,'7','0','0','2','-','5','4','6',' ','T','/','L','D'};
#endif
//电表生产日期
//const U8 produce_date[10] = {0,0,0,0,0,0,0,0,0,0};

/*********************************************************************************************************
** 函数名称:     process_uart_Broadcast
** 功能描述:     处理串口广播指令
** 输　入:       DevNum 串口编号
**
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
static U8 InitUart645Frame(LPC_Uart_Channel_t DevNum)
{
	U8 databuf[10],datalong,i;
		
	switch(DevNum){
		case UART0:
			uart0_645frame.Control = 0;
			uart0_645frame.Datalong = 0;
			uart0_645frame.HandleState = 0;
			uart0_645frame.DataBuf = uart0_645data;
			for(i=0;i<UART0_645DATALONG;i++){
				*(uart0_645frame.DataBuf+i) = 0;
			}
			uart0_645frame.TimeReciveFrameTrail = 0;
			//读取速率特征字
			datalong = getEepromData(4,0,7,3,databuf);
			if(datalong == 1){
				uart0_645frame.speed_state = databuf[0];
			}else{
				uart0_645frame.speed_state = 0x8;
			}
			for(i=0;i<4;i++){
				uart0_645frame.last_frame_ID[i] = 0;
			}
			uart0_645frame.last_frame_seq = 0;
			uart0_645frame.addr = 0;
			uart0_645frame.chip = 0;
			uart0_645frame.last_frame_datalong = 0;

			//表号
			datalong = getEepromData(4,0,4,2,databuf);
			if(datalong == 6){
				for(i=0;i<6;i++){
					meter_state_info.meter_number[i] = databuf[i];
				}
			}else{
				meter_state_info.meter_number[0] = 1;
				for(i=0;i<5;i++){
					meter_state_info.meter_number[i+1] = 0;
				}	
			}
			//表通讯地址
			datalong = getEepromData(4,0,4,1,databuf);
			if(datalong == 6){
				for(i=0;i<6;i++){
					meter_state_info.meter_comm_addr[i] = databuf[i];
				}
			}else{
				meter_state_info.meter_comm_addr[0] = 1;
				for(i=0;i<5;i++){
					meter_state_info.meter_number[i+1] = 0;
				}	
			}

		break;
		case UART1:
			uart1_645frame.Control = 0;
			uart1_645frame.Datalong = 0;
			uart1_645frame.HandleState = 0;
			uart1_645frame.DataBuf = uart1_645data;
			for(i=0;i<UART1_645DATALONG;i++){
				*(uart1_645frame.DataBuf+i) = 0;
			}
			uart1_645frame.TimeReciveFrameTrail = 0;
			//读取速率特征字
			datalong = getEepromData(4,0,7,4,databuf);
			if(datalong == 1){
				uart1_645frame.speed_state = databuf[0];
			}else{
				uart1_645frame.speed_state = 0x8;
			}
			for(i=0;i<4;i++){
				uart1_645frame.last_frame_ID[i] = 0;
			}
			uart1_645frame.last_frame_seq = 0;
			uart1_645frame.addr = 0;
			uart1_645frame.chip = 0;
			uart1_645frame.last_frame_datalong = 0;
		break;
		case UART3:
			uart3_645frame.Control = 0;
			uart3_645frame.Datalong = 0;
			uart3_645frame.HandleState = 0;
			uart3_645frame.DataBuf = uart3_645data;
			for(i=0;i<UART3_645DATALONG;i++){
				*(uart3_645frame.DataBuf+i) = 0;
			}
			uart3_645frame.TimeReciveFrameTrail = 0;
			//读取速率特征字
			datalong = getEepromData(4,0,7,1,databuf);
			if(datalong == 1){
				uart3_645frame.speed_state = databuf[0];
			}else{
				uart3_645frame.speed_state = 0x4;
			}
			for(i=0;i<4;i++){
				uart3_645frame.last_frame_ID[i] = 0;
			}
			uart3_645frame.last_frame_seq = 0;
			uart3_645frame.addr = 0;
			uart3_645frame.chip = 0;
			uart3_645frame.last_frame_datalong = 0;
		break;
		default:
		return 0;
	}
	return 1;
}
/*********************************************************************************************************
** 函数名称:     check_bcd_data
** 功能描述:     检测bcd数据
** 输　入:       buf     检测数据首地址
**				 datalong  	 数据长度
** 输　出:       1 是BCD
**				 0 不全是
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 check_bcd_data(U8 *buf,U8 datalong){
	U8 i;
	for(i = 0;i<datalong;i++){
		if(((*(buf+i)&0x0f)>9)||(((*(buf+i)&0x0f0)>>4)>9)){
			return 0;
		}
	}
	return 1;	
}
/*********************************************************************************************************
** 函数名称:     check_baud_Z
** 功能描述:     检查波特率特征字
** 输　入:       buf     检测数据首地址
**				 datalong  	 数据长度
** 输　出:       1 正确
**				 0 错误
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 check_baud_Z(U8 data){
	U8 i,flag = 0;
	if((data&0x80)||(data&0x1)){
		return 0;
	} 
	for(i = 0;i<7;i++){
		if((data>>i)&0x1){
			flag++;	
		}
	}
	if(flag == 1){
		return 1;
	}else{
		return 0;
	}	
}
/*********************************************************************************************************
** 函数名称:     get_uart_speed
** 功能描述:     取串口速率
** 输　入:       DevNum
**				 speed_state
** 输　出:       速度
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U32 get_uart_speed(LPC_Uart_Channel_t DevNum,U8 speed_state)
{
	U32 speed = 0;

	switch(DevNum){
		case UART0:
			speed = 2400;
		break;
		case UART1:
			speed = 2400;
		break;
		case UART3:
			speed = 1200;
		break;
		default:
			speed = 2400;
		break;
	}

	if(check_baud_Z(speed_state)){
		speed = (speed_state&0x7e)*300;
	}

	return speed;	
}
/*********************************************************************************************************
** 函数名称:     get_speed_addr
** 功能描述:     取串口速率状态字存储地址
** 输　入:       DevNum
**				 
** 输　出:       地址
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 get_speed_addr(LPC_Uart_Channel_t DevNum)
{
	U8 addr = 0;

	switch(DevNum){
		case UART0:
			addr = 3;
		break;
		case UART1:
			addr = 4;
		break;
		case UART3:
			addr = 1;
		break;
		default:
		break;
	}
	return addr;	
}
/*********************************************************************************************************
** 函数名称:     com_read_energy_data
** 功能描述:     读取能量数据
** 输　入:       id2
**				 id1
**				 id0
**				 databuf
** 输　出:       0 无有效数据
**				 1 有数据
**				 2 参数错误
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 com_read_energy_data(U8 id2,U8 id1,U8 id0,U8 *buf,U8 *datalong)
{
	U8 flag = 0,i=0;
	if((id2<=0x0a)&&(id1<=0x4)&&(id0<=0xc)){ //总的
		if(read_one_energy_info(id0,id1,id2,buf,&flag)){
			if(flag){
		   		*(buf+3) |= 0x80;
			}
			*datalong = 4;
			return 1;
		}
	}else if((id2<=0x0a)&&((id1<=0x63)||(id1==0x0ff))&&(id0<=0xc)){	   //总的
		if(id1 != 0x0ff){
			*buf = 0;
			*(buf+1) = 0;
			*(buf+2) = 0;
			*(buf+3) = 0;
			*datalong = 4;
			return 1;
		}else{
			for(i=0;i<=time_zone_con.max_fee_num;i++){
				if(read_one_energy_info(id0,i,id2,buf+4*i,&flag)){
					if(flag){
				   		*(buf+4*i+3) |= 0x80;
					}
				}else{
					return 0;
				}		
			}
			*datalong = 4*(time_zone_con.max_fee_num+1);
			return 1;	

		}
	}else if((((id2>=0x15)&&(id2<=0x1e))||((id2>=0x29)&&(id2<=0x32))||((id2>=0x3d)&&(id2<=0x46)))&&(id1==0x00)&&(id0<=0xc)){   //分相
	 	if(read_one_energy_info(id0,id1,id2,buf,&flag)){
			if(flag){
		   		*(buf+3) |= 0x80;
			}
			*datalong = 4;
			return 1;
		}
	}else if((((id2>=0x80)&&(id2<=0x86))||((id2>=0x94)&&(id2<=0x9a))||((id2>=0xa8)&&(id2<=0xae))||((id2>=0xbc)&&(id2<=0xc2)))&&(id1==0x00)&&(id0<=0xc)){ //相关能量
		*buf = 0;
		*(buf+1) = 0;
		*(buf+2) = 0;
		*(buf+3) = 0;
		*datalong = 4;
		return 1;
	}else{
		return 2;
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称:     com_read_demand_data
** 功能描述:     读取需量数据
** 输　入:       id2
**				 id1
**				 id0
**				 databuf
** 输　出:       0 无有效数据
**				 1 有数据
**				 2 参数错误
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 com_read_demand_data(U8 id2,U8 id1,U8 id0,U8 *buf,U8 *datalong)
{
	U8 flag = 0,i=0;
	if((id2!=0x00)&&(id2<=0x0a)&&(id1<=0x4)&&(id0<=0xc)){ //总的
		if(read_one_demand_info(id0,id1,id2,buf,&flag)){
			if(flag){
		   		*(buf+2) |= 0x80;
			}
			*datalong = 8;
			return 1;
		}
	}else if((id2!=0x00)&&(id2<=0x0a)&&((id1<=0x63)||(id1==0x0ff))&&(id0<=0xc)){	   //总的
		if(id1 != 0x0ff){
			for(i=0;i<8;i++){
				*(buf+i) = 0;
			}
			*datalong = 8;
			return 1;
		}else{
			for(i=0;i<=time_zone_con.max_fee_num;i++){
				if(read_one_demand_info(id0,i,id2,buf+8*i,&flag)){
					if(flag){
				   		*(buf+8*i+2) |= 0x80;
					}
				}else{
					return 0;
				}		
			}
			*datalong = 8*(time_zone_con.max_fee_num+1);
			return 1;	

		}
	}else if((((id2>=0x15)&&(id2<=0x1e))||((id2>=0x29)&&(id2<=0x32))||((id2>=0x3d)&&(id2<=0x46)))&&(id1==0x00)&&(id0<=0xc)){   //分相
	 	if(read_one_demand_info(id0,id1,id2,buf,&flag)){
			if(flag){
		   		*(buf+2) |= 0x80;
			}
			*datalong = 8;
			return 1;
		}
	}else if((((id2>=0x80)&&(id2<=0x86))||((id2>=0x94)&&(id2<=0x9a))||((id2>=0xa8)&&(id2<=0xae))||((id2>=0xbc)&&(id2<=0xc2)))&&(id1==0x00)&&(id0<=0xc)){ //相关能量
		for(i=0;i<8;i++){
			*(buf+i) = 0;
		}
		*datalong = 8;
		return 1;
	}else{
		return 2;
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称:     com_read_demand_data
** 功能描述:     读取事件数据
** 输　入:       id2
**				 id1
**				 id0
**				 databuf
** 输　出:       0 无有效数据
**				 1 有数据
**				 2 参数错误
**				 3 数据未完
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 com_read_event_data(LPC_Uart_Channel_t DevNum,U8 id2,U8 id1,U8 id0,U8 *buf,U8 *datalong)
{
	U8 id = 0;

	switch(id2)
	{
		case 1://失压
		case 2://欠压
		case 3://过压
		case 4://断相
		case 0x0b://失流
		case 0x0c://过流
		case 0x0d://断流
		case 0x0e://潮流反向
		case 0x0f://过载
		{
			switch(id1)
			{
				case 0:
				{
					if(id0 == 0){
						*datalong = getEepromData(3,id2,0,0,buf);
						if(*datalong == 18){
							return 1;
						}		
					}else{
						return 2;
					}
				}
				break;
				case 1:
				case 2:
				case 3:
				{
					if((id0<=0x0a)&&(id0!=0)){
				   		if(change_event_id(&id,id1,id2,id0,0)){
							if(id == 1){
								id = 0x0b;
							}else{
								id--;
							}
							*datalong = getEepromData(3,id2,id1,id,buf);
							if(*datalong != 0){
								return 1;
							}
						}
					}else{
						return 2;
					}
				}
				break;
				default:
				return 2;
			}		
		}
		break;
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
		case 0x0a:
		{
			if(id1==0){
				if(id0 == 0){
					*datalong = getEepromData(3,id2,0,0,buf);
					if(*datalong != 0){
						return 1;
					}
				}else if(id0<=0x0a){
					if(change_event_id(&id,0,id2,id0,0)){
						if(id == 1){
							id = 0x0b;
						}else{
							id--;
						}
						*datalong = getEepromData(3,id2,0,id,buf);
						if(*datalong != 0){
							return 1;
						}
					}
				}else{
					return 2;
				}
			}else{
				return 2;
			}
		}
		break;
		case 0x11:
		{
			if(id1==0){
				if(id0 == 0){
					*datalong = getEepromData(3,id2,0,0,buf);
					if(*datalong != 0){
						return 1;
					}
				}else if(id0<=0x0a){
					if(change_event_id(&id,0,id2,id0,0)){
						if(id == 1){
							id = 0x0a;
						}else{
							id--;
						}
						*datalong = getEepromData(3,id2,0,id,buf);
						if(*datalong != 0){
							return 1;
						}
					}
				}else{
					return 2;
				}
			}else{
				return 2;
			}
		}
		break;
		case 0x10:
		{

		}
		break;
		case 0x12:
		{
			switch(id1)
			{
				case 0:
				{
					if(id0 == 0){
						*datalong = getEepromData(3,0x12,0,0,buf);
						if(*datalong == 18){
							return 1;
						}		
					}else{
						return 2;
					}
				}
				break;
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				{
					if((id0<=0x0a)&&(id0!=0)){
				   		if(change_event_id(&id,id1,0x12,id0,0)){
							if(id == 1){
								id = 0x0b;
							}else{
								id--;
							}
							*datalong = getEepromData(3,0x12,id1,id,buf);
							if(*datalong != 0){
								return 1;
							}
						}
					}else{
						return 2;
					}
				}
				break;
				default:
				return 2;
			}
		}
		break;
		case 0x30:
		{
			switch(id1)
			{
				case 0:	//编程记录
				case 0x0d:
				case 0x0e:
				{
					if(id0 == 0){
						*datalong = getEepromData(3,0x30,id1,0,buf);
						if(*datalong != 0){
							return 1;
						}
					}else if(id0<=0x0a){
						if(change_event_id(&id,id1,0x30,id0,0)){
							if(id == 1){
								id = 0x0b;
							}else{
								id--;
							}
							*datalong = getEepromData(3,0x30,id1,id,buf);
							if(*datalong != 0){
								return 1;
							}
						}
					}else{
						return 2;
					}
				}
				break;
				case 1://电表清零
				case 3://事件清零
				case 4://校时总次数
				case 6://时区表编程
				case 7://周休日
				case 9:
				case 0x0a:
				case 0x0b:
				case 0x0c:

				{
					if(id0 == 0){
						*datalong = getEepromData(3,0x30,id1,0,buf);
						if(*datalong != 0){
							return 1;
						}
					}else if(id0<=0x0a){
						if(change_event_id(&id,id1,0x30,id0,0)){
							if(id == 1){
								id = 0x0a;
							}else{
								id--;
							}
							*datalong = getEepromData(3,0x30,id1,id,buf);
							if(*datalong != 0){
								return 1;
							}
						}
					}else{
						return 2;
					}
				}
				break;
				case 5://时段表   有可能超200
				{
			   		if(id0 == 0){
						*datalong = getEepromData(3,0x30,id1,0,buf);
						if(*datalong != 0){
							return 1;
						}
					}else if(id0<=0x0a){
						if(change_event_id(&id,id1,0x30,id0,0)){
							if(id == 1){
								id = 0x0b;
							}else{
								id--;
							}
							
						}
					}else{
						return 2;
					}
				}
				break;
				case 8://节假日   有可能超200
				{
					if(id0 == 0){
						*datalong = getEepromData(3,0x30,id1,0,buf);
						if(*datalong != 0){
							return 1;
						}
					}else if(id0<=0x0a){
						if(change_event_id(&id,id1,0x30,id0,0)){
							if(id == 1){
								id = 0x0b;
							}else{
								id--;
							}
							
						}
					}else{
						return 2;
					}
				}
				break;
				case 2://需量清零
				{
					U16 addr = 0;
					U32 len = 0;
					if(id0 == 0){
						*datalong = getEepromData(3,0x30,2,0,buf);
						if(*datalong != 0){
							return 1;
						}
					}else if(id0<=0x0a){
						if(change_event_id(&id,id1,0x30,id0,0)){
							if(id == 1){
								id = 0x0b;
							}else{
								id--;
							}
							if(check_data_first_addr(3,0x30,2,id,&addr,&len)){
								if(len>197){
									if(read_data(3,addr,195,buf)){
										*datalong = 195;
										switch(DevNum)
										{
											case UART0:
											{
												uart0_645frame.last_frame_ID[0] = id0;
												uart0_645frame.last_frame_ID[1] = id1;
												uart0_645frame.last_frame_ID[2] = id2;
												uart0_645frame.last_frame_ID[3] = 0x03;
												uart0_645frame.last_frame_seq = 0;
												uart0_645frame.addr = addr;
												uart0_645frame.chip = 3;
												uart0_645frame.last_frame_datalong = len-1;
											}
											break;
											case UART1:
											{
												uart1_645frame.last_frame_ID[0] = id0;
												uart1_645frame.last_frame_ID[1] = id1;
												uart1_645frame.last_frame_ID[2] = id2;
												uart1_645frame.last_frame_ID[3] = 0x03;
												uart1_645frame.last_frame_seq = 0;
												uart1_645frame.addr = addr;
												uart1_645frame.chip = 3;
												uart1_645frame.last_frame_datalong = len-1;
											}
											break;
											case UART3:
											{
												uart3_645frame.last_frame_ID[0] = id0;
												uart3_645frame.last_frame_ID[1] = id1;
												uart3_645frame.last_frame_ID[2] = id2;
												uart3_645frame.last_frame_ID[3] = 0x03;
												uart3_645frame.last_frame_seq = 0;
												uart3_645frame.addr = addr;
												uart3_645frame.chip = 3;
												uart3_645frame.last_frame_datalong = len-1;
											}
											break;
											default:
											return 2;
										}
										return 3;
									}
								}else{
									if(read_data(3,addr,len,buf)){
										*datalong = len-1;
										return 1;
									}
								}
							}	
						}
					}else{
						return 2;
					}
				}
				break;
				default:
				return 2;
			}
		}
		break;
		default:
		return 2;
	}	
	return 0;
}
/*********************************************************************************************************
** 函数名称:     com_read_state_event_data
** 功能描述:     读取事件数据
** 输　入:
**               DevNum 串口号
**				 id3	数据标示
**               id2
**				 id1
**				 id0
**				 buf 数据首地址
**				 datalong 数据长度
** 输　出:       0 无有效数据
**				 1 有数据
**				 2 参数错误
**				 3 数据未完
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 com_read_state_event_data(LPC_Uart_Channel_t DevNum,U8 id3,U8 id2,U8 id1,U8 id0,U8 *buf,U8 *datalong)
{
	U8 id = 0;
	switch(id3)
	{
		case 0x10: //失压
		{
			switch(id2)
			{
				case 0:
				{
					switch(id1)	
					{
						case 0:
						{
							if((id0<=2)&&(id0!=0)){
								*datalong = getEepromData(0x10,0,0,id0,buf);
								if(*datalong != 0){
									return 1;
								}	
							}else{
								return 2;
							}
						}
						break;
						case 1:
						case 2:
						{
							if(id0==1){
								*datalong = getEepromData(0x10,0,id1,1,buf);
								if(*datalong != 0){
									return 1;
								}	
							}else{
								return 2;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 1:
				case 2:
				case 3:
				{
					if(id1 == 0){
						if((id0<=2)&&(id0!=0)){
							*datalong = getEepromData(0x10,id2,0,id0,buf);
							if(*datalong != 0){
								return 1;
							}	
						}else{
							return 2;
						}
					}else if((id1 <= 0x35)&&(id0<11)&&(id0 != 0)){
						if(state_change_event_id(&id,id2,0x10,id0,0)){
							if(id == 1){
								id = 0x0b;
							}else{
								id--;
							}
							*datalong = getEepromData(0x10,id2,id1,id,buf);
							if(*datalong != 0){
								return 1;
							}
						}

					}else if((id1 == 0xff)&&(id0<11)&&(id0 != 0)){

					}else{
						return 2;
					}
				}
				break;
				default:
				return 0;
			}
		}
		break;
		case 0x11://欠压
		case 0x12://过压
		case 0x13://断相
		{
			switch(id2)
			{
		   		case 1:
				case 2:
				case 3:
				{
					if(id1 == 0){
						if((id0<=2)&&(id0!=0)){
							*datalong = getEepromData(id3,id2,0,id0,buf);
							if(*datalong != 0){
								return 1;
							}	
						}else{
							return 2;
						}
					}else if((id1 <= 0x35)&&(id0<11)&&(id0 != 0)){
						if(state_change_event_id(&id,id2,id3,id0,0)){
							if(id == 1){	  
								id = 0x0b;
							}else{
								id--;
							}
							*datalong = getEepromData(id3,id2,id1,id,buf);
							if(*datalong != 0){
								return 1;
							}
						}

					}else if((id1 == 0xff)&&(id0<11)&&(id0 != 0)){

					}else{
						return 2;
					}
				}
				break;
				default:
				return 2;
			}
		}
		break;
		case 0x14: //电压逆相序
		case 0x15: //电流逆相序
		{
			if(id2 == 0){
				if(id1 == 0){
					if((id0<=2)&&(id0!=0)){
						*datalong = getEepromData(id3,0,0,id0,buf);
						if(*datalong != 0){
							return 1;
						}	
					}else{
						return 2;
					}
				}else if((id1 <= 0x22)&&(id0<11)&&(id0 != 0)){
					if(state_change_event_id(&id,0,id3,id0,0)){
						if(id == 1){
							id = 0x0b;
						}else{
							id--;
						}
						*datalong = getEepromData(id3,0,id1,id,buf);
						if(*datalong != 0){
							return 1;
						}
					}

				}else if((id1 == 0xff)&&(id0<11)&&(id0 != 0)){

				}else{
					return 2;
				}
			}else{
				return 2;
			}
		}
		break;
		case 0x16://电压不平衡
		case 0x17://电流不平衡
		{
			if(id2 == 0){
				if(id1 == 0){
					if((id0<=2)&&(id0!=0)){
						*datalong = getEepromData(id3,0,0,id0,buf);
						if(*datalong != 0){
							return 1;
						}	
					}else{
						return 2;
					}
				}else if((id1 <= 0x23)&&(id0<11)&&(id0 != 0)){
					if(state_change_event_id(&id,0,id3,id0,0)){
						if(id == 1){
							id = 0x0b;
						}else{
							id--;
						}
						*datalong = getEepromData(id3,0,id1,id,buf);
						if(*datalong != 0){
							return 1;
						}
					}

				}else if((id1 == 0xff)&&(id0<11)&&(id0 != 0)){

				}else{
					return 2;
				}
			}else{
				return 2;
			}
		}
		break;
		case 0x18://失流
		case 0x19://过流
		case 0x1A://断流
		{
			switch(id2)
			{
		   		case 1:
				case 2:
				case 3:
				{
					if(id1 == 0){
						if((id0<=2)&&(id0!=0)){
							*datalong = getEepromData(id3,id2,0,id0,buf);
							if(*datalong != 0){
								return 1;
							}	
						}else{
							return 2;
						}
					}else if((id1 <= 0x31)&&(id0<11)&&(id0 != 0)){
						if(state_change_event_id(&id,id2,id3,id0,0)){
							if(id == 1){
								id = 0x0b;
							}else{
								id--;
							}
							*datalong = getEepromData(id3,id2,id1,id,buf);
							if(*datalong != 0){
								return 1;
							}
						}

					}else if((id1 == 0xff)&&(id0<11)&&(id0 != 0)){

					}else{
						return 2;
					}
				}
				break;
				default:
				return 2;
			}
		}
		break;
		case 0x1B://潮流反向
		case 0x1C://过载
		{
			switch(id2)
			{
		   		case 1:
				case 2:
				case 3:
				{
					if(id1 == 0){
						if((id0<=2)&&(id0!=0)){
							*datalong = getEepromData(id3,id2,0,id0,buf);
							if(*datalong != 0){
								return 1;
							}	
						}else{
							return 2;
						}
					}else if((id1 <= 0x22)&&(id0<11)&&(id0 != 0)){
						if(state_change_event_id(&id,id2,id3,id0,0)){
							if(id == 1){
								id = 0x0b;
							}else{
								id--;
							}
							*datalong = getEepromData(id3,id2,id1,id,buf);
							if(*datalong != 0){
								return 1;
							}
						}

					}else if((id1 == 0xff)&&(id0<11)&&(id0 != 0)){

					}else{
						return 2;
					}
				}
				break;
				default:
				return 2;
			}
		}
		break;
		case 0x1D://跳闸
		case 0x1E://合闸
		{
			if(id2 == 0){
				if(id1 == 0){
					if(id0==1){
						*datalong = getEepromData(id3,0,0,1,buf);
						if(*datalong != 0){
							return 1;
						}	
					}else{
						return 2;
					}
				}else if((id1 <= 0x8)&&(id0<11)&&(id0 != 0)){
					if(state_change_event_id(&id,0,id3,id0,0)){
						if(id == 1){
							id = 0x0b;
						}else{
							id--;
						}
						*datalong = getEepromData(id3,0,id1,id,buf);
						if(*datalong != 0){
							return 1;
						}
					}

				}else if((id1 == 0xff)&&(id0<11)&&(id0 != 0)){

				}else{
					return 2;
				}
			}else{
				return 2;
			}
		}
		break;
		case 0x1f: //总功率下限
		{
			if(id2 == 0){
				if(id1 == 0){
					if((id0<=2)&&(id0!=0)){
						*datalong = getEepromData(id3,0,0,id0,buf);
						if(*datalong != 0){
							return 1;
						}	
					}else{
						return 2;
					}
				}else if((id1 <= 0xa)&&(id0<11)&&(id0 != 0)){
					if(state_change_event_id(&id,0,id3,id0,0)){
						if(id == 1){
							id = 0x0b;
						}else{
							id--;
						}
						*datalong = getEepromData(id3,0,id1,id,buf);
						if(*datalong != 0){
							return 1;
						}
					}

				}else if((id1 == 0xff)&&(id0<11)&&(id0 != 0)){

				}else{
					return 2;
				}
			}else{
				return 2;
			}
		}
		break;
		default:
		return 2;
	}	
	return 0;
}
/*********************************************************************************************************
** 函数名称:     com_read_parameter_data
** 功能描述:     读取参数数据
** 输　入:       id2
**				 id1
**				 id0
**				 databuf
** 输　出:       0 无有效数据
**				 其他 数据长度
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 com_read_parameter_data(U8 id2,U8 id1,U8 id0,U8 *buf,U8 *datalong)
{
	U8 i;

	switch(id2)
	{
		case 0:
		{
			switch(id1)
			{
				case 1:
				{
					if(id0 == 1){	//日期
						*(buf) = DecToBCD(currentClock[Week]);
						*(buf+1) = DecToBCD(currentClock[Date]);
						*(buf+2) = DecToBCD(currentClock[Month]);
						*(buf+3) = DecToBCD(currentClock[Year]);
						*datalong = 4;	
					}else if(id0 == 2){	 //时间
						*(buf) = DecToBCD(currentClock[Second]);
						*(buf+1) = DecToBCD(currentClock[Minute]);
						*(buf+2) = DecToBCD(currentClock[Hour]);
						*datalong = 3;
					}else if(id0<=0x0a){	 //其他
						*datalong = getEepromData(4,0,1,id0,buf);
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
					
				}
				break;
				case 2:
				{
					if((id0 != 0)&&(id0<=0x08)){
						*datalong = getEepromData(4,0,2,id0,buf);
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 3:
				{
					if((id0 != 0)&&(id0<=0x08)){
						*datalong = getEepromData(4,0,3,id0,buf);	
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 4://有很多常量
				{
					switch(id0)
					{
						case 1:
						case 2:
						case 3:
						{
							*datalong = getEepromData(4,0,4,id0,buf);
							 if((*datalong) != 0){
								return 1;
							}
						}
						break;
						case 4://额定电压
						{
							U8 i = 0;
							for(i=0;i<6;i++){
							 	*(buf+i) = rate_voltage[i];
							}
							*datalong = 6;
							return 1;
						}
//						break;
						case 5://基本电流
						{
							U8 i = 0;
							for(i=0;i<6;i++){
							 	*(buf+i) = rate_current[i];
							}
							*datalong = 6;
							return 1;
						}
//						break;
						case 6://最大电流
						{
							U8 i = 0;
							for(i=0;i<6;i++){
							 	*(buf+i) = max_rate_current[i];
							}
							*datalong = 6;
							return 1;
						}
//						break;
						case 7://有功精度等级
						{
							U8 i = 0;
							for(i=0;i<4;i++){
							 	*(buf+i) = active_precision_grade[i];
							}
							*datalong = 4;
							return 1;
						}
//						break;
						case 8://无功精度等级
						{
							U8 i = 0;
							for(i=0;i<4;i++){
							 	*(buf+i) = reactive_precision_grade[i];
							}
							*datalong = 4;
							return 1;
						}
//						break;
						case 9://电表有功常数
						{
							*(buf) = DecToBCD(ACTIVE_PULSE_CONSTANT%100);
							*(buf+1) = DecToBCD(ACTIVE_PULSE_CONSTANT/100%100);
							*(buf+2) = DecToBCD(ACTIVE_PULSE_CONSTANT/10000%100);
							*datalong = 3;
							return 1;
						}
//						break;
						case 0x0a://电表无功常数
						{
							*(buf) = DecToBCD(REACTIVE_PULSE_CONSTANT%100);
							*(buf+1) = DecToBCD(REACTIVE_PULSE_CONSTANT/100%100);
							*(buf+2) = DecToBCD(REACTIVE_PULSE_CONSTANT/10000%100);
							*datalong = 3;
							return 1;
						}
//						break;
						case 0x0b://电表型号
						{
							U8 i = 0;
							for(i=0;i<10;i++){
							 	*(buf+i) = meter_type[i];
							}
							*datalong = 10;
							return 1;
						}
//						break;
						case 0x0c://电表生产日期
						{
//							U8 i = 0;
//							for(i=0;i<10;i++){
//							 	*(buf+i) = produce_date[i];
//							}
							if(read_data(1,0x0ff80,10,buf)){
								*datalong = 10;
								return 1;
							}
						}
//						break;
						case 0x0d://电表协议版本号
						{
					   		U8 i = 0;
							for(i=0;i<16;i++){
							 	*(buf+i) = protocol_edition[i];
							}
							*datalong = 16;
							return 1;
						}
//						break;

#if HAVE_ESAM == 1
						case 0x0e://电表用户编号
						{
							U8 i = 0;
							for(i=0;i<6;i++){
							 	*(buf+i) = account_control.account_num[i];
							}
							*datalong = 6;
							return 1;
							
						}
#endif
//						break;
						default:
						return 2;
					}
				}
				break;
				case 5://电表运行状态字
				{
					if((id0 != 0)&&(id0<=7)){
						*buf = meter_state_info.meter_run_state[id0-1];
						*(buf+1) = (meter_state_info.meter_run_state[id0-1]>>8);
						*datalong = 2;
					}else if(id0 == 0x0ff){
						for(i=0;i<7;i++){
							*(buf+2*i) = meter_state_info.meter_run_state[i];
							*(buf+2*i+1) = (meter_state_info.meter_run_state[i]>>8);
						}
						*datalong = 14;
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}	
				}
				break;
				case 6:
				{
					if((id0 != 0)&&(id0<=3)){
						*datalong = getEepromData(4,0,6,id0,buf);
						
					}else{
						return 2;
					}

					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 7:
				{
					if((id0 != 0)&&(id0<=5)){
						*datalong = getEepromData(4,0,7,id0,buf);
						
					}else{
						return 2;
					}

					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 8:
				{
					if((id0 != 0)&&(id0<=2)){
						*datalong = getEepromData(4,0,8,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 9:
				{
					if((id0 != 0)&&(id0<=6)){
						*datalong = getEepromData(4,0,9,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 0x0a:
				{
					if((id0 != 0)&&(id0<=7)){
						*datalong = getEepromData(4,0,10,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 0x0b:
				{
					if((id0 != 0)&&(id0<=3)){
						*datalong = getEepromData(4,0,0x0b,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 0x0c:
				{
					if((id0 != 0)&&(id0<=0x0a)){
						*datalong = getEepromData(4,0,0x0c,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 0x0d: //应该是变量
				{
					if((id0 != 0)&&(id0<=12)){
						*datalong = getEepromData(4,0,0x0d,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}	
				}
				break;
				case 0x0e:
				{
					if((id0 != 0)&&(id0<=4)){
						*datalong = getEepromData(4,0,0x0e,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 0x0f:
				{
					if((id0<=4)){
						*datalong = getEepromData(4,0,0x0f,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 0x10:
				{
					if((id0<=5)){
						*datalong = getEepromData(4,0,0x10,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 0x11:
				{
					if((id0 != 0)&&(id0<=1)){
						*datalong = getEepromData(4,0,0x11,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 0x12:
				{
					if((id0 != 0)&&(id0<=3)){
						*datalong = getEepromData(4,0,0x12,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 0x14:
				{
					if((id0 != 0)&&(id0<=1)){
						*datalong = getEepromData(4,0,0x14,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				default:
				return 2; 
			}
		}
		break;
		case 1:
		{
			if((id1 == 0)&&(id0<=8)){
				*datalong = getEepromData(4,1,0,id0,buf);
				
			}else{
				return 2;
			}
			if((*datalong) != 0){
				return 1;
			}
		}
		break;
		case 2:
		{
			if((id1 == 0)&&(id0<=8)){
				*datalong = getEepromData(4,2,0,id0,buf);	
			}else{
				return 2;
			}
			if((*datalong) != 0){
				return 1;
			}
		}
		break;
		case 3:
		{
			if((id1 == 0)&&(id0 != 0)&&(id0<=0x80)){
				*datalong = getEepromData(4,3,0,id0,buf);	
			}else if((id1 == 0)&&(id0 != 0)&&(id0<=0xfe)){
				*datalong = 4;
				*buf = 0;
				*(buf+1) = 0;
				*(buf+2) = 0;
				*(buf+3) = 0;	
			}else{
				return 2;
			}
			if((*datalong) != 0){
				return 1;
			}
		}
		break;
		case 4:
		{
			if(((id1 == 1)||(id1 == 2))&&(id0 != 0)&&(id0 <= 0x80)){
				*datalong = getEepromData(4,4,id1,id0,buf);
			}else if(((id1 == 1)||(id1 == 2))&&(id0 != 0)&&(id0<=0xfe)){
			 	*datalong = 5;
				*buf = 0;
				*(buf+1) = 0;
				*(buf+2) = 0;
				*(buf+3) = 0;
				*(buf+4) = 0;
			}else{
				return 2;
			}
			if((*datalong) != 0){
				return 1;
			}
		}
		break;
		case 5:
		{ //一类参数（本地需通过卡设置）
			if(((id1 == 1)||(id1 == 2))&&(id0 != 0)&&(id0 <= 0x08)){
				*datalong = getEepromData(4,5,id1,id0,buf);
			}else{
				return 2;
			}
			if((*datalong) != 0){
				return 1;
			}
		}
		break;
		case 6:
		{ //阶梯电价 本地
			switch(id1)
			{
				case 0:
				case 2:
				{
					if((id0 != 0)&&(id0<=0x10)){
						*datalong = getEepromData(4,6,id1,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 1:
				case 3:
				{
					if((id0 != 0)&&(id0<=0x11)){
						*datalong = getEepromData(4,6,id1,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				default:
				return 2;
			}
		}
		break;
		case 7:	 //第一套费率梯度混合电价
		case 8:	 //第一套费率梯度混合电价
		{
			switch(id1)
			{
				case 0:
				{
					if((id0 != 0)&&(id0<=0x08)){
						*datalong = getEepromData(4,id2,id1,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				{
					if((id0 != 0)&&(id0<=0x21)){
						*datalong = getEepromData(4,id2,id1,id0,buf);
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				default:
				return 2;
			} 
		}
		break;
		case 9:
		{ //事件触发阀值
			switch(id1)
			{
				case 1:
				{
					if((id0 != 0)&&(id0<=4)){
						*datalong = getEepromData(4,9,0x01,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 2:
				{
					if((id0 != 0)&&(id0<=2)){
						*datalong = getEepromData(4,9,0x02,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 3:
				{
					if((id0 != 0)&&(id0<=2)){
						*datalong = getEepromData(4,9,0x03,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 4:
				{
					if((id0 != 0)&&(id0<=3)){
						*datalong = getEepromData(4,9,0x04,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 5:
				{
					if((id0 != 0)&&(id0<=2)){
						*datalong = getEepromData(4,9,0x05,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 6:
				{
					if((id0 != 0)&&(id0<=2)){
						*datalong = getEepromData(4,9,0x06,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 7:
				{
					if((id0 != 0)&&(id0<=4)){
						*datalong = getEepromData(4,9,0x07,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 8:
				{
					if((id0 != 0)&&(id0<=2)){
						*datalong = getEepromData(4,9,0x08,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 9:
				{
					if((id0 != 0)&&(id0<=3)){
						*datalong = getEepromData(4,9,0x09,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 0x0a:
				{
					if((id0 != 0)&&(id0<=2)){
						*datalong = getEepromData(4,9,0x0a,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 0x0b:
				{
					if((id0 != 0)&&(id0<=2)){
						*datalong = getEepromData(4,9,0x0b,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 0x0c:
				{
					if((id0 != 0)&&(id0<=2)){
						*datalong = getEepromData(4,9,0x0c,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 0x0d:
				{
					if((id0 != 0)&&(id0<=3)){
						*datalong = getEepromData(4,9,0x0d,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 0x0e:
				{
					if((id0 != 0)&&(id0<=2)){
						*datalong = getEepromData(4,9,0x0e,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				case 0x0f:
				{
					if((id0 != 0)&&(id0<=2)){
						*datalong = getEepromData(4,9,0x0f,id0,buf);
						
					}else{
						return 2;
					}
					if((*datalong) != 0){
						return 1;
					}
				}
				break;
				default:
				return 2;
			}
		}
		break;
		case 0x80://常量
		{
			switch(id1)
			{
				case 0:
				{
					switch(id0)
					{
						case 1:	//厂家软件版本号
						{
					   		U8 i;
							*datalong = 32;
							for(i=0;i<32;i++){
								*(buf+i) = manufacturer_software_ver[i];
							}
							return 1;
						}
//						break;
						case 2://厂家硬件版本号
						{
							U8 i;
							*datalong = 32;
							for(i=0;i<32;i++){
								*(buf+i) = manufacturer_hardware_ver[i];
							}
							return 1;
						}
//						break;
						case 3://厂家编号
						{
							U8 i;
							*datalong = 32;
							for(i=0;i<32;i++){
								*(buf+i) = manufacturer_serial_number[i];
							}
							return 1;
						}
//						break;
						default:
						return 2;
					}
				}
//				break;
				default:
				return 2;
			}
		}
//		break;
		default:
		return 2;
	}
	
	return 0;
}
/*********************************************************************************************************
** 函数名称:     com_read_variable_data
** 功能描述:     读取变量数据
** 输　入:       id2
**				 id1
**				 id0
**				 databuf
** 输　出:       0 无有效数据
**				 1 数据
**				 2 参数错误
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U16 com_read_variable_data(U8 id2,U8 id1,U8 id0,U8 *buf,U8 *datalong)
{
	switch(id2)
	{
		case 1:	  //电压
		{
			
			if(id0 == 0){
				switch(id1)
				{
					case 1:
					{
						*buf = DecToBCD(a_phase_info.voltage/100%100);
						*(buf+1) = DecToBCD(a_phase_info.voltage/100/100%100);
						*datalong = 2;	
					}
					return 1;
					case 2:
					{
						*buf = DecToBCD(b_phase_info.voltage/100%100);
						*(buf+1) = DecToBCD(b_phase_info.voltage/100/100%100);
						*datalong = 2;
					}
					return 1;
					case 3:
					{
						*buf = DecToBCD(c_phase_info.voltage/100%100);
						*(buf+1) = DecToBCD(c_phase_info.voltage/100/100%100);
						*datalong = 2;
					}
					return 1;
					case 0x0ff:
					{
						*buf = DecToBCD(a_phase_info.voltage/100%100);
						*(buf+1) = DecToBCD(a_phase_info.voltage/100/100%100);
						*(buf+2) = DecToBCD(b_phase_info.voltage/100%100);
						*(buf+3) = DecToBCD(b_phase_info.voltage/100/100%100);
						*(buf+2) = DecToBCD(c_phase_info.voltage/100%100);
						*(buf+3) = DecToBCD(c_phase_info.voltage/100/100%100);
						*datalong = 6;
					}
					return 1;
					default:
					return 2;
				}
			}
		}
		break;
		case 2:
		{
			if(id0 == 0){
				switch(id1)
				{
					case 1:
					{
						*buf = DecToBCD((a_phase_info.current&0x7fffffff)%100);
						*(buf+1) = DecToBCD((a_phase_info.current&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((a_phase_info.current&0x7fffffff)/10000%100);
						if(a_phase_info.current&0x80000000){
							*(buf+2) |= 0x80;
						}
						*datalong = 3;	
					}
					return 1;
					case 2:
					{
						*buf = DecToBCD((b_phase_info.current&0x7fffffff)%100);
						*(buf+1) = DecToBCD((b_phase_info.current&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((b_phase_info.current&0x7fffffff)/10000%100);
						if(b_phase_info.current&0x80000000){
							*(buf+2) |= 0x80;
						}
						*datalong = 3;
					}
					return 1;
					case 3:
					{
						*buf = DecToBCD((c_phase_info.current&0x7fffffff)%100);
						*(buf+1) = DecToBCD((c_phase_info.current&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((c_phase_info.current&0x7fffffff)/10000%100);
						if(c_phase_info.current&0x80000000){
							*(buf+2) |= 0x80;
						}
						*datalong = 3;
					}
					return 1;
					case 0x0ff:
					{
						*buf = DecToBCD((a_phase_info.current&0x7fffffff)%100);
						*(buf+1) = DecToBCD((a_phase_info.current&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((a_phase_info.current&0x7fffffff)/10000%100);
						if(a_phase_info.current&0x80000000){
							*(buf+2) |= 0x80;
						}
						*(buf+3) = DecToBCD((b_phase_info.current&0x7fffffff)%100);
						*(buf+4) = DecToBCD((b_phase_info.current&0x7fffffff)/100%100);
						*(buf+5) = DecToBCD((b_phase_info.current&0x7fffffff)/10000%100);
						if(b_phase_info.current&0x80000000){
							*(buf+5) |= 0x80;
						}
						*(buf+6) = DecToBCD((c_phase_info.current&0x7fffffff)%100);
						*(buf+7) = DecToBCD((c_phase_info.current&0x7fffffff)/100%100);
						*(buf+8) = DecToBCD((c_phase_info.current&0x7fffffff)/10000%100);
						if(c_phase_info.current&0x80000000){
							*(buf+8) |= 0x80;
						}
						*datalong = 9;
					}
					return 1;
					default:
					return 2;
				}
			}
		}
		break;
		case 3://有功
		{
			if(id0 == 0){
				switch(id1)
				{
					case 0:
					{
						*buf = DecToBCD((sum_info.active_power&0x7fffffff)%100);
						*(buf+1) = DecToBCD((sum_info.active_power&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((sum_info.active_power&0x7fffffff)/10000%100);
						if(sum_info.active_power&0x80000000){
							*(buf+2) |= 0x80;
						}
						*datalong = 3;
					}
					return 1;
					case 1:
					{
						*buf = DecToBCD((a_phase_info.active_power&0x7fffffff)%100);
						*(buf+1) = DecToBCD((a_phase_info.active_power&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((a_phase_info.active_power&0x7fffffff)/10000%100);
						if(a_phase_info.active_power&0x80000000){
							*(buf+2) |= 0x80;
						}
						*datalong = 3;
					}
					return 1;
					case 2:
					{
						*buf = DecToBCD((b_phase_info.active_power&0x7fffffff)%100);
						*(buf+1) = DecToBCD((b_phase_info.active_power&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((b_phase_info.active_power&0x7fffffff)/10000%100);
						if(b_phase_info.active_power&0x80000000){
							*(buf+2) |= 0x80;
						}
						*datalong = 3;
					}
					return 1;
					case 3:
					{
						*buf = DecToBCD((c_phase_info.active_power&0x7fffffff)%100);
						*(buf+1) = DecToBCD((c_phase_info.active_power&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((c_phase_info.active_power&0x7fffffff)/10000%100);
						if(c_phase_info.active_power&0x80000000){
							*(buf+2) |= 0x80;
						}
						*datalong = 3;
					}
					return 1;
					case 0x0ff:
					{
						*buf = DecToBCD((sum_info.active_power&0x7fffffff)%100);
						*(buf+1) = DecToBCD((sum_info.active_power&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((sum_info.active_power&0x7fffffff)/10000%100);
						if(sum_info.active_power&0x80000000){
							*(buf+2) |= 0x80;
						}
						*(buf+3) = DecToBCD((a_phase_info.active_power&0x7fffffff)%100);
						*(buf+4) = DecToBCD((a_phase_info.active_power&0x7fffffff)/100%100);
						*(buf+5) = DecToBCD((a_phase_info.active_power&0x7fffffff)/10000%100);
						if(a_phase_info.active_power&0x80000000){
							*(buf+5) |= 0x80;
						}
						*(buf+6) = DecToBCD((b_phase_info.active_power&0x7fffffff)%100);
						*(buf+7) = DecToBCD((b_phase_info.active_power&0x7fffffff)/100%100);
						*(buf+8) = DecToBCD((b_phase_info.active_power&0x7fffffff)/10000%100);
						if(b_phase_info.active_power&0x80000000){
							*(buf+8) |= 0x80;
						}
						*(buf+9) = DecToBCD((c_phase_info.active_power&0x7fffffff)%100);
						*(buf+10) = DecToBCD((c_phase_info.active_power&0x7fffffff)/100%100);
						*(buf+11) = DecToBCD((c_phase_info.active_power&0x7fffffff)/10000%100);
						if(c_phase_info.active_power&0x80000000){
							*(buf+11) |= 0x80;
						}
						*datalong = 12;
					}
					return 1;
					default:
					return 2;
				}
			}
		}
		break;
		case 4://无功
		{
			if(id0 == 0){
				switch(id1)
				{
					case 0:
					{
						*buf = DecToBCD((sum_info.reactive_power&0x7fffffff)%100);
						*(buf+1) = DecToBCD((sum_info.reactive_power&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((sum_info.reactive_power&0x7fffffff)/10000%100);
						if(sum_info.reactive_power&0x80000000){
							*(buf+2) |= 0x80;
						}
						*datalong = 3;
					}
					return 1;
					case 1:
					{
						*buf = DecToBCD((a_phase_info.reactive_power&0x7fffffff)%100);
						*(buf+1) = DecToBCD((a_phase_info.reactive_power&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((a_phase_info.reactive_power&0x7fffffff)/10000%100);
						if(a_phase_info.reactive_power&0x80000000){
							*(buf+2) |= 0x80;
						}
						*datalong = 3;
					}
					return 1;
					case 2:
					{
						*buf = DecToBCD((b_phase_info.reactive_power&0x7fffffff)%100);
						*(buf+1) = DecToBCD((b_phase_info.reactive_power&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((b_phase_info.reactive_power&0x7fffffff)/10000%100);
						if(b_phase_info.reactive_power&0x80000000){
							*(buf+2) |= 0x80;
						}
						*datalong = 3;
					}
					return 1;
					case 3:
					{
						*buf = DecToBCD((c_phase_info.reactive_power&0x7fffffff)%100);
						*(buf+1) = DecToBCD((c_phase_info.reactive_power&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((c_phase_info.reactive_power&0x7fffffff)/10000%100);
						if(c_phase_info.reactive_power&0x80000000){
							*(buf+2) |= 0x80;
						}
						*datalong = 3;
					}
					return 1;
					case 0x0ff:
					{
						*buf = DecToBCD((sum_info.reactive_power&0x7fffffff)%100);
						*(buf+1) = DecToBCD((sum_info.reactive_power&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((sum_info.reactive_power&0x7fffffff)/10000%100);
						if(sum_info.reactive_power&0x80000000){
							*(buf+2) |= 0x80;
						}
						*(buf+3) = DecToBCD((a_phase_info.reactive_power&0x7fffffff)%100);
						*(buf+4) = DecToBCD((a_phase_info.reactive_power&0x7fffffff)/100%100);
						*(buf+5) = DecToBCD((a_phase_info.reactive_power&0x7fffffff)/10000%100);
						if(a_phase_info.reactive_power&0x80000000){
							*(buf+5) |= 0x80;
						}
						*(buf+6) = DecToBCD((b_phase_info.reactive_power&0x7fffffff)%100);
						*(buf+7) = DecToBCD((b_phase_info.reactive_power&0x7fffffff)/100%100);
						*(buf+8) = DecToBCD((b_phase_info.reactive_power&0x7fffffff)/10000%100);
						if(b_phase_info.reactive_power&0x80000000){
							*(buf+8) |= 0x80;
						}
						*(buf+9) = DecToBCD((c_phase_info.reactive_power&0x7fffffff)%100);
						*(buf+10) = DecToBCD((c_phase_info.reactive_power&0x7fffffff)/100%100);
						*(buf+11) = DecToBCD((c_phase_info.reactive_power&0x7fffffff)/10000%100);
						if(c_phase_info.reactive_power&0x80000000){
							*(buf+11) |= 0x80;
						}
						*datalong = 12;
					}
					return 1;
					default:
					return 2;
				}
			}
		}
		break;
		case 5:	 //视在
		{
			if(id0 == 0){
				switch(id1)
				{
					case 0:
					{
						*buf = DecToBCD((sum_info.apparent_power&0x7fffffff)%100);
						*(buf+1) = DecToBCD((sum_info.apparent_power&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((sum_info.apparent_power&0x7fffffff)/10000%100);
						if(sum_info.apparent_power&0x80000000){
							*(buf+2) |= 0x80;
						}
						*datalong = 3;
					}
					return 1;
					case 1:
					{
						*buf = DecToBCD((a_phase_info.apparent_power&0x7fffffff)%100);
						*(buf+1) = DecToBCD((a_phase_info.apparent_power&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((a_phase_info.apparent_power&0x7fffffff)/10000%100);
						if(a_phase_info.apparent_power&0x80000000){
							*(buf+2) |= 0x80;
						}
						*datalong = 3;
					}
					return 1;
					case 2:
					{
						*buf = DecToBCD((b_phase_info.apparent_power&0x7fffffff)%100);
						*(buf+1) = DecToBCD((b_phase_info.apparent_power&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((b_phase_info.apparent_power&0x7fffffff)/10000%100);
						if(b_phase_info.apparent_power&0x80000000){
							*(buf+2) |= 0x80;
						}
						*datalong = 3;
					}
					return 1;
					case 3:
					{
						*buf = DecToBCD((c_phase_info.apparent_power&0x7fffffff)%100);
						*(buf+1) = DecToBCD((c_phase_info.apparent_power&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((c_phase_info.apparent_power&0x7fffffff)/10000%100);
						if(c_phase_info.apparent_power&0x80000000){
							*(buf+2) |= 0x80;
						}
						*datalong = 3;
					}
					return 1;
					case 0x0ff:
					{
						*buf = DecToBCD((sum_info.apparent_power&0x7fffffff)%100);
						*(buf+1) = DecToBCD((sum_info.apparent_power&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((sum_info.apparent_power&0x7fffffff)/10000%100);
						if(sum_info.apparent_power&0x80000000){
							*(buf+2) |= 0x80;
						}
						*(buf+3) = DecToBCD((a_phase_info.apparent_power&0x7fffffff)%100);
						*(buf+4) = DecToBCD((a_phase_info.apparent_power&0x7fffffff)/100%100);
						*(buf+5) = DecToBCD((a_phase_info.apparent_power&0x7fffffff)/10000%100);
						if(a_phase_info.apparent_power&0x80000000){
							*(buf+5) |= 0x80;
						}
						*(buf+6) = DecToBCD((b_phase_info.apparent_power&0x7fffffff)%100);
						*(buf+7) = DecToBCD((b_phase_info.apparent_power&0x7fffffff)/100%100);
						*(buf+8) = DecToBCD((b_phase_info.apparent_power&0x7fffffff)/10000%100);
						if(b_phase_info.apparent_power&0x80000000){
							*(buf+8) |= 0x80;
						}
						*(buf+9) = DecToBCD((c_phase_info.apparent_power&0x7fffffff)%100);
						*(buf+10) = DecToBCD((c_phase_info.apparent_power&0x7fffffff)/100%100);
						*(buf+11) = DecToBCD((c_phase_info.apparent_power&0x7fffffff)/10000%100);
						if(c_phase_info.apparent_power&0x80000000){
							*(buf+11) |= 0x80;
						}
						*datalong = 12;
					}
					return 1;
					default:
					return 2;
				}
			}
		}
		break;
		case 6://功率因数
		{
			if(id0 == 0){
				switch(id1)
				{
					case 0:
					{
						*buf = DecToBCD((sum_info.power_factor&0x7fffffff)%100);
						*(buf+1) = DecToBCD((sum_info.power_factor&0x7fffffff)/100%100);
						if(sum_info.power_factor&0x80000000){
							*(buf+1) |= 0x80;
						}
						*datalong = 2;
					}
					return 1;
					case 1:
					{
						*buf = DecToBCD((a_phase_info.power_factor&0x7fffffff)%100);
						*(buf+1) = DecToBCD((a_phase_info.power_factor&0x7fffffff)/100%100);
						if(a_phase_info.power_factor&0x80000000){
							*(buf+1) |= 0x80;
						}
						*datalong = 2;
					}
					return 1;
					case 2:
					{
						*buf = DecToBCD((b_phase_info.power_factor&0x7fffffff)%100);
						*(buf+1) = DecToBCD((b_phase_info.power_factor&0x7fffffff)/100%100);
						if(b_phase_info.power_factor&0x80000000){
							*(buf+1) |= 0x80;
						}
						*datalong = 2;
					}
					return 1;
					case 3:
					{
						*buf = DecToBCD((c_phase_info.power_factor&0x7fffffff)%100);
						*(buf+1) = DecToBCD((c_phase_info.power_factor&0x7fffffff)/100%100);
						if(c_phase_info.power_factor&0x80000000){
							*(buf+1) |= 0x80;
						}
						*datalong = 2;
					}
					return 1;
					case 0x0ff:
					{
						*buf = DecToBCD((sum_info.power_factor&0x7fffffff)%100);
						*(buf+1) = DecToBCD((sum_info.power_factor&0x7fffffff)/100%100);
						if(sum_info.power_factor&0x80000000){
							*(buf+1) |= 0x80;
						}
						*(buf+2) = DecToBCD((a_phase_info.power_factor&0x7fffffff)%100);
						*(buf+3) = DecToBCD((a_phase_info.power_factor&0x7fffffff)/100%100);
						if(a_phase_info.power_factor&0x80000000){
							*(buf+3) |= 0x80;
						}
						*(buf+4) = DecToBCD((b_phase_info.power_factor&0x7fffffff)%100);
						*(buf+5) = DecToBCD((b_phase_info.power_factor&0x7fffffff)/100%100);
						if(b_phase_info.power_factor&0x80000000){
							*(buf+5) |= 0x80;
						}
						*(buf+6) = DecToBCD((c_phase_info.power_factor&0x7fffffff)%100);
						*(buf+7) = DecToBCD((c_phase_info.power_factor&0x7fffffff)/100%100);
						if(c_phase_info.power_factor&0x80000000){
							*(buf+7) |= 0x80;
						}
						*datalong = 8;
					}
					return 1;
					default:
					return 2;
				}
			}
		}
		break;
		case 7:	  //相角
		{
			if(id0 == 0){
				switch(id1)
				{
					case 1:
					{
						*buf = DecToBCD(a_phase_info.angle%100);
						*(buf+1) = DecToBCD(a_phase_info.angle/100%100);
						*datalong = 2;	
					}
					return 1;
					case 2:
					{
						*buf = DecToBCD(b_phase_info.angle%100);
						*(buf+1) = DecToBCD(b_phase_info.angle/100%100);
						*datalong = 2;
					}
					return 1;
					case 3:
					{
						*buf = DecToBCD(c_phase_info.angle%100);
						*(buf+1) = DecToBCD(c_phase_info.angle/100%100);
						*datalong = 2;
					}
					return 1;
					case 0x0ff:
					{
						*buf = DecToBCD(a_phase_info.angle%100);
						*(buf+1) = DecToBCD(a_phase_info.angle/100%100);
						
						*(buf+2) = DecToBCD(b_phase_info.angle%100);
						*(buf+3) = DecToBCD(b_phase_info.angle/100%100);

						
						*(buf+4) = DecToBCD(c_phase_info.angle%100);
						*(buf+5) = DecToBCD(c_phase_info.angle/100%100);
					
						*datalong = 6;
					}
					return 1;
					default:
					return 2;
				}
			}
		}
		break;
		case 8:
		case 9:
		{

		}
		break;
		case 0x0a:
		case 0x0b:
		{

		}
		break;
		case 0x80:
		{
			if(id1 == 0){
				switch(id0)
				{
					case 1:	  //零线电流
					{
						*buf = 0;
						*(buf+1) = 0;
						*(buf+2) = 0;

						*datalong = 3;
					}
					return 1;
					case 2://电网频率
					{
						*buf = DecToBCD(period_line%100);
						*(buf+1) = DecToBCD(period_line/100%100);
						*datalong = 2;
					}
					return 1;
					case 3://一分钟有功总平均功率
					{

					}
					return 1;
					case 4://当前有功需量
					{
						*buf = DecToBCD((demand_info.active_demand&0x7fffffff)%100);
						*(buf+1) = DecToBCD((demand_info.active_demand&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((demand_info.active_demand&0x7fffffff)/10000%100);
						if(demand_info.active_demand&0x80000000){
					   		*(buf+2) |= 0x80;
						}
						*datalong = 3;
					}
					return 1;
					case 5:	//当前无功需量
					{
						*buf = DecToBCD((demand_info.reactive_demand&0x7fffffff)%100);
						*(buf+1) = DecToBCD((demand_info.reactive_demand&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((demand_info.reactive_demand&0x7fffffff)/10000%100);
						if(demand_info.reactive_demand&0x80000000){
					   		*(buf+2) |= 0x80;
						}
						*datalong = 3;
					}
					return 1;
					case 6://当前视在需量
					{
						*buf = DecToBCD((demand_info.apparent_demand&0x7fffffff)%100);
						*(buf+1) = DecToBCD((demand_info.apparent_demand&0x7fffffff)/100%100);
						*(buf+2) = DecToBCD((demand_info.apparent_demand&0x7fffffff)/10000%100);
						if(demand_info.apparent_demand&0x80000000){
					   		*(buf+2) |= 0x80;
						}
						*datalong = 3;
					}
					return 1;
					case 7://表内温度
					{
						*buf = 0;
						*(buf+1) = 0;

						*datalong = 2;
					}
					return 1;
					case 8:	//时钟电池电压
					{
						*buf = DecToBCD(voltageClockBat%100);
						*(buf+1) = DecToBCD(voltageClockBat/100%100);

						*datalong = 2;
					}
					return 1;
					case 9:	//停抄电池电压
					{
						*buf = DecToBCD(voltageStopBat%100);
						*(buf+1) = DecToBCD(voltageStopBat/100%100);

						*datalong = 2;
					}
					return 1;
					case 10: //内部电池工作时间
					{
						
						if(read_data(3,EVENT_STATE_ADDR+0x80,4,buf)){
							*datalong = 4;
						}else{
							return 0;
						}
							
					}
					return 1;
					default:
					return 2;
				}
			}
		}
		break;
		default:
		return 2;
	}	
	return 0;
}
/*********************************************************************************************************
** 函数名称:     check_freeze_data_mode
** 功能描述:     读取冻结数据
** 输　入:       id 数据类型
**				 mode 冻结模式字
**				 
** 输　出:       0 不要求应答数据
**				 1 应答数据
**				 2 参数错误
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 check_freeze_data_mode(U8 id,U8 mode)
{
	U8 result = 0;
		
	switch(id)
	{
		case 0:
			result = 1;
		break;
		case 1:
		{
			if(mode&1){
				result = 1;	
			}
		}
		break;
		case 2:
		{
			if(mode&2){
				result = 1;	
			}
		}
		break;
		case 3:
		{
			if(mode&4){
				result = 1;	
			}
		}
		break;
		case 4:
		{
			if(mode&8){
				result = 1;	
			}
		}
		break;
		case 5:
		case 6:
		case 7:
		case 8:
		{
			if(mode&0x10){
				result = 1;	
			}
		}
		break;
		case 9:
		{
			if(mode&0x20){
				result = 1;	
			}
		}
		break;
		case 10:
		{
			if(mode&0x40){
				result = 1;	
			}
		}
		break;
		case 0x10:
		{
			if(mode&0x80){
				result = 1;	
			}
		}
		break;
		default:
		{
			result = 2;
		}
		break;
	}

	return result;
}
/*********************************************************************************************************
** 函数名称:     com_read_load_data
** 功能描述:     读取冻结数据
** 输　入:       id2
**				 id1
**				 id0
**				 databuf
** 输　出:       0 无有效数据
**				 1 有有效数据
**				 2 参数错误
**				 3 数据未读完
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 com_read_load_data(U8 type,U8 *buf1,U8 id2,U8 id1,U8 id0,U8 *buf,U8 *datalong)
{
	U8 result = 0,type1 = 0,i;
	if(type == 1){
		if(id0 == 2){
			type1 = 2;
		}else{
			type1 = 0;
		}
		if((id2 <= 6)&&(id1 == 0)&&(id0 <= 2)){
			*datalong = read_load_data(buf,type1,id2);
			if((*datalong) == 0){
				result = 0;
			}else{
				result = 1;
			}
		}else{
			result = 2;
		}	
	}else if(type == 2){
		type1 = 1;
		for(i=0;i<5;i++){
			*(buf+i) = *(buf1+i);
		}
		if((id2 <= 6)&&(id1 == 0)&&(id0 <= 2)){
			*datalong = read_load_data(buf,type1,id2);
			if((*datalong) == 0){
				result = 0;
			}else{
				result = 1;
			}
		}else{
			result = 2;
		}
	}else{
		result = 2;
	}

	return result;
}
/*********************************************************************************************************
** 函数名称:     com_read_freeze_data
** 功能描述:     读取冻结数据
** 输　入:       id2
**				 id1
**				 id0
**				 databuf
** 输　出:       0 无有效数据
**				 1 有有效数据
**				 2 参数错误
**				 3 数据未读完
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 com_read_freeze_data(U8 id2,U8 id1,U8 id0,U8 *buf,U8 *datalong)
{
	U8 ID = 0;	
	U8 result = 0;
	U8 len = 0;

	switch(id2)
	{
		case 0x00:	//定时冻结分相信息
		{
			if(((id1<=10)||(id1==0x10))&&(id0>=1)&&(id0<=12)){
				if(change_cyc_save_position(TIMING_FREEZE,id0,&ID,0)){
					if(ID == 1){
						ID = MAX_TIMING_FREEZE_NUM;
					}else{
						ID--;
					}
					switch(id1)
					{
						case 0:
						case 0x10:
						{
					   		switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[0]))
							{
								case 1:
								{			   
									*datalong = getEepromData(5,id2,id1,ID,buf);
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						case 0x01:
						case 0x02:
						case 0x03:
						case 0x04:
						case 0x05:
						case 0x06:
						case 0x07:
						case 0x08:
						{
							switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[0]))
							{
								case 1:
								{			   
									len = getEepromData(5,id2,id1,ID,buf);
									if(len>=(4*(time_zone_con.max_fee_num+1))){
										*datalong = 4*(time_zone_con.max_fee_num+1);
									}else{
										*datalong = 0;
									}
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						case 0x09:
						case 0x0a:
						{
							switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[0]))
							{
								case 1:
								{			   
									len = getEepromData(5,id2,id1,ID,buf);
									if(len>=(8*(time_zone_con.max_fee_num+1))){
										*datalong = 8*(time_zone_con.max_fee_num+1);
									}else{
										*datalong = 0;
									}
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						default:
						{
							*datalong = 0;
							result = 2;
						}
						break;
					}			
				}
			}else if((id1==0x0ff)&&(id0>=1)&&(id0<=12)){
			
			}	
		}
		break;
		case 0x01:	//瞬时冻结分相信息
		{
			if(((id1<=10)||(id1==0x10))&&(id0>=1)&&(id0<=3)){
				if(change_cyc_save_position(INSTANTANEOUS_FREEZE,id0,&ID,0)){
					if(ID == 1){
						ID = MAX_INSTANTANEOUS_FREEZE_NUM;
					}else{
						ID--;
					}
					switch(id1)
					{
						case 0:
						case 0x10:
						{
					   		switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[1]))
							{
								case 1:
								{			   
									*datalong = getEepromData(5,id2,id1,ID,buf);
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						case 0x01:
						case 0x02:
						case 0x03:
						case 0x04:
						case 0x05:
						case 0x06:
						case 0x07:
						case 0x08:
						{
							switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[1]))
							{
								case 1:
								{			   
									len = getEepromData(5,id2,id1,ID,buf);
									if(len>=(4*(time_zone_con.max_fee_num+1))){
										*datalong = 4*(time_zone_con.max_fee_num+1);
									}else{
										*datalong = 0;
									}
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						case 0x09:
						case 0x0a:
						{
							switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[1]))
							{
								case 1:
								{			   
									len = getEepromData(5,id2,id1,ID,buf);
									if(len>=(8*(time_zone_con.max_fee_num+1))){
										*datalong = 8*(time_zone_con.max_fee_num+1);
									}else{
										*datalong = 0;
									}
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						default:
						{
							*datalong = 0;
							result = 2;
						}
						break;
					}		
				}
			}else if((id1==0x0ff)&&(id0>=1)&&(id0<=3)){
			
			}				
		}
		break;
		case 0x02:	//两个时区切换冻结
		{
			if(((id1<=10)||(id1==0x10))&&(id0>=1)&&(id0<=2)){
				if(change_cyc_save_position(ZONES_CHANGE_ENERGY,id0,&ID,0)){
					if(ID == 1){
						ID = MAX_ZONES_CHANGE_ENERGY_NUM;
					}else{
						ID--;
					}
					switch(id1)
					{
						case 0:
						case 0x10:
						{
					   		switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[2]))
							{
								case 1:
								{			   
									*datalong = getEepromData(5,id2,id1,ID,buf);
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						case 0x01:
						case 0x02:
						case 0x03:
						case 0x04:
						case 0x05:
						case 0x06:
						case 0x07:
						case 0x08:
						{
							switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[2]))
							{
								case 1:
								{			   
									len = getEepromData(5,id2,id1,ID,buf);
									if(len>=(4*(time_zone_con.max_fee_num+1))){
										*datalong = 4*(time_zone_con.max_fee_num+1);
									}else{
										*datalong = 0;
									}
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						case 0x09:
						case 0x0a:
						{
							switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[2]))
							{
								case 1:
								{			   
									len = getEepromData(5,id2,id1,ID,buf);
									if(len>=(8*(time_zone_con.max_fee_num+1))){
										*datalong = 8*(time_zone_con.max_fee_num+1);
									}else{
										*datalong = 0;
									}
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						default:
						{
							*datalong = 0;
							result = 2;
						}
						break;
					}			   		
				}
			}else if((id1==0x0ff)&&(id0>=1)&&(id0<=2)){
			
			}				
		}
		break;
		case 0x03:	//两个时段切换冻结
		{
			if(((id1<=10)||(id1==0x10))&&(id0>=1)&&(id0<=2)){
				if(change_cyc_save_position(PERIODS_CHANGE_ENERGY,id0,&ID,0)){
					if(ID == 1){
						ID = MAX_PERIODS_CHANGE_ENERGY_NUM;
					}else{
						ID--;
					}
					switch(id1)
					{
						case 0:
						case 0x10:
						{
					   		switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[2]))
							{
								case 1:
								{			   
									*datalong = getEepromData(5,id2,id1,ID,buf);
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						case 0x01:
						case 0x02:
						case 0x03:
						case 0x04:
						case 0x05:
						case 0x06:
						case 0x07:
						case 0x08:
						{
							switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[2]))
							{
								case 1:
								{			   
									len = getEepromData(5,id2,id1,ID,buf);
									if(len>=(4*(time_zone_con.max_fee_num+1))){
										*datalong = 4*(time_zone_con.max_fee_num+1);
									}else{
										*datalong = 0;
									}
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						case 0x09:
						case 0x0a:
						{
							switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[2]))
							{
								case 1:
								{			   
									len = getEepromData(5,id2,id1,ID,buf);
									if(len>=(8*(time_zone_con.max_fee_num+1))){
										*datalong = 8*(time_zone_con.max_fee_num+1);
									}else{
										*datalong = 0;
									}
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						default:
						{
							*datalong = 0;
							result = 2;
						}
						break;
					}			   	
				}
			}else if((id1==0x0ff)&&(id0>=1)&&(id0<=2)){
			
			}								
		}
		break;
		case 0x05:	//两套费率
		{
			if(((id1<=10)||(id1==0x10))&&(id0>=1)&&(id0<=2)){
				if(change_cyc_save_position(FEES_FREEZE,id0,&ID,0)){
					if(ID == 1){
						ID = MAX_PERIODS_CHANGE_ENERGY_NUM;
					}else{
						ID--;
					}
					switch(id1)
					{
						case 0:
						case 0x10:
						{
					   		switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[2]))
							{
								case 1:
								{			   
									*datalong = getEepromData(5,id2,id1,ID,buf);
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						case 0x01:
						case 0x02:
						case 0x03:
						case 0x04:
						case 0x05:
						case 0x06:
						case 0x07:
						case 0x08:
						{
							switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[2]))
							{
								case 1:
								{			   
									len = getEepromData(5,id2,id1,ID,buf);
									if(len>=(4*(time_zone_con.max_fee_num+1))){
										*datalong = 4*(time_zone_con.max_fee_num+1);
									}else{
										*datalong = 0;
									}
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						case 0x09:
						case 0x0a:
						{
							switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[2]))
							{
								case 1:
								{			   
									len = getEepromData(5,id2,id1,ID,buf);
									if(len>=(8*(time_zone_con.max_fee_num+1))){
										*datalong = 8*(time_zone_con.max_fee_num+1);
									}else{
										*datalong = 0;
									}
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						default:
						{
							*datalong = 0;
							result = 2;
						}
						break;
					}
				}
			}else if((id1==0x0ff)&&(id0>=1)&&(id0<=2)){
			
			}											
		}
		break;
		case 0x06:	//日冻结
		{
			if(((id1<=10)||(id1==0x10))&&(id0>=1)&&(id0<=62)){
				if(change_cyc_save_position(DAY_FREEZE,id0,&ID,0)){
					if(ID == 1){
						ID = MAX_DAY_FREEZE_NUM;
					}else{
						ID--;
					}
					switch(id1)
					{
						case 0:
						case 0x10:
						{
					   		switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[4]))
							{
								case 1:
								{			   
									*datalong = getEepromData(5,id2,id1,ID,buf);
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						case 0x01:
						case 0x02:
						case 0x03:
						case 0x04:
						case 0x05:
						case 0x06:
						case 0x07:
						case 0x08:
						{
							switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[4]))
							{
								case 1:
								{			   
									len = getEepromData(5,id2,id1,ID,buf);
									if(len>=(4*(time_zone_con.max_fee_num+1))){
										*datalong = 4*(time_zone_con.max_fee_num+1);
									}else{
										*datalong = 0;
									}
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						case 0x09:
						case 0x0a:
						{
							switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[4]))
							{
								case 1:
								{			   
									len = getEepromData(5,id2,id1,ID,buf);
									if(len>=(8*(time_zone_con.max_fee_num+1))){
										*datalong = 8*(time_zone_con.max_fee_num+1);
									}else{
										*datalong = 0;
									}
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						default:
						{
							*datalong = 0;
							result = 2;
						}
						break;
					}		
				}
			}else if((id1==0x0ff)&&(id0>=1)&&(id0<=2)){
			
			}
		}
		break;
		case 0x07:	//阶梯电价冻结
		{
			if(((id1<=10)||(id1==0x10))&&(id0>=1)&&(id0<=2)){
				if(change_cyc_save_position(ELECTROVALENCE_FREEZE,id0,&ID,0)){
					if(ID == 1){
						ID = MAX_PERIODS_CHANGE_ENERGY_NUM;
					}else{
						ID--;
					}
					switch(id1)
					{
						case 0:
						case 0x10:
						{
					   		switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[2]))
							{
								case 1:
								{			   
									*datalong = getEepromData(5,id2,id1,ID,buf);
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						case 0x01:
						case 0x02:
						case 0x03:
						case 0x04:
						case 0x05:
						case 0x06:
						case 0x07:
						case 0x08:
						{
							switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[2]))
							{
								case 1:
								{			   
									len = getEepromData(5,id2,id1,ID,buf);
									if(len>=(4*(time_zone_con.max_fee_num+1))){
										*datalong = 4*(time_zone_con.max_fee_num+1);
									}else{
										*datalong = 0;
									}
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						case 0x09:
						case 0x0a:
						{
							switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[2]))
							{
								case 1:
								{			   
									len = getEepromData(5,id2,id1,ID,buf);
									if(len>=(8*(time_zone_con.max_fee_num+1))){
										*datalong = 8*(time_zone_con.max_fee_num+1);
									}else{
										*datalong = 0;
									}
									if((*datalong) != 0){
										result = 1;
									}else{
										result = 0;
									}
								}
								break;
								case 0:
								{
									*datalong = 0;
									result = 1;
								}
								break;
								default:
								{
									*datalong = 0;
									result = 2;
								}
								break;
							}
						}
						break;
						default:
						{
							*datalong = 0;
							result = 2;
						}
						break;
					}	
				}
			}else if((id1==0x0ff)&&(id0>=1)&&(id0<=2)){
			
			}								
		}
		break;
		case 0x04:	//整点冻结相信息
		{
			if((id1<=2)&&(id0>=1)&&(id0<=254)){
				if(change_cyc_save_position(WHOLE_HOUR_FREEZE,id0,&ID,0)){
					if(ID == 1){
						ID = MAX_WHOLE_HOUR_FREEZE_NUM;
					}else{
						ID--;
					}			   
					switch(check_freeze_data_mode(id1,freeze_config.freeze_mode[3]))
					{
						case 1:
						{			   
							*datalong = getEepromData(5,id2,id1,ID,buf);
							if((*datalong) != 0){
								result = 1;
							}else{
								result = 0;
							}
						}
						break;
						case 0:
						{
							*datalong = 0;
							result = 1;
						}
						break;
						default:
						{
							*datalong = 0;
							result = 2;
						}
						break;
					}			
				}
			}else if((id1==0x0ff)&&(id0>=1)&&(id0<=254)){
			
			}				
		}
		break;
		default:
		break;
	}
	return result;
}
/*********************************************************************************************************
** 函数名称:     comm_read_after_frame_data
** 功能描述:     读后续数据帧数据		   在0x11指令最大读取196个字节，在0x12指令每次最大读取195
** 输　入:       data_ID  数据ID标识
**				 seq	  帧序号
**				 databuf  数据缓冲
**				 datalong 数据长度
** 输　出:       0 无有效数据
**				 1 数据读完
**				 2 参数错误
**				 3 数据未完
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 		 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 comm_read_after_frame_data(LPC_Uart_Channel_t DevNum,U8 id0,U8 id1,U8 id2,U8 id3,U8 seq,U8 *databuf,U8 *datalong)
{	
	U8 len = 0,i;
	switch(DevNum)
	{
		case UART0:
		{
			if((id0 == uart0_645frame.last_frame_ID[0])&&(id1 == uart0_645frame.last_frame_ID[1])&&(id2 == uart0_645frame.last_frame_ID[2])&&(id3 == uart0_645frame.last_frame_ID[3])&&(seq == (uart0_645frame.last_frame_seq+1))){
				if(uart0_645frame.last_frame_datalong>(seq*195+195)){
					if(read_data(uart0_645frame.chip,uart0_645frame.addr+seq*195,195,databuf)){
						uart0_645frame.last_frame_seq++;
						return 3;	
					}
				}else{
					len = uart0_645frame.last_frame_datalong-seq*195;
					if(read_data(uart0_645frame.chip,uart0_645frame.addr+seq*195,len,databuf)){
						for(i=0;i<4;i++){
							uart0_645frame.last_frame_ID[i] = 0;
						}
						uart0_645frame.last_frame_seq = 0;
						uart0_645frame.addr = 0;
						uart0_645frame.chip = 0;
						uart0_645frame.last_frame_datalong = 0;
						uart0_645frame.last_frame_seq = 0;
						*datalong = len;
						return 1;	
					}
				}			
			}
		}
		break;
		case UART1:
		{
			if((id0 == uart1_645frame.last_frame_ID[0])&&(id1 == uart1_645frame.last_frame_ID[1])&&(id2 == uart1_645frame.last_frame_ID[2])&&(id3 == uart1_645frame.last_frame_ID[3])&&(seq == (uart1_645frame.last_frame_seq+1))){
				if(uart1_645frame.last_frame_datalong>(seq*195+195)){
					if(read_data(uart1_645frame.chip,uart1_645frame.addr+seq*195,195,databuf)){
						uart1_645frame.last_frame_seq++;
						return 3;	
					}
				}else{
					len = uart1_645frame.last_frame_datalong-seq*195;
					if(read_data(uart1_645frame.chip,uart1_645frame.addr+seq*195,len,databuf)){
						for(i=0;i<4;i++){
							uart1_645frame.last_frame_ID[i] = 0;
						}
						uart1_645frame.last_frame_seq = 0;
						uart1_645frame.addr = 0;
						uart1_645frame.chip = 0;
						uart1_645frame.last_frame_datalong = 0;
						uart1_645frame.last_frame_seq = 0;
						return 1;	
					}
				}			
			}
		}
		break;
		case UART3:
		{
	   		if((id0 == uart3_645frame.last_frame_ID[0])&&(id1 == uart3_645frame.last_frame_ID[1])&&(id2 == uart3_645frame.last_frame_ID[2])&&(id3 == uart3_645frame.last_frame_ID[3])&&(seq == (uart3_645frame.last_frame_seq+1))){
				if(uart3_645frame.last_frame_datalong>(seq*195+195)){
					if(read_data(uart3_645frame.chip,uart3_645frame.addr+seq*195,195,databuf)){
						uart3_645frame.last_frame_seq++;
						return 3;	
					}
				}else{
					len = uart3_645frame.last_frame_datalong-seq*195;
					if(read_data(uart3_645frame.chip,uart3_645frame.addr+seq*195,len,databuf)){
						for(i=0;i<4;i++){
							uart3_645frame.last_frame_ID[i] = 0;
						}
						uart3_645frame.last_frame_seq = 0;
						uart3_645frame.addr = 0;
						uart3_645frame.chip = 0;
						uart3_645frame.last_frame_datalong = 0;
						uart3_645frame.last_frame_seq = 0;
						return 1;	
					}
				}			
			}
		}
		break;
		default:
		return 2;
	}	
	return 0;
}
/*********************************************************************************************************
** 函数名称:     comm_write_parameter_data
** 功能描述:     写参数
** 输　入:       id2  数据标识2
**				 id1  数据标识1
**				 id0  数据标识0
**				 databuf  数据缓冲
**				 datalong 数据长度
** 输　出:       0 失败
**				 1 成功
**				 2 ID无效
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 comm_write_parameter_data(U8 id2,U8 id1,U8 id0,U8 *databuf,U8 datalong)
{
	U8 i;	
	
	switch(id2)
	{
		case 0:
		{
			switch(id1)
			{
				case 1:
				{
					switch(id0)
					{
						case 1://设置日期
						{	
							U8 later_time[6],after_time[6];
								
							if((datalong == (4))&&(adjustDate(databuf))){
								for(i=0;i<6;i++){
									later_time[i] = currentClock[i];
								}
								for(i=0;i<3;i++){
									after_time[i] = currentClock[i];
								}
								for(i=0;i<3;i++){
									after_time[i+3] = BCDToDec(databuf[i+1]);
								}
								creatAdjTimeZero(later_time,after_time);//产生校时事件

								demand_info.total_demand_moment = 0;
								demand_info.moment_demand_moment = 0;

								return 1;
							}
						}
						break;
						case 2://时间
						{
							U8 later_time[6],after_time[6];

							if((datalong == (3))&&(adjustTime(databuf))){
								for(i=0;i<6;i++){
									later_time[i] = currentClock[i];
								}
								for(i=0;i<3;i++){
									after_time[i] = BCDToDec(databuf[i]);
								}
								for(i=0;i<3;i++){
									after_time[i+3] = currentClock[i+3];
								}
								creatAdjTimeZero(later_time,after_time); //产生校时事件

								demand_info.total_demand_moment = 0;
								demand_info.moment_demand_moment = 0;

								return 1;
							}
						}
						break;
						case 3:	 //需量周期
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,0,1,3,databuf))){
								demand_info.demand_interval = BCDToDec(*databuf);
								demand_info.total_demand_moment = 0;
								demand_info.moment_demand_moment = 0;
							   	return 1;
							}
						}
						break;
						case 4:	//滑差时间
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,0,1,4,databuf))){
						   		demand_info.sliding_time = BCDToDec(*databuf);
								demand_info.total_demand_moment = 0;
								demand_info.moment_demand_moment = 0;
								if(demand_info.sliding_time != 0){
									meter_state_info.meter_run_state[0] &= (~0x1);
								}else{
									meter_state_info.meter_run_state[0] |= (1);
								}
								return 1;
							}
						}
						break;
						case 5://脉冲宽度 不能设置
						{
							
						}
						break;
						case 6://两套时区切换时间
						{
							if((check_bcd_data(databuf,5))&&(check_day(BCDToDec(*(databuf+4)),BCDToDec(*(databuf+3)),BCDToDec(*(databuf+2))))&&(BCDToDec(*(databuf+1))<24)&&(BCDToDec(*(databuf+0))<60)&&(datalong == (5))&&(writeItem(4,0,1,6,databuf))){
								for(i = 0;i<5;i++){
									time_zone_con.zones_change_time[i] = BCDToDec(*(databuf+i));
								}
								return 1;
							}
						}
						break;
						case 7://两套时段切换时间
						{
							if((check_bcd_data(databuf,5))&&(check_day(BCDToDec(*(databuf+4)),BCDToDec(*(databuf+3)),BCDToDec(*(databuf+2))))&&(BCDToDec(*(databuf+1))<24)&&(BCDToDec(*(databuf+0))<60)&&(datalong == (5))&&(writeItem(4,0,1,7,databuf))){
								for(i = 0;i<5;i++){
									time_zone_con.periods_change_time[i] = BCDToDec(*(databuf+i));
								}
								return 1;
							}
						}
						break;
						case 8://两套费率
						{
					   		if((check_bcd_data(databuf,5))&&(check_day(BCDToDec(*(databuf+4)),BCDToDec(*(databuf+3)),BCDToDec(*(databuf+2))))&&(BCDToDec(*(databuf+1))<24)&&(BCDToDec(*(databuf+0))<60)&&(datalong == (5))&&(writeItem(4,0,1,8,databuf))){
								for(i = 0;i<5;i++){
//									time_zone_con.periods_change_time[i] = BCDToDec(*(databuf+i));
								}
								return 1;
							}
						}
						break;
						case 9://两套阶梯
						{
							if((check_bcd_data(databuf,5))&&(check_day(BCDToDec(*(databuf+4)),BCDToDec(*(databuf+3)),BCDToDec(*(databuf+2))))&&(BCDToDec(*(databuf+1))<24)&&(BCDToDec(*(databuf+0))<60)&&(datalong == (5))&&(writeItem(4,0,1,0x09,databuf))){
								for(i = 0;i<5;i++){
//									time_zone_con.periods_change_time[i] = BCDToDec(*(databuf+i));
								}
								return 1;
							}
						}
						break;
						case 0x0a://两套费率阶梯
						{
							if((check_bcd_data(databuf,5))&&(check_day(BCDToDec(*(databuf+4)),BCDToDec(*(databuf+3)),BCDToDec(*(databuf+2))))&&(BCDToDec(*(databuf+1))<24)&&(BCDToDec(*(databuf+0))<60)&&(datalong == (5))&&(writeItem(4,0,1,0x0a,databuf))){
								for(i = 0;i<5;i++){
//									time_zone_con.periods_change_time[i] = BCDToDec(*(databuf+i));
								}
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 2:
				{
			   		switch(id0)
					{
						case 1:	  //年时区数
						{
							if((check_bcd_data(databuf,1))&&(BCDToDec(*databuf)<=14)&&(datalong == (1))&&(writeItem(4,0,2,1,databuf))){
								time_zone_con.max_zone_num = BCDToDec(*databuf);
								return 1;
							}
						}
						break;
						case 2:	 //日时段表数
						{
							if((check_bcd_data(databuf,1))&&(BCDToDec(*databuf)<=8)&&(datalong == (1))&&(writeItem(4,0,2,2,databuf))){
								time_zone_con.max_data_table = BCDToDec(*databuf);
								return 1;
							}
						}
						break;
						case 3:	//时段数
						{
							if((check_bcd_data(databuf,1))&&(BCDToDec(*databuf)<=14)&&(datalong == (1))&&(writeItem(4,0,2,3,databuf))){
								time_zone_con.max_period_num = BCDToDec(*databuf);
								return 1;
							}
						}
						break;
						case 4:	//费率数
						{
							if((check_bcd_data(databuf,1))&&(BCDToDec(*databuf)<=63)&&(datalong == (1))&&(writeItem(4,0,2,4,databuf))){
								time_zone_con.max_fee_num = BCDToDec(*databuf);
								return 1;
							}
						}
						break;
						case 5://节假日数
						{
							if((check_bcd_data(databuf,2))&&((BCDToDec(*databuf)+(BCDToDec(*(databuf+1)))*100)<=254)&&(datalong == (2))&&(writeItem(4,0,2,5,databuf))){
								time_zone_con.max_holiday = BCDToDec(*databuf)+(BCDToDec(*(databuf+1)))*100;
								return 1;
							}
						}
						break;
						case 6://谐波分析次数 没做
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,0,2,6,databuf))){
//								time_zone_con.max_holiday = BCDToDec(*databuf)+(BCDToDec(*(databuf+1)))*100;
								return 1;
							}
						}
						break;
						case 7:	//阶梯
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,0,2,7,databuf))){
//								time_zone_con.max_holiday = BCDToDec(*databuf)+(BCDToDec(*(databuf+1)))*100;
								return 1;
							}	
						}
						break;
						case 8:
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,0,2,8,databuf))){
//								time_zone_con.max_holiday = BCDToDec(*databuf)+(BCDToDec(*(databuf+1)))*100;
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 3:
				{
			   		switch(id0)
					{
						case 1:	 //循显屏数
						{
							if((check_bcd_data(databuf,1))&&(BCDToDec(*databuf)<=99)&&(datalong == (1))&&(writeItem(4,0,3,1,databuf))){
								lcd_control.cyc_display_num = BCDToDec(*databuf);
								return 1;
							}
						}
						break;
						case 2:	//循显时间
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,0,3,2,databuf))){
								lcd_control.cyc_time = BCDToDec(*databuf);
								return 1;
							}
						}
						break;
						case 3://电能小数位数 没做
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,0,3,3,databuf))){
//								lcd_control.cyc_time = BCDToDec(*databuf);
								return 1;
							}
						}
						break;
						case 4:	//功率小数位数 没做
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,0,3,4,databuf))){
//								lcd_control.cyc_time = BCDToDec(*databuf);
								return 1;
							}
						}
						break;
						case 5:	 //键显屏数
						{
							if((check_bcd_data(databuf,1))&&(BCDToDec(*databuf)<=99)&&(datalong == (1))&&(writeItem(4,0,3,5,databuf))){
							 	lcd_control.key_display_num = BCDToDec(*databuf);
								return 1;
							}
						}
						break;
						case 6://电流互感器变比
						{
							if((check_bcd_data(databuf,3))&&(datalong == (3))&&(writeItem(4,0,3,6,databuf))){
//								lcd_control.cyc_time = BCDToDec(*databuf);
								return 1;
							}
						}
						break;
						case 7://电压互感器变比
						{
					   		if((check_bcd_data(databuf,3))&&(datalong == (3))&&(writeItem(4,0,3,7,databuf))){
//								lcd_control.cyc_time = BCDToDec(*databuf);
								return 1;
							}
						}
						break;
						case 8://显示代码位数
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,0,3,8,databuf))){
//								lcd_control.cyc_time = BCDToDec(*databuf);
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 4:
				{
					switch(id0)
					{
						case 1:	 //通讯地址
						{
							if((check_bcd_data(databuf,6))&&(datalong == (6))&&(writeItem(4,0,4,1,databuf))){
								for(i = 0;i<6;i++){
									meter_state_info.meter_comm_addr[i] = *(databuf+i);
								}
								return 1;
							}
						}
						break;
						case 2:	//表号
						{
							if((check_bcd_data(databuf,6))&&(datalong == (6))&&(writeItem(4,0,4,2,databuf))){
								for(i = 0;i<6;i++){
									meter_state_info.meter_number[i] = *(databuf+i);
								}
								return 1;
							}
						}
						break;
						case 3: //资产管理号
						{
							if((datalong == (32))&&(writeItem(4,0,4,3,databuf))){
								return 1;
							}
						}
						break;
						case 0x0c://电表生产日期
						{
							if((datalong == (10))&&(save_data(1,0x0ff80,10,databuf))){   //LC51201		0x0ff80
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 5://电表运行状态字 不能设置
				{

				}
				break;
				case 6:
				{
					switch(id0)
					{
				   		case 1:	//有功组合特征字
						{
							if((chech_combination_active_mode(*databuf))&&(datalong == (1))&&(writeItem(4,0,6,1,databuf))){
								
								programPComboInit(); //产生组合有功编程记录
								
								energy_info.active_comb = *databuf;
								return 1;
							}
						}
						break;
						case 2:	//无功组合1特征字
						{
							if((chech_combination_reactive_mode(*databuf))&&(datalong == (1))&&(writeItem(4,0,6,2,databuf))){
								
								programQCombo1Init();//产生组合无功1编程记录
								
								energy_info.reactive_comb1 = *databuf;
								return 1;
							}
						}
						break;
						case 3:	//无功组合2特征字
						{
							if((chech_combination_reactive_mode(*databuf))&&(datalong == (1))&&(writeItem(4,0,6,3,databuf))){
								
								programQCombo2Init();//产生组合无功2编程记录

								energy_info.reactive_comb2 = *databuf;
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 7:
				{
					switch(id0)
					{
						case 1:
						{
							if(check_baud_Z(*databuf)&&(datalong == (1))&&(writeItem(4,0,7,1,databuf))){
								//应该检测特征字
								uart3_645frame.speed_state = *databuf;
								return 1;
							}
						}
						break;
						case 2:
						{
							if(check_baud_Z(*databuf)&&(datalong == (1))&&(writeItem(4,0,7,2,databuf))){
//								//应该检测特征字
//								uart3_645frame.speed_state = *databuf;
								return 1;
							}
						}
						break;
						case 3:
						{
							if(check_baud_Z(*databuf)&&(datalong == (1))&&(writeItem(4,0,7,3,databuf))){
								//应该检测特征字
								uart0_645frame.speed_state = *databuf;
								return 1;
							}
						}
						break;
						case 4:
						{
							if(check_baud_Z(*databuf)&&(datalong == (1))&&(writeItem(4,0,7,4,databuf))){
								//应该检测特征字
								uart1_645frame.speed_state = *databuf;
								return 1;
							}
						}
						break;
						case 5:
						{
							if(check_baud_Z(*databuf)&&(datalong == (1))&&(writeItem(4,0,7,5,databuf))){
//								//应该检测特征字
//								uart3_645frame.speed_state = *databuf;
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 8:
				{
			   		switch(id0)
					{
						case 1:	//周修日特征字
						{
							if((!(*databuf&0x80))&&(datalong == (1))&&(writeItem(4,0,8,1,databuf))){
								//应该检测特征字
								time_zone_con.weekday_mode = *databuf;
								return 1;
							}
						}
						break;
						case 2:	//周休日时段表
						{
							if((check_bcd_data(databuf,1))&&(BCDToDec(*databuf)<=8)&&(datalong == (1))&&(writeItem(4,0,8,2,databuf))){
								
								programDayInit();//产生周休日编程记录
								//应该检测特征字
								time_zone_con.weekday_data_table = BCDToDec(*databuf);
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 9:
				{
					switch(id0)
					{
				   		case 1://负荷记录模式字
						{
							if((!(*databuf&0x0c0))&&(datalong == (1))&&(writeItem(4,0,9,1,databuf))){
								//应该检测特征字
								load_config.lode_mode = *databuf;
								return 1;
							}
						}
						break;
						case 2:	//定时冻结模式字
						case 3:	//瞬时冻结模式字
						case 4:	//约定冻结模式字
						case 5:	//日冻结模式字
						case 6:	//整点冻结模式字
						{
							if((datalong == (1))&&(writeItem(4,0,9,id0,databuf))){
								//应该检测特征字
								freeze_config.freeze_mode[id0-2] = *databuf;
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 0x0a:
				{
					switch(id0)
					{
						case 1:	//负荷记录起始时间
						{
							if((check_bcd_data(databuf,4))&&(check_day(currentClock[Year],BCDToDec(*(databuf+3)),BCDToDec(*(databuf+2))))&&(BCDToDec(*(databuf+1))<24)&&(BCDToDec(*(databuf+0))<60)&&(datalong == (4))&&(writeItem(4,0,0x0a,1,databuf))){
								//应该检测特征字
								for(i=0;i<4;i++){
									load_config.load_start_time[i] = BCDToDec(*(databuf+i));
								}
								return 1;
							}
						}
						break;
						case 2://一类负荷间隔时间
						case 3://二类负荷间隔时间
						case 4://三类负荷间隔时间
						case 5://四类负荷间隔时间
						case 6://五类负荷间隔时间
						case 7://六类负荷间隔时间
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,0,0x0a,id0,databuf))){
								//应该检测特征字
								load_config.load_save_config[id0-2].load_interval = BCDToDec(*databuf)+BCDToDec(*(databuf+1))*100;
								return 1;
							}
						}
						break;
						default:
						return 2;

					}
				}
				break;
				case 0x0b: //结算日
				{
					if((id0 != 0)&&(id0<=3)){
						if((check_bcd_data(databuf,2))&&((BCDToDec(*(databuf+1))<31)||(BCDToDec(*(databuf+1)) == 99))&&((BCDToDec(*databuf)<24)||(BCDToDec(*databuf) == 99))&&(datalong == (2))&&(writeItem(4,0,0x0b,id0,databuf))){
							
							programSADInit();//产生计算日编程记录
						
							balance_config[id0-1].balance_time[0] = BCDToDec(*databuf);
							balance_config[id0-1].balance_time[1] = BCDToDec(*(databuf+1));

							return 1;
						}
					}else{
						return 2;
					}
				}
				break;
				case 0x0c://密码 不在这里设置
				{

				}
				break;
				case 0x0d: //铁铜损系数
				{
					if((check_bcd_data(databuf,2))&&(id0 != 0)&&(id0<=12)){
						//	BCD码
						if((datalong == (2))&&(writeItem(4,0,0x0d,id0,databuf))){
							return 1;
						}
					}else{
						return 2;
					}
				}
				break;
				case 0x0e: //报警限制
				{
					switch(id0)
					{
						case 1://正向有功功率上限
						{
							if((check_bcd_data(databuf,3))&&(datalong == (3))&&(writeItem(4,0,0x0e,1,databuf))){

								return 1;
							}
						}
						break;
						case 2:	//反向有功功率上限
						{
							if((check_bcd_data(databuf,3))&&(datalong == (3))&&(writeItem(4,0,0x0e,2,databuf))){

								return 1;
							}
						}
						break;
						case 3://电压上限
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,0,0x0e,3,databuf))){

								return 1;
							}
						}
						break;
						case 4:	//电压下限
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,0,0x0e,4,databuf))){

								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 0x0f:
				{
					switch(id0)
					{
						case 0://预付电费
						{
							if((check_bcd_data(databuf,4))&&(datalong == (4))&&(writeItem(4,0,0x0f,0,databuf))){

								return 1;
							}
						}
						break;
						case 1://报警电量1
						{
							if((check_bcd_data(databuf,4))&&(datalong == (4))&&(writeItem(4,0,0x0f,1,databuf))){

								return 1;
							}
						}
						break;
						case 2://报警电量2
						{
							if((check_bcd_data(databuf,4))&&(datalong == (4))&&(writeItem(4,0,0x0f,2,databuf))){

								return 1;
							}
						}
						break;
						case 3://囤积电量值
						{
							if((check_bcd_data(databuf,4))&&(datalong == (4))&&(writeItem(4,0,0x0f,3,databuf))){

								return 1;
							}
						}
						break;
						case 4://透支电量
						{
							if((check_bcd_data(databuf,4))&&(datalong == (4))&&(writeItem(4,0,0x0f,4,databuf))){

								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 0x10:
				{
					switch(id0)
					{
						case 0://预付金额
						{
							if((check_bcd_data(databuf,4))&&(datalong == (4))&&(writeItem(4,0,0x10,0,databuf))){

								return 1;
							}
						}
						break;
						case 1://报警金额1
						{
							if((check_bcd_data(databuf,4))&&(datalong == (4))&&(writeItem(4,0,0x10,1,databuf))){

								return 1;
							}
						}
						break;
						case 2://报警金额2
						{
							if((check_bcd_data(databuf,4))&&(datalong == (4))&&(writeItem(4,0,0x10,2,databuf))){

								return 1;
							}
						}
						break;
						case 3://透支金额
						{
							if((check_bcd_data(databuf,4))&&(datalong == (4))&&(writeItem(4,0,0x10,3,databuf))){

								return 1;
							}
						}
						break;
						case 4://囤积金额
						{
							if((check_bcd_data(databuf,4))&&(datalong == (4))&&(writeItem(4,0,0x10,4,databuf))){

								return 1;
							}
						}
						break;
						case 5://合闸允许金额
						{
							if((check_bcd_data(databuf,4))&&(datalong == (4))&&(writeItem(4,0,0x10,5,databuf))){

								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 0x11:
				{
					switch(id0)
					{
						case 1:	//电表运行状态
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,0,0x11,1,databuf))){
		
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
						
				}
				break;
				case 0x12:
				{
					switch(id0)
					{
						case 1://整点冻结起始时间
						{
							if((check_bcd_data(databuf,2))&&(datalong == (5))&&(writeItem(4,0,0x12,1,databuf))){
								//应该检测特征字
								for(i=0;i<5;i++){
									freeze_config.hour_freeze_start_time[i] = BCDToDec(*(databuf+i));
								}
								freeze_config.last_hour_freeze = 0;
								return 1;
							}
						}
						break;
						case 2://整点冻结间隔时间
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,0,0x12,2,databuf))){
								//应该检测特征字
								freeze_config.hour_freeze_interval = BCDToDec(*databuf);
								return 1;
							}
						}
						break;
						case 3:	//日冻结时间
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,0,0x12,3,databuf))){
								//应该检测特征字
								freeze_config.day_freeze_time[0] = BCDToDec(*databuf);
								freeze_config.day_freeze_time[1] = BCDToDec(*(databuf+1));
								freeze_config.last_day_freeze = 0;
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 0x14:
				{
					switch(id0)
					{
						case 1://跳闸延时
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,0,0x14,1,databuf))){
		
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				default:
				return 2;
			}	
		}
		break;
		case 1:	  //时区时段表
		case 2:
		{
			if((id1 == 0)&&(id0<=8)){
				if((datalong == (42))&&(writeItem(4,id2,0,id0,databuf))){
					return 1;
				}
			}else{
				return 2;
			}
		}
		break;
		case 3:	//节假日
		{
			if((id1 == 0)&&(id0 <= 0x80)&&(id0 != 0)){
				if((datalong == (4))&&(writeItem(4,3,0,id0,databuf))){
					return 1;
				}
			}else{
				return 2;
			}
		}
		break;
		case 4://循显键显
		{
			if(((id1 == 1)||(id1 == 2))&&(id0 != 0)&&(id0 <= 0x80)){
//				U8* data = databuf ;
				if((datalong == (5))&&(writeItem(4,4,id1,id0,databuf))){
					return 1;
				}
			}else{
				return 2;
			}
		}
		break;
		case 5://费率
		{
			if(((id1 == 1)||(id1 == 2))&&(id0 != 0)&&(id0 <= 8)){
				if((datalong == (4))&&(writeItem(4,5,id1,id0,databuf))){
					return 1;
				}
			}else{
				return 2;
			}
		}
		break;
		case 6:
		{
			switch(id1)
			{
				case 0://梯度
				case 2:
				{
					if((id0 != 0)&&(id0 <= 0x10)){
						if((datalong == (4))&&(writeItem(4,6,id1,id0,databuf))){
							return 1;
						}
					}
				}
				break;
				case 1://梯度电价
				case 3:
				{
					if((id0 != 0)&&(id0 <= 0x11)){
						if((datalong == (4))&&(writeItem(4,6,id1,id0,databuf))){
							return 1;
						}
					}
				}
				break;
				default:
				return 2;
			}
		}
		break;
		case 7:	 //梯度费率混合
		case 8:
		{
			switch(id1)
			{
				case 0:
				{
					if((id0 != 0)&&(id0 <= 0x08)){
						if((datalong == (4))&&(writeItem(4,id2,id1,id0,databuf))){
							return 1;
						}
					}
				}
				break;
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				{
					if((id0 != 0)&&(id0 <= 0x21)){
						if((datalong == (4))&&(writeItem(4,id2,id1,id0,databuf))){
							return 1;
						}
					}
				}
				break;
				default:
				return 2;
				
			}
		}
		break;
		case 9://事件触发阀值设置
		{
			switch(id1)
			{
				case 1:	//失压
				{
					switch(id0)
					{
						case 1:
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,9,1,1,databuf))){
								event_config.lost_voltage.voltage_upper_limit = BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100);
								return 1;
							}	
						}
						break;
						case 2:
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,9,1,2,databuf))){
								event_config.lost_voltage.voltage_renew_lower_limit = BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100);
								return 1;
							}
						}
						break;
						case 3:
						{
							if((check_bcd_data(databuf,3))&&(datalong == (3))&&(writeItem(4,9,1,3,databuf))){
								event_config.lost_voltage.current_lower_limit = (BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100)+(BCDToDec(*(databuf+2))*10000))/10;
								return 1;
							}
						}
						break;
						case 4:
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,9,1,4,databuf))){
								event_config.lost_voltage.time_limit = BCDToDec(*(databuf));
								return 1;
							}
						}
						break;
						default:
						return 2; 
					}
				}
				break;
				case 2:	//欠压
				{
					switch(id0)
					{
						case 1:
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,9,2,1,databuf))){
								event_config.lack_voltage.voltage_upper_limit = BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100);
								return 1;
							}
						}
						break;
						case 2:
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,9,2,2,databuf))){
								event_config.lack_voltage.time_limit = BCDToDec(*(databuf));
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 3:	//过压
				{
					switch(id0)
					{
						case 1:
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,9,3,1,databuf))){
								event_config.over_voltage.voltage_lower_limit = BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100);
								return 1;
							}
						}
						break;
						case 2:
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,9,3,2,databuf))){
								event_config.over_voltage.time_limit = BCDToDec(*(databuf));
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 4:	//断相
				{
					switch(id0)
					{
						case 1:
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,9,4,1,databuf))){
								event_config.break_off.voltage_upper_limit = BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100);
								return 1;
							}
						}
						break;
						case 2:
						{
							if((check_bcd_data(databuf,3))&&(datalong == (3))&&(writeItem(4,9,4,2,databuf))){
								event_config.break_off.current_upper_limit = (BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100)+(BCDToDec(*(databuf+2))*10000))/10;
								return 1;
							}
						}
						break;
						case 3:
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,9,4,3,databuf))){
								event_config.break_off.time_limit = BCDToDec(*(databuf));
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
//				case 5:	//全失压
//				{
//					switch(id0)
//					{
//						case 1:
//						{
//							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,9,5,1,databuf))){
//								event_config.all_lost_voltage.voltage_upper_limit = BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100);
//								return 1;
//							}	
//						}
//						break;
//						case 2:
//						{
//							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,9,5,2,databuf))){
//								event_config.all_lost_voltage.current_lower_limit = BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100);
//								return 1;
//							}
//						}
//						break;
//						case 3:
//						{
//							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,9,5,3,databuf))){
//								event_config.all_lost_voltage.time_limit = BCDToDec(*(databuf));
//								return 1;
//							}
//						}
//						break;
//						default:
//						return 2; 
//					}
//				}
//				break;
				case 5://电压不平衡
				{
					switch(id0)
					{
						case 1:
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,9,5,1,databuf))){
								event_config.voltage_no_balance.no_balance_limit = BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100);
								return 1;
							}	
						}
						break;
						case 2:
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,9,5,2,databuf))){
								event_config.voltage_no_balance.time_limit = BCDToDec(*(databuf));
								return 1;
							}
						}
						break;
						default:
						return 2; 
					}
				}
				break;
				case 6://电流不平衡
				{
					switch(id0)
					{
						case 1:
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,9,6,1,databuf))){
								event_config.current_no_balance.no_balance_limit = BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100);
								return 1;
							}	
						}
						break;
						case 2:
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,9,6,2,databuf))){
								event_config.current_no_balance.time_limit = BCDToDec(*(databuf));
								return 1;
							}
						}
						break;
						default:
						return 2; 
					}
				}
				break;
				case 7://失流
				{
					switch(id0)
					{
						case 1:
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,9,7,1,databuf))){
								event_config.lost_current.voltage_lower_limit = BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100);
								return 1;
							}	
						}
						break;
						case 2:
						{
							if((check_bcd_data(databuf,3))&&(datalong == (3))&&(writeItem(4,9,7,2,databuf))){
								event_config.lost_current.current_upper_limit = (BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100)+(BCDToDec(*(databuf+2))*10000))/10;
								return 1;
							}
						}
						break;
						case 3:
						{
							if((check_bcd_data(databuf,3))&&(datalong == (3))&&(writeItem(4,9,7,3,databuf))){
								event_config.lost_current.current_lower_limit = (BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100)+(BCDToDec(*(databuf+2))*10000))/10;
								return 1;
							}
						}
						break;
						case 4:
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,9,7,4,databuf))){
								event_config.lost_current.time_limit = BCDToDec(*(databuf));
								return 1;
							}
						}
						break;
						default:
						return 2; 
					}
				}
				break;
				case 8:	 //过流
				{
					switch(id0)
					{
						case 1:
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,9,8,1,databuf))){
								event_config.over_current.current_lower_limit = (BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100))*100;
								return 1;
							}	
						}
						break;
						case 2:
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,9,8,2,databuf))){
								event_config.over_current.time_limit = BCDToDec(*(databuf));
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 0x09: //断流
				{
					switch(id0)
					{
						case 1:
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,9,9,1,databuf))){
								event_config.break_current.voltage_lower_limit = BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100);
								return 1;
							}
						}
						break;
						case 2:
						{
							if((check_bcd_data(databuf,3))&&(datalong == (3))&&(writeItem(4,9,9,2,databuf))){
								event_config.break_current.current_upper_limit = (BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100)+(BCDToDec(*(databuf+2))*10000))/10;
								return 1;
							}
						}
						break;
						case 3:
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,9,9,3,databuf))){
								event_config.break_current.time_limit = BCDToDec(*(databuf));
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 0x0a:	//潮流反向
				{
					switch(id0)
					{
						case 1:
						{
							if((check_bcd_data(databuf,3))&&(datalong == (3))&&(writeItem(4,9,0x0a,1,databuf))){
								event_config.power_converse.active_power_lower_limit = (BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100)+(BCDToDec(*(databuf+2))*10000));
								return 1;
							}	
						}
						break;
						case 2:
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,9,0x0a,2,databuf))){
								event_config.power_converse.time_limit = BCDToDec(*(databuf));
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 0x0b:	//过载
				{
					switch(id0)
					{
						case 1:
						{
							if((check_bcd_data(databuf,3))&&(datalong == (3))&&(writeItem(4,9,0x0b,1,databuf))){
								event_config.over_power.active_power_lower_limit = (BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100)+(BCDToDec(*(databuf+2))*10000));
								return 1;
							}	
						}
						break;
						case 2:
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,9,0x0b,2,databuf))){
								event_config.over_power.time_limit = BCDToDec(*(databuf));
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 0x0c:	//电压合格率
				{
					switch(id0)
					{
						case 1:
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,9,0x0c,1,databuf))){
								event_config.voltage_regular.voltage_upper_limit = BCDToDec(*(databuf))+(BCDToDec(*(databuf+1)))*100;
								return 1;
							}	
						}
						break;
						case 2:
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,9,0x0c,2,databuf))){
								event_config.voltage_regular.voltage_lower_limit = BCDToDec(*(databuf))+(BCDToDec(*(databuf+1)))*100;
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 0x0d: //需量超限
				{
					switch(id0)
					{
						case 1:
						{
							if((check_bcd_data(databuf,3))&&(datalong == (3))&&(writeItem(4,9,0xd,1,databuf))){
								event_config.demand_limit.active_demand_lower_limit = BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100)+(BCDToDec(*(databuf+2))*10000);
								return 1;
							}
						}
						break;
						case 2:
						{
							if((check_bcd_data(databuf,3))&&(datalong == (3))&&(writeItem(4,9,0xd,2,databuf))){
								event_config.demand_limit.reactive_demand_lower_limit = BCDToDec(*(databuf))+(BCDToDec(*(databuf+1))*100)+(BCDToDec(*(databuf+2))*10000);
								return 1;
							}
						}
						break;
						case 3:
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,9,0xd,3,databuf))){
								event_config.demand_limit.time_limit = BCDToDec(*(databuf));
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 0x0e: //功率因数超限
				{
					switch(id0)
					{
						case 1:
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,9,0x0e,1,databuf))){
								event_config.power_factor_limit.power_factor_lower_limit = BCDToDec(*(databuf))+(BCDToDec(*(databuf+1)))*100;
								return 1;
							}	
						}
						break;
						case 2:
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,9,0x0e,2,databuf))){
								event_config.power_factor_limit.time_limit = BCDToDec(*(databuf));
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 0x0f: //电流严重不平衡
				{
					switch(id0)
					{
						case 1:
						{
							if((check_bcd_data(databuf,2))&&(datalong == (2))&&(writeItem(4,9,0x0f,1,databuf))){
								event_config.current_sever_no_balance.current_no_balance_limit = BCDToDec(*(databuf))+(BCDToDec(*(databuf+1)))*100;
								return 1;
							}	
						}
						break;
						case 2:
						{
							if((check_bcd_data(databuf,1))&&(datalong == (1))&&(writeItem(4,9,0x0f,2,databuf))){
								event_config.current_sever_no_balance.time_limit = BCDToDec(*(databuf));
								return 1;
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				default:
				return 2;
			}
		}
		break;
		default:
		return 2;
	}		
	return 0;
}
/*********************************************************************************************************
** 函数名称:     comm_write_parameter_99
** 功能描述:     99级密码写数据
** 输　入:       id2  数据标识2
**				 id1  数据标识1
**				 id0  数据标识0
**				 databuf  数据缓冲
**				 datalong 数据长度
** 输　出:       0 失败
**				 1 成功
**				 2 ID无效
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
#if HAVE_ESAM == 1
U8 comm_write_parameter_99(U8 id2,U8 id1,U8 id0,U8 *databuf,U8 datalong)
{
	switch(id2)
	{
		case 0:
		{
			switch(id1)
			{
		   		case 1:
				{
					U8 buf[10],mac[4],i;
					U16 result = 0;

					if((id0 == 8)&&(datalong == 9)){//两套分时费率切换时间
						for(i=0;i<5;i++){
							buf[i] = *(databuf+5-1-i);
						}
						for(i=0;i<4;i++){
							mac[i] = *(databuf+9-1-i);
						}
						result = esam_write_binaryfile_mac(0x82,buf,5,10,mac);
						if(result == 0x9000){
							return 1;
						}
					}
				}
				break;
				case 3:
				{
					U8 buf[10],mac[4],i;
					U16 result = 0;

					if((id0 == 6)&&(datalong == 7)){//电流互感器变比
						for(i=0;i<3;i++){
							buf[i] = *(databuf+3-1-i);
						}
						for(i=0;i<4;i++){
							mac[i] = *(databuf+7-1-i);
						}
						result = esam_write_binaryfile_mac(0x82,buf,3,24,mac);
						if(result == 0x9000){
							return 1;
						}
					}else if((id0 == 7)&&(datalong == 7)){//电压互感器变比
						for(i=0;i<3;i++){
							buf[i] = *(databuf+3-1-i);
						}
						for(i=0;i<4;i++){
							mac[i] = *(databuf+7-1-i);
						}
						result = esam_write_binaryfile_mac(0x82,buf,3,27,mac);
						if(result == 0x9000){
							return 1;
						}
					}
				}
				break;
				case 4:
				{
					U8 buf[10],mac[4],i;
					U16 result = 0;

					if((id0 == 0xe)&&(datalong == 10)){//客户编号
						for(i=0;i<6;i++){
							buf[i] = *(databuf+6-1-i);
						}
						for(i=0;i<4;i++){
							mac[i] = *(databuf+10-1-i);
						}
						result = esam_write_binaryfile_mac(0x82,buf,3,36,mac);
						if(result == 0x9000){
							return 1;
						}
					}
				}
				break;
				case 0x10:
				{
					U8 buf[10],mac[4],i;
					U16 result = 0;

					if((id0 == 1)&&(datalong == 8)){//报警金额1
						for(i=0;i<4;i++){
							buf[i] = *(databuf+4-1-i);
						}
						for(i=0;i<4;i++){
							mac[i] = *(databuf+8-1-i);
						}
						result = esam_write_binaryfile_mac(0x82,buf,4,16,mac);
						if(result == 0x9000){
							return 1;
						}
					}else if((id0 == 2)&&(datalong == 8)){//报警金额2
						for(i=0;i<4;i++){
							buf[i] = *(databuf+4-1-i);
						}
						for(i=0;i<4;i++){
							mac[i] = *(databuf+8-1-i);
						}
						result = esam_write_binaryfile_mac(0x82,buf,4,20,mac);
						if(result == 0x9000){
							return 1;
						}
					}
				}
				break;
				default:
				return 2;
			}
		}
		break;
		case 5:
		{
			switch(id1)
			{
				case 1:
				{
					U8 buf[10],mac[4],i;
					U16 result = 0;
					if((id0>=1)&&(id0<=0x3f)&&(datalong == 8)){	  //第1套费率
					   for(i=0;i<4;i++){
							buf[i] = *(databuf+4-1-i);
						}
						for(i=0;i<4;i++){
							mac[i] = *(databuf+8-1-i);
						}
						result = esam_write_binaryfile_mac(0x83,buf,4,id0*4,mac);
						if(result == 0x9000){
							return 1;
						}
					}
				}
				break;
				case 2:
				{
					U8 buf[10],mac[4],i;
					U16 result = 0;
					if((id0>=1)&&(id0<=0x3f)&&(datalong == 8)){	 //第2套费率
					   for(i=0;i<4;i++){
							buf[i] = *(databuf+4-1-i);
						}
						for(i=0;i<4;i++){
							mac[i] = *(databuf+8-1-i);
						}
						result = esam_write_binaryfile_mac(0x84,buf,4,id0*4,mac);
						if(result == 0x9000){
							return 1;
						}
					}
				}
				break;
				default:
				return 2;
			}
		}
		break;
		default:
		return 2;
	}
	return 0;
}
#endif
/*********************************************************************************************************
** 函数名称:     comm_write_parameter_98
** 功能描述:     98级密码写数据
** 输　入:       id2  数据标识2
**				 id1  数据标识1
**				 id0  数据标识0
**				 databuf  数据缓冲
**				 datalong 数据长度
** 输　出:       0 失败
**				 1 成功
**				 2 ID无效
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
#if HAVE_ESAM == 1
U8 comm_write_parameter_98(U8 id2,U8 id1,U8 id0,U8 *databuf,U8 datalong)
{
	switch(id2)
	{
		case 0:
		{
			switch(id1)
			{
				case 1:
				{
					switch(id0)
					{
						case 6:	//两套时区表
						{
							U8 buf[10],len = 0,i;
							if(decode_98_mac(0,databuf,datalong,buf,&len,0)){
								//倒序数据
								if((check_bcd_data(buf,5))&&(check_day(BCDToDec(*(buf+4)),BCDToDec(*(buf+3)),BCDToDec(*(buf+2))))&&(BCDToDec(*(buf+1))<24)&&(BCDToDec(*(buf+0))<60)&&(len == (5))&&(writeItem(4,0,1,6,buf))){
									for(i = 0;i<5;i++){
										time_zone_con.zones_change_time[i] = BCDToDec(*(buf+i));
									}
									return 1;
								}
							}		
						}
						break;
						case 7:	//两套时段表
						{
							U8 buf[10],len = 0,i;
							if(decode_98_mac(0,databuf,datalong,buf,&len,0)){
								if((check_bcd_data(buf,5))&&(check_day(BCDToDec(*(buf+4)),BCDToDec(*(buf+3)),BCDToDec(*(buf+2))))&&(BCDToDec(*(buf+1))<24)&&(BCDToDec(*(buf+0))<60)&&(len == (5))&&(writeItem(4,0,1,7,buf))){
									for(i = 0;i<5;i++){
										time_zone_con.periods_change_time[i] = BCDToDec(*(buf+i));
									}
									return 1;
								}
							}
						}
						break;
						case 9:	//两套梯度
						{
							U8 buf[10],len = 0;
							if(decode_98_mac(0,databuf,datalong,buf,&len,0)){
								if((check_bcd_data(buf,5))&&(check_day(BCDToDec(*(buf+4)),BCDToDec(*(buf+3)),BCDToDec(*(buf+2))))&&(BCDToDec(*(buf+1))<24)&&(BCDToDec(*(buf+0))<60)&&(len == (5))&&(writeItem(4,0,1,9,buf))){
									//修改变量
									return 1;
								}
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 2:
				{
					switch(id0)
					{
						case 1:	//年最大时区数
						{
							U8 buf[10],len = 0;
							if(decode_98_mac(0,databuf,datalong,buf,&len,0)){
								if((check_bcd_data(buf,1))&&(BCDToDec(*buf)<=14)&&(len == (1))&&(writeItem(4,0,2,1,buf))){
									//修改变量
									time_zone_con.max_zone_num = BCDToDec(*buf);
									return 1;
								}
							}
						}
						break;
						case 2://日时段表数
						{
							U8 buf[10],len = 0;
							if(decode_98_mac(0,databuf,datalong,buf,&len,0)){
								if((check_bcd_data(buf,1))&&(BCDToDec(*buf)<=8)&&(len == (1))&&(writeItem(4,0,2,2,buf))){
									time_zone_con.max_data_table = BCDToDec(*buf);
									return 1;
								}
							}
						}
						break;
						case 3://日时段数
						{
							U8 buf[10],len = 0;
							if(decode_98_mac(0,databuf,datalong,buf,&len,0)){
								if((check_bcd_data(buf,1))&&(BCDToDec(*buf)<=14)&&(len == (1))&&(writeItem(4,0,2,3,buf))){
									time_zone_con.max_period_num = BCDToDec(*buf);
									return 1;
								}
							}
						}
						break;
						case 4://费率
						{
							U8 buf[10],len = 0;
							if(decode_98_mac(0,databuf,datalong,buf,&len,0)){
								if((check_bcd_data(buf,1))&&(BCDToDec(*buf)<=63)&&(len == (1))&&(writeItem(4,0,2,4,buf))){
									time_zone_con.max_fee_num = BCDToDec(*buf);
									return 1;
								}
							}
						}
						break;
						case 5:	//节假日
						{
							U8 buf[10],len = 0;
							if(decode_98_mac(0,databuf,datalong,buf,&len,0)){
								if((check_bcd_data(buf,2))&&((BCDToDec(*buf)+(BCDToDec(*(buf+1)))*100)<=254)&&(len == (2))&&(writeItem(4,0,2,5,buf))){
									time_zone_con.max_holiday = BCDToDec(*buf)+(BCDToDec(*(buf+1)))*100;
									return 1;
								}
							}
						}
						break;
						case 7:	//梯度
						{
							U8 buf[10],len = 0;
							if(decode_98_mac(0,databuf,datalong,buf,&len,0)){
								if((check_bcd_data(buf,1))&&(BCDToDec(*buf)<=8)&&(len == (1))&&(writeItem(4,0,2,7,buf))){
									//设置参数
									return 1;
								}
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 8:
				{
					switch(id0)
					{
						case 1:	//周修日特征字
						{
							U8 buf[10],len = 0;
							if(decode_98_mac(0,databuf,datalong,buf,&len,0)){
								if((!(*buf&0x80))&&(len == (1))&&(writeItem(4,0,8,1,buf))){
									time_zone_con.weekday_mode = *buf;
									return 1;
								}
							}
						}
						break;
						case 2:	//周休日时段表
						{
							U8 buf[10],len = 0;
							if(decode_98_mac(0,databuf,datalong,buf,&len,0)){
								if((check_bcd_data(buf,1))&&(BCDToDec(*buf)<=8)&&(len == (1))&&(writeItem(4,0,8,2,buf))){
									programDayInit();//产生周休日编程记录
									//应该检测特征字
									time_zone_con.weekday_data_table = BCDToDec(*buf);
									return 1;
								}
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				case 0x0b:
				{
					U8 buf[10],len = 0;
					if((id0 != 0)&&(id0<=3)){		//结算日
						if(decode_98_mac(0,databuf,datalong,buf,&len,0)){
							if((check_bcd_data(buf,2))&&((BCDToDec(*(buf+1))<31)||(BCDToDec(*(buf+1)) == 99))&&((BCDToDec(*buf)<24)||(BCDToDec(*buf) == 99))&&(len == (2))&&(writeItem(4,0,0x0b,id0,buf))){
								
								programSADInit();//产生计算日编程记录
							
								balance_config[id0-1].balance_time[0] = BCDToDec(*buf);
								balance_config[id0-1].balance_time[1] = BCDToDec(*(buf+1));
	
								return 1;
							}
						}
						
					}else{
						return 2;
					}
				}
				break;
				case 0x10:
				{
					switch(id0)
					{
						case 4:	//囤积金额限制
						{
							U8 buf[10],len = 0;
							if(decode_98_mac(0,databuf,datalong,buf,&len,0)){
								if((len == (4))&&(writeItem(4,0,0x10,4,buf))){
									
									return 1;
								}
							}
						}
						break;
						case 5:	//合闸允许金额限制
						{
							U8 buf[10],len = 0;
							if(decode_98_mac(0,databuf,datalong,buf,&len,0)){
								if((len == (4))&&(writeItem(4,0,0x10,5,buf))){
									
									return 1;
								}
							}
						}
						break;
						default:
						return 2;
					}
				}
				break;
				default:
				return 2;
			}
		}
		break;
		case 1:	//两套时区时段表
		case 2:
		{
			U8 buf[100],len = 0;
			
			if((id1 == 0)&&(id0<=8)){
				if(decode_98_mac(id2,databuf,datalong,buf,&len,1)){
					if((len == (42))&&(writeItem(4,id2,0,id0,buf))){
										
						return 1;
					}
				}
			}else{
				return 2;
			}
		}
		break;
		case 3:	//节假日时段表
		{
	   		U8 buf[10],len = 0;
			
			if((id1 == 0)&&(id0 <= 254)&&(id0 != 0)){
				if(decode_98_mac(3,databuf,datalong,buf,&len,0)){
					if((len == (4))&&(writeItem(4,3,0,id0,buf))){
										
						return 1;
					}
				}
			}else{
				return 2;
			}
		}
		break;
		case 6:	//梯度
		{
			
			switch(id1)
			{
				case 0:	//梯度值
				case 2:
				{
					U8 buf[10],len = 0;
			
					if((id0 <= 0x10)&&(id0 != 0)){
						if(decode_98_mac(6,databuf,datalong,buf,&len,0)){
							if((len == (4))&&(writeItem(4,6,id1,id0,buf))){
												
								return 1;
							}
						}
					}else{
						return 2;
					}
				}
				break;
				case 1://梯度电价
				case 3:
				{
					U8 buf[10],len = 0;
			
					if((id0 <= 0x11)&&(id0 != 0)){
						if(decode_98_mac(6,databuf,datalong,buf,&len,0)){
							if((len == (4))&&(writeItem(4,6,id1,id0,buf))){
												
								return 1;
							}
						}
					}else{
						return 2;
					}
				}
				break;
				default:
				return 2;
			}
			
			
		}
		break;
		default:
		return 2;
	}
	return 0;
}
#endif
/*********************************************************************************************************
** 函数名称:     com_security_handle
** 功能描述:     安全处理
** 输　入:       id3 id2 id1 id0 标识码
**				 input 数据输入指针
**				 output 数据输出指针
**				 input_len 数据输入长度
**				 input_len 数据输出长度
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
#if HAVE_ESAM == 1
U8 com_security_handle(U8 id3,U8 id2,U8 id1,U8 id0,U8 *input,U8 *output,U8 input_len,U8 *output_len)
{
	U8 i;

	if((id3 == 7)&&(id0 == 0x0ff)){
		switch(id2)
		{
			case 0:	//安全认证
			{
				switch(id1)
				{
					case 0://认证
					{
				   		if(input_len == 24){
							if(remote_authen(input+16,input+8,input,output)){
								*output_len = 12;
								return 0;
							}else{
								*output_len = 0;
								return 4;
							}
						}
					}
					break;
					case 1://认证有效时长
					{
						if(input_len == 6){
							*output_len = 0;
							if(remote_authen_ActiveDly(input)){
								return 0;
							}else{
								return 1;
							}
						}
					}
					break;
					case 2://认证失效
					{
						if(input_len == 0){
							if(remote_authen_disable(output)){
								*output_len = 18;
								return 0;
							}else{
						   		*output_len = 0;
								return 1;
							}
						}
					}
					break;
					default:
					break;
				}
			}
			break;
			case 1:	//开户或充值
			{
				switch(id1)
				{
					case 1:	//开户
					{
						if((extern_control.flag&(IDENTITY_AUTHEN_TIME_BIT|IDENTITY_AUTHEN_BIT)) == (IDENTITY_AUTHEN_TIME_BIT|IDENTITY_AUTHEN_BIT)){
							if((input_len == 22)&&(check_bcd_data(input+12,6))){
								switch(remote_crush_money(1,input,22))
								{
									case 0://成功
									{
										*output_len = 0;
										return 0;
									}
//									break;
									case 2:
									{
										*output_len = 0;
										return 7;
									}
//									break;
									default:
									{
										*output_len = 0;
										return 1;
									}
//									break;
								}
							}
						}	
					}
					break;
					case 2://充值
					{
						if((extern_control.flag&(IDENTITY_AUTHEN_TIME_BIT|IDENTITY_AUTHEN_BIT)) == (IDENTITY_AUTHEN_TIME_BIT|IDENTITY_AUTHEN_BIT)){
							if((input_len == 22)&&(check_bcd_data(input+12,6))){
								switch(remote_crush_money(0,input,22))
								{
									case 0://成功
									{
										*output_len = 0;
										return 0;
									}
//									break;
									case 1:
									{
										*output_len = 0;
										return 5;
									}
//									break;
									case 2:
									{
										*output_len = 0;
										return 7;
									}
//									break;
									case 3:
									{
										*output_len = 0;
										return 6;
									}
//									break;
									case 4:
									{
										*output_len = 0;
										return 2;
									}

//									break;
									default:
									{
										*output_len = 0;
										return 1;
									}
//									break;
								}
							}
						}
					}
					break;
					default:
					break;
				}
			}
			break;
			case 2:	//密钥更新
			{
				switch(id1)
				{
					case 1:	//控制密钥更新
					case 2://参数密钥更新
					case 3://远程身份认证密钥
					{
						if((extern_control.flag&(IDENTITY_AUTHEN_TIME_BIT|IDENTITY_AUTHEN_BIT)) == (IDENTITY_AUTHEN_TIME_BIT|IDENTITY_AUTHEN_BIT)){
							switch(remote_modify_key(id1,0x0ff,input,input_len))
							{
								case 0:
								{
									*output_len = 0;
									return 0;
								}
//								break;
								case 1:
								{
									*output_len = 0;
									return 1;
								}
//								break;
								case 4:
								{
									*output_len = 0;
									return 4;
								}
//								break;
								default:
								break;
							}
						}
					}
					break;
					default:
					break;
				}
			}
			break;
			case 0x80://数据回抄
			{
				if(id1 == 1){
					if(input_len == 8){
						//身份认证有效
						if((extern_control.flag&(IDENTITY_AUTHEN_TIME_BIT|IDENTITY_AUTHEN_BIT)) == (IDENTITY_AUTHEN_TIME_BIT|IDENTITY_AUTHEN_BIT)){ 
							if(remote_read_data((*(input+6))+((*(input+7))<<8),(*(input+4))+((*(input+5))<<8),(*(input+2))+((*(input+3))<<8),(*input)+((*(input+1))<<8),output+8,output_len)){
								for(i=0;i<8;i++){
									*(output+i) = *(input+i);
								}
								return 0;
							}else{
								*output_len = 0;
								return 3;
							}
						}else{
							//身份认证失效
							*output_len = 0;
							return 1;
						}
					}
				}
			}
			break;
			case 0x81://状态查询
			{
				if(id1 == 2){
					if(input_len == 0){
						//身份认证有效
						if((extern_control.flag&(IDENTITY_AUTHEN_TIME_BIT|IDENTITY_AUTHEN_BIT)) == (IDENTITY_AUTHEN_TIME_BIT|IDENTITY_AUTHEN_BIT)){ 
							if(remote_read_state(output)){
								*output_len = 26;
								return 0;
							}else{
								*output_len = 0;
								return 3;
							}
						}else{
							//身份认证失效
							*output_len = 0;
							return 1;
						}
					}
				}
			}
			break;
			default:
			break;
		}
	}
	return 0x0ff;
}
#endif
/*********************************************************************************************************
** 函数名称:     process_uart_Broadcast
** 功能描述:     处理串口广播指令
** 输　入:       DevNum 串口编号 全AAAAAAAAAAAA
**				 p645Frame 645数据结构指针
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 process_645_BroadcastA(LPC_Uart_Channel_t DevNum,Uart_645Data_t *p645Frame)
{
	U8 control,datalong = 0,databuf[200],i,flag_respondent = 0;

	switch(p645Frame->Control)
	{
   		case 0x13: //读取通讯地址或MAC
		{	//是否需要从eeprom中读取相关信息
			if(p645Frame->Datalong == 0){
				for(i=0;i<6;i++){
					databuf[i] = meter_state_info.meter_comm_addr[i];	
				}
				datalong = 6;
				control = 0x93;
				flag_respondent = 1;
			}else if(p645Frame->Datalong == 1){
				if(p645Frame->DataBuf[0] == 0){
				 	for(i=0;i<6;i++){
						databuf[i] = meter_state_info.meter_number[i];	
					}
					datalong = 6;
					control = 0x93;
					flag_respondent = 1;
				}

			}
		}
		break;
		case 0x15:	 //设置通讯地址或MAC 要有编程键标志
		{
			if(p645Frame->Datalong == 6){
			
				if((check_bcd_data(p645Frame->DataBuf,6))&&(writeItem(4,0,4,1,p645Frame->DataBuf))){
					for(i=0;i<6;i++){
						meter_state_info.meter_comm_addr[i] = p645Frame->DataBuf[i];	
					}
					//保存数据
					//保存正确
					datalong = 0;
					control = 0x95;
					flag_respondent = 1;
					//保存错误恢复地址，否定应答
				}

			}else if(p645Frame->Datalong == 8){
				if((p645Frame->DataBuf[0] == 0)&&(p645Frame->DataBuf[1] == 0)){
					if((check_bcd_data(&(p645Frame->DataBuf[2]),6))&&(writeItem(4,0,4,2,(p645Frame->DataBuf)+2))){
						for(i=0;i<6;i++){
							meter_state_info.meter_number[i] = p645Frame->DataBuf[i];	
						}
						datalong = 0;
						control = 0x95;
						flag_respondent = 1;
					}
				}
			}
		}
		break;
		case 0x11:	//读指令 只做读表号和读通讯地址
		{
			if(p645Frame->Datalong == 4){
				switch((p645Frame->DataBuf[3]<<24)+(p645Frame->DataBuf[2]<<16)+(p645Frame->DataBuf[1]<<8)+p645Frame->DataBuf[0]){
					case 0x04000401://通讯地址
					{
						databuf[0] = p645Frame->DataBuf[0];
						databuf[1] = p645Frame->DataBuf[1];
						databuf[2] = p645Frame->DataBuf[2];
						databuf[3] = p645Frame->DataBuf[3];

						for(i=0;i<6;i++){
							databuf[i+4] = meter_state_info.meter_comm_addr[i];	
						}
						datalong = 10;
						control = 0x91;
						flag_respondent = 1;
					}
					break;
					case 0x04000402: //表号
					{
						databuf[0] = p645Frame->DataBuf[0];
						databuf[1] = p645Frame->DataBuf[1];
						databuf[2] = p645Frame->DataBuf[2];
						databuf[3] = p645Frame->DataBuf[3];
						for(i=0;i<6;i++){
							databuf[i+4] = meter_state_info.meter_number[i];	
						}
						datalong = 10;
						control = 0x91;
						flag_respondent = 1;
					}
					break;
					default:
					break;
				}
			}
		}
		break;
		default:
		break;
	}
	if(flag_respondent){
   		Send_645_Format(DevNum,meter_state_info.meter_comm_addr,control,databuf,datalong);
	}

	return 1;
}
/*********************************************************************************************************
** 函数名称:     process_uart_Broadcast
** 功能描述:     处理串口广播指令
** 输　入:       DevNum 串口编号 全999999999999
**				 p645Frame 645数据结构指针
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 process_645_Broadcast9(LPC_Uart_Channel_t DevNum,Uart_645Data_t *p645Frame)
{
	switch(p645Frame->Control){
		case 0x08:	//广播校时 红外支持
		{
			if(p645Frame->Datalong == 6){
				U8 last_time[6],new_time[6],i=0;
				U32 time1=0,time2=0;
				for(i=0;i<6;i++){
					last_time[i] = currentClock[i];
					new_time[i] =  BCDToDec(p645Frame->DataBuf[i]);
				} 
				//时间信息合法性验证
				if((new_time[3] == last_time[3])&&(new_time[4] == last_time[4])&&(new_time[5] == last_time[5])&&(new_time[2] != 0)&&(new_time[2] != 23)&&(new_time[2]<23)&&(new_time[1]<60)&&(new_time[0]<60)){
					time2 = new_time[2]*60+new_time[1];
					time1 = last_time[2]*60+last_time[1];
					if(time1>=time2){
						if((time1-time2)<=5){
							adjustDateTime(p645Frame->DataBuf);
						}
					}else{
						if((time2-time1)<=5){
							adjustDateTime(p645Frame->DataBuf);
						}
					} 		
				}
				
			}
		}
		break;
		case 0x16:	//广播冻结
		{
			if(p645Frame->Datalong == 4){
				U8 i=0;
		   		//设置冻结
				if(check_all_same(&(p645Frame->DataBuf[0]),0x99,4)){
					//瞬时冻结
					instantaneous_freeze();
					
				}else{
					//设置定时冻结时间
					//判别时间是否合法
					//保存定时冻结时间
					//更新冻结时间
					for(i=0;i<4;i++){
						freeze_config.dingshi_freeze_time[i] = BCDToDec(p645Frame->DataBuf[i]);
					}
					for(i=0;i<5;i++){
						freeze_config.last_dingshi_freeze_time[i] = 0;
					}
				}
			}
		}
		break;
		default:
		break;
	}
	return 1;
}
/*********************************************************************************************************
** 函数名称:     command_0x1c_deal
** 功能描述:     处理1c命令
** 输　入:       buf 数据
**				 mode 模式
** 输　出:       0 失败
**				 1 成功
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 command_0x1c_deal(U8 *buf,U8 mode)
{
	U8 databuf[6],i;
	if(mode == 0){
		if(1){		 //远程控制指令无效
			return 0;
		}
	}
	//时间信息的有效性判断
	for(i=0;i<6;i++){
		databuf[i] = BCDToDec(*(buf+7-i));
	}
	if((check_day(databuf[5],databuf[4],databuf[3]))&&(databuf[2]<24)&&(databuf[1]<60)&&(databuf[0]<60)){
		//时间阀值限定
		if(check_zone_timeout(databuf,currentClock,6)){
			switch(*buf)
			{
				case 0x1a:	//继电器操作
				{
			   		if(output_control.keep_deal != 0x3a){
						output_control.relay_deal = *buf;
						return 1;	
					}
				}
				break;
				case 0x1b:
				{
					output_control.relay_deal = *buf;
					return 1;
				}
//				break;
				case 0x2a:	//报警操作
				case 0x2b:
				{
					output_control.alarm_deal = *buf; //保存远程报警状态
					return 1;
				}
//				break;
				case 0x3a:	 //保电操作
				case 0x3b:
				{
					output_control.keep_deal = *buf; //保存远程保电状态
					return 1;
				}
//				break;
				default:
				break;
			}
		}
	}
	return 0;
}
/*********************************************************************************************************
** 函数名称:     process_uart_protocol
** 功能描述:     处理串口其他指令
** 输　入:       DevNum 串口编号
**				 p645Frame 645数据结构指针
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 process_645_protocol(LPC_Uart_Channel_t DevNum,Uart_645Data_t *p645Frame)
{
	U8 control = 0,datalong = 0,databuf[200],i,flag_respondent = 0;

	switch(p645Frame->Control)
	{
#if HAVE_ESAM == 1
		case 0x03: //安全操作
		{
			//依据标识读取数据
			databuf[0] = p645Frame->DataBuf[0];
			databuf[1] = p645Frame->DataBuf[1];
			databuf[2] = p645Frame->DataBuf[2];
			databuf[3] = p645Frame->DataBuf[3];
			if(amend_handler_num((p645Frame->DataBuf)+4)){
				switch(com_security_handle(p645Frame->DataBuf[3],p645Frame->DataBuf[2],p645Frame->DataBuf[1],p645Frame->DataBuf[0],(p645Frame->DataBuf)+8,databuf+4,(p645Frame->Datalong)-0x08,&datalong))
				{
					case 0://返回正常
					{
						datalong += 4;
						control = 0x83;
						flag_respondent = 1;
					}
					break;
					case 1://其他错误
					{
						datalong = 2;
						control = 0xc3;
						databuf[0] = 0x01;
						databuf[1] = 0x00;
						flag_respondent = 1;
					}
					break;
					case 2://重复充值
					{
						datalong = 2;
						control = 0xc3;
						databuf[0] = 0x02;
						databuf[1] = 0x00;
						flag_respondent = 1;
					}
					break;
					case 3://esam验证失败
					{
						datalong = 2;
						control = 0xc3;
						databuf[0] = 0x04;
						databuf[1] = 0x00;
						flag_respondent = 1;
					}
					break;
					case 4://身份认证失败
					{
						datalong = 2;
						control = 0xc3;
						databuf[0] = 0x08;
						databuf[1] = 0x00;
						flag_respondent = 1;
					}
					break;
					case 5://客户编号不匹配
					{
						datalong = 2;
						control = 0xc3;
						databuf[0] = 0x10;
						databuf[1] = 0x00;
						flag_respondent = 1;
					}
					break;
					case 6://充值次数错
					{
						datalong = 2;
						control = 0xc3;
						databuf[0] = 0x20;
						databuf[1] = 0x00;
						flag_respondent = 1;
					}
					break;
					case 7://购电超囤积
					{
						datalong = 2;
						control = 0xc3;
						databuf[0] = 0x40;
						databuf[1] = 0x00;
						flag_respondent = 1;
					}
					break;
					default:
					{
						datalong = 2;
						control = 0xc3;
						databuf[0] = 0x01;
						databuf[1] = 0x00;
						flag_respondent = 1;
					}
					break;
				}
			} 	
		}
		break;
#endif

		case 0x11: //主站请求
		{
			if(p645Frame->Datalong == 4){
				//依据标识读取数据
				databuf[0] = p645Frame->DataBuf[0];
				databuf[1] = p645Frame->DataBuf[1];
				databuf[2] = p645Frame->DataBuf[2];
				databuf[3] = p645Frame->DataBuf[3]; 

				switch(p645Frame->DataBuf[3])
				{
					case 0:	 //电能量
					{
						switch(com_read_energy_data(p645Frame->DataBuf[2],p645Frame->DataBuf[1],p645Frame->DataBuf[0],databuf+4,&datalong))
						{
							case 0:
							{
								datalong = 1;
								control = 0xd1;
								databuf[0] = 2;
								flag_respondent = 1;
							}
							break;
							case 1:
							{
								datalong += 4;
								control = 0x91;
								flag_respondent = 1;
							}
							break;
							default:
							{
						   		datalong = 1;
								control = 0xd1;
								databuf[0] = 1;
								flag_respondent = 1;
							}
							break;
						}
					}
					break;
					case 1:	 //需量
					{
						switch(com_read_demand_data(p645Frame->DataBuf[2],p645Frame->DataBuf[1],p645Frame->DataBuf[0],databuf+4,&datalong))
						{
							case 0:
							{
								datalong = 1;
								control = 0xd1;
								databuf[0] = 2;
								flag_respondent = 1;
							}
							break;
							case 1:
							{
								datalong += 4;
								control = 0x91;
								flag_respondent = 1;
							}
							break;
							default:
							{
						   		datalong = 1;
								control = 0xd1;
								databuf[0] = 1;
								flag_respondent = 1;
							}
							break;
						}
					}
					break;
					case 2:	 //变量
					{
						switch(com_read_variable_data(p645Frame->DataBuf[2],p645Frame->DataBuf[1],p645Frame->DataBuf[0],databuf+4,&datalong))
						{
							case 0:
							{
								datalong = 1;
								control = 0xd1;
								databuf[0] = 2;
								flag_respondent = 1;
							}
							break;
							case 1:
							{
								datalong += 4;
								control = 0x91;
								flag_respondent = 1;
							}
							break;
							default:
							{
						   		datalong = 1;
								control = 0xd1;
								databuf[0] = 1;
								flag_respondent = 1;
							}
							break;
						}
					}
					break;
					case 3:	 //事件记录
					{
						switch(com_read_event_data(DevNum,p645Frame->DataBuf[2],p645Frame->DataBuf[1],p645Frame->DataBuf[0],databuf+4,&datalong))
						{
							case 0:
							{
								datalong = 1;
								control = 0xd1;
								databuf[0] = 2;
								flag_respondent = 1;
							}
							break;
							case 1:
							{
								datalong += 4;
								control = 0x91;
								flag_respondent = 1;
							}
							break;
							case 3:
							{
								datalong += 4;
								control = 0xb1;
								flag_respondent = 1;
							}
							break;
							default:
							{
								datalong = 1;
								control = 0xd1;
								databuf[0] = 1;
								flag_respondent = 1;
							}
							break;
						}
					}
					break;
					case 4:	 //参变量
					{
				   		switch(com_read_parameter_data(p645Frame->DataBuf[2],p645Frame->DataBuf[1],p645Frame->DataBuf[0],databuf+4,&datalong))
						{
							case 0:
							{
								datalong = 1;
								control = 0xd1;
								databuf[0] = 2;
								flag_respondent = 1;
							}
							break;
							case 1:
							{
								datalong += 4;
								control = 0x91;
								flag_respondent = 1;
							}
							break;
							default:
								datalong = 1;
								control = 0xd1;
								databuf[0] = 1;
								flag_respondent = 1;
							break;
						}
					}
					break;
					case 5:	 //冻结
					{
						switch(com_read_freeze_data(p645Frame->DataBuf[2],p645Frame->DataBuf[1],p645Frame->DataBuf[0],databuf+4,&datalong))
						{
					   		case 1:
							{
								datalong += 4;
								control = 0x91;
								flag_respondent = 1;
							}
							break;
							case 0:
							{
						   		datalong = 1;
								control = 0xd1;
								databuf[0] = 2;
								flag_respondent = 1;
							}
							break;
							case 3:
							{
								datalong += 4;
								control = 0xb1;
								flag_respondent = 1;
							}
							break;
							default:
							{
								datalong = 1;
								control = 0xd1;
								databuf[0] = 1;
								flag_respondent = 1;
							}
							break;

						}
					}
					break;
					case 0x10:		  //__________________________-国网规约事件
					case 0x11:
					case 0x12:
					case 0x13:
					case 0x14:
					case 0x15:
					case 0x16:
					case 0x17:
					case 0x18:
					case 0x19:
					case 0x1A:
					case 0x1B:
					case 0x1C:
					case 0x1D:
					case 0x1E:
					case 0x1F:
					{
						switch(com_read_state_event_data(DevNum,p645Frame->DataBuf[3],p645Frame->DataBuf[2],p645Frame->DataBuf[1],p645Frame->DataBuf[0],databuf+4,&datalong))
						{
							case 0:
							{
								datalong = 1;
								control = 0xd1;
								databuf[0] = 2;
								flag_respondent = 1;
							}
							break;
							case 1:
							{
								datalong += 4;
								control = 0x91;
								flag_respondent = 1;
							}
							break;
							case 3:
							{
								datalong += 4;
								control = 0xb1;
								flag_respondent = 1;
							}
							break;
							default:
							{
								datalong = 1;
								control = 0xd1;
								databuf[0] = 1;
								flag_respondent = 1;
							}
							break;
						}
					}
					break;
					default:
					break;
				}
			}else if(p645Frame->Datalong == 5){	//读取负荷
				databuf[0] = p645Frame->DataBuf[0];
				databuf[1] = p645Frame->DataBuf[1];
				databuf[2] = p645Frame->DataBuf[2];
				databuf[3] = p645Frame->DataBuf[3];
//				databuf[4] = p645Frame->DataBuf[4];
				if((p645Frame->DataBuf[3]) == 6){
					switch(com_read_load_data(1,(p645Frame->DataBuf)+4,p645Frame->DataBuf[2],p645Frame->DataBuf[1],p645Frame->DataBuf[0],databuf+4,&datalong))
					{
						case 0:
						{
							datalong = 1;
							control = 0xd1;
							databuf[0] = 2;
							flag_respondent = 1;
						}
						break;
						case 1:
						{
							datalong += 4;
							control = 0x91;
							flag_respondent = 1;
						}
						break;
						default:
						{
							datalong = 1;
							control = 0xd1;
							databuf[0] = 1;
							flag_respondent = 1;
						}
						break;
					}	
				
				}	

			}else if(p645Frame->Datalong == 10){ //读取给定时间负荷
				databuf[0] = p645Frame->DataBuf[0];
				databuf[1] = p645Frame->DataBuf[1];
				databuf[2] = p645Frame->DataBuf[2];
				databuf[3] = p645Frame->DataBuf[3];
				if((p645Frame->DataBuf[3]) == 6){
					switch(com_read_load_data(2,(p645Frame->DataBuf)+4,p645Frame->DataBuf[2],p645Frame->DataBuf[1],p645Frame->DataBuf[0],databuf+4,&datalong))
					{
						case 0:
						{
							datalong = 1;
							control = 0xd1;
							databuf[0] = 2;
							flag_respondent = 1;
						}
						break;
						case 1:
						{
							datalong += 4;
							control = 0x91;
							flag_respondent = 1;
						}
						break;
						default:
						{
							datalong = 1;
							control = 0xd1;
							databuf[0] = 1;
							flag_respondent = 1;
						}
						break;
					}	
				}
			}
		}
		break;
		case 0x12://读后续数据
		{
			if(p645Frame->Datalong == 5){
				//依据标志和帧序号读取后续数据
			
			   	databuf[0] = p645Frame->DataBuf[0];
				databuf[1] = p645Frame->DataBuf[1];
				databuf[2] = p645Frame->DataBuf[2];
				databuf[3] = p645Frame->DataBuf[3];	
				switch(comm_read_after_frame_data(DevNum,(p645Frame->DataBuf[0]),((p645Frame->DataBuf[1])),(p645Frame->DataBuf[2]),(p645Frame->DataBuf[3]),p645Frame->DataBuf[4],databuf+4,&datalong)){
					case 1:
					{
						control = 0x92;
						datalong += 5;
						databuf[datalong-1] = p645Frame->DataBuf[4];
						flag_respondent =1;
					}
					break;
					case 3:
					{
						control = 0xB2;
						datalong += 5;
						databuf[datalong-1] = p645Frame->DataBuf[4];
						flag_respondent =1;
					}
					break;
					case 0:
					{
						control = 0xd2;
						datalong = 1;
						databuf[0] = 2;
						flag_respondent =1;
					}
					break;
					default:
					{
						datalong = 1;
						control = 0xd2;
						databuf[0] = 1;
						flag_respondent = 1;
					}
					break;
				}
			}
		}
		break;
		case 0x14://设置数据
		{
			if(p645Frame->Datalong > 0x0c){
				
				switch(p645Frame->DataBuf[4])
				{
					case 0x02:	   //编程键打开//密码正确//操作员代码正确
					case 0x04:
					{
						if((meter_state_info.meter_run_state[2]&0x8)&&(check_password(&(p645Frame->DataBuf[5]),p645Frame->DataBuf[4]))&&(amend_handler_num(&(p645Frame->DataBuf[8])))){
							if((p645Frame->DataBuf[3]) == 4){
								switch(comm_write_parameter_data(p645Frame->DataBuf[2],p645Frame->DataBuf[1],p645Frame->DataBuf[0],(p645Frame->DataBuf)+12,(p645Frame->Datalong)-0x0c))
								{	
									case 0:
									{
										control = 0xd4;
										datalong = 1;
										databuf[0] = 1;
										flag_respondent =1;
									}
									break;
									case 1:
									{
										control = 0x94;
										datalong = 0;
										flag_respondent =1;
										save_programe_note(password_handler.handler_num,((p645Frame->DataBuf[3])<<24)+((p645Frame->DataBuf[2])<<16)+((p645Frame->DataBuf[1])<<8)+(p645Frame->DataBuf[0]));
									}
									break;
									default:
									{
										control = 0xd4;
										datalong = 1;
										databuf[0] = 1;
										flag_respondent =1;
									}
									break;
								}
							}
						}else{
							control = 0xd4;
							datalong = 1;
							databuf[0] = 4;
							flag_respondent =1;
						}
					}
					break;

#if HAVE_ESAM == 1
					case 0x99:	  //一类参数 明文+mac 存到esam和eeprom中
					{
						//在身份认证时效内
						if(((extern_control.flag&(IDENTITY_AUTHEN_BIT|IDENTITY_AUTHEN_TIME_BIT)) == (IDENTITY_AUTHEN_BIT|IDENTITY_AUTHEN_TIME_BIT))&&(amend_handler_num(&(p645Frame->DataBuf[8])))){
							if((p645Frame->DataBuf[3]) == 4){
								switch(comm_write_parameter_99(p645Frame->DataBuf[2],p645Frame->DataBuf[1],p645Frame->DataBuf[0],(p645Frame->DataBuf)+12,(p645Frame->Datalong)-0x0c))
								{	
									case 0:
									{
										control = 0xd4;
										datalong = 1;
										databuf[0] = 1;
										flag_respondent =1;	
									}
									break;
									case 1:
									{
										control = 0x94;
										datalong = 0;
										flag_respondent =1;
										save_programe_note(password_handler.handler_num,((p645Frame->DataBuf[3])<<24)+((p645Frame->DataBuf[2])<<16)+((p645Frame->DataBuf[1])<<8)+(p645Frame->DataBuf[0]));
									}
									break;
									default:
									{
										control = 0xd4;
										datalong = 1;
										databuf[0] = 1;
										flag_respondent =1;
									}
									break;
								}
							}
						}else{
							control = 0xd4;
							datalong = 1;
							databuf[0] = 4;
							flag_respondent =1;
						}
					}
					break;
					case 0x98:	 //二类参数 密文+mac 解密后存到eeprom
					{
						//在身份认证时效内
						if(((extern_control.flag&(IDENTITY_AUTHEN_BIT|IDENTITY_AUTHEN_TIME_BIT)) == (IDENTITY_AUTHEN_BIT|IDENTITY_AUTHEN_TIME_BIT))&&(amend_handler_num(&(p645Frame->DataBuf[8])))){
					   		if((p645Frame->DataBuf[3]) == 4){
								switch(comm_write_parameter_98(p645Frame->DataBuf[2],p645Frame->DataBuf[1],p645Frame->DataBuf[0],(p645Frame->DataBuf)+12,(p645Frame->Datalong)-0x0c))
								{	
									case 0:
									{
										control = 0xd4;
										datalong = 1;
										databuf[0] = 1;
										flag_respondent =1;
									}
									break;
									case 1:
									{
										control = 0x94;
										datalong = 0;
										flag_respondent =1;
										save_programe_note(password_handler.handler_num,((p645Frame->DataBuf[3])<<24)+((p645Frame->DataBuf[2])<<16)+((p645Frame->DataBuf[1])<<8)+(p645Frame->DataBuf[0]));
									}
									break;
									default:
									{
										control = 0xd4;
										datalong = 1;
										databuf[0] = 1;
										flag_respondent =1;
									}
									break;
								}
							}	
						}else{
							control = 0xd4;
							datalong = 1;
							databuf[0] = 4;
							flag_respondent =1;
						}
					}
					break;
#endif

					default:
					break;
				}	
			}
		}
		break;
		case 0x16: //冻结
		{
			if(p645Frame->Datalong == 4){
		   		//设置冻结
				if(check_all_same(&(p645Frame->DataBuf[0]),0x99,4)){
					//瞬时冻结
					if(instantaneous_freeze()){
						control = 0x96;
						datalong = 0;
						flag_respondent =1;
					}else{
				   		control = 0xd6;
						datalong = 1;
						databuf[0] = 1;
						flag_respondent =1;
					}
				}else{
					//设置定时冻结时间
					//判别时间是否合法
					//保存定时冻结时间
					//更新冻结时间
					for(i=0;i<4;i++){
						freeze_config.dingshi_freeze_time[i] = BCDToDec(p645Frame->DataBuf[i]);
					}
					for(i=0;i<5;i++){
						freeze_config.last_dingshi_freeze_time[i] = 0;
					}
					control = 0x96;
					datalong = 0;
					flag_respondent =1;
				}

			}
		}
		break;
		case 0x17: //更改通讯波特率
		{
			if(p645Frame->Datalong == 1){
			
				//保存通讯速率特征字
				if((check_baud_Z(p645Frame->DataBuf[0]))&&writeItem(4,0,7,get_speed_addr(DevNum),&(p645Frame->DataBuf[0]))){
					if(!UART_Init (DevNum, get_uart_speed(DevNum,p645Frame->DataBuf[0]),0,0, WordLength8, 0, 1, ParitySelEven, 0, 1, FIFORXLEV3,1, IER_RBR)){
						meter_state_info.meter_err &= ~0x08;
						control = 0x97;
						datalong = 1;
						databuf[0] = p645Frame->DataBuf[0];
						flag_respondent =1;
					}else{
						//更改为以前的通讯速率？
						meter_state_info.meter_err |= 0x08;
						control = 0xD7;
						datalong = 1;
						databuf[0] = meter_state_info.meter_err;	//错误信息字
						flag_respondent =1;
					}
				}else{
						//更改为以前的通讯速率？
					meter_state_info.meter_err |= 0x08;
					control = 0xD7;
					datalong = 1;
					databuf[0] = meter_state_info.meter_err;	//错误信息字
					flag_respondent =1;
				}			
					
			}
		}
		break;
		case 0x18: //修改密码
		{
			if(p645Frame->Datalong == 0x0c){
				if((meter_state_info.meter_run_state[2]&0x8)&&(amend_password(p645Frame->DataBuf[0],p645Frame->DataBuf[1],p645Frame->DataBuf[2],p645Frame->DataBuf[3],&(p645Frame->DataBuf[5]),p645Frame->DataBuf[4],&(p645Frame->DataBuf[9]),p645Frame->DataBuf[8]))){
					password_handler.handler_num= 0x0ffffffff;
					control = 0x98;
					datalong = 0;
					flag_respondent =1;	
				}else{
					control = 0xd8;
					datalong = 1;
					databuf[0] = 4;
					flag_respondent =1;
				}
			}
		}
		break;
		case 0x19: //需量清零
		{
			if(p645Frame->Datalong == 0x08){
		   		if((meter_state_info.meter_run_state[2]&0x8)&&((p645Frame->DataBuf[0])<=4)&&(check_password(&(p645Frame->DataBuf[1]),p645Frame->DataBuf[0]))&&(amend_handler_num(&(p645Frame->DataBuf[4])))){
					clear_zero.demand_clear_zero.flag = 1;
					control = 0x99;
					datalong = 0;
					flag_respondent =1;
				}else{
					control = 0xd9;
					datalong = 1;
					databuf[0] = 4;
					flag_respondent =1;
				}
			}
		}
		break;
		case 0x1A: //电表清零
		{
			if(p645Frame->Datalong == 0x08){
				//编程按键打开
		   		//密码正确 权限正确
				if((meter_state_info.meter_run_state[2]&0x8)&&((p645Frame->DataBuf[0])<=2)&&(check_password(&(p645Frame->DataBuf[1]),p645Frame->DataBuf[0]))&&(amend_handler_num(&(p645Frame->DataBuf[4])))){
					 //处理电表清零事件
					control = 0x9a;
					datalong = 0;
					flag_respondent =1;
					//置电表清零标志
					clear_zero.meter_clear_zero.flag = 0xaa;
						 
				}else{
					control = 0xda;
					datalong = 1;
					databuf[0] = 4;
					flag_respondent =1;
				}
			}
		}
		break;
		case 0x1B: //事件清零
		{
			if(p645Frame->Datalong == 0x0c){
		   		//密码正确 权限正确
				if((meter_state_info.meter_run_state[2]&0x8)&&((p645Frame->DataBuf[0])<=2)&&(check_password(&(p645Frame->DataBuf[1]),p645Frame->DataBuf[0]))&&(amend_handler_num(&(p645Frame->DataBuf[4])))){
						
						control = 0x9b;
						datalong = 0;
						flag_respondent =1;	
						//置事件清零标志
						clear_zero.event_clear_zero.flag = 1;
						clear_zero.event_clear_zero.data_id[0] = p645Frame->DataBuf[8];
						clear_zero.event_clear_zero.data_id[1] = p645Frame->DataBuf[9];
						clear_zero.event_clear_zero.data_id[2] = p645Frame->DataBuf[10];
						clear_zero.event_clear_zero.data_id[3] = p645Frame->DataBuf[11];
					
				}else{
					control = 0xdb;
					datalong = 1;
					databuf[0] = 4;
					flag_respondent =1;
				}

			}
		}
		break;

#if HAVE_ESAM == 1
		case 0x1c: //安全命令 合闸 报警 保电
		{
			if(p645Frame->DataBuf[0] == 2){

			}else if(p645Frame->DataBuf[0] == 0x98){
				if((p645Frame->Datalong == 0x1c)&&(amend_handler_num(&(p645Frame->DataBuf[4])))){
					U8 buf[10],len = 0;
					switch(remote_control(p645Frame->DataBuf+8,p645Frame->Datalong-8,buf,&len)){
						case 1:
						{
							if((len == 8)&&(command_0x1c_deal(buf,1))){
								control = 0x9c;
								datalong = 0;
								flag_respondent =1;
							}else{
								control = 0xdc;
								datalong = 1;
								databuf[0] = 1;
								flag_respondent =1;
							}
							
						}
						break;
						default:
						{
							control = 0xdc;
							datalong = 1;
							databuf[0] = 4;
							flag_respondent =1;
						}
						break;
					}
				}
			}
				
		}
		break;
#endif
		case 0x1d: //多功能端子输出
		{
			if(p645Frame->Datalong == 0x01){
				switch(p645Frame->DataBuf[0])
				{
					case 0://时钟秒脉冲
					{
						FOE_Flag = 0;
						Multiple_Port_Flag = 0;
						control = 0x9d;
						datalong = 1;
						databuf[0] = p645Frame->DataBuf[0];
						flag_respondent =1;
					}
					break;
					case 1://需量周期
					{
						FOE_Flag = 1;
						Multiple_Port_Flag = 0;
						control = 0x9d;
						datalong = 1;
						databuf[0] = p645Frame->DataBuf[0];
						flag_respondent =1;
					}
					break;
					case 2://时段投切
					{
						FOE_Flag = 2;
						Multiple_Port_Flag = 0;
						control = 0x9d;
						datalong = 1;
						databuf[0] = p645Frame->DataBuf[0];
						flag_respondent =1;
					}
					break;
					default:
					{
						control = 0xdd;
						datalong = 1;
						databuf[0] = p645Frame->DataBuf[0];
						flag_respondent =1;
					}
					break;
				}
			}	
		}
		break;
		case 0x0ea:	//调校
		{
			//密码操 作者代码 编程键 初始短路环
			if((((p645Frame->DataBuf[4])+((p645Frame->DataBuf[5])<<8)+((p645Frame->DataBuf[6])<<16)+((p645Frame->DataBuf[7])<<24)) == 0x66666600)&&(((p645Frame->DataBuf[8])+((p645Frame->DataBuf[9])<<8)+((p645Frame->DataBuf[10])<<16)+((p645Frame->DataBuf[11])<<24)) == 0x88888888)&&(meter_state_info.meter_run_state[2]&0x8)&&(READ_FAC_STA == 0)){
				if(p645Frame->Datalong == 0x0c){
					if(((p645Frame->DataBuf[0])+((p645Frame->DataBuf[1])<<8)+((p645Frame->DataBuf[2])<<16)+((p645Frame->DataBuf[3])<<24)) == 0x0eaf0f0f0){
					//进入base状态 
						os_tsk_create (ade7878_adjust_zero,5);//进入base状态
						control = 0x06a;
						datalong = 0;
						flag_respondent =1;
					}else if(((p645Frame->DataBuf[0])+((p645Frame->DataBuf[1])<<8)+((p645Frame->DataBuf[2])<<16)+((p645Frame->DataBuf[3])<<24)) == 0x0eaf0f0f3){
						databuf[0] = p645Frame->DataBuf[0];
						databuf[1] = p645Frame->DataBuf[1];
						databuf[2] = p645Frame->DataBuf[2];
						databuf[3] = p645Frame->DataBuf[3];
						if(ade7878_adjust_read_base(databuf+4,&datalong)){
					   		datalong += 4;
							control = 0x06a;
							flag_respondent = 1;
						}else{
							datalong = 1;
							control = 0x0ea;
							databuf[0] = 2;
							flag_respondent = 1;
						}
					
					}
					
				}else if(p645Frame->Datalong == (0x0c+98)){
					if(((p645Frame->DataBuf[0])+((p645Frame->DataBuf[1])<<8)+((p645Frame->DataBuf[2])<<16)+((p645Frame->DataBuf[3])<<24)) == 0x0eaf0f0f1){

						if((ade7878_adjust_gain((p645Frame->DataBuf)+12,98))&&(ade7878_adjust_write_base((p645Frame->DataBuf)+12,98))){
							datalong = 0;
							control = 0x06a;
							flag_respondent = 1;
						}else{
					   		datalong = 1;
							control = 0x0ea;
							databuf[0] = 2;
							flag_respondent = 1;
						}

					}
				}
			}else{
				datalong = 1;
				control = 0x0ea;
				databuf[0] = 4;
				flag_respondent = 1;
			}	
		}
		break;
		case 0x20://测试专业
		{
			//测试逆相序
//			if(p645Frame->Datalong == 0x1){
//				if((p645Frame->DataBuf[0]) == 0){
//					alternate_with_info.flag = 1;
//				}else{
//					alternate_with_info.flag = 0;
//				}
//			}
		}
		break;
		default:
		break;
	}
   	if(flag_respondent){
   		Send_645_Format(DevNum,meter_state_info.meter_comm_addr,control,databuf,datalong);
	}
	return 1;
}
/*********************************************************************************************************
** 函数名称:     process_uart0_protocol
** 功能描述:     串口0 645数据处理
** 输　入:       无
**
** 输　出:       无
**				 
** 全局变量: 
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
extern U8 comm_display_state;
__task void process_uart_protocol(void)
{
	
	U16 evt_uart_flag;

	InitUart645Frame(UART0);
	InitUart645Frame(UART1);
	InitUart645Frame(UART3);
	
	UART_Init (UART0, get_uart_speed(UART0,uart0_645frame.speed_state),0,0, WordLength8, 0, 1, ParitySelEven, 0, 1, FIFORXLEV3,1, IER_RBR);
	UART_Init (UART1, get_uart_speed(UART1,uart1_645frame.speed_state),0,0, WordLength8, 0, 1, ParitySelEven, 0, 1, FIFORXLEV3,1, IER_RBR);
	UART_Init (UART3, get_uart_speed(UART3,uart3_645frame.speed_state),0,0, WordLength8, 0, 1, ParitySelEven, 0, 1, FIFORXLEV3,1, IER_RBR);

	read_password();

	for(;;)
	{
		os_evt_wait_or(EVT_UART0_PROCESS|EVT_UART1_PROCESS|EVT_UART3_PROCESS,0xffff);

		evt_uart_flag = os_evt_get();
		switch(evt_uart_flag)
		{
			case EVT_UART0_PROCESS:
			{
				switch(uart0_645frame.HandleState){
					case 1:
						process_645_protocol(UART0,&uart0_645frame);
						comm_display_state &= (~0xc);
						comm_display_state |= 0x4;
					break;
					case 2:
						process_645_BroadcastA(UART0,&uart0_645frame);
						comm_display_state &= (~0xc);
						comm_display_state |= 0x4;
					break;
					case 3:
					   	process_645_Broadcast9(UART0,&uart0_645frame);
						comm_display_state &= (~0xc);
						comm_display_state |= 0x4;
					break;
					default:
					break;
		
				}
				uart0_645frame.HandleState = 0;
			}
			break;
			case EVT_UART1_PROCESS:
			{
				switch(uart1_645frame.HandleState){
					case 1:
						process_645_protocol(UART1,&uart1_645frame);
						comm_display_state &= (~0x30);
						comm_display_state |= 0x10;
					break;
					case 2:
						process_645_BroadcastA(UART1,&uart1_645frame);
						comm_display_state &= (~0x30);
						comm_display_state |= 0x10;
					break;
					case 3:
					   	process_645_Broadcast9(UART1,&uart1_645frame);
						comm_display_state &= (~0x30);
						comm_display_state |= 0x10;
					break;
					default:
					break;
		
				}
				uart1_645frame.HandleState = 0;
			}
			break;
			case EVT_UART3_PROCESS:
			{
				switch(uart3_645frame.HandleState){
					case 1:
						process_645_protocol(UART3,&uart3_645frame);
						comm_display_state &= (~3);
						comm_display_state |= 1;
					break;
					case 2:
						process_645_BroadcastA(UART3,&uart3_645frame);
						comm_display_state &= (~3);
						comm_display_state |= 1;
					break;
					case 3:
					   	process_645_Broadcast9(UART3,&uart3_645frame);
						comm_display_state &= (~3);
						comm_display_state |= 1;
					break;
					default:
					break;
		
				}
				uart3_645frame.HandleState = 0;
			}
			break;
			default:
			break;
		}
		
	}
}
