/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述: 负荷记录，使用RL_ARM版本：4.13                                  **/
/*****************************************************************************/
#ifndef __LOAD_H
#define __LOAD_H

	#define FIRST_KIND_BLOCK 23
	#define SECOND_KIND_BLOCK 30
	#define THIRD_KIND_BLOCK 14
	#define FOURTH_KIND_BLOCK 22
	#define FIFTH_KIND_BLOCK 22
	#define SIXTH_KIND_BLOCK 12
	typedef enum {
		first_kind_load = 0,		//第一类负载
		second_kind_load,			//第二类负载
		third_kind_load,			//第三类负载
		fourth_kind_load,			//第四类负载
		fifth_kind_load,			//第五类负载
		sixth_kind_load,			//第六类负载
	}load_type_t;
	
	#define MAX_LOAD_NOTE_NUM 	57600

	#define FIRST_KIND_LOAD_ADDR 0
	#define SECOND_KIND_LOAD_ADDR 0x150000				
	#define THIRD_KIND_LOAD_ADDR 0x300000
	#define FOURTH_KIND_LOAD_ADDR 0x3D0000
	#define FIFTH_KIND_LOAD_ADDR 0x510000
	#define SIXTH_KIND_LOAD_ADDR 0x650000

	#define MAX_LOAD_TYPE 6

	typedef struct{
		U32 load_save_num;
		U16	load_interval;
		U8 last_load_save_time[5];//分时日月年 
	}load_save_t;

	typedef struct{
		U8 load_start_time[4];
		U8 lode_mode;
		load_save_t load_save_config[MAX_LOAD_TYPE];
	}load_config_t; 

	extern U8 check_load_position(void);
	extern U8 check_load(void);
	extern U32 read_load_data(U8 *loaddata,U8 n,U8 type);
	extern U8 init_load(void);
	extern load_config_t load_config;

#endif 

