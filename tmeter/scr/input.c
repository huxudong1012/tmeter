/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#include <RTL.h>                      /* RTX kernel functions & defines      */
#include <LPC17xx.H>                  /* LPC17xx definitions                 */
#include "input.h"                      
#include "lcd.h"
#include "meter_output.h"
#include "time_period_zone.h"
#include "power_check.h"
#include "config.h"
#include "uart_task.h"
#include "RX8025t.h"



U8 input[7];					//用于标示各个输入按键的状态
U32 key_program_time = 0;       //按键有效时间
U8 key_program = 0;

/*********************************************************************************************************
** 函数名称: 按键检测 
** 功能描述: 检测两按键
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 check_key_value(void)
{
	static U8 up_value = 0,down_value = 0;
	static U8 up_flag = 0,down_flag = 0;
	
	up_value <<= 1;
	down_value <<= 1;
	if((READ_KEYUP)){
		up_value |= 1;	
	}
	if((READ_KEYDOWN)){
		down_value |= 1;
	}
	
	if(up_flag){ //检测到下降沿
		if((up_value&0x0f) == 0x0f){
			up_flag = 0;	
		}else{
			up_flag++;
			if(up_flag>40){
				up_flag = 1;
				input[KeyUp]++;
			}	
		}
	}else{
		if(((up_value&0x0f) == 0x0c)||((up_value&0x0f) == 0x00)){
			up_flag ++;
			input[KeyUp]++;
		}
	}
	
	if(down_flag){ //检测到下降沿
		if((down_value&0x0f) == 0x0f){
			down_flag = 0;	
		}else{
			down_flag++;
			if(down_flag>40){	 //没过400ms 键盘输入次数自加
				down_flag =1;
				input[KeyDown]++;
			}	
		}
	}else{	//上升沿检测
		if(((down_value&0x0f) == 0x0c)||((down_value&0x0f) == 0x00)){
			down_flag ++;
			input[KeyDown]++;
		}
	}
	
	return 1;			
}
/*********************************************************************************************************
** 函数名称: check_input_edge 
** 功能描述: 检查输入信号的边沿
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 check_input_edge(void)
{
	static U8 program_value = 0;
	static U8 program_flag = 0;
	//编程键

	program_value <<= 1;
	if((READ_KEYPROGRAM)){
		program_value |= 1;
	}
	if(program_flag&0x80){ //检测到下降沿
		if((program_value&0x0f) == 0x0f){
			program_flag = 0;	
		}
	}else{	//上升沿检测
		if(((program_value&0x0f) == 0x0c)||((program_value&0x0f) == 0x00)){
			program_flag |= 0x80;
			input[KeyProgram]++;
		}
	}
	return 1;

}
/*********************************************************************************************************
** 函数名称: checkKeyInput
** 功能描述: 输入检测，没10ms检测一次状态100ms更新一次结果
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void checkKeyInput(void){

	static U8 count = 0;
   	static U8 top_value = 0,trail_value = 0,iccard_value = 0,relay_value = 0;;

	if(!(READ_KEYTOP)) 		top_value++;
	if(!(READ_KEYTRAIL)) 	trail_value++;
	if((READ_KEYIC)) 		iccard_value++;
	if((READ_RELATSTATE)) 	relay_value++;

	count++;

	if(count>10){
   		count = 0;
		if(top_value>6){
			input[KeyTop] = 1;	
		}else{
			input[KeyTop] = 0;
		}
		top_value = 0;
		if(trail_value>6){
			input[KeyTrail] = 1;	
		}else{
			input[KeyTrail] = 0;
		}
		trail_value = 0;
		if(iccard_value>6){
			input[IcCard] = 1;	
		}else{
			input[IcCard] = 0;
		}
		iccard_value = 0;
		if(relay_value>8){
			input[RelayState] = 1;	
		}else{
			input[RelayState] = 0;
		}
		relay_value = 0;
		
	}
}


/*********************************************************************************************************
** 函数名称: judgeInput
** 功能描述: 输入检测结果处理
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
void judgeInput(void){
	static U16 relay_time = 0;
	static U8 count = 0;
	count++;
	if(count>20){
		count = 0;
		if(output_control.relay_deal == 1){
			
			if((input[KeyUp])&&(input[KeyDown])){//合闸操作
				input[KeyUp]--;
				input[KeyDown]--;
	
				//时间处理
				relay_time++;
				if(relay_time == 10){
					output_control.relay_deal = 0;
					Display_Ark_Flag = 0;
					Hold_Relay_Flag = 1;
					meter_state_info.meter_run_state[2] &= (~0x40);
					relay_time = 0;
				}
			}
			else	relay_time = 0;
		}
			
		if(input[KeyUp] && (!relay_time)){			//KeyUp按键按下
			//相应程序处理
			input[KeyUp]--;
			lcd_control.display_time = TimeTick;
			if(lcd_control.display_mode == 1){
				lcd_control.display_mode = 2;
				lcd_control.display_num = 1;
				lcd_all_clear();
				
			}else{
				lcd_control.display_num++;
				if(lcd_control.display_num > lcd_control.key_display_num){
					lcd_control.display_num = 1;
				}
			}
			check_display_item(lcd_control.display_mode);
			
		}
		//其它按键处理程序
		if(input[KeyDown] && (!relay_time)){			//KeyDown按键按下
			//相应程序处理
			input[KeyDown]--;
			lcd_control.display_time = TimeTick;
			if(lcd_control.display_mode == 1){
				lcd_control.display_mode = 2;
				lcd_all_clear();
				lcd_control.display_num = 1;
				
			}else{
				if(lcd_control.display_num == 1){
					lcd_control.display_num = lcd_control.key_display_num;
				}else{
					lcd_control.display_num--;
				}
			}
			check_display_item(lcd_control.display_mode);
			
		}
	}
	
	//编程按键
	if(input[KeyProgram]){
   		input[KeyProgram]--;
		key_program++;
		key_program_time = currentClock[5]*365*24*60+currentClock[4]*30*24*60+currentClock[3]*24*60+currentClock[2]*60+currentClock[1];
	}
	if((key_program&0x1)){
		if((currentClock[5]*365*24*60+currentClock[4]*30*24*60+currentClock[3]*24*60+currentClock[2]*60+currentClock[1]-key_program_time)>=120){
			key_program_time = 0; 
			key_program = 0;
		}
		
	}else{
		key_program = 0;
		key_program_time = 0;	
	}

	if(key_program&0x01){
		meter_state_info.meter_run_state[2] |= 0x8;	
	}else{
		meter_state_info.meter_run_state[2] &= (~0x8);
	}
	//继电器状态检测
	if(input[RelayState]){
   		meter_state_info.meter_run_state[2] |= 0x10;	
	}else{
		meter_state_info.meter_run_state[2] &= (~0x10);
	}
	//扩展状态
	//保电状态
	if(output_control.keep_deal == 0x3a){
		meter_state_info.meter_run_state[0] |= 0x8000;
	}else{
		meter_state_info.meter_run_state[0] &= 0x7fff;	
	}
	//欲跳闸报警状态
	if(output_control.alarm_deal == 0x2a){
		meter_state_info.meter_run_state[2] |= 0x80;
	}else{
		meter_state_info.meter_run_state[2] &= (~0x80);	
	}

}
/*********************************************************************************************************
** 函数名称: init_input
** 功能描述: 初始化按键
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
U8 init_input(void)
{
	return 1;
}

/*********************************************************************************************************
** 函数名称: Input
** 功能描述: 输入检测任务
** 输　入: 无
** 输　出: 无
** 全局变量: 无
** 调用模块: 
**
** 作　者: 
** 日　期: 
**-------------------------------------------------------------------------------------------------------
** 修改人: 
** 日　期: 
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
			
__task void Input (void) {
	
	init_input();

	for(;;){
		check_key_value();
		check_input_edge();
		checkKeyInput();
		judgeInput();

		os_dly_wait(1);			//每10ms读取一次按键
	}
}


