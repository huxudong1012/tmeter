/*****************************************************************************/
/*********************************Copyright (C)*******************************/
/**                            Company Name                                 **/
/**                              技 术 部                                   **/
/*****************************************************************************/
/*****************************************************************************/

/******************文件信息***************************************************/
/** 文件名: filename.c                                                      **/
/** 创建人:                                                                 **/
/** 修改日:                                                                 **/
/** 描  述: 基于lpc17xx  操纵系统选用keil_RTX                               **/
/******************历史版本信息***********************************************/
/** 创建人:                                                                 **/
/** 版  本:                                                                 **/
/** 创建日:                                                                 **/
/** 描　述:                                                                 **/
/******************当前版本修订***********************************************/
/** 修改人:                                                                 **/
/** 修改日:                                                                 **/
/** 描　述:                                                                 **/
/*****************************************************************************/

#ifndef __ESAM_CARD_H
#define __ESAM_CARD_H



//esam_rst
#define EASM_RST_SEL    (LPC_PINCON->PINSEL1 &= ~((3<< 16))) // 选择p0.24为GPIO
#define EASM_RST_DIR    (LPC_GPIO0->FIODIR |= (1<<24)) // 选择p0.24为输出
#define EASM_RST(x)	   (x?(LPC_GPIO0->FIOSET |= (1<<24)):(LPC_GPIO0->FIOCLR |= (1<<24)))//设置p0.24为高或低

//esam_io out
#define EASM_OUT_SEL    (LPC_PINCON->PINSEL1 &= ~((3<< 18))) // 选择p0.25为GPIO
#define EASM_OUT_DIR    (LPC_GPIO0->FIODIR |= (1<<25)) // 选择p0.25为输出
#define EASM_OUT(x)	   (x?(LPC_GPIO0->FIOSET |= (1<<25)):(LPC_GPIO0->FIOCLR |= (1<<25)))//设置p0.25为高或低

//esam_io in
#define EASM_IN_DIR    (LPC_GPIO0->FIODIR &= (~(1<<25))) // 选择p0.25为输入
#define EASM_IN	   	   ((LPC_GPIO0->FIOPIN & (1<<25)))//读取p0.25状态
						  
//esam_clk
#define EASM_CLK(x)	   (x?(enable_pwm1_ch3()):(disable_pwm1_ch3()))//esam 时钟使能和关闭


//card 电源控制
#define IC_CTRL_SEL    (LPC_PINCON->PINSEL2 &= ~((3))) // 选择p1.0为GPIO
#define IC_CTRL_DIR    (LPC_GPIO1->FIODIR |= (1)) // 选择p1.0为输出
#define IC_CTRL(x)	   (x?(LPC_GPIO1->FIOSET |= (1)):(LPC_GPIO1->FIOCLR |= (1)))//设置p1.0为高或低

//card 复位
#define IC_RST_SEL    (LPC_PINCON->PINSEL2 &= ~((3<<16))) // 选择p1.8为GPIO
#define IC_RST_DIR    (LPC_GPIO1->FIODIR |= (1<<8)) // 选择p1.8为输出
#define IC_RST(x)	   (x?(LPC_GPIO1->FIOSET |= (1<<8)):(LPC_GPIO1->FIOCLR |= (1<<8)))//设置p1.8为高或低

//card out
#define IC_OUT_SEL    (LPC_PINCON->PINSEL3 &= ~((3<<2))) // 选择p1.17为GPIO
#define IC_OUT_DIR    (LPC_GPIO1->FIODIR |= (1<<17)) // 选择p1.17为输出
#define IC_OUT(x)	   (x?(LPC_GPIO1->FIOSET |= (1<<17)):(LPC_GPIO1->FIOCLR |= (1<<17)))//设置p1.17为高或低

//card in
#define IC_IN_SEL    (LPC_PINCON->PINSEL3 &= ~((3))) // 选择p1.16为GPIO
#define IC_IN_DIR    (LPC_GPIO1->FIODIR &= (~(1<<16))) // 选择p1.16为输出
#define IC_IN(x)	 ((LPC_GPIO1->FIOPIN & (1<<16)))//读取p1.16

//卡的检测在输入任务中检测

#define esam_send_data()  os_evt_set(EVT_ESAM,esam_task)

typedef struct {

	U16 data;  //发送数据
	U8 state;  //成功失败状态
	U8 flag;   //操作状态
	
}esam_card_one_byte_t;

#define ESAM_READ_STATE 0x01
#define ESAM_WRITE_STATE 0x02
#define ESAM_DELAY_STATE 0x04
#define ESAM_COMPLETE_STATE 0x80

#define ESAM_SUCCEED_STATE 0x80
#define ESAM_ERROR_STATE 0x40


#define MAX_ESAM_CARD_NUM 178
typedef struct {

	U8 data_len;  //操作的数据长度
	U8 data_count;//缓冲中的数据个数
	U8 data_buf[MAX_ESAM_CARD_NUM];  //操作的数据缓冲
	U8 flag;
	U8 state;
	
}esam_card_one_frame_t;
#define ESAM_FRAME_READ_ERR 0x01
#define ESAM_FRAME_WRITE_ERR 0x02
#define ESAM_FRAME_TIME_OUT 0x03
#define ESAM_FRAME_SUCCEED 0x80

typedef struct {

	U32 esam_authen_time;  //esam认证开始时间
	U16 esam_authen_minutes;//esam认证时效（分钟）
	U8 flag;		//远程控制标志
	U8 authen_random[8]; //认证随机数
	U8 authen_detract[8];//认证分散因子
	
}extern_control_t;

typedef struct {

	U8 account_flag;	//用户标志
	U8 account_num[6]; //用户编号
	U32 crush_num; 		//充值次数
	U32 crush_money;    //剩余金额
	
}account_control_t;

#define IDENTITY_AUTHEN_BIT 0x01//身份认证有效
#define IDENTITY_AUTHEN_TIME_BIT 0x02//身份认证时间有效

extern esam_card_one_byte_t esam_one_byte,card_one_byte;
extern extern_control_t extern_control;
extern account_control_t account_control;

#define EVT_ESAM		  	0x0001//esam 处理

extern __task void esam_one_frame_process(void);
extern U16 reset_esam(U8 *id);
extern U8 init_esam(void);
extern U16 get_esam_random(U8 *buf,U8 len);
extern U8 remote_authen(U8 *detract_data,U8 *random_data,U8 *cryptograph_data1,U8 *response_data);
extern U8 remote_authen_ActiveDly(U8 *cryptograph_data);
extern U8 remote_authen_disable(U8 *buf);
extern U16 esam_read_binaryfile(U8 file_id,U8 offset,U8 *data,U8 len);
extern U8 remote_read_data(U16 list_id,U16 file_id,U16 offset,U16 data_len,U8 *data,U8 *len);
extern U8 remote_read_state(U8 *data);
extern void check_authen_time(void);
extern U8 init_account_info(void);
extern U8 remote_crush_money(U8 mode,U8 *buf,U8 data_len);
extern U8 remote_control(U8 *buf,U8 len,U8 *outbuf,U8 *outlen);
extern U16 esam_write_binaryfile_mac(U8 file_id,U8 *parameter,U8 parameter_len,U8 offset,U8 *mac);
extern U8 decode_98_mac(U8 di2,U8 *inbuf,U8 inlen,U8 *outbuf,U8 *outlen,U8 mode);
extern U8 remote_modify_key(U8 id1,U8 key_id,U8 *data,U8 len);
#endif /*__ESAM_CARD_H */
